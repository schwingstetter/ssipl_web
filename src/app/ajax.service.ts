import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
/* import 'rxjs/add/Observable/throw'; */
import { environment } from '../environments/environment';

@Injectable()
export class AjaxService {

  constructor( private http:Http ) { }
  public ajaxpost(data, methods, url) {
    var obj = {      
      methods: methods,    
      submitUrl: url,
      data:data,
      dataType: "JSON"
    } 
    return this.http.post(environment.api_url + obj.submitUrl, obj.data)
    //return this.http.request(environment.api_url + obj.submitUrl, options)  
    .map(response =>response.json())
    .catch(this._errorhandler);
  };

  public ajaxget(data, methods, url) {
    var obj = {
      methods: methods,
      submitUrl: url,
      data: data,
      dataType: "JSON"
    }
    return this.http.get(environment.api_url + obj.submitUrl)
      .map(
        (response: Response) => {
          response.json()
        }
      )
      .catch(this._errorhandler);
  };

  private _errorhandler(error: Response){
    console.error(error);
    return Observable.throw(error || "some error occured");
  }
  

}
