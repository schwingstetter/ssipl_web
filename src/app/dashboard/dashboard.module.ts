import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { dashboardRoutes } from './dashboard.routing';
import { SharedModule } from '../shared/shared.module';
//import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import 'rxjs/add/observable/of';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
//import { CountUpModule } from 'countup.js-angular2';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(dashboardRoutes),
    SharedModule,
  // InfiniteScrollModule,
//  CountUpModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
//platformBrowserDynamic().bootstrapModule(DashboardModule);