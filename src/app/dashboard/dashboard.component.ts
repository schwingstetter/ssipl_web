import { HostListener,Component, OnInit } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../ajax.service';//'../../../ajax.service';
import { AuthService } from '../auth.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
declare const $: any;
declare var Morris: any;
declare var Highcharts:any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public cone_count=0;
  public drum_count=0;
  public mixer_count=0;
  public mounting_count=0;
  public filtermodel='1';
  public productionfiltermodel='1';
  public rawmaterialfiltermodel ='1';
  public rawmaterial='0';
  public rawmaterialpartfiltermodel='0';
  public part_no=[];
  public workstationdata=[];
  public snagtypefiltermodel='1';
  public snagwsfiltermodel='0';
  public snagfiltermodel='2';
  public planactualfiltermodel='1';
  //public wippartfiltermodel=''
  public wippartfiltermodel='0'
  public wipfiltermodel='1';
  public wip_partno=[];
  public fgfiltermodel='1';
  public drumstockfiltermodel='1';
  //public wip_partno='0';
  content = ` Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at,`;
  timeline = [
    { caption: '16 Jan', date: new Date('2014, 1, 16'), selected: true, title: 'Horizontal Timeline', content: this.content },
    { caption: '28 Feb', date: new Date('2014, 2, 28'), title: 'Event title here', content: this.content },
    { caption: '20 Mar', date: new Date('2014, 3, 20'), title: 'Event title here', content: this.content },
    { caption: '20 May', date: new Date('2014, 5, 20'), title: 'Event title here', content: this.content },
    { caption: '09 Jul', date: new Date('2014, 7, 9'), title: 'Event title here', content: this.content },
    { caption: '30 Aug', date: new Date('2014, 8, 30'), title: 'Event title here', content: this.content },
    { caption: '15 Sep', date: new Date('2014, 9, 15'), title: 'Event title here', content: this.content },
    { caption: '01 Nov', date: new Date('2014, 11, 1'), title: 'Event title here', content: this.content },
    { caption: '10 Dec', date: new Date('2014, 12, 10'), title: 'Event title here', content: this.content },
    { caption: '29 Jan', date: new Date('2015, 1, 19'), title: 'Event title here', content: this.content },
    { caption: '3 Mar', date: new Date('2015,  3,  3'), title: 'Event title here', content: this.content },
  ];

  constructor(public http: Http, private AjaxService: AjaxService) { }

  ngOnInit() {
    this.getdashboardcount('1');
    this.getproductionchart('1');
    this.getrawmaterialgraph('1','0');
    this.getpartnos();
    this.getworkstations('1');
    this.getsnagreport();
    this.getplanactualgraph();
    this.getwippartnos();
    this.getwipchart();
    this.getfgchart();
    this.getdrumstockgraph();
    setTimeout(() => {
      $('.resource-barchart1').sparkline([5, 6, 9, 7, 8, 4, 6], {
        type: 'bar',
        barWidth: '6px',
        height: '32px',
        barColor: '#1abc9c',
        tooltipClassname: 'abc'
      });

      $('.resource-barchart2').sparkline([6, 4, 8, 7, 9, 6, 5], {
        type: 'bar',
        barWidth: '6px',
        height: '32px',
        barColor: '#1abc9c',
        tooltipClassname: 'abc'
      });


      /*new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'area-example',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: [
          { day: 'Monday', pushups: 20, beers: 2 },
          { day: 'Tuesday', pushups: 10, beers: 2 },
          { day: 'Wednesday', pushups: 5, beers: 3 },
          { day: 'Thursday', pushups: 5, beers: 4 },
          { day: 'Friday', pushups: 20, beers: 1 }
        ],
        // The name of the data record attribute that contains x-values.
        xkey: 'day',
        parseTime: false,
        // A list of names of data record attributes that contain y-values.
        ykeys: ['pushups','beers'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Pushups','Beers'],
        lineColors: ['#373651','#E65A26']
      });*/

     /* Morris.Area({
        element: 'area-example',
        data: [{
          period: '2010',
          iphone: 0,
          ipad: 0,
          itouch: 0
        }, {
          period: '2011',
          iphone: 50,
          ipad: 15,
          itouch: 5
        }, {
          period: '2012',
          iphone: 20,
          ipad: 50,
          itouch: 65
        }, {
          period: '2013',
          iphone: 60,
          ipad: 12,
          itouch: 7
        }, {
          period: '2014',
          iphone: 30,
          ipad: 20,
          itouch: 120
        }, {
          period: '2015',
          iphone: 25,
          ipad: 80,
          itouch: 40
        }, {
          period: '2016',
          iphone: 10,
          ipad: 10,
          itouch: 10
        }


        ],
        lineColors: ['#fb9678', '#7E81CB', '#01C0C8'],
        xkey: 'period',
        ykeys: ['iphone', 'ipad', 'itouch'],
        labels: ['Site A', 'Site B', 'Site C'],
        pointSize: 0,
        lineWidth: 0,
        resize: true,
        fillOpacity: 0.8,
        behaveLikeLine: true,
        gridLineColor: '#5FBEAA',
        hideHover: 'auto'

      });*/
      
// Create the chart

      /*Morris.Line({
        element: 'plan-example',
        data: [
          { y: '2006', a: 100, b: 90 },
          { y: '2007', a: 75, b: 65 },
          { y: '2008', a: 50, b: 40 },
          { y: '2009', a: 75, b: 65 },
          { y: '2010', a: 50, b: 40 },
          { y: '2011', a: 75, b: 65 },
          { y: '2012', a: 100, b: 90 }
        ],
        xkey: 'y',
        redraw: true,
        ykeys: ['a', 'b'],
        hideHover: 'auto',
        labels: ['Plan', 'Actual'],
        lineColors: ['#B4C1D7', '#FF9F55']
      });*/
   
    /*  Morris.Line({
        element: 'line-example',
        data: [
          { y: '2006', a: 100, b: 90 },
          { y: '2007', a: 75, b: 65 },
          { y: '2008', a: 50, b: 40 },
          { y: '2009', a: 75, b: 65 },
          { y: '2010', a: 50, b: 40 },
          { y: '2011', a: 75, b: 65 },
          { y: '2012', a: 100, b: 90 }
        ],
        xkey: 'y',
        redraw: true,
        ykeys: ['a', 'b'],
        hideHover: 'auto',
        labels: ['Series A', 'Series B'],
        lineColors: ['#B4C1D7', '#FF9F55']
      });*/
   /*   Morris.Area({
        element: 'snag-example',
        data: [{
          period: '2010',
          SiteA: 0,
          SiteB: 0,

        }, {
          period: '2011',
          SiteA: 130,
          SiteB: 100,

        }, {
          period: '2012',
          SiteA: 80,
          SiteB: 60,

        }, {
          period: '2013',
          SiteA: 70,
          SiteB: 200,

        }, {
          period: '2014',
          SiteA: 180,
          SiteB: 150,

        }, {
          period: '2015',
          SiteA: 105,
          SiteB: 90,

        }, {
          period: '2016',
          SiteA: 250,
          SiteB: 150,

        }],
        xkey: 'period',
        ykeys: ['SiteA', 'SiteB'],
        labels: ['Site A', 'Site B'],
        pointSize: 0,
        fillOpacity: 0.4,
        pointStrokeColors: ['#b4becb', '#01c0c8'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 0,
        smooth: false,
        hideHover: 'auto',
        lineColors: ['#b4becb', '#01c0c8'],
        resize: true
      });*/
      /* Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
          y: '2006',
          a: 100,
          b: 90,
          c: 60
        }, {
          y: '2007',
          a: 75,
          b: 65,
          c: 40
        }, {
          y: '2008',
          a: 50,
          b: 40,
          c: 30
        }, {
          y: '2009',
          a: 75,
          b: 65,
          c: 40
        }, {
          y: '2010',
          a: 50,
          b: 40,
          c: 30
        }, {
          y: '2011',
          a: 75,
          b: 65,
          c: 40
        }, {
          y: '2012',
          a: 100,
          b: 90,
          c: 40
        }],
        xkey: 'y',
        ykeys: ['a', 'b', 'c'],
        labels: ['A', 'B', 'C'],
        barColors: ['#5FBEAA', '#5D9CEC', '#cCcCcC'],
        hideHover: 'auto',
        gridLineColor: '#eef0f2',
        resize: true
      }); */
    }, 1);

  }
  getplanactualgraph()
  {
    var data: { period: string } = { period: this.planactualfiltermodel };
      var method = "post";
      var url = "get_plan_actual_graph";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
          
          //console.log(datas);
             new Highcharts.chart('line-example', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: datas.data.category,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: datas.data.series
});
              // this.dropdownList=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  changedashboardcount(event: any)
  {
    console.log(event.target.value)

    this.getdashboardcount(event.target.value);
  }
  
  getsnagreport()
  {
    var data: { period: string,workstation: string,snagtype: string } = { period: this.snagfiltermodel,workstation: this.snagwsfiltermodel,snagtype: this.snagtypefiltermodel};
      var method = "post";
      var url = "getsnagchart";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
           new Highcharts.chart('snag-example', {

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },
 xAxis: {
                      categories: datas.data.category
                  },
    yAxis: {
        title: {
            text: 'No of Snags'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
          //  pointStart: 2010
        }
    },

    series: datas.data.series,

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
              },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
 getdashboardcount(filtertype)
  {
     var data: { filtertype: string } = { filtertype: filtertype };
      var method = "post";
      var url = "getdashboardcount";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
          // $("#mixercount").attr("data-count", "500");
            this.cone_count= datas.data['cone'];
            this.drum_count= datas.data['drum'];
            this.mixer_count= datas.data['mixer'];
            this.mounting_count= datas.data['mounting'];
            this.loadcounter();
              // this.dropdownList=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  onchartchange(event: any)
  {
     this.getproductionchart(event.target.value);
  }
  getproductionchart(filtertype)
  {
    var data: { filtertype: string } = { filtertype: filtertype };
      var method = "post";
      var url = "getproductiongraph";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
          
         console.log(datas.data.category)
            console.log(datas.data.series)

            /*Chart Start */
            new Highcharts.chart('area-example', {
                  chart: {
                      type: 'line'
                  },
                  title: {
                      text: ''
                  },
                  subtitle: {
                      text: ''
                  },
                  xAxis: {
                      categories: datas.data.category
                  },
                  yAxis: {
                      title: {
                          text: 'No of products'
                      }
                  },
                  plotOptions: {
                      line: {
                          dataLabels: {
                              enabled: true
                          },
                          enableMouseTracking: false
                      }
                  },
                  series: datas.data.series
              });
            /*Chart End */
              // this.dropdownList=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  rawmaterialgraphchange(event: any)
  {
    this.getrawmaterialgraph(this.rawmaterialfiltermodel,this.rawmaterialpartfiltermodel)
  }
  rawmaterialpartgraphchange(event: any)
  {
    this.getrawmaterialgraph(this.rawmaterialfiltermodel,this.rawmaterialpartfiltermodel)
  }
  getpartnos()
  {
    var data: { filtertype: string } = { filtertype: '1'};
      var method = "post";
      var url = "getpartnos_dashboard";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
           this.part_no=datas.data;
              },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
getrawmaterialgraph(filtertype,partno)
{
  var data: { filtertype: string,partno: string } = { filtertype: filtertype,partno: partno};
      var method = "post";
      var url = "getrawmaterialgraph";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            /**Chart start */
                        new Highcharts.chart('plan-example', {

                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },
                xAxis: {
                      categories: datas.data.category
                        },
                yAxis: {
                    title: {
                        text: 'Weight'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                  //      pointStart: 2010
                    }
                },
              tooltip: {
              /*formatter: function (tooltip) {
                    //   return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name+'  '+this.series.tooltip;
                    return 'The value '+this.;
                    }*/
                    formatter: function() {
                      return 'Scrap weight:'+this.series.userOptions.scrap[this.series.data.indexOf( this.point )]+' Kg<br> Cut part weight:'+this.series.userOptions.cutpart[this.series.data.indexOf( this.point )]+' Kg';
                        }
                },
                series: datas.data.series,

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });
            /**Chart end */
              },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
}
 onScroll() {
    console.log('scrolled!!');
  }
  snagtypechanged(event:any)
  {
    this.snagwsfiltermodel='0';
    this.getsnagreport();
  }
  snagtypechange(event:any)
  {
    var data: { snagtype: string } = { snagtype: event.target.value};
    var method = "post";
    var url = "getworkstations_dashboard";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          this.workstationdata=datas.data;
            },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getworkstations(snagtype)
  {
    var data: { snagtype: string } = { snagtype: snagtype};
      var method = "post";
      var url = "getworkstations_dashboard";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            this.workstationdata=datas.data;
              },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
   getwippartnos()
  {
    var data: { snagtype: string } = { snagtype: '1'};
      var method = "post";
      var url = "get_wip_partno";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            this.wip_partno=datas.data;
              },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  getwipchart()
  {
    var data: { duration: string,partno: string } = { duration: this.wipfiltermodel, partno: this.wippartfiltermodel};
      var method = "post";
      var url = "wipchart";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
           // this.wip_partno=datas.data;
      
// Build the chart
Highcharts.chart('wip-example', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Count',
        colorByPoint: true,
        data: datas.data
    }]
});
              },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  loadcounter() {
  $('.counter-up').each(function() {

    var $this = $(this),
        casecount = $this.attr('data-count');
  //  alert('casecount'+casecount)
    var countTo=0;
    switch(casecount) {
        case '1':
           // alert();
             $({ countNum: $this.text()}).animate({
                countNum: this.cone_count
                },
            
                {
                duration: 2000,
                easing:'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                    console.log(this.countNum)
                },
                complete: function() {
                    $this.text(this.countNum);
                     console.log(this.countNum)
                }
            
                });
    break;
    case '2':
        $({ countNum: $this.text()}).animate({
                countNum: this.drum_count
                },
            
                {
                duration: 2000,
                easing:'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                     console.log(this.countNum)
                },
                complete: function() {
                    $this.text(this.countNum);
                     console.log(this.countNum)
                }
                });
    break;
    case '3':
        $({ countNum: $this.text()}).animate({
                countNum: this.mixer_count
                },
            
                {
                duration: 2000,
                easing:'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                     console.log(this.countNum)
                },
                complete: function() {
                    $this.text(this.countNum);
                     console.log(this.countNum)
                }
            
                });
    break;
     case '3':
        $({ countNum: $this.text()}).animate({
                countNum: this.mounting_count
                },
            
                {
                duration: 2000,
                easing:'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                }
            
                });
    break;
    }
   /* if(casecount=='1')
    {
        countTo=parseInt(this.cone_count);
    }
    else if()
    {

    }
    else{

    }*/
  //  alert('countto'+countTo);
   
  });  
   }  
   getfgchart()
  {
    var data: { duration: string } = { duration: this.fgfiltermodel};
      var method = "post";
      var url = "getfgchart";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
           // this.wip_partno=datas.data;
      
// Build the chart
Highcharts.chart('fg-example', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Count',
        colorByPoint: true,
        data: datas.data
    }]
});
              },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  
 getdrumstockgraph()
  {
    var data: { period: string } = { period: this.drumstockfiltermodel};
      var method = "post";
      var url = "drumstockgraph";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
           // this.wip_partno=datas.data;
      
// Build the chart
Highcharts.chart('drumstock-example', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
     //   pointFormat: '{series.name}: {point.y}--<b>{point.percentage:.1f}%</b>'
     pointFormat: '{series.name}:<b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Count',
        colorByPoint: true,
        data: datas.data
    }]
});
              },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }

  @HostListener('window:scroll', ['$event']) 
    scrollHandler(event) {
      console.debug("Scroll Event");
    }

}



// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/dashboard/dashboard.component.ts