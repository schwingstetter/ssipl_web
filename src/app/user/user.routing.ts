import { Routes } from '@angular/router';


export const userRoutes: Routes = [
    {
        path: '',
        data: {
            /* breadcrumb: 'User Management', */
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'userrole',
                loadChildren: './user-role/user-role.module#UserRoleModule'
            },
            {
                path: 'userposition',
                loadChildren: './user-position/user-position.module#UserPositionModule'
            },
            {
                path: 'users',
                loadChildren: './users/users.module#UsersModule'
            },
            {
                path: 'userworkcategory',
                loadChildren: './user-workcategory/user-workcategory.module#UserWorkcategoryModule'
            },
            {
                path: 'changepwd',
                loadChildren: './changepwd/changepwd.module#ChangepwdModule'
            }
            
        ]
    }
];
