import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserWorkcategoryComponent } from './user-workcategory.component';

describe('UserWorkcategoryComponent', () => {
  let component: UserWorkcategoryComponent;
  let fixture: ComponentFixture<UserWorkcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserWorkcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserWorkcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
