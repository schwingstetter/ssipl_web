import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserWorkcategoryComponent } from './user-workcategory.component';
import { SharedModule } from '../../shared/shared.module';

export const userWorkcategoryRoutes: Routes = [
  {
    path: '',
    component: UserWorkcategoryComponent,
    data: {
      breadcrumb: 'User WorkCategory',
      status: true
    }
  }
];



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(userWorkcategoryRoutes),
    SharedModule
  ],
  declarations: [UserWorkcategoryComponent]
})
export class UserWorkcategoryModule { }
