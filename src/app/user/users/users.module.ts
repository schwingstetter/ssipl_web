import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users.component';
import { SharedModule } from '../../shared/shared.module';

export const usersRoutes: Routes = [
  {
    path: '',
    component: UsersComponent,
    data: {
      breadcrumb: 'Users',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(usersRoutes),
    SharedModule
  ],
  declarations: [UsersComponent]
})
export class UsersModule { }
