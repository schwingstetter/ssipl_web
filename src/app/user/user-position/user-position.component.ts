import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-position',
  templateUrl: './user-position.component.html',
  styleUrls: ['./user-position.component.css']
})
export class UserPositionComponent implements OnInit {
  private modalRef: NgbModalRef;
  closeResult: string;
  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "";
  public sortOrder: string = "desc";
  public employees:string="";
  public employeesdetails:string="";
  public userrole:string="";
  public isavailable:string;
  public contractorname:string;
  public editemployee:any = { };
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);

   }


  ngOnInit() {
   var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get_dropdownlist" };
    var method = "post";
    var url = "usercreate";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          this.getrole();
          if(datas.code == 201){
            table.destroy();
            this.employees = datas.message;
              // table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
              }, 1000);
             
          } 
          
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
          
        }
       
      );
      this.getrole();
     
  }
getrole()
{
  var data: { switchcase: string } = { switchcase: "get" };
  var method = "post";
  var url = "usersrole";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {

        if(datas.code == 201){
          this.userrole = datas.message;
       
        }else{

        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
    this.getdata();
  
}
reloadPage() {
   this.ngOnInit();
   window.location.reload();
}
getdata()
{
  var data: { switchcase: string } = { switchcase: "get" };
  var method = "post";
  var url = "usercreate";
  this.AjaxService.ajaxpost(data, method, url)
    .toPromise()
    .then(
      (datas) => {
        if (datas.code == 201) {
         
          this.employeesdetails = datas.message;
          console.log(this.employeesdetails);
        } else {

        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
    
}
  createuser(form: NgForm,add)
  {

      var data = form.value;
      var switchcase = "switchcase";
      var value = "insert";
      data[switchcase] = value;
      var method= "post";
      var url="usercreate";
      this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => 
        {
          if(datas.code==201){
            this.toastr.success(datas.message, datas.type);
            form.resetForm(); 
            this.reloadPage();
            this.modalRef.close();
            this.ngOnInit();
          }
          else{
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            alert(err._body);
          }
        }
        
      
      );
  }
    // delete Employee in base on id and checking login then can't delete-31/05/2019.
  deleteempid(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "usercreate";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your Data has been deleted.',
                  'success'
                )
                this.ngOnInit();
                this.reloadPage();
              }
              else {
                swal(
                  // {
                  // response.code==202
                  // title: 'Alert',
                  // text: "This user Can't Delete,He/She is login now!!!",
                  // type: 'warning',
                  // confirmButtonColor: '#3085d6',}
                  'Not Deleted!',
                  'This user Can\'t Delete,He/She is login now!!!',
                  'error'
              )
                // this.reloadPage();
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
      
      }
    });
  };
 
// Edit model create and update details
// getdetails

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return `with: ${reason}`;
  }
};
openedit(edit) {
  this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
  this.modalRef.result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
};

editfunction(item) {
  this.editemployee=item;
  // console.log(this.editemployee);
  this.getrole();
  
};

editemployees(editfrm){
  var data = editfrm.value;
  var switchcase = "switchcase";
  var value = "edit";
  data[switchcase] = value;
  var method = "post";
  var url = "usercreate";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if (datas.code == 201) {
          this.toastr.success(datas.message, datas.type);    
          //edit.hide();
           this.reloadPage();
           this.modalRef.close(); 
            this.getdata();
        }
        else {
          this.toastr.error(datas.message, datas.type);
        }
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
    );
    //this.toastr.success('Hello world!', 'Toastr fun!');
};

  reset(template)
  {
  //  this.ngOnInit();
   this.modalRef.close(); 
  //  this.getdata();
  this.reloadPage();
  }
}


// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/user/user-position/user-position.component.ts