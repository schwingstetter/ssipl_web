import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserPositionComponent } from './user-position.component';
import { SharedModule } from '../../shared/shared.module';

export const userpositionRoutes: Routes = [
  {
    path: '',
    component: UserPositionComponent,
    data: {
      breadcrumb: 'User Details',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(userpositionRoutes),
    SharedModule
    
  ],
  declarations: [UserPositionComponent]
})
export class UserPositionModule { }
