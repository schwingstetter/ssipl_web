import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { CommonModule } from '@angular/common';
import { userRoutes } from './user.routing';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes),
  ],
  declarations: [UserComponent]
})
export class UserModule { }
