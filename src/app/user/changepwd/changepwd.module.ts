import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ChangepwdComponent } from './changepwd.component';
import { SharedModule } from '../../shared/shared.module';

export const changepwdRoutes: Routes = [
  {
    path: '',
    component: ChangepwdComponent,
    data: {
      breadcrumb: 'Change Password',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(changepwdRoutes),
    SharedModule
  ],
  declarations: [ChangepwdComponent]
})
export class ChangepwdModule { }
