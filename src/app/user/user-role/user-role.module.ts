import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserRoleComponent } from './user-role.component';
import { SharedModule } from '../../shared/shared.module';

export const userroleRoutes: Routes = [
  {
    path: '',
    component: UserRoleComponent,
    data: {
      breadcrumb: 'User Role',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(userroleRoutes),
    SharedModule
  ],
  declarations: [UserRoleComponent]
})
export class UserRoleModule { }
