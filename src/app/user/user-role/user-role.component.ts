import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TouchSequence } from '../../../../node_modules/@types/selenium-webdriver';

@Component({
  selector: 'app-user-role',
  templateUrl: './user-role.component.html',
  styleUrls: ['./user-role.component.css']
})
export class UserRoleComponent implements OnInit {

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "";
  public sortOrder: string = "desc";
  public employees:string="";
  public userrole:string="";
  public roles:string="";
  public menu:string="";
  public accessmenu:string="";
  menuaccess:string[] = new Array();
  menuaccessnew:string[] = new Array();
  menuaccesstemp:string[] = new Array();
  public i:any=0;
  public j:any=0;
  public a:any=0;
  truestate: boolean = true;
  falsestate: boolean = false;
  public editemployee:any = { };
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }
  ngOnInit() {
  var data: { switchcase: string } = { switchcase: "get" };
  var method = "post";
  var url = "usersrole";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if(datas.code == 201){
          this.userrole = datas.message;       
        }else{
        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
    this.getmenu();
  }
  getmenu()
  {  
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "menu";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){
            if(datas.message.length >0 )
            {
            this.menu = datas.message;         
            }
            else
            {
              this.reloadPage();
            }
          }else{  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
     // this.getaccess();
  }
  reloadPage() {
    this.menuaccess = new Array();
    this.ngOnInit();
  }
  getrole(event)
  {
    this.roles=event;
  }
  role(event)
  {
//  this.reloadPage();
    this.accessmenu = "";
    this.menuaccess= [];
    console.log(this.menuaccess);
    var data ={"role": event};
    var method = "post";
    var url = "getmenuaccess";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){           
            if(datas.message.length > 0)
            {
            this.accessmenu = datas.message;
            if(datas.message=="undefined" || datas.message==""){
            }else if(datas.message){
              var headers = datas.message;
              console.log(headers.length);
              for(var i=1; i <= headers.length; i++){ 
                console.log(i);
                var e2 = <HTMLInputElement> document.getElementById(headers[i]);
                console.log(e2);
                e2.checked = true;              
                this.menuaccess.push(headers[i]);               
             }
            }         
            }
            else
            {
              this.reloadPage();
            }
          }else{
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    //  this.getmenu();
  }
  checkValue(option,options){
  if(this.menuaccess.length == 0)
   {
  this.menuaccess.push(option);
   } 
   else
   {     
    if(options == false)
    {
      this.menuaccess.splice(this.menuaccess.indexOf(option),1);
    }
    else
    {
     this.menuaccess.push(option); 
    }  
   }
  }
  addmenuaccess(form)
  {
    var data = form.value;
    var menu = "menu";
    var menus = this.menuaccess;
    data[menu]=menus;
    var method = "post";
    var url = "savemenu";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){
             this.toastr.success(datas.message, datas.type);
              this.reloadPage();
          }else{
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  openConfirmsSwal() {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      swal(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
    }).catch(swal.noop);
  }


}
  