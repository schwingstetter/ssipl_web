import { Component, OnInit, ViewChild, ViewEncapsulation, ElementRef, AfterViewInit } from '@angular/core';
import 'rxjs/add/operator/filter';
import { state, style, transition, animate, trigger, AUTO_STYLE } from '@angular/animations';
import { AuthService } from './../../auth.service';
import { AjaxService } from '../../ajax.service';

export interface Options {
  heading?: string;
  removeFooter?: boolean;
  mapHeader?: boolean;
}

@Component({
  selector: 'app-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css'],
  providers: [AjaxService, AuthService],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('slideOnOff', [
      state('on', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('off', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('on => off', animate('400ms ease-in-out')),
      transition('off => on', animate('400ms ease-in-out'))
    ]),
    trigger('mobileMenuTop', [
      state('no-block, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('yes-block',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('no-block <=> yes-block', [
        animate('400ms ease-in-out')
      ])
    ])
  ]
})

export class AdminLayoutComponent implements OnInit {
  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  chatToggle = 'out';
  chatInnerToggle = 'off';
  innerHeight: string;
  isScrolled = false;
  isCollapsedMobile = 'no-block';
  isCollapsedSideBar = 'no-block';
  toggleOn = true;
  windowWidth: number;
  menunames: string;
  item: string;
  count: number;
  public dashboard = [];
  public user = [];
  public master = [];
  public mastersubmenu = [];
  public shiftengineer = [];
  public shiftengineersubmenu = [];
  public productionplan = [];
  public productionplansubmenu = [];
  public reports = [];
  public reportssubmenu = [];
  public commercial = [];
  public stores = [];
  public dispatch = [];
  public livedashboard = [];
  public username : any;
  @ViewChild('searchFriends') search_friends: ElementRef;
  @ViewChild('toggleButton') toggle_button: ElementRef;
  @ViewChild('sideMenu') side_menu: ElementRef;
  constructor(public auth: AuthService, private AjaxService: AjaxService) {
    const scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);

  }

  ngOnInit() {
    this.username = localStorage.getItem('LoggedInUsername');
    var data = { "role": localStorage.getItem("LoggedinRole") };
    var method = "post";
    var url = "getmenuaccess";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          var sorted = datas.message.sort(); 
          //console.log(sorted);
          for (var i = 0; i < sorted.length; i++) {
            if (sorted[i].includes(1)) {
              this.dashboard.push(sorted[i]);
            }
            else if (sorted[i].includes(2)) {
              this.user.push(sorted[i]);
            }
            else if (sorted[i].includes(3)) {
              if (sorted[i] == '3gg' || sorted[i] == '3gga' || sorted[i] == '3ggb') {
                if (sorted[i].length == 4) {
                  this.mastersubmenu.push(sorted[i]);
                }
                else {
                  this.master.push(sorted[i]);
                }
              }
              else {
               
                if (sorted[i].length == 3) {
                  if (sorted[i] == '3aa' || sorted[i] == '3ab')
                  {
                    this.master.push(sorted[i]);
                  }
                  this.mastersubmenu.push(sorted[i]);
                }
                else {
                  this.master.push(sorted[i]);
                }
              }

            }
            else if (sorted[i].includes(4)) {

              if (sorted[i].length == 3) {
                this.shiftengineersubmenu.push(sorted[i]);
              }
              else {
                this.shiftengineer.push(sorted[i]);
              }

            }
            else if (sorted[i].includes(5)) {

              if (sorted[i].length == 3) {
                this.productionplansubmenu.push(sorted[i]);
              }
              else {
                this.productionplan.push(sorted[i]);
              }

            }
            else if (sorted[i].includes(6)) {

              if (sorted[i].length == 3) {
                this.reports.push(sorted[i]);
              }
              else {
                this.reportssubmenu.push(sorted[i]);
              }

            }
            else if (sorted[i].includes(7)) {
              this.stores.push(sorted[i]);
            }
            else if (sorted[i].includes(8)) {
              this.commercial.push(sorted[i]);

            }
            else if (sorted[i].includes(9)) {
              this.dispatch.push(sorted[i]);

            }

          }

        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    //  console.log("MenuItems",this.menuItems.getAll());


  }

  onClickedOutside(e: Event) {
    if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
      this.toggleOn = true;
      this.verticalNavType = 'offcanvas';
    }
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    let reSizeFlag = true;
    if (this.deviceType === 'tablet' && this.windowWidth >= 768 && this.windowWidth <= 1024) {
      reSizeFlag = false;
    } else if (this.deviceType === 'mobile' && this.windowWidth < 768) {
      reSizeFlag = false;
    }

    if (reSizeFlag) {
      this.setMenuAttributs(this.windowWidth);
    }
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  searchFriendList(event) {
    const search = (this.search_friends.nativeElement.value).toLowerCase();
    let search_input: string;
    let search_parent: any;
    const friendList = document.querySelectorAll('.userlist-box .media-body .chat-header');
    Array.prototype.forEach.call(friendList, function (elements, index) {
      search_input = (elements.innerHTML).toLowerCase();
      search_parent = (elements.parentNode).parentNode;
      if (search_input.indexOf(search) !== -1) {
        search_parent.classList.add('show');
        search_parent.classList.remove('hide');
      } else {
        search_parent.classList.add('hide');
        search_parent.classList.remove('show');
      }
    });
  }

  toggleChat() {
    this.chatToggle = this.chatToggle === 'out' ? 'in' : 'out';
  }

  toggleChatInner() {
    this.chatInnerToggle = this.chatInnerToggle === 'off' ? 'on' : 'off';
  }

  toggleOpened() {
    if (this.windowWidth < 768) {
      this.toggleOn = this.verticalNavType === 'offcanvas' ? true : this.toggleOn;
      this.verticalNavType = this.verticalNavType === 'expanded' ? 'offcanvas' : 'expanded';
    } else {
      this.verticalNavType = this.verticalNavType === 'expanded' ? 'collapsed' : 'expanded';
    }
  }

  toggleOpenedSidebar() {
    this.isCollapsedSideBar = this.isCollapsedSideBar === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }
}



// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/layouts/admin/admin-layout.component.ts