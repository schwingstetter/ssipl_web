import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}



const MENUITEMS = [
  {
    label: 'Navigation',
    main: [
      {
        state: 'dashboard',
        short_label: 'D',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-home',
      },{
        state: 'user',
        name: 'User Management',
        type: 'sub',
        icon: 'ti-user',
        children: [
          {
            state: 'userrole',
            name: 'User Access'
          },
          {
            state: 'userposition',
            name: 'Users'
          },
        ]
      },
     
      {
        state: 'master',
        short_label: 'D',
        name: 'Master Management',
        type: 'sub',
        icon: 'ti-harddrive',
        children: [
          {
            state: 'employee',
            name: 'Employee Master'
          },
          {
            state: 'drumtype',
            name: 'Drum Master',
            type: 'sub',
            icon: 'ti-user',
            children: [
              {
                state: 'drumtypes',
                name: 'Drum Type Master'
              }, {
                state: 'subassybom',
                name: 'Sub Assy BOM'
              }
            ]

          },{
              state: 'mixermaster',
              name: 'Mixer Assembly Master',
              type: 'sub',
              icon: 'ti-user',
              children: [
                {
                  state: 'hydrauliccombination',
                  name: 'Hydraulic Combination'
                }, {
                  state: 'mixertypemaster',
                  name: 'Mixer Assembly Master'
                },{
                  state: 'distancecorrelation',
                  name: 'Distance Correlation'
                }, {
                  state: 'subassy',
                  name: 'Sub Assy Master'
                }
              ]
  
            },
          {
            state: 'fabstages',
            name: 'Task Assignment Targets'
          }, 
          {
            state: 'fabprocess',
            name: 'Fabrication Process Flow'
          },{
            state: 'rawmaterial',
            name: 'Raw Material Master',
            type: 'sub',
            icon: 'ti-user',
            children: [
              {
                state: 'rawmaterialdetails',
                name: 'Raw Material Details'
              },
              {
                state: 'conedetails',
                name: 'Cone Details'
              },
              {
                state: 'conemapdetails',
                name: 'RM Cone Mapping'
              },
            ]

          }, {
            state: 'sopmaster',
            name: 'SOP Master',
            short_label: 'D',
              type: 'sub',
              icon: 'ti-user',
              children: [
                {
                  state: 'cmvariance',
                  name: 'CM Variance'
                },{
                  state: 'productvariant',
                  name: 'Product Variant'
                },
                {
                  state: 'sopprocedure',
                  name: 'SOP Procedure'
                },
              ]
        
          },{
            state: 'druminternal',
            name: 'Drum Internal Parts (BOM)',
            short_label: 'D',
            type: 'sub',
            icon: 'ti-user',
            children: [
              {
                state: 'drumheader',
                name: 'Drum Header'
              }, {
                state: 'drumsubassy',
                name: 'Sub Assy Master(BOM)'
              }
            ]

          }, {
            state: 'assyinternal',
            name: 'Assembly Internal Parts (BOM)',
            short_label: 'D',
            type: 'sub',
            icon: 'ti-user',
            children: [
              {
                state: 'assyheader',
                name: 'Assembly Header'
              }, {
                state: 'assysubassy',
                name: 'Sub Assy Master(BOM)'
              }
            ]
          },{
            state: 'mountingarrangement',
            name: 'Mounting Arrangements (BOM)',
            short_label: 'D',
            type: 'sub',
            icon: 'ti-user',
            children: [
              {
                state: 'mounting_header',
                name: 'Mounting Header'
              }, {
                state: 'mounting_bom',
                name: 'Sub Assy Master(BOM)'
              }
            ]

          },{
            state: 'packagingarrangement',
            name: 'Packaging Arrangements (BOM)',
            short_label: 'D',
            type: 'sub',
            icon: 'ti-user',
            children: [
              {
                state: 'packaging_header',
                name: 'Packaging Header'
              }, {
                state: 'packaging_bom',
                name: 'Packaging Master(BOM)'
              }
            ]

          },{
            state: 'delayrework',
            name: 'Delay/Rework Master'
          },{
            state: 'snagmaster',
            name: 'Snag Master'
          }, {
            state: 'qamaster',
            name: 'QA Master',
            short_label: 'D',
            type: 'sub',
            icon: 'ti-user',
            children: [
              {
                state: 'drumins',
                name: 'Drum Inspection'
              },
              {
                state: 'pdichecklist',
                name: 'PDI Checklist'
              }, {
                state: 'processchecklist',
                name: 'Process Checklist'
              }, {
                state: 'shfhchecklist',
                name: 'SH/FH Checklist'
              }, {
                state: 'fbvchecklist',
                name: 'FBV Checklist'
              }, {
                state: 'chassisins',
                name: 'Chassis Inspection'
              },
            ]

          },{
            state: 'jobsheet',
            name: 'Assembly Job Sheet'
          }          
        ]
      },
      {
        state: 'shiftengineer',
        short_label: 'D',
        name: 'Shift Engineer',
        type: 'sub',
        icon: 'ti-harddrive',
        children: [
          {
            state: 'viewproductionplan',
            name: 'Production Plan',
          },
          {
            state: 'assignresources',
            name: 'Assign Resources',
            type: 'sub',
            icon: 'ti-user',
            children: [            
              {
                state: 'weeklyallotment',
                name: 'Weekly Allotment'
              }, {
                state: 'shiftallotment',
                name: 'Shift Allotment'
              }
            ]

          },
          {
            state: 'shiftjobsheet',
            name: 'Job Sheet',
          },
          {
            state: 'completionmounting',
            name: 'Mounting Completion',
          }
        ]},
      {
        state: 'planner',
        short_label: 'D',
        name: 'Production Planning',
        type: 'sub',
        icon: 'ti-briefcase',
        children: [
          {
            state: 'drumassembly',
            name: 'Drum Fabrication',
            type: 'sub',
            short_label: 'D',
              icon: 'ti-user',
              children: [
                {
                  state: 'workorder',
                  name: 'Drum W/O Generation'
                },
                {
                  state: 'weeklyplan',
                  name: 'Drum Weekly Plan Schedule'
                },
              ]
          }, {
            state: 'mixerassembly',
            name: 'Mixer Assembly'
          }, {
            state: 'mountrelease',
            name: 'Mount Release'
          }
        ]
      },
   
     {
        state: 'reports',
        short_label: 'D',
        name: 'Reports Management',
        type: 'sub',
        icon: 'ti-bar-chart',
        children: [
          {
            state: 'productionreport',
            name: 'Production Report'
          },/* {
            state: 'productivityreport',
            name: 'Productivity Report'
          }, */
          {
            state: 'qareports',
            name: 'QA Reports',
            short_label: 'D',
            type: 'sub',
            icon: 'ti-user',
            children: [
              {
                state: 'pdicheklist',
                name: 'PDI Checklist'
              }, {
                state: 'precesscheklist',
                name: 'PDI Checklist'
              }, 
            ]
          },
          {
            state: 'planvsactualproduction',
            name: 'Product Delivery Reports (Target vs delivered)'
          },/* {
            state: 'lossanalysis',
            name: 'Loss analysis'
          }, */
          {
            state: 'rawmaterialconsumption',
            name: 'Raw Material Consumption Report'
          },
          {
            state: 'machineruntimereport',
            name: 'Machine Run Time Report'
          },
          {
            state: 'materialconsumptionreport',
            name: 'Line Rejection Report'
          },
          {
            state: 'paintingreport',
            name: 'Painting Report'
          },
          {
            state: 'wipreport',
            name: 'WIP Report'
          },{
            state: 'slnotracking',
            name: 'Sl No Tracking'
          },{
            state: 'snagreport',
            name: 'Snag Report'
          },{
            state: 'mixerreport',
            name: 'Mixer Report'
          },
          
        ]
      },{
        state: 'stores',
        short_label: 'D',
        name: 'Stores Management',
        type: 'sub',
        icon: 'ti-wallet',

        children: [
          {
            state: 'materialpicking',
            name: 'Material Picking'
          },{
            state: 'materialconfirmation',
            name: 'Material Issue Confirmation'
          },{
            state: 'chassisclearance',
            name: 'Chassis Inward Clearance'
          },{
            state: 'binsystemupdate',
            name: '2 Bin System'
          }          
        ]
      },{
        state: 'commercial',
        short_label: 'D',
        name: 'Commercial Management',
        type: 'sub',
        icon: 'ti-layout-grid2',
        children: [
          {
            state: 'customerspec',
            name: 'Customer Specification'
          },
          {
            state: 'foc',
            name: 'Customer Details'
          }
          , {
            state: 'chassisinward',
            name: 'Chassis Inward Clearance'
          },{
            state: 'workinprogress',
            name: 'Mixer WIP'
          }, {
            state: 'mountingscope',
            name: 'Mounting Scope'
          }, {
            state: 'dispatchplan',
            name: 'Dispatch Plan'
          },
          {
            state: 'billingstatus',
            name: 'Billing Status'
          }
        ]
      }, {
        state: 'dispatch',
        short_label: 'D',
        name: 'Dispatch Management',
        type: 'sub',
        icon: 'ti-package',
        children: [
           {
            state: 'mixerloading',
            name: 'Mixer Loading'
          }
        ]
      }, {
        state: 'livedashboard',
        short_label: 'D',
        name: 'Live Dashboards',
        type: 'sub',
        icon: 'ti-book',
        children: [
          {
            state: 'noofworkers',
            name: 'No OF Workers'
          }
        ]
      },
    ],
  },
];


@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
