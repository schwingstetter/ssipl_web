import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appEmitfirstspace]'
})
export class EmitfirstspaceDirective {

  constructor(private _el: ElementRef) { }

  @HostListener('keydown', ['$event']) onKeyDown(e) {
    if (e.which === 32 && !e.target.value.length)
      e.preventDefault();
  }   

}
