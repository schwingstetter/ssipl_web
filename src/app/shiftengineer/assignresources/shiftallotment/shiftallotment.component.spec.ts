import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftallotmentComponent } from './shiftallotment.component';

describe('ShiftallotmentComponent', () => {
  let component: ShiftallotmentComponent;
  let fixture: ComponentFixture<ShiftallotmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftallotmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftallotmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
