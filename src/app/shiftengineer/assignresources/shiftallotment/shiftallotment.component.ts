import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-shiftallotment',
  templateUrl: './shiftallotment.component.html',
  styleUrls: ['./shiftallotment.component.css']
})
export class ShiftallotmentComponent implements OnInit {

  public technicianname:string="";
  public modelno:string="";
  public name:string="";
  public options:string="";
  public options1:string="";
  public options2:string="";
  public optionSelected:string="";
  public editshiftallotment: any = "";
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  public workstationnames: boolean;
  public operationcodes: boolean;
  public version: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  public month: number;
  public day: number;
  public year: number;
  public date: string;
  model :NgbDateStruct;
  dtOptions: any = {};

  // settingsObj: any = 
  // {
  //   "dom": '<"top"Bfrt<"clear">>lrt<"bottom"ip<"clear">>',
  //   buttons: 
  //   [{
  //     extend: 'excel', text: 'Download',exportOptions: 
  //     {
  //       columns: [2, 3, 4,5] },title: '', 
  //     }] 
  //   }
  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
   
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "empid";
    var i:number;
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.technicianname = datas.message;
            
          
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getdata();
    }
    getstationname(event)
    {

      this.getworkstation(event);
    }
    getworkstation(event)
    {
      var data = {"opcode" : event};
      var method = "post";
      var url = "workstation";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {          
            if (datas.code == 201) {
          
              this.workstationnames = datas.message[0].list_of_operation;
              this.editshiftallotment.workstation = datas.message[0].list_of_operation;
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
      
    }
    shiftallotment(form)
    {
      let ngbDate = form.controls['dp'].value;
      let myDate = new Date(ngbDate.year, ngbDate.month-1, ngbDate.day);
      var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    data['dp'] = myDate;
    var method= "post";
    var url="shiftallotment";
    this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => 
      {
        if(datas.code==201){
          this.toastr.success(datas.message, datas.type);
          form.resetForm(); 
          this.reloadPage();
          this.modalRef.close();          this.toastr.success(datas.message, datas.type);
          form.resetForm(); 
          this.reloadPage();
          this.modalRef.close();
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
      
    
    );
    }
    reloadPage() {
      this.ngOnInit();
    };
    getoperationcode()
    {
      var data = {"basedon": this.optionSelected};
      var method = "post";
      var url = "operationcode";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {          
            if (datas.code == 201) {
              this.operationcodes = datas.message;
            
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
       
    }
    getdata()
    {
      $('.datatable').DataTable();
      var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "shiftallotment";
      var i:number;
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.options = datas.message;
              for(i=0;i<datas.message.length;i++)
              {
              datas.message[0].date = datas.message[0].date.substring(0, 10);           
              }
              $('.datatable').DataTable().destroy();
              setTimeout(() => 
              {
              //  alert("sdf");
              //   $('.datatable').DataTable( {
              //     dom: 'Bfrtip',
              //     buttons: [
              //         'copy', 'excel', 'pdf'
              //     ]
              // } )
              }, 1000);
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
    onOptionsSelected(event)
    {
 
  var data = {"empid":event};
  var method = "post";
  var url = "technicianname";
  var i: number; 
  
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {          
        if (datas.code == 201) {
          for(i=0;i<datas.message.length;i++)
          {
            this.options1 = datas.message[0].name;
            this.options2 = datas.message[0].user_role;
            this.editshiftallotment.technician_name = datas.message[0].name;
    
            this.editshiftallotment.designation = datas.message[0].user_role;
          }
       
        } else {
  
        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
    }
    getmodelno(event)
    {
      this.optionSelected = event;
      this.name=event;
    //  alert(event);
      var data = {"stationame":event};
      var method = "post";
      var url = "shiftgetmodelno";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {          
            if (datas.code == 201) {
              this.modelno = datas.message;
           
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
        this.getoperationcode();
    }
    editsallotment(form,edit)
    {
      
      let ngbDate = form.controls['dp'].value;
      let myDate = new Date(ngbDate.year, ngbDate.month-1, ngbDate.day);
        var data = form.value;
        var switchcase = "switchcase";
        var value = "edit";
        data[switchcase] = value;
        data['dp'] = myDate;
        var method = "post";
        var url = "shiftallotment";
        console.log(data);
        this.AjaxService.ajaxpost(data, method, url)
          .subscribe(
            (datas) => {
              if (datas.code == 201) {
         
                this.toastr.success(datas.message, datas.type);    
                this.reloadPage();        
                this.modalRef.close();

              }
              else {
                this.toastr.error(datas.message, datas.type);
              }
            },
            (err) => {
              if (err.status == 403) {
                this.toastr.error(err._body);
                //alert(err._body);
              }
            }
          );
          //this.toastr.success('Hello world!', 'Toastr fun!');
      };
    editfunction(item) {
      // alert("");
 console.log(item);
       this.editshiftallotment = item;
       console.log(item);
       var dateObj = new Date(this.editshiftallotment.date);
this.month = dateObj.getUTCMonth() + 1; //months from 1-12
this.day = dateObj.getUTCDate();
this.year = dateObj.getUTCFullYear();
this.model ={year: this.year, month: this.month, day:this.day};
this.getmodelno(this.editshiftallotment.type);
     //  const isDisabled = (date: NgbDateStruct, current: {month: number}) => day.date === 13;
      
     };
     open(addNew) {
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
      //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
    };
    openbulkupload(bulkupload) {
      this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    };
    openedit(edit) {
      this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    };
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }
}
