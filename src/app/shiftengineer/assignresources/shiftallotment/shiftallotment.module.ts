import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShiftallotmentComponent } from './shiftallotment.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const ShiftallotmentRoutes: Routes = [
  {
    path: '',
    component: ShiftallotmentComponent,
    data: {
      breadcrumb: 'Shift Allotment',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ShiftallotmentRoutes),
    SharedModule   
  ],
  declarations: [ShiftallotmentComponent]
})
export class ShiftallotmentModule { }
