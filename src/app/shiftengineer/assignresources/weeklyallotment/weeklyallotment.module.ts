import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeeklyallotmentComponent } from './weeklyallotment.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const WeeklyallotmentRoutes: Routes = [
  {
    path: '',
    component: WeeklyallotmentComponent,
    data: {
      breadcrumb: 'Weekly Allotment',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(WeeklyallotmentRoutes),
    SharedModule   
  ],
  declarations: [WeeklyallotmentComponent]
})
export class WeeklyallotmentModule { }
