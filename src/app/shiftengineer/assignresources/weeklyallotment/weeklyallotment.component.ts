import { Component, OnInit, ViewContainerRef} from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-weeklyallotment',
  templateUrl: './weeklyallotment.component.html',
  styleUrls: ['./weeklyallotment.component.css']
})
export class WeeklyallotmentComponent implements OnInit {
  public technicianname:string="";
  public options1:string="";
  public options:string="";
  public options2:string="";
  public optionSelected:string="";
  public weeklyallotment:any="";
  public editsnagmasters: string = "";
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  public workstationnames: boolean;
  public operationcodes: boolean;
  public modelno: boolean;
  public version: boolean;
  public name: string;
  public month: number;
  public day: number;
  public year: number;
  public month1: number;
  public day1: number;
  public year1: number;
  public month2: number;
  public day2: number;
  public year2: number;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
model :NgbDateStruct;
model1:NgbDateStruct;
model2:NgbDateStruct;
fromdate :string;
todate :string;
date :string;
todaydate :string;

public minDate: Date = void 0; 
public datepickermindate:any= { "year": (new Date()).getFullYear(), "month": ((new Date()).getMonth()+1), "day": (new Date()).getDate() }
// optionsdate: DatePickerOptions = {
//   format: 'DD-MM-YYYY',
//   todayText: 'Oggi',
//   style: 'big'
// };
  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  isDisabled(date: NgbDateStruct) {
    const d = new Date(date.year, date.month - 1, date.day);
    return d.getDay() === 0 ;
  }
  ngOnInit() {
    var today = new Date();
    this.todaydate = today.toDateString();
    var table=$('#example').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "empid";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.technicianname = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getdata();
    }
    getstationname(event)
    {

      this.getworkstation(event);
    }
    getworkstation(event)
    {
      var data = {"opcode" : event};
      var method = "post";
      var url = "workstation";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {          
            if (datas.code == 201) {
         // console.log( datas.message[0].list_of_operation);
         if(datas.message.length>0)
         {
            this.workstationnames = datas.message[0].list_of_operation;
              this.weeklyallotment.workstation = datas.message[0].list_of_operation;
         }
             
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
      
    }
    getmodelno(event)
    {
      this.optionSelected = event;
      this.name=event;
    //  alert(event);
      var data = {"stationame":event};
      var method = "post";
      var url = "getmodelno";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {          
            if (datas.code == 201) {
              this.modelno = datas.message;
       
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
        this.getoperationcode();
    }
    getoperationcode()
    {
      var data = {"basedon": this.optionSelected};
      var method = "post";
      var url = "operationcode";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {          
            if (datas.code == 201) {
              this.operationcodes = datas.message;
            
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  
    }
    getdata()
    {
      var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "weeklyallotment";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.options = datas;
              for(i=0;i<datas.message.length;i++)
              {
              datas.message[0].date = datas.message[0].date.substring(0, 10);
              datas.message[0].from_date = datas.message[0].from_date.substring(0, 10);
              datas.message[0].to_date = datas.message[0].to_date.substring(0, 10);
              }
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
    onOptionsSelected(event)
    {

  var data = {"empid":event};
  var method = "post";
  var url = "technicianname";
  var i: number; 
  
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {          
        if (datas.code == 201) {
          for(i=0;i<datas.message.length;i++)
          {
            this.options1 = datas.message[0].name;
            this.options2 = datas.message[0].user_role;
        
            this.weeklyallotment.technician_name = datas.message[0].name;
    
            this.weeklyallotment.user_role = datas.message[0].user_role;
          }
       
        } else {
  
        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
    }
    editfunction(item) {
      // alert("");
      console.log(item);
       this.weeklyallotment = item;
      
       var dateObj = new Date(this.weeklyallotment.date);
this.month = dateObj.getUTCMonth() + 1; //months from 1-12
this.day = dateObj.getUTCDate();
this.year = dateObj.getUTCFullYear();
this.model ={year: this.year, month: this.month, day:this.day};
var dateObj1 = new Date(this.weeklyallotment.from_date);
this.month1 = dateObj1.getUTCMonth() + 1; //months from 1-12
this.day1 = dateObj1.getUTCDate();
this.year1 = dateObj1.getUTCFullYear();
this.model1={year: this.year1, month: this.month1, day:this.day1};
this.minDate=this.weeklyallotment.from_date;
var dateObj2 = new Date(this.weeklyallotment.to_date);
this.month2 = dateObj2.getUTCMonth() + 1; //months from 1-12
this.day2 = dateObj2.getUTCDate();
this.year2 = dateObj2.getUTCFullYear();
this.model2={year: this.year2, month: this.month2, day:this.day2};
       this.getmodelno(this.weeklyallotment.type);
     //  const isDisabled = (date: NgbDateStruct, current: {month: number}) => day.date === 13;
      
     };
     onChange(event)
     {
       
      this.minDate=event;
     }
     editweeklyallotment(form,edit)
     {  
      let ngbDate = form.controls['dp'].value;
      let myDate = new Date(ngbDate.year, ngbDate.month-1, ngbDate.day);
      let ngbDate1 = form.controls['dp1'].value;
      let myDate1 = new Date(ngbDate1.year, ngbDate1.month-1, ngbDate1.day);
      let ngbDate2 = form.controls['dp2'].value;
      let myDate2 = new Date(ngbDate2.year, ngbDate2.month-1, ngbDate2.day);
         var data = form.value;
         var switchcase = "switchcase";
         var value = "edit";
         data[switchcase] = value;
         data['dp'] = myDate;
         data['dp1'] = myDate1;
         data['dp2'] = myDate2;
         var method = "post";
         var url = "weeklyallotment";
         this.AjaxService.ajaxpost(data, method, url)
           .subscribe(
             (datas) => {
               if (datas.code == 201) {
                 this.reloadPage();   
                 edit.hide();
                 this.toastr.success(datas.message, datas.type);    
                      
               }
               else {
                 this.toastr.error(datas.message, datas.type);
               }
             },
             (err) => {
               if (err.status == 403) {
                 this.toastr.error(err._body);
                 //alert(err._body);
               }
             }
           );
           //this.toastr.success('Hello world!', 'Toastr fun!');
       };
       delete(id)
       {
        var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
        var method = "post";
        var url = "weeklyallotment";
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!',
        }).then((isconfirm) => {
          if (isconfirm.value == true) {
            this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
              .subscribe(
                response => {
                  if (response.code == 201) {
                    swal(
                      'Deleted!',
                      'Your file has been deleted.',
                      'success'
                    )
                    this.ngOnInit();
                  }
                  else if(response.code==403){
                    swal(
                      'Not Deleted!',
                      'Your file is safe.',
                      'error'
                    )
                  }
                },
                error => console.log('error : ' + error)
              );
          } else {
    
          }
        });
       }
    allotweekly(form)
    {
      let ngbDate = form.controls['dp'].value;
      let myDate = new Date(ngbDate.year, ngbDate.month-1, ngbDate.day);
      let ngbDate1 = form.controls['dp1'].value;
      let myDate1 = new Date(ngbDate1.year, ngbDate1.month-1, ngbDate1.day);
      let ngbDate2 = form.controls['dp2'].value;
      let myDate2 = new Date(ngbDate2.year, ngbDate2.month-1, ngbDate2.day);
      var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    data['dp'] = ngbDate;
    data['dp1'] = ngbDate1;
    data['dp2'] = ngbDate2;
    var method= "post";
    var url="weeklyallotment";
    this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => 
      {
        if(datas.code==201){
          this.toastr.success(datas.message, datas.type);
          form.resetForm(); 
        //  this.reloadPage();
        this.getdata();
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
      
    
    );
    }
    reloadPage() {
     
       this.ngOnInit();
    }
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  open(addNew) {
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
   this.file_name=fileList;
  }
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
     let options = new RequestOptions({ headers: headers });
     var data = formData;
      var method = "post";
      var url = "uploadfilesweeklyallotment";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        data => {
          for (i = 0; i < data.length; i++) {
            if (data[i].code == 201) {
              this.toastr.success(data[i].message, 'Success!');
            } else {
              this.toastr.error(data[i].message, 'Error!');
            }
          }
          this.reloadPage();
          this.modalRef.close(); 
        },
        error => this.toastr.error(error)
      );
  } else {

  }
  };
  reset(frm)
  {
frm.resetForm();
  }
  downloadFile() {

    return this.http
      .get('assets/uploads/weeklyallotment.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""  })
      .map(res => {
        return {
          filename: 'Weekly Allotment.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
          console.log('start download:',res);
          var url = window.URL.createObjectURL(res.data);
          var a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = url;
          a.download = res.filename;
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove(); // remove the element
        }, error => {
          console.log('download error:', JSON.stringify(error));
        }, () => {
          console.log('Completed file download.')
        });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
