import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyallotmentComponent } from './weeklyallotment.component';

describe('WeeklyallotmentComponent', () => {
  let component: WeeklyallotmentComponent;
  let fixture: ComponentFixture<WeeklyallotmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyallotmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyallotmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
