import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReworkassignmentComponent } from './reworkassignment.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const ReworkassignmentRoutes: Routes = [
  {
    path: '',
    component: ReworkassignmentComponent,
    data: {
      breadcrumb: 'Rework Assignment',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ReworkassignmentRoutes),
    SharedModule   
  ],
  declarations: [ReworkassignmentComponent]
})
export class ReworkassignmentModule { }
