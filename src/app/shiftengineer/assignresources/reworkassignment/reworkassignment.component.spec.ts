import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReworkassignmentComponent } from './reworkassignment.component';

describe('ReworkassignmentComponent', () => {
  let component: ReworkassignmentComponent;
  let fixture: ComponentFixture<ReworkassignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReworkassignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReworkassignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
