import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-reworkassignment',
  templateUrl: './reworkassignment.component.html',
  styleUrls: ['./reworkassignment.component.css']
})
export class ReworkassignmentComponent implements OnInit {
  public options:string="";
  public options1:string="";
  public options2:string="";
  public optionSelected:string="";
  public editsnagmasters: string = "";
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  public workstationnames: boolean;
  public operationcodes: boolean;
  public version: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  model: NgbDateStruct;
  date: {year: number, month: number};
  constructor(public http: Http,private calendar: NgbCalendar, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "assignrework";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.options = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getoperationcode();
    }
    getstationname(event)
    {

      this.getworkstation(event);
    }
    getworkstation(event)
    {
      var data = {"opcode" : event};
      var method = "post";
      var url = "workstation";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {          
            if (datas.code == 201) {
          
              this.workstationnames = datas.message[0].list_of_operation;
            
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
      
    }
    assignrework(form: NgForm,add){
   
      var data = form.value;
      var switchcase = "switchcase";
      var value = "insert";
      data[switchcase] = value;
      var method= "post";
      var url="assignrework";
      this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => 
        {
          if(datas.code==201){
            this.toastr.success(datas.message, datas.type);
            form.resetForm(); 
            this.reloadPage();
            this.modalRef.close();
          }
          else{
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
        
      
      );
    
    };
    reloadPage() {
      // Solution 1:   
      // this.router.navigate('localhost:4200/new');
    
      // Solution 2:
       this.ngOnInit();
    }
    getoperationcode()
    {
      var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "alloperationcode";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {          
            if (datas.code == 201) {
              this.operationcodes = datas.message;
            
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
    onOptionsSelected(event)
    {
 
  var data = {"empid":event};
  var method = "post";
  var url = "technicianname";
  var i: number; 
  
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {          
        if (datas.code == 201) {
          for(i=0;i<datas.message.length;i++)
          {
            this.options1 = datas.message[0].name;
            this.options2 = datas.message[0].emp_designation;
  
          }
       
        } else {
  
        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
    }
}

