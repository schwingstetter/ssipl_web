import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignresourcesComponent } from './assignresources.component';
import { assignresourcesRoutes } from './assignresources.routing';
import { RouterModule } from '@angular/router';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(assignresourcesRoutes)
  ],
  declarations: [AssignresourcesComponent]
})
export class AssignresourcesModule { }
