import { Routes } from '@angular/router';


export const assignresourcesRoutes: Routes = [
    {
        path: '',
        data: {
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'weeklyallotment',
                loadChildren: './weeklyallotment/weeklyallotment.module#WeeklyallotmentModule'
            },
            {
                path: 'shiftallotment',
                loadChildren: './shiftallotment/shiftallotment.module#ShiftallotmentModule'
            },
            {
                path: 'reworkassignment',
                loadChildren: './reworkassignment/reworkassignment.module#ReworkassignmentModule'
            },

        ]
    }
];