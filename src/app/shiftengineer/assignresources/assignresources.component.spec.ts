import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignresourcesComponent } from './assignresources.component';

describe('AssignresourcesComponent', () => {
  let component: AssignresourcesComponent;
  let fixture: ComponentFixture<AssignresourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignresourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignresourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
