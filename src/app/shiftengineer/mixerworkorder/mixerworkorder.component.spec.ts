import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixerworkorderComponent } from './mixerworkorder.component';

describe('MixerworkorderComponent', () => {
  let component: MixerworkorderComponent;
  let fixture: ComponentFixture<MixerworkorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixerworkorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixerworkorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
