import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mixerworkorder',
  templateUrl: './mixerworkorder.component.html',
  styleUrls: ['./mixerworkorder.component.css']
})
export class MixerworkorderComponent implements OnInit {
  public mixerworkorder: string = "";
  public workorder: string = "";
  public wo: string = "";
  public selectedpartno: string = "";
  public selectedwono:string="";
  public partno: string = "";
  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mixerworkorder";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
           
            this.mixerworkorder = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      var operationdatas: { switchcase: string } = { switchcase: "getallpartno" };
      var meth = "post";
      var ur = "mixerplanninggetpartno";
      this.AjaxService.ajaxpost(operationdatas, meth, ur)
        .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
              this.partno = mixerplannings;
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );  
    setTimeout(() => {
      $('.datatable').DataTable();
    }, 1000);
    //this.confirm(1,status);
  }
  confirm(id,status)
  {
  var data: { switchcase: string, id: any ,status:any} = { switchcase: "approvalorreject", id: id ,status: status };
  var method = "post";
  var url = "mixerworkorderconfirmation";
 
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, Approve!',
  }).then((isconfirm) => {
    if (isconfirm.value == true) {
      this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
        .subscribe(
          response => {
            if (response.code == 201) {
              swal(
                'Approved!',
                'The Request has been Approved.',
                'success'
              );
             
           this.getdetails();
            }
            else {
              
            }
          },
          error => console.log('error : ' + error)
        );
    } else {     
     // this.reloadPage();
    }
  });
 
  }
  getwo(event)
  {
    this.wo=event;
    console.log(this.wo)
    //this.selectedwono
  }
  getworkorder(value)
  {
    /*if(value == 'ALL')
    {
      this.workorder ="";
      //this.reloadPage();
    }
    else
    {*/
    this.selectedpartno=value;
    var data: { switchcase: string, id: string } = { switchcase: "getworkorderno", id: value };
    var method = "post";
    var url = "addmixerplanning";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {         
          if (datas.code == 201) { 
            /*if()
            {

            }  */         
            this.workorder = datas;
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
     /* var table = $('.datatable').DataTable();
      var data1: { switchcase: string, id: string } = { switchcase: "getdetails", id: value };
      var method1 = "post";
      var url1 = "mixerworkorderdetails";
      this.AjaxService.ajaxpost(data1, method1, url1)
        .subscribe(
          (datass1) => {         
            if (datass1.code == 201) {            
              this.mixerworkorder = datass1.message;
              
            }
            else {
              this.toastr.error(datass1.message, datass1.type);
            }
          },
          (err) => {
            if (err.status == 403) {
              this.toastr.error(err._body);
              //alert(err._body);
            }
          }
        );*/
      //  }
  }
  getdetails()
  {
   /* if(this.wo == 'ALL')
    {
      var table = $('.datatable').DataTable();
      var data1: { switchcase: string, id: string,workorder:string } = { switchcase: "getdetails", id: this.selectedpartno,workorder: this.selectedwono };
      var method1 = "post";
      var url1 = "mixerworkorderdetails";
      this.AjaxService.ajaxpost(data1, method1, url1)
        .subscribe(
          (datass1) => {         
            if (datass1.code == 201) {            
              this.mixerworkorder = datass1.message;
              table.destroy();
              setTimeout(() => {
                $('.datatable').DataTable();
              }, 1000);
            }
            else {
              this.toastr.error(datass1.message, datass1.type);
            }
          },
          (err) => {
            if (err.status == 403) {
              this.toastr.error(err._body);
              //alert(err._body);
            }
          }
        );
    }
    else
    {*/
      var table = $('.datatable').DataTable();
    var data: { switchcase: string, id: string , workorder:string} = { switchcase: "getdetails", workorder: this.wo,id:this.selectedpartno };
    var method = "post";
    var url = "mixerpartnoworkorderdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {         
          if (datas.code == 201) {            
            this.mixerworkorder = datas.message;
            console.log(this.mixerworkorder);
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //}
  }
  reloadPage() {
    this.ngOnInit();
  };
}
