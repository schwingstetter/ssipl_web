import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MixerworkorderComponent } from './mixerworkorder.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const mixerworkorderRoutes: Routes = [
  {
    path: '',
    component: MixerworkorderComponent,
    data: {
      breadcrumb: 'Mixer Workorder Confirmation',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mixerworkorderRoutes),
    SharedModule   
  ],
  declarations: [MixerworkorderComponent]
})
export class MixerworkorderModule { }
