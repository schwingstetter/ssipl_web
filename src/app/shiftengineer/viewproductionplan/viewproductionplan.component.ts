import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-viewproductionplan',
  templateUrl: './viewproductionplan.component.html',
  styleUrls: ['./viewproductionplan.component.css']
})
export class ViewproductionplanComponent implements OnInit {
  options1 :any[];
  optionscomp :any[];
  optionsMap:any[];
  dateval: string = null;

options :any[];
optionmodel :any[];
//priority_list :any[];
public priority_list:any[] = new Array();
compoptions:string="";
partno:string="";
optionSelected: any = "Select";
optionSelected1: any = "Select";
optionmodelSelected: any = "Select";
private getmixerparts: any = {};
editplanning : string ="";
details : string ="";
private modalRef: NgbModalRef;
closeOther: boolean = false;
closeResult: string;
public minDate: Date = void 0; 

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef , private modalService: NgbModal,config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var lastdays=new Date(yyyy, mm +1, 0).getDate();
   // config.minDate = { year: yyyy, month: mm, day: dd };
   }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string} = { switchcase: "get"};
    var method = "post";
    var url = "addmixerplanning";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            console.log("length",datas.message.length);
            this.details = datas.message;
            for(var i=1;i<=datas.message.length;i++)
            {
              this.priority_list.push(i);
            }
            console.log("priority",this.priority_list);
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      this.getmixerpartno();
  }
  getmixerpartno() {
    var mixer_data: { switchcase: string } = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data, mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      }
  onChange(event)
  {
   this.minDate=event;  
  }
  search()
  {
    this.priority_list = new Array();
    console.log("mindate",this.minDate);
    console.log("partno",this.partno);
  if((this.minDate == undefined || this.partno == ""))
  {
    this.toastr.error("Select All Fields","Warning");    
  }
  else
  {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string ,date:Date,partno:string} = { switchcase: "getbydate",date:this.minDate,partno:this.partno };
    var method = "post";
    var url = "addmixerplanning";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            console.log("length",datas.message.length);
            this.details = datas.message;
            for(var i=1;i<=datas.message.length;i++)
            {
              this.priority_list.push(i);
            }
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
            // this.reloadPage();
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      }
  }
  getpartno(event)
  {
    this.partno=event;
  }
  editfunction(item) {
   // alert("");
    this.editplanning = item;
  };
  editmixerplanning(form,edit)
  {
      var data = form.value;
      var switchcase = "switchcase";
      var value = "edit";
      data[switchcase] = value;
      var method = "post";
      var url = "addmixerplanning";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.modalRef.close();
              this.toastr.success(datas.message, datas.type);    
              this.search();        
            }
            else 
            {
              this.toastr.error(datas.message, datas.type);
            }
          },
          (err) => {
            if (err.status == 403) {
              this.toastr.error(err._body);
              //alert(err._body);
            }
          }
        );
        //this.toastr.success('Hello world!', 'Toastr fun!');
    };
    reloadPage() {
      // Solution 1:   
      // this.router.navigate('localhost:4200/new');
    
      // Solution 2:
       this.ngOnInit();
    }
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
