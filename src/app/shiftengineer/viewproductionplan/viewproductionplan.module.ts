import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewproductionplanComponent } from './viewproductionplan.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const productionplanRoutes: Routes = [
  {
    path: '',
    component: ViewproductionplanComponent,
    data: {
      breadcrumb: 'Production Plan',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(productionplanRoutes),
    SharedModule   
  ],
  declarations: [ViewproductionplanComponent]
})
export class ViewproductionplanModule { }
