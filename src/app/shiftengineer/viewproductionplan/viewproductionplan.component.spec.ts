import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewproductionplanComponent } from './viewproductionplan.component';

describe('ViewproductionplanComponent', () => {
  let component: ViewproductionplanComponent;
  let fixture: ComponentFixture<ViewproductionplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewproductionplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewproductionplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
