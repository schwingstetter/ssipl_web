import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-viewrawmaterialissue',
  templateUrl: './viewrawmaterialissue.component.html',
  styleUrls: ['./viewrawmaterialissue.component.css']
})
export class ViewrawmaterialissueComponent implements OnInit {
  options1 :any[];
  optionscomp :any[];
  optionsMap:any[];
  dateval: string = null;
  segmentdetails: string = "";
options :any[];
optionmodel :any[];
compoptions:string="";
optionSelected: any = "Select";
optionSelected1: any = "Select";
optionmodelSelected: any = "Select";
editplanning : string ="";
details : string ="";
partno : string ="";
private modalRef: NgbModalRef;
closeOther: boolean = false;
closeResult: string;
searchpartno: string = "";
idpartno: string = "";

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef , private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "getdataforaproval" };
    var method = "post";
    var url = "rawmaterialissue";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.details = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      this.getpartno();
  }
  get_partno(id)
  {
    this.searchpartno = id;
  }
  acceptrawmaterial(id,partnumber,quantity,approvalstatus) {
    var data: { switchcase: string, id: any,partnumber : any,approvalstatus : any,quantity:any } = { switchcase: "rawmaterialissueapprovalstatus", id: id,partnumber : partnumber,approvalstatus : approvalstatus,quantity:quantity};
    var method = "post";
    var url = "rawmaterialissue";
    swal({
      title: 'Are you sure to approve?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Approved!',
                  'Raw material issue is approved.',
                  'success'
                )
              }
              else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {
        this.reloadPage();
      }
    });
  };
  getpartno()
  {
    var data: { switchcase: string } = { switchcase: "getpartno" };
    var method = "post";
    var url = "cncsegment";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.partno = datas.message;       
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getdetails()
  {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string,id:string } = { switchcase: "getdataforaproval",id:this.searchpartno };
    var method = "post";
    var url = "rawmaterialissue";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.details = datas.message;           
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  rejectrawmaterial(id,partnumber,quantity,approvalstatus) {
    var data: { switchcase: string, id: any,partnumber : any,approvalstatus : any,quantity:any } = { switchcase: "rawmaterialissueapprovalstatus", id: id,partnumber : partnumber,approvalstatus : approvalstatus,quantity:quantity};
    var method = "post";
    var url = "rawmaterialissue";
    swal({
      title: 'Are you sure to reject?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Approved!',
                  'Raw material issue is rejected.',
                  'success'
                )
              }
              else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.reloadPage();
      }
    });
  };
   reloadPage() {
    this.ngOnInit();
  }
}
