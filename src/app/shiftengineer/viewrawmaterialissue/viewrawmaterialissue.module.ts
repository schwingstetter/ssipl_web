import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewrawmaterialissueComponent } from './viewrawmaterialissue.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const productionplanRoutes: Routes = [
  {
    path: '',
    component: ViewrawmaterialissueComponent,
    data: {
      breadcrumb: 'Raw Material Issue',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(productionplanRoutes),
    SharedModule
  ],
  declarations: [ViewrawmaterialissueComponent]
})
export class ViewrawmaterialissueModule { }
