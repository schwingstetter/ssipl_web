import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnepointlessonComponent } from './onepointlesson.component';

describe('OnepointlessonComponent', () => {
  let component: OnepointlessonComponent;
  let fixture: ComponentFixture<OnepointlessonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnepointlessonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnepointlessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
