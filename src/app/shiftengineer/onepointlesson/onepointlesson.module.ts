import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnepointlessonComponent } from './onepointlesson.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const onepointlessonRoutes: Routes = [
  {
    path: '',
    component: OnepointlessonComponent,
    data: {
      breadcrumb: 'One Point Lesson',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(onepointlessonRoutes),
    SharedModule 
  ],
  declarations: [OnepointlessonComponent]
})
export class OnepointlessonModule { }
