import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixerfinishComponent } from './mixerfinish.component';

describe('MixerfinishComponent', () => {
  let component: MixerfinishComponent;
  let fixture: ComponentFixture<MixerfinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixerfinishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixerfinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
