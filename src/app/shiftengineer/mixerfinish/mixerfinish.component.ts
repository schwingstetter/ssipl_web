import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
@Component({
  selector: 'app-mixerfinish',
  templateUrl: './mixerfinish.component.html',
  styleUrls: ['./mixerfinish.component.css']
})
export class MixerfinishComponent implements OnInit {
  public action: string = "";
  public contractor_name: string = "";  
  private getmixerparts: any = {};
  private modalRef: NgbModalRef;
  closeResult: string;
  public workstationnames: string = "";
  public sno: string = "";
  public assemblystage: string = "";
  public part_no: string = "";
  public isselected: string = '0';
  public editmixerfinishing: string = "";
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);

  }
  ngOnInit() {
    // var data = { 'status': '1' };
    // var method = "post";
    // var url = "mixerfinishserialno";
    // this.AjaxService.ajaxpost(data, method, url)
    //   .subscribe(
    //     (datas) => {
    //       if (datas.code == 201) {
    //         this.sno = datas.message;
    //       } else {
    //       }
    //     },
    //     (err) => {
    //       if (err.status == 403) {
    //         alert(err._body);
    //       }
    //     }
    //   );
      this.getmixerpartno();
  }
  getmixerpartno() {
    var mixer_data: { switchcase: string } = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data, mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      }
  getaction(value) 
  {
    this.assemblystage=value;
 if(value=='27')
 {
this.action="Mixer Finishing";
 }
 if(value=='28')
 {
this.action="SAP Posting to 830 Location";
 }
 if(value=='31')
 {
this.action="SAP Posting to 800 Location";
 }
  }
  getpartno(event)
  {
    this.part_no=event;
  }
  getdetails()
  {
    var table = $('.datatable').DataTable();
    this.isselected='1';
    var partno:any;
    partno=this.part_no ;
    var data = { 'status': '1','partno':partno,'assystage':this.assemblystage};
    var method = "post";
    var url = "mixerfinishserialno";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {

            this.sno = datas.message;
            table.destroy();
              setTimeout(() => {
                $('.datatable').DataTable();
              }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  editfunction(item)
  {
    console.log(item);
    this.editmixerfinishing = item;
  }
  updatemixerfinishprocess(form)
  {
    var assystage =form.value.assystage;
    var data = form.value;
    var method = "post";
    var url = "updatemixerfinishprocess";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
           
            this.toastr.success(datas.message, datas.type);   
            form.reset();    
            this.modalRef.close();
            this.getdetails();
           this.getaction(this.assemblystage);
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );
  }
  reloadPage() {
    this.ngOnInit();
  }
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
