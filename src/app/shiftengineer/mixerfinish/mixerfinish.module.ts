import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MixerfinishComponent } from './mixerfinish.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const mixerfinishRoutes: Routes = [
  {
    path: '',
    component: MixerfinishComponent,
    data: {
      breadcrumb: 'Mixer Finishing',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mixerfinishRoutes),
    SharedModule   
  ],
  declarations: [MixerfinishComponent]
})
export class MixerfinishModule { }
