import { Component, OnInit, ViewContainerRef, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { visitValue } from '../../../../node_modules/@angular/compiler/src/util';
//import { ConsoleReporter } from 'jasmine';

@Component({
  selector: 'app-shiftjobsheet',
  templateUrl: './shiftjobsheet.component.html',
  styleUrls: ['./shiftjobsheet.component.css']
})
export class ShiftjobsheetComponent implements OnInit {

  public processname: any;
  public stationname: any;
  public process: any;
  public inputVar: any;
  public details: any = "";
  public fitterdetail:string[];
  public welderdetail: any = "";
  public model_no: any = "";
  public wo: any = "";
  public engine_subassy: any = "";
  public ubolt: any = "";
  public enginemake: any = "";
  public gearboxmake: any = "";
  public hydpumpmake: any = "";
  public hydmotormake: any = "";
  public waterpumpmake: any = "";
  public channelassemblystopcable:any="";
  public enginecapacity: any = "";
  public gearboxcapacity: any = "";
  public hydpumpcapacity: any = "";
  public hydmotorcapacity: any = "";
  public waterpumpcapacity: any = "";
  words2 = [{ value: '' }];
  public mixerplanning:any={};
  public welder6='';
  public welder8='';
  public welder16='';
  public welder17='';
  public fitter5='';
  public fitter6='';
  public fitter7='';
  public fitter9='';
  public fitter10='';
  public fitter12='';
  public fitter13='';
  public fitter14='';
  public fitter16='';
  public fitter17='';
  public fittersi5='';
  public fittersi6='';
  public fittersi7='';
  public fittersi9='';
  public fittersi10='';
  public fittersi12='';
  public fittersi13='';
  public fittersi14='';
  public fittersi16='';
  public fittersi17='';
  public roll:number;
  public rollassembly='';
  public torque='';
  public hysrno:any="";
  public enginesr:any="";
  public hymsrno:any="";
  public waterno:any="";
  public tqqty:any="";
  public mixno:any="";
  public contractor_name:any="";
  public uqty:any="";
  public subassy:any="";
  public sqty:any="";
  public roll_pitch:any="";
  public rack_ring:any="";
  public welderis6:any="";
  public welderis8:any="";
  public welderis16:any=" ";
  public welderis17:any="";
  public gearno:any="";
  public drumno:any="";
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    setTimeout(() => {
      (<any>$('.datatable')).DataTable();
    }, 2000);
    var operationdatas: { switchcase: string } = { switchcase: "get" };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
     
  }
  onOptionstationSelected(event) {
    this.process = event;
   
  }
  getwo(event)
  {
    this.wo=event;
    this.generate();
    this.getfitterdetails();
    this.get_rollassembly();
    this.get_torque();
    this.getcapacitydetails
   
  }
  generate() {
    var data = { "workorder_no": this.wo };
    var method = "post";
    var url = "jobsheetmodelno";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.inputVar = datas.message;
            var ids = [];
            for (let i = 0; i < datas.message.length; i++) {
              ids.push(datas.message[i].mixer_modelno)
            }
            this.model_no = ids;
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    
  };
  generatesheet(value) {
    var data = { "part_no": value };
    var method = "post";
    var url = "jobsheetdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          this.engine_subassy = "";
          if (datas.code == 201) {
            this.details = datas.message;
            console.log(this.details)
            for (let i = 0; i < datas.message.length; i++) {
              this.enginemake = datas.message[i].engine_make;
              this.gearboxmake = datas.message[i].gearbox_make;
              this.hydpumpmake = datas.message[i].hydpump_make;
              this.hydmotormake = datas.message[i].hydmotor_make;
              this.waterpumpmake = datas.message[i].waterpump_make;
              this.channelassemblystopcable= datas.message[i].channelassembly_stopcable;
              
            }
            this.getcapacitydetails();
            
            if ((value.charAt(3) + value.charAt(4)) == 'SH') {
              this.engine_subassy = "AVP Tq : 100 Nm";
            }
            // if (value.charAt(2) > '6') {
            //   this.ubolt = "U bolt Tq : 360 nm";
            // }
            // else {
            //   this.ubolt = "U bolt Tq : 300 nm";
            // }
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      this.getfitterdetails();
      this.get_rollassembly();
      this.get_torque();
  };
  getfitterdetails() {
  //  alert("");
    //this.valuecouont=this.valuecouont+1;
    var data =  {"userid":"1", "work_order_no": this.wo };
    var method = "post";
    var url = "jobsheetfitter";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
        //   if (datas.code == 201) {
            
        //       this.stationname=datas.message.stationname;
        //       this.fitterdetail = datas.message.fitterdetails;
        //     for (let i = 0; i < this.stationname.length; i++) 
        //     {
        //       this.fitterdetail= datas.message.fitterdetails[i];
              
        //       console.log(this.fitterdetail);
        //     }        
        //     console.log(this.fitterdetail)    
        //   }
        //   else {
        //     this.toastr.error(datas.message, datas.type);
        //   }
        // },
        // (err) => {
        //   if (err.status == 403) {
        //     this.toastr.error(err._body);
        //     //alert(err._body);
        //   }
        // }
        this.fitter5=datas.message['ft5'];
        this.fitter6=datas.message['ft6'];
        this.fitter7=datas.message['ft7'];
        this.fitter9=datas.message['ft9'];
        this.fitter10=datas.message['ft10'];
        this.fitter12=datas.message['ft12'];
        this.fitter13=datas.message['ft13'];
        this.fitter14=datas.message['ft14'];
        this.fitter16=datas.message['ft16'];
        this.fitter17=datas.message['ft17'];
        this.fittersi5=datas.message['ftsi5'];
        this.fittersi6=datas.message['ftsi6'];
        this.fittersi7=datas.message['ftsi7'];
        this.fittersi9=datas.message['ftsi9'];
        this.fittersi10=datas.message['ftsi10'];
        this.fittersi12=datas.message['ftsi12'];
        this.fittersi13=datas.message['ftsi13'];
        this.fittersi14=datas.message['ftsi14'];
        this.fittersi16=datas.message['ftsi16'];
        this.fittersi17=datas.message['ftsi17'];
       
        }
        );
      this.getwelderdetails();
      }
      getwelderdetails() {
        //  alert("");
          //this.valuecouont=this.valuecouont+1;
          var data ={ "workorder_no": this.wo };
          var method = "post";
          var url = "jobsheetwelder";
          this.AjaxService.ajaxpost(data, method, url)
            .subscribe(
              (datas) => {
                if (datas.code == 201) {
                  /*for (let i = 0; i < datas.message.welderdetails.length; i++) 
                  {
                  
                    this.welderdetail = datas.message.welderdetails[i];
                    this.stationname=datas.message.stationname[i];
                    console.log(this.stationname);
                    console.log(this.welderdetail);
                  }*/
                 this.welder6=datas.message['ws6'];
                  this.welder8=datas.message['ws8'];
                  this.welder16=datas.message['ws16'];
                  this.welder17=datas.message['ws17'];
                  this.welderis6=datas.message['wis6'];
                  this.welderis8=datas.message['wis8'];
                  this.welderis16=datas.message['wis16'];
                  this.welderis17=datas.message['wis17'];
                  
                }
                else {
                  this.toastr.error(datas.message, datas.type);
                }
              },
              (err) => {
                if (err.status == 403) {
                  this.toastr.error(err._body);
                  //alert(err._body);
                }
              }
            );
           
            }
            
            get_rollassembly()
            {
              var data =  { "work_order_no": this.wo };
              var method = "post";
              var url = "rollassembly";
              this.AjaxService.ajaxpost(data, method, url)
                .subscribe(
                  (datas) => {
                    this.rollassembly="";
                    this.contractor_name="";
                    if (datas.code == 201) {
                      this.roll=datas.message[0]['roll_assembly'];
                      this.roll_pitch=datas.message[0]['roll_pitch_distance'];
                     this.rack_ring=datas.message[0][' rack_ring_back_mark_distance'];
                       if(this.roll==2)
                        {
                      this.rollassembly="Other"
                      this.contractor_name=datas.message[0]['other_name'];
                        }
                      else if(this.roll==1)
                        {
                      this.rollassembly="SSIL"
                        }
                       
                   
                  }
                    else {
                      this.toastr.error(datas.message, datas.type);
                    }
                    },
                  (err) => {
                    if (err.status == 403) {
                      this.toastr.error(err._body);
                      //alert(err._body);
                    }
                  }
                );
               
            }
            // details about torque function
            get_torque()
            {
               var data =  { "work_order_no": this.wo };
              var method = "post";
              var url = "jobsheettorque";
              this.AjaxService.ajaxpost(data, method, url)
                .subscribe(
                  (datas) => {
                   
                    this.torque="";
                  
                   if (datas.code == 201) {
                  
                    this.torque=datas.message;
                    // this.tqqty=datas.message;
                    console.log(this.torque);
                  
                  }
                 
                  else {
                    
                    this.toastr.error(datas.message, datas.type);
                  }
                },
                (err) => {
                  if (err.status == 403) {
                    this.toastr.error(err._body);
                    //alert(err._body);
                  }
                  }
                );
                this. get_serno()
            }
            get_serno()
            {
              var data =  { "work_order_no": this.wo };
              var method = "post";
              var url = "srno";
              this.AjaxService.ajaxpost(data, method, url)
                .subscribe(
                  (datas) => {
                    if (datas.code == 201) {
                    this.enginesr=datas.message[0]['engine_sl_no'];
                    this.hysrno=datas.message[0]['hyd_pump_s_no'];
                    this.hymsrno=datas.message[0]['hyd_motor_s_no'];
                    this.waterno=datas.message[0]['water_pump_s_no'];
                    this.gearno=datas.message[0]['gear_box_sl_no'];
                    this.mixno=datas.message[0]['mixer_s_no'];
                    this.drumno=datas.message[0]['drum_sl_no']; 
                    console.log(this.hysrno);
                  }
                  else {
                    this.toastr.error(datas.message, datas.type);
                  }
                },
                (err) => {
                  if (err.status == 403) {
                    this.toastr.error(err._body);
                    //alert(err._body);
                  }
                  }
                  
                );
            }
  getcapacitydetails() {
    //this.valuecouont=this.valuecouont+1;
    var data = { "engine_model": "Engine Model", "engine_make": this.enginemake };
    var method = "post";
    var switchcase = "switchcase";
    var value = "engine";
    data[switchcase] = value;
    var url = "jobsheetcapacity";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {
              this.enginecapacity = datas.message[0].capacity;
            }
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    var data1 = { "gear_model": "Gear Box Model", "gear_make": this.gearboxmake };
    var method = "post";
    var switchcase = "switchcase";
    var value = "gear";
    data1[switchcase] = value;
    var url = "jobsheetcapacity";
    this.AjaxService.ajaxpost(data1, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {
              this.gearboxcapacity = datas.message[0].capacity;
            }

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    var data2 = { "hydpump_model": "Hyd. Pump Model", "hydpump_make": this.hydpumpmake };
    var method = "post";
    var switchcase = "switchcase";
    var value = "hydpump";
    data2[switchcase] = value;
    var url = "jobsheetcapacity";
    this.AjaxService.ajaxpost(data2, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {
              this.hydpumpcapacity = datas.message[0].capacity;
            }

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    var data3 = { "hydmotor_model": "Hyd. Motor Model", "hydmotor_make": this.hydmotormake };
    var method = "post";
    var switchcase = "switchcase";
    var value = "hydmotor";
    data3[switchcase] = value;
    var url = "jobsheetcapacity";
    this.AjaxService.ajaxpost(data3, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {
              this.hydmotorcapacity = datas.message[0].capacity;
            }

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    var data4 = { "waterpump_model": "Water pump Model", "waterpump_make": this.waterpumpmake };
    var method = "post";
    var switchcase = "switchcase";
    var value = "waterpump";
    data4[switchcase] = value;
    var url = "jobsheetcapacity";
    this.AjaxService.ajaxpost(data4, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {
              this.waterpumpcapacity = datas.message[0].capacity;
            }

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      this. get_ubolt()
  }
  get_ubolt()
  {
     var data =  { "work_order_no": this.wo };
    var method = "post";
    var url = "jobsheetubolt";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
         
        
          this.ubolt="";
         if (datas.code == 201) {
         
          this.ubolt=datas.message;
          
          console.log(this.torque);
        
        }
       
        else {
          
          this.toastr.error(datas.message, datas.type);
        }
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
        }
      );
    this.get_subassy()
  }
  get_subassy()
  {
     var data =  { "work_order_no": this.wo };
    var method = "post";
    var url = "jobsheetsubass";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
         
          this.subassy="";
        
         if (datas.code == 201) 
         {
          this.subassy=datas.message;
          console.log(this.torque);
         
        }
       
        else {
          
          this.toastr.error(datas.message, datas.type);
        }
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
        }
      );
    
  }
}
