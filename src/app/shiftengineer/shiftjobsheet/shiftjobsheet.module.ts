import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShiftjobsheetComponent } from './shiftjobsheet.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const shiftjobsheetRoutes: Routes = [
  {
    path: '',
    component: ShiftjobsheetComponent,
    data: {
      breadcrumb: 'Job Sheet',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(shiftjobsheetRoutes),
    SharedModule   
  ],
  declarations: [ShiftjobsheetComponent]
})
export class ShiftjobsheetModule { }
