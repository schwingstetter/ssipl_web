import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftjobsheetComponent } from './shiftjobsheet.component';

describe('ShiftjobsheetComponent', () => {
  let component: ShiftjobsheetComponent;
  let fixture: ComponentFixture<ShiftjobsheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftjobsheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftjobsheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
