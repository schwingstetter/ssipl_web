import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftengineerComponent } from './shiftengineer.component';

describe('ShiftengineerComponent', () => {
  let component: ShiftengineerComponent;
  let fixture: ComponentFixture<ShiftengineerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftengineerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftengineerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
