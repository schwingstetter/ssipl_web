import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelaysnagreworkComponent } from './delaysnagrework.component';

describe('DelaysnagreworkComponent', () => {
  let component: DelaysnagreworkComponent;
  let fixture: ComponentFixture<DelaysnagreworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelaysnagreworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelaysnagreworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
