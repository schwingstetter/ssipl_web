import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delaysnagrework',
  templateUrl: './delaysnagrework.component.html',
  styleUrls: ['./delaysnagrework.component.css']
})
export class DelaysnagreworkComponent implements OnInit {
  public workstationnames: any;
  public details: any;
  public datacount: any;
  public selectedoptiontype: any;
  public selectedtype: any;
  public isselected: any = '0';
  public selectedstation: any;
  private modalRef: NgbModalRef;
  closeResult: string;
constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
  this.toastr.setRootViewContainerRef(vcr);  
}
  ngOnInit() {    
    var table = $('.datatable').DataTable();
    if(this.selectedoptiontype!=undefined && this.selectedstation!=undefined && this.selectedtype!=undefined)
    {
      this.isselected=1;
    var data = { "processtype":this.selectedoptiontype,"workstation":this.selectedstation,"type":this.selectedtype };
      var method = "post";
      var url = "delaysnagrework";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.details = datas.message;    
            this.datacount=this.details.length;   
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      }
      
  }
  getworkstation(event)
  {
    console.log(event);
    this.selectedoptiontype=event;
    var data = { "basedon": event };
    var method = "post";
    var url = "operationcode";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if (datas.code == 201) {
          this.workstationnames = datas.message;        
        } else {
        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
}
getselectedworkstation(event)
{
  this.selectedstation=event;
}
getselectedtype(event)
{
  this.selectedtype=event;
}
getdetails()
{
  var table = $('.datatable').DataTable();
  if(this.selectedoptiontype!=undefined && this.selectedstation!=undefined && this.selectedtype!=undefined)
  {
    this.isselected=1;
  var data = { "processtype":this.selectedoptiontype,"workstation":this.selectedstation,"type":this.selectedtype };
    var method = "post";
    var url = "delaysnagrework";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if (datas.code == 201) {
          this.details = datas.message;    
          this.datacount=this.details.length;   
          table.destroy();
          setTimeout(() => {
            $('.datatable').DataTable();
          }, 1000);
        } else {

        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
    }
    else
    {
      this.toastr.error("Select All Fields","Warning");
    }
}
approveorreject(id,status)
{
var data: { switchcase: string, id: any ,type:any,status:any,processtype:any} = { switchcase: "approvalorreject", id: id ,type: this.selectedtype,status:status,processtype:this.selectedoptiontype };
var method = "post";
var url = "delayrework";
if(status == 1)
{
swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, Approve!',
}).then((isconfirm) => {
  if (isconfirm.value == true) {
    console.log(data);
    this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
      .subscribe(
        response => {
          if (response.code == 201) {
            swal(
              'Approved!',
              'The Request has been Approved.',
              'success'
            );
           
            this.getdetails();
          }
          else {
            
          }
        },
        error => console.log('error : ' + error)
      );
  } else {
   
    this.reloadPage();
  }
});
}
else
{
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, Reject!',
  }).then((isconfirm) => {
    if (isconfirm.value == true) {
      this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
        .subscribe(
          response => {
            if (response.code == 201) {
              swal(
                'Rejected!',
                'The Request has been Rejected.',
                'success'
              );
             
              this.reloadPage();
            }
            else {
              
            }
          },
          error => console.log('error : ' + error)
        );
    } else {
     
      this.reloadPage();
    }
  });
}  
}
reloadPage() {
  this.ngOnInit();
};
private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return `with: ${reason}`;
  }
};
openedit(edit) {
  this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
  this.modalRef.result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
};
open(addNew) {
  this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
  this.modalRef.result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
  //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
};
}
