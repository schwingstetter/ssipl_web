import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DelaysnagreworkComponent } from './delaysnagrework.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const delaysnagreworkRoutes: Routes = [
  {
    path: '',
    component: DelaysnagreworkComponent,
    data: {
      breadcrumb: 'Delay/Snag/Rework',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(delaysnagreworkRoutes),
    SharedModule   
  ],
  declarations: [DelaysnagreworkComponent]
})
export class DelaysnagreworkModule { }
