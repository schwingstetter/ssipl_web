import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinerejectionComponent } from './linerejection.component';

describe('LinerejectionComponent', () => {
  let component: LinerejectionComponent;
  let fixture: ComponentFixture<LinerejectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinerejectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinerejectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
