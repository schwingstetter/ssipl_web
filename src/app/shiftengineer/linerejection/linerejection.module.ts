import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinerejectionComponent } from './linerejection.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const linerejectionRoutes: Routes = [
  {
    path: '',
    component: LinerejectionComponent,
    data: {
      breadcrumb: 'Line Rejection',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(linerejectionRoutes),
    SharedModule   
  ],
  declarations: [LinerejectionComponent]
})
export class LinerejectionModule { }
