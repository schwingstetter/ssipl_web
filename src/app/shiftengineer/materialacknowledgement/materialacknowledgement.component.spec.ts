import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialacknowledgementComponent } from './materialacknowledgement.component';

describe('MaterialacknowledgementComponent', () => {
  let component: MaterialacknowledgementComponent;
  let fixture: ComponentFixture<MaterialacknowledgementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialacknowledgementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialacknowledgementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
