import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-materialacknowledgement',
  templateUrl: './materialacknowledgement.component.html',
  styleUrls: ['./materialacknowledgement.component.css']
})
export class MaterialacknowledgementComponent implements OnInit {
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  public workorder: string;
  public basedon: string;
  public mixer_value: any = {};
  public drum_value: any = {};
  menuaccess: string[] = new Array();
  public getmixervalues: any = {};
  public mixer_values: any = {};
  private getmixerparts: any = {};
  private mixerplanning: any = {};
  private drumworkorder: any = {};
  private materialdetails: string = "";
  private bommaterials: string = "";
  storestatus: any;
  public availqty: any = new Array();
  public names: any = {};
  date: string;
  //selectedAll: any;
  public selectedAll: boolean = false;
  headers: any;
  public selectall_value: any;
  public remainingqtys: any = {};
  public mounting_wo: any = {};
  public mountingqty: any = {};


  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }

  getvalue(mixervalue) {
    this.drum_value = {};
    this.menuaccess = [];
    var data: { switchcase: string, id: string } = { switchcase: "getmixervalue", id: mixervalue };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {

            this.mixer_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getmixerpartno();

  };

  getmixerpartno() {
    var mixer_data: { switchcase: string } = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data, mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    this.getmountingwo();
  }

  getworkorderno(id) {
    var operationdatas: { switchcase: string, id: string } = { switchcase: "getwo", id: id };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  drum_getbom(drummodel) {
    this.drum_value = {};
    this.menuaccess = [];
    var data: { switchcase: string, id: string } = { switchcase: "getdrumbom", id: drummodel };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (var i = 0; i < datas.message.length; i++) {
              datas.message[i].selected = false;
            }
            this.drum_value = datas;
            console.log(this.drum_value);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };

  getmountingwo() {
    var data: { switchcase: string } = { switchcase: "getmounting_bom" };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mounting_wo = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  /* selectAll() {
    var headers = this.drum_value.message;
    for(var i=0; i < headers.length; i++){
      var e2 = <HTMLTableElement>document.getElementById(headers[i].id);
      var check=e2.getAttribute("checked");
      if(check==null){
        e2.setAttribute('checked', 'true');
        this.menuaccess.push(headers[i].id);
      }else{
        e2.removeAttribute('checked');
        this.menuaccess=[];
      }
    }
  } */

  selectAll($event) {
    this.selectall_value = "";
    this.menuaccess = [];
    var headers = this.drum_value.message;
    this.selectall_value = $event.srcElement.checked;
    for (var i = 0; i < headers.length; i++) {
      headers[i].selected = $event.srcElement.checked;
      if (headers[i].selected == true) {
        this.menuaccess.push(headers[i].id);
      }
    }
    console.log(this.menuaccess);
  }
  checkIfAllSelected() {
    var headers = this.drum_value.message;
    this.selectedAll = headers.every(function (item: any) {
      return item.selected == true;
    })
  }

  checkValue(option, options) {
    if (this.menuaccess.length == 0) {
      this.menuaccess.push(option);
    } else {
      if (options == false) {
        this.menuaccess.splice(this.menuaccess.indexOf(option), 1);
        console.log("new deleted numbers is : " + this.menuaccess);
      } else {
        var length = this.menuaccess.push(option);
        console.log("new numbers is : " + this.menuaccess);
      }
      console.log(this.menuaccess);
    }
  };

  getassyvalue(form: NgForm) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "get";
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if (datas.message[0] == "undefined" || datas.message[0] == "") {
            } else if (datas.message[0]) {
              var headers = datas.message[0].bom_elements.split(',');
              for (var i = 0; i < headers.length; i++) {
                var e2 = <HTMLTableElement>document.getElementById(headers[i]);
                e2.setAttribute('checked', 'true');
                this.menuaccess.push(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getmixerassyvalue(id) {
    var data = id;
    var switchcase = "switchcase";
    var value = "get";
    var materialissue = "materialissue";
    var mat_value = '2';
    data[materialissue] = mat_value;
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if (datas.message[0] == "undefined" || datas.message[0] == "") {
            } else if (datas.message[0]) {
              var headers = datas.message[0].bom_elements.split(',');
              for (var i = 0; i < headers.length; i++) {
                var e2 = <HTMLTableElement>document.getElementById(headers[i]);
                e2.setAttribute('checked', 'true');
                this.menuaccess.push(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  reset(frm: NgForm) {
    this.drum_value = {};
    this.menuaccess = [];
    frm.resetForm();
  };
  getmixermodel(mixerwono) {
    this.getmixervalues = {};
    var data: { switchcase: string, id: string } = { switchcase: "getmixerassembly", id: mixerwono };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.getmixervalues.part_no = datas.message[0].part_no;
            this.getmixervalues.mixer_modelno = datas.message[0].mixer_model;
            this.getmixervalues.mixer_master_id = datas.message[0].mixer_master_id;
            this.getmixervalues.mixer_production_id = datas.message[0].mixer_production_id;
            this.mixer_values = datas;
            this.mixerbom(this.getmixervalues.mixer_master_id);
            this.getmixerassyvalue(datas.message[0]);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  mixerbom(mixer_master_id) {
    this.drum_value = {};
    this.menuaccess = [];
    var data: { switchcase: string, id: string } = { switchcase: "getmixerbom", id: mixer_master_id };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            console.log(datas);
            for (var i = 0; i < datas.message.length; i++) {
              datas.message[i].selected = false;
            }
            this.drum_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  mounting_getbom() {
    this.drum_value = {};
    this.menuaccess = [];
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mountingarrangement";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.drum_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  drum_getwo(id) {
    var data: { switchcase: string, id: string } = { switchcase: "get", id: id };
    var method = "post";
    var url = "drumwogeneration";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.drumworkorder = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmaterials(workorder_qty, basedon) {

    this.availqty = [];
    this.workorder = workorder_qty;
    this.basedon = basedon;
  }
  getdrumdetails() {
    
    var table = $('.datatable').DataTable();

    var data: { switchcase: string, id: string, basedon: string } = { switchcase: "getmaterialdetails", id: this.workorder, basedon: this.basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
          
            this.materialdetails = datas.message;
            // for (var i = 0; i < datas.message.length; i++) {
             
            // }         
            console.log(this.materialdetails);
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  editfunction(item) {
    this.bommaterials = item;
    if (item.status == 1) {
      this.storestatus = "Material Issued 100%";
    }
    else if (item.status == 2) {
      this.storestatus = "Partially Issued";
    }
    else if (item.status == 3) {
      this.storestatus = "Material Shortage";
    }
  };
  editshiftengineerstatus(frm, edit) {
    var data = frm.value;
    var method = "post";
    var url = "materialacknowledge";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.modalRef.close();
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmountreleaseqty(value, basedon) {
    var data: { switchcase: string, id: string, basedon: string } = { switchcase: "getmounting_bom", id: value, basedon: basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            console.log(datas);
            //this.mountingqty=datas.message[0];
            //    this.drumwoqty=datas.message.total_qty;            
            var i: number;
            this.remainingqtys = datas.message;
            if (datas.message.remaining_qty == 0) {
              this.toastr.error("All Materials are given to this Work Order");
            } else {
              for (i = 0; i < datas.message.remaining_qty; i++) {
                this.availqty[i] = i + 1;
              }
            }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };

}
