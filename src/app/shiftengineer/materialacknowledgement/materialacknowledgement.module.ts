import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialacknowledgementComponent } from './materialacknowledgement.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const materialacknowledgeRoutes: Routes = [
  {
    path: '',
    component: MaterialacknowledgementComponent,
    data: {
      breadcrumb: 'Material Acknowledgement',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(materialacknowledgeRoutes),
    SharedModule   
  ],
  declarations: [MaterialacknowledgementComponent]
})
export class MaterialacknowledgementModule { }
