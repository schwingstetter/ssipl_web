import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CncsegmentComponent } from './cncsegment.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const CncsegmentRoutes: Routes = [
  {
    path: '',
    component: CncsegmentComponent,
    data: {
      breadcrumb: 'CNC Segment Cutting Request',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CncsegmentRoutes),
    SharedModule   
  ],
  declarations: [CncsegmentComponent]
})
export class CncsegmentModule { }
