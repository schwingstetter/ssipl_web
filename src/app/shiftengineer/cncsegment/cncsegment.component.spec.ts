import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CncsegmentComponent } from './cncsegment.component';

describe('CncsegmentComponent', () => {
  let component: CncsegmentComponent;
  let fixture: ComponentFixture<CncsegmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CncsegmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CncsegmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
