import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TypeaheadOptions } from 'ngx-bootstrap';
@Component({
  selector: 'app-cncsegment',
  templateUrl: './cncsegment.component.html',
  styleUrls: ['./cncsegment.component.css']
})
export class CncsegmentComponent implements OnInit {
  private modalRef: NgbModalRef;
  closeOther: boolean = false;
  closeResult: string;
  segmentdetails: string = "";
  partno: string = "";
  createddate: string = "";
  userid: string = "";
  searchpartno: string = "";
  drumdetails: string = "";
  date: string = "";
  idpartno: string = "";
  no_of_plates: string = "";
  total_segment_weight: any = "";
  alternate_unique_value: any = "";
  rm_weight: any = "";
  scrap_percentage: any = "";
  scrap: any = "";
  weight: any = 0;
  action: any = 0;
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string,id:string } = { switchcase: "get",id:'ALL' };
    var method = "post";
    var url = "cncsegment";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.segmentdetails = datas.message;
            for(var i=0;i<datas.message.length;i++)
            {
              datas.message[i].created_at= datas.message[i].created_at.substring(0, 10);
            }
          //  this.idpartno = datas.message[0].id;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getpartno();
  }
  getpartno()
  {
    var data: { switchcase: string } = { switchcase: "getpartno" };
    var method = "post";
    var url = "cncsegment";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.partno = datas.message;       
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  get_partno(id)
  {
    this.searchpartno = id;
  }
  getdetails()
  {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string,id:string } = { switchcase: "get",id:this.searchpartno };
    var method = "post";
    var url = "cncsegment";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.segmentdetails = datas.message;
            for(var i=0;i<datas.message.length;i++)
            {
              datas.message[i].created_at= datas.message[i].created_at.substring(0, 10);
         //   this.idpartno = datas.message[0].id;
            }          
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getdrumdetails(partno) {
    this.action=0;
    this.createddate = partno.created_at;
    this.userid = partno.userid;
    this.alternate_unique_value = partno.alternate_unique_value;
    var data: { switchcase: string, alternate_unique_value: string } = { switchcase: "getdrumdetails", alternate_unique_value: partno.alternate_unique_value };
    var method = "post";
    var url = "cncsegment";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {

            this.drumdetails = datas.message.mixerdata;        
            this.rm_weight = datas.message.rm_weight;
            this.scrap_percentage = datas.message.scrap_percentage;
            this.weight = datas.message.total_Segment_weight;
            this.searchpartno = partno.part_no;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

  }
  noofplates(event) {
    this.no_of_plates = event;
  }
  accept(partid, status) {
    console.log(partid);
    if (status == 0) {
      this.action =1;
      if (this.no_of_plates.length == 0) {
        this.toastr.error("warning","Enter No. Of Plates");
      }
      else {        
        var data = { "alternate_unique_value": this.alternate_unique_value, "status": status, "no_of_plates": this.no_of_plates};
        var method = "post";
        var url = "request_approval";
        this.AjaxService.ajaxpost(data, method, url)
          .subscribe(
            (datas) => {
              if (datas.code == 201) {
                this.modalRef.close();
                this.getdetails();
                this.no_of_plates="";
                this.toastr.success(datas.message, datas.type);            
              } else {
              }
            },
            (err) => {
              if (err.status == 403) {
                alert(err._body);
              }
            }
          );
      }
    }
    else {
      this.action =0;
      var data1 = { "alternate_unique_value": this.alternate_unique_value, "status": status, "no_of_plates": this.no_of_plates};
      var method1 = "post";
      var url1 = "request_approval";
      this.AjaxService.ajaxpost(data1, method1, url1)
        .subscribe(
          (datas1) => {
            if (datas1.code == 201) {
              this.modalRef.close();
              this.reloadPage();
              this.toastr.success(datas1.message, datas1.type);
            } else {
            }
          },
          (err) => {
            if (err.status == 403) 
            {
              alert(err._body);
            }
          }
        );
    }
  }
  reloadPage() {
    // Solution 1:   
    // this.router.navigate('localhost:4200/new');

    // Solution 2:
    this.ngOnInit();
  }
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
