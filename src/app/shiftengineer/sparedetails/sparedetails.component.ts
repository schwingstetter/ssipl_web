import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sparedetails',
  templateUrl: './sparedetails.component.html',
  styleUrls: ['./sparedetails.component.css']
})
export class SparedetailsComponent implements OnInit {
  public columnvalue: any;
  public selectedtype: any;
  public selected_type: any;
  public drummodel: string = "";
  public coneselected: string = "";
  public sparedetails: string = "";
  public mixer_value: any = {};
  private drumworkorder: any = {};
  conecategory: string[] = new Array();
  shellcategory: string[] = new Array();
  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string, type: string } = { switchcase: "get", type: this.selectedtype };
    var method = "post";
    var url = "sparedetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.sparedetails = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

  }
  getvalue(mixervalue) {
    console.log(mixervalue);
    var data: { switchcase: string, id: string } = { switchcase: "getmixervalue", id: '1' };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mixer_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getconecategory(drummodel) {
    this.drummodel=drummodel;
    this.conecategory = Array();
    this.shellcategory = Array();
    var data = { "drummodel": drummodel };
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "getconedetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {

            this.conecategory.push('Select Cone Category');
            if (datas.message.length > 0) {
              for (var i = 1; i <= datas.message[0].noofshell; i++) {
                this.shellcategory.push("Shell" + ' ' + (+(i)));
              }
              for (var i = 1; i <= datas.message[0].noofcone; i++) {
                this.conecategory.push("Cone" + ' ' + (+(i)));
              }
              // if (datas.message[0].noofcone == 1) {
              //   this.conecategory.push('Cone 1');
              // }
              // else if (datas.message[0].noofcone == 2) {
              //   this.conecategory.push('Cone 1');
              //   this.conecategory.push('Cone 2');
              // }
              // else if (datas.message[0].noofcone == 3) {
              //   this.conecategory.push('Cone 1');
              //   this.conecategory.push('Cone 2');
              //   this.conecategory.push('Cone 3');
              // }
              // else if (datas.message[0].noofcone == 4) {
              //   this.conecategory.push('Cone 1');
              //   this.conecategory.push('Cone 2');
              //   this.conecategory.push('Cone 3');
              //   this.conecategory.push('Cone 4');
              // }

            }
            //  this.reloadPage();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  }
  onconeselected(event)
  {
    this.coneselected="";
      this.coneselected=event;
  }
  getwo(event)
  {this.coneselected="";
      this.coneselected=event;
  }
  selectedoption(event) {
    this.selected_type = event;
  }
  spare() {
    this.selectedtype = this.selected_type;
    var table = $('.datatable').DataTable();
    var data: { switchcase: string, type: string,drummodel:string,category:string } = { switchcase: "get", type: this.selectedtype,drummodel:this.drummodel,category:this.coneselected };
    var method = "post";
    var url = "sparedetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            console.log(datas.message);
            this.sparedetails = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  drum_getwo(id) {
    this.drummodel=id;
    var data: { switchcase: string, id: string } = { switchcase: "get", id: id };
    var method = "post";
    var url = "drumwogeneration";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.drumworkorder = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  reloadPage() {
    this.ngOnInit();
  };
  reverttoprocess(id, selectedtype) {
    if (selectedtype == '2') {
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Revert to Process!',
      }).then((isconfirm) => {
        var data: { switchcase: string, id: any, empid: any, status: any } = { switchcase: "drumapprovalorreject", id: id, empid: localStorage.getItem("LoggedInUser"), status: 0 };
        var method = "post";
        var url = "revertdrum";
        if (isconfirm.value == true) {
          this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
            .subscribe(
              response => {
                if (response.code == 201) {

                  swal(
                    'Approved!',
                    'The Request has been Approved.',
                    'success'
                  );

                  this.reloadPage();
                }
                else {

                }
              },
              error => console.log('error : ' + error)
            );
        } else {

          // this.reloadPage();
        }
      });
    }
    else {

      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Revert to Process!',
      }).then((isconfirm) => {
        var data: { switchcase: string, id: any, empid: any, status: any } = { switchcase: "approvalorreject", id: id, empid: localStorage.getItem("LoggedInUser"), status: 0 };
        var method = "post";
        var url = "revertspare";
        if (isconfirm.value == true) {
          this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
            .subscribe(
              response => {
                if (response.code == 201) {

                  swal(
                    'Approved!',
                    'The Request has been Approved.',
                    'success'
                  );

                  this.reloadPage();
                }
                else {

                }
              },
              error => console.log('error : ' + error)
            );
        } else {

          // this.reloadPage();
        }
      });
    }



  }
}
