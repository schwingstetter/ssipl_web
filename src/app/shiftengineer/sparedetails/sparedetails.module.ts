import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SparedetailsComponent } from './sparedetails.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const sparedetailsRoutes: Routes = [
  {
    path: '',
    component: SparedetailsComponent,
    data: {
      breadcrumb: 'Spare Details',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(sparedetailsRoutes),
    SharedModule   
  ],
  declarations: [SparedetailsComponent]
})
export class SparedetailsModule { }
