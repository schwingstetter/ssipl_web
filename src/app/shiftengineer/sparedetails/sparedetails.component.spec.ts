import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparedetailsComponent } from './sparedetails.component';

describe('SparedetailsComponent', () => {
  let component: SparedetailsComponent;
  let fixture: ComponentFixture<SparedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
