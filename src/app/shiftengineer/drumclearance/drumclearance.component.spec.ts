import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumclearanceComponent } from './drumclearance.component';

describe('DrumclearanceComponent', () => {
  let component: DrumclearanceComponent;
  let fixture: ComponentFixture<DrumclearanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumclearanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumclearanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
