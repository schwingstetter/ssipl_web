import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumclearanceComponent } from './drumclearance.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const drumclearanceRoutes: Routes = [
  {
    path: '',
    component: DrumclearanceComponent,
    data: {
      breadcrumb: 'Drum Clearance',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(drumclearanceRoutes),
    SharedModule   
  ],
  declarations: [DrumclearanceComponent]
})
export class DrumclearanceModule { }
