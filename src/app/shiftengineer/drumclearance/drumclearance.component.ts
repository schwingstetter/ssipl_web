import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-drumclearance',
  templateUrl: './drumclearance.component.html',
  styleUrls: ['./drumclearance.component.css']
})
export class DrumclearanceComponent implements OnInit {

  options1 :any[];
  optionscomp :any[];
  optionsMap:any[];
  dateval: string = null;

options :any[];
optionmodel :any[];
compoptions:string="";
optionSelected: any = "Select";
optionSelected1: any = "Select";
optionmodelSelected: any = "Select";
editprocess : string ="";
details : string ="";
partno : string ="";
serialno : string ="";
serial_no : string ="";
private modalRef: NgbModalRef;
closeOther: boolean = false;
closeResult: string;

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef , private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }

  ngOnInit() {
   
    var data: { switchcase: string } = { switchcase: "getdata" };
    var method = "post";
    var url = "get_drum_clearance";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.partno = datas.message;
           
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  getserialno(event)
  {
    var data={"id":event};
    var method = "post";
    var url = "get_drum_serial_no";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.serialno = datas.message;
     
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  onserialchanged(event)
  {
this.serial_no=event;
  }
  getdetails()
  {
    var table = $('.datatable').DataTable();
    console.log(event);
    var data={"serialno":this.serial_no};
    var method = "post";
    var url = "drum_clearance";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.details = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      
  }

  editfunction(item) {
   // alert("");
    this.editprocess = item;
    console.log(item);
  };
  proceed(id,status)
  {
      var data = {"id":id,"status":status};
  
      var method = "post";
      var url = "revertdrumspare";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.modalRef.close();
              this.toastr.success(datas.message, datas.type);    
              this.reloadPage();        
            }
            else {
              this.toastr.error(datas.message, datas.type);
            }
          },
          (err) => {
            if (err.status == 403) {
              this.toastr.error(err._body);
              //alert(err._body);
            }
          }
        );
        //this.toastr.success('Hello world!', 'Toastr fun!');
    };
    reloadPage() {
     
       this.ngOnInit();
    }
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
