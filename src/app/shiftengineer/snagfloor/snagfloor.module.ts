import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnagfloorComponent } from './snagfloor.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const SnagfloorRoutes: Routes = [
  {
    path: '',
    component: SnagfloorComponent,
    data: {
      breadcrumb: 'Snag Floor Shop',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SnagfloorRoutes),
    SharedModule   
  ],
  declarations: [SnagfloorComponent]
})
export class SnagfloorModule { }
