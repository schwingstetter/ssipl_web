import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-snagfloor',
  templateUrl: './snagfloor.component.html',
  styleUrls: ['./snagfloor.component.css']
})
export class SnagfloorComponent implements OnInit {
  public snagdetails: string = "";
  public stage: string = "PDI";
  public type: string = "";
  public wono: string = "";
  public stationcode: string = "";
  public categry: string = "";
  public subcategry: string = "";
  public operationcodes: string = "";
  public optionselected: string = "";
  public workstationnames: string = "";
  public respectiveperson: string = "";
  public testedby: string = "";
  public clearedby: string = "";
  public enginesno: string = "";
  public serialno: string = "";
  public slno: any = [];
  public pdislno: any = [];
  public category: any = [];
  public subcategory: any = [];
  public workorderno: any = [];
  public editonepointlesson: string = "";
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  public delay: boolean;
  public waranty: boolean;
  public productivity: boolean;
  public order: boolean;
  public material: boolean;
  public reputation: boolean;
  file_name: FileList;
  public employees: string = "";
  private modalRef: NgbModalRef;
  closeResult: string;
  public processimage: string = "";
  public timerequiredinminutes: any = "";

  constructor(public http: Http, public AjaxService: AjaxService, private router: Router, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "onepointlesson";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {

            for (var i = 0; i < datas.message.length; i++) {
              datas.message[i].created_at = datas.message[i].created_at.substring(0, 10);
              //   this.idpartno = datas.message[0].id;
            }
            this.snagdetails = datas.message;
            console.log(this.snagdetails);
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getoperationcode();
  }
  getoperationcode() {
    var data = { "basedon": 2 };
    var method = "post";
    var url = "operationcode";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.operationcodes = datas.message;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    // this.getsnagcategory(event);
  }
  timechanged(newTime) {

    var minutes = (+newTime.hour) * 60 + (+newTime.minute);
    this.timerequiredinminutes = minutes;

  }
  oncategoryselected(event) {
    this.categry = event;
    this.getsubcategory();
  }
  onsubcategoryselected(event) {
    this.subcategry = event;
  }
  getdetails(event) {
    var table = $('.datatable').DataTable();
    if (this.type == '1' || this.type == '3') {
      var data = { switchcase: "get", type: this.type, enginesno: this.serialno, category: this.categry, subcategory: this.subcategry };
      var method = "post";
      var url = "onepointlesson";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              for (var i = 0; i < datas.message.length; i++) {
                datas.message[i].created_at = datas.message[i].created_at.substring(0, 10);
                if(datas.message[i].closed_date != null)
                {
                datas.message[i].closed_date = datas.message[i].closed_date.substring(0, 10);
                }
                if(datas.message[i].target_date != null)
                {
                datas.message[i].target_date = datas.message[i].target_date.substring(0, 10);
                }
                //   this.idpartno = datas.message[0].id;
              }
              this.snagdetails = datas.message;
              table.destroy();
              setTimeout(() => {
                $('.datatable').DataTable();
              }, 1000);
            } else {
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
    else if (this.type == '2') {
      var data1 = { switchcase: "get", type: this.type, workorderno: this.wono, stationame: this.stationcode };
      var method1 = "post";
      var url1 = "onepointlesson";
      this.AjaxService.ajaxpost(data1, method1, url1)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              for (var i = 0; i < datas.message.length; i++) {
                datas.message[i].created_at = datas.message[i].created_at.substring(0, 10);
                //   this.idpartno = datas.message[0].id;
              }
              this.snagdetails = datas.message;
              table.destroy();
              setTimeout(() => {
                $('.datatable').DataTable();
              }, 1000);
            } else {
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
  }
  gettype(event) {
    this.type = event;
    if (this.type == '1') {
      this.getserialno();
    }
    else if (this.type == '2') {
      this.getwo();
      this.getoperationcode();
    }
    else if (this.type == '3') {
      this.getserialno();
    }
    this.reloadPage();
  }
  storesno(event) {
    this.serialno = event;
  }
  storewo(event) {
    this.wono = event;
  }
  storestation(event) {
    this.stationcode = event;
  }

  getserialno() {
    var data = { switchcase: "getserialno", type: this.type };
    var method = "post";
    var url = "onepointlesson";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.slno = datas.message;
            console.log(this.slno);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getcategory();
  }
  getcategory() {
    var data = { "opcode": '3' };
    var method = "post";
    var url = "snagcategory";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.category = datas.message;
            console.log(this.category);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

  }
  getsubcategory() {
    var data = { "opcode": '3', "category": this.categry };
    var method = "post";
    var url = "snagsubcategory";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.subcategory = datas.message;
            console.log(this.category);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getwo() {
    var data = { "opcode": '3', "category": this.categry };
    var method = "post";
    var url = "mixerwo";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.workorderno = datas.message;
            console.log(this.workorderno);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getoperationcode();
  }
  reloadPage() {
    this.ngOnInit();
  }
  showimage(image) {
    this.processimage = image;
  };
  getworkstation(event) {
    var data = { "opcode": event };
    var method = "post";
    var url = "workstation";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.workstationnames = datas.message[0].list_of_operation;
           
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
this.getpersondetails(event,this.enginesno);
  }
  editfunction(item) {
    this.editonepointlesson = item;
    this.enginesno = item.engine_s_no;
    //this.workstationnames=item.station_name;
    this.respectiveperson = item.respective_person;
    this.testedby = item.tested_by;
    this.clearedby = item.cleared_by;
    if (item.delay_in_delivery == 1) {
      this.delay = true;
    }
    else {
      this.delay = false;
    }
    if (item.waranty_replacement == 1) {
      this.waranty = true;
    }
    else {
      this.waranty = false;
    }
    if (item.reputaion_loss == 1) {
      this.reputation = true;
    }
    else {
      this.reputation = false;
    }
    if (item.order_cancellation == 1) {
      this.order = true;
    }
    else {
      this.order = false;
    }
    if (item.material_loss == 1) {
      this.material = true;
    }
    else {
      this.material = false;
    }
    if (item.productivity_loss == 1) {
      this.productivity = true;
    }
    else {
      this.productivity = false;
    }
    this.getemployeename();
  };
  getemployeename() {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "empid";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {

          if (datas.code == 201) {

            this.employees = datas.message;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getpersondetails(operationcode, engine_no) {
    var data = { "opcode": operationcode, "engineno": engine_no };
    var method = "post";
    var url = "persondetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if (datas.message) {
              this.respectiveperson = datas.message[0].respective_person;
              this.testedby = datas.message[0].tested_by;
              this.clearedby = datas.message[0].cleared_by;
            }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  // generatereport(item)
  // {
  //   this.router.navigate(["/shiftengineer/onepointlessonreport"]);
  // }
  generatereport(item) {
    var text = 'sdfsdf';
    localStorage.removeItem('enginesno');
    localStorage.setItem("enginesno", item);
    this.router.navigateByUrl('/shiftengineer/onepointlessonreport');
  }
  editonepoint(editfrm, edit) {
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "editsnagfloor";
    var time = "time";
    data[switchcase] = value;
    data[time]=this.timerequiredinminutes;
    var type = "type";
    data[type] = this.type;
    var method = "post";
    var url = "onepointlesson";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.ngOnInit();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  downloadFile() {
    return this.http
      .get('assets/uploads/drummaster.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'Drumtype.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  };
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;

    let fileList: FileList = event.target.files;
    this.file_name = fileList;
  }
  filesubmit(bulkupload) {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({ headers: headers });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/

      var data = formData;
      var method = "post";
      var url = "drum_uploadfiles";
      var i: number;
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for (i = 0; i < data.length; i++) {
              if (data[i].code == 201) {
                this.toastr.success(data[i].message, 'Success!');
                this.modalRef.close();
              } else {
                this.toastr.error(data[i].message, 'Error!');
                this.modalRef.close();
              }
            }
            this.reloadPage();

          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };
  deletedrumtype(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "drummaster";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                this.ngOnInit();
              }
              else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.reloadPage();
      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };

}