import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnagfloorComponent } from './snagfloor.component';

describe('SnagfloorComponent', () => {
  let component: SnagfloorComponent;
  let fixture: ComponentFixture<SnagfloorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnagfloorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnagfloorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
