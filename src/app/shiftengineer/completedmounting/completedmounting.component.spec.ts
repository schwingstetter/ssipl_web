import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedmountingComponent } from './completedmounting.component';

describe('CompletedmountingComponent', () => {
  let component: CompletedmountingComponent;
  let fixture: ComponentFixture<CompletedmountingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedmountingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedmountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
