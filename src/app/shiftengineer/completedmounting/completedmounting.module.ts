import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompletedmountingComponent } from './completedmounting.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const mountingcompletedRoutes: Routes = [
  {
    path: '',
    component: CompletedmountingComponent,
    data: {
      breadcrumb: 'Completed Mounting',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mountingcompletedRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [CompletedmountingComponent]
})
export class CompletedmountingModule { }
