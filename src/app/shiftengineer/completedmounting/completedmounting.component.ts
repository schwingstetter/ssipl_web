import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-completedmounting',
  providers: [AjaxService, AuthService],
  templateUrl: './completedmounting.component.html',
  styleUrls: ['./completedmounting.component.css']
})
export class CompletedmountingComponent implements OnInit {

  public completedmounting:any={};
  public dropdownSettings = {};
  private mailid: any = ""; 
  public dropdownList = [];
  private modalRef: NgbModalRef;
  menuaccess:string[] = new Array();
  public selectedItems = [];

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "getmountingdetails" };
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.completedmounting = datas;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.dropdownSettings = {
      text: "Select Users",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "multidropdowncustom",
      primaryKey: "email",
      labelKey: "email",
      noDataLabel: "Select an User",
      enableSearchFilter: true,
      showCheckbox: true,
      disabled: false
    };
    this.getusers();
    this.selectedItems=[];
    this.menuaccess=[];
  };
  openmodal(addNew)
  {
    if(this.menuaccess.length > 0){
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    }else{
      this.toastr.error("No values are selected", "error");
    }
  }  
  getusers()
  {
    var data={"roleid":''};
    var method = "post";
    var url = "getmailidall";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
            this.mailid = datas;
            this.dropdownList=datas;
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  checkValue(option,options){
    if(this.menuaccess.length == 0){
      this.menuaccess.push(option);
    }else{
      if(options == false){
        this.menuaccess.splice(this.menuaccess.indexOf(option),1);
        console.log("new deleted numbers is : " + this.menuaccess );  
      }else{
        var length = this.menuaccess.push(option);
        console.log("new numbers is : " + this.menuaccess );
      }
      console.log(this.menuaccess);
    }
  };
  onItemSelect(item:any){
    //console.log(item);
    //console.log(this.selectedItems);
  }
  OnItemDeSelect(item:any){
    //  console.log(item);
    //  console.log(this.selectedItems);
  }
  onSelectAll(items: any){
      // console.log(items);
  }
  onDeSelectAll(items: any){
    //  console.log(items);
  }
  addbillingstatus(frm:NgForm){
    if(this.selectedItems.length > 0 || this.menuaccess.length > 0){
      var data: { switchcase: string,id:any,email:any } = { switchcase: "sendingmail",id:this.menuaccess,email:this.selectedItems };
      var method = "post";
      var url = "getdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.toastr.success(datas.message, datas.type);
            } else {
              this.toastr.error(datas.message, datas.type);
            }
            this.selectedItems=[];
            this.menuaccess=[];
            this.modalRef.close();
            this.ngOnInit();
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }else{
      this.toastr.error("Mail Id is not yet selected", "error");
    }
  }

}
