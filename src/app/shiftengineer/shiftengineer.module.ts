import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ShiftengineerComponent } from './shiftengineer.component';
import { shidtengineerRoutes } from './shiftengineer.routing';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(shidtengineerRoutes)
  ],
  declarations: [ShiftengineerComponent]
})
export class ShiftengineerModule { }
