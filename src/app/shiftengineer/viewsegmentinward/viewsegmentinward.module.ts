import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewsegmentinwardComponent } from './viewsegmentinward.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const productionplanRoutes: Routes = [
  {
    path: '',
    component: ViewsegmentinwardComponent,
    data: {
      breadcrumb: 'Shell Segment inward',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(productionplanRoutes),
    SharedModule
  ],
  declarations: [ViewsegmentinwardComponent]
})
export class ViewsegmentinwardModule { }
