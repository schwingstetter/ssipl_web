import { Routes } from '@angular/router';


export const shidtengineerRoutes: Routes = [
    {
        path: '',
        data: {
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'viewproductionplan',
                loadChildren: './viewproductionplan/viewproductionplan.module#ViewproductionplanModule'
            },
            {
                path: 'assignresources',
                loadChildren: './assignresources/assignresources.module#AssignresourcesModule'
            },
            {
                path: 'shiftjobsheet',
                loadChildren: './shiftjobsheet/shiftjobsheet.module#ShiftjobsheetModule'
            
            },
            {
                path: 'completedmounting',
                loadChildren: './completedmounting/completedmounting.module#CompletedmountingModule'
            },
            {
                path: 'cncsegment',
                loadChildren: './cncsegment/cncsegment.module#CncsegmentModule'
            }
            ,
            {
                path: 'delaysnagrework',
                loadChildren: './delaysnagrework/delaysnagrework.module#DelaysnagreworkModule'
            }
            ,
            {
                path: 'sparedetails',
                loadChildren: './sparedetails/sparedetails.module#SparedetailsModule'
            }
            ,
            {
                path: 'paintingprocess',
                loadChildren: './paintingprocess/paintingprocess.module#PaintingprocessModule'
            }
            ,
            {
                path: 'mixerfinish',
                loadChildren: './mixerfinish/mixerfinish.module#MixerfinishModule'
            }
            ,
            {
                path: 'mixerworkorder',
                loadChildren: './mixerworkorder/mixerworkorder.module#MixerworkorderModule'
            },
            {
                path: 'viewsegmentinward',
                loadChildren: './viewsegmentinward/viewsegmentinward.module#ViewsegmentinwardModule'
            },
            {
                path: 'viewrawmaterialissue',
                loadChildren: './viewrawmaterialissue/viewrawmaterialissue.module#ViewrawmaterialissueModule'
            }
            ,
            {
                path: 'materialacknowledge',
                loadChildren: './materialacknowledgement/materialacknowledgement.module#MaterialacknowledgementModule'
            }
            ,
            {
                path: 'onepointlesson',
                loadChildren: './onepointlesson/onepointlesson.module#OnepointlessonModule'
            }
            ,
            {
                path: 'drumclearance',
                loadChildren: './drumclearance/drumclearance.module#DrumclearanceModule'
            }
            ,
            {
                path: 'onepointlessonreport',
                loadChildren: './onepointlessonreport/onepointlessonreport.module#OnepointlessonreportModule'
            }
            ,
            {
                path: 'linerejection',
                loadChildren: './linerejection/linerejection.module#LinerejectionModule'
            }
            ,
            {
                path: 'snagfloor',
                loadChildren: './snagfloor/snagfloor.module#SnagfloorModule'
            }
        ]
    }
];
