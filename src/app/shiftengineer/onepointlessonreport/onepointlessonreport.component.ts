import { Component, OnInit, ViewContainerRef, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
//import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { visitValue } from '../../../../node_modules/@angular/compiler/src/util';
import { NavigationEnd, ActivatedRoute } from '@angular/router';
declare const $: any;
declare var Morris: any;
declare var Highcharts: any;
@Component({
  selector: 'app-onepointlessonreport',
  templateUrl: './onepointlessonreport.component.html',
  styleUrls: ['./onepointlessonreport.component.css']
})
export class OnepointlessonreportComponent implements OnInit {
  public processname: any;
  public stationname: any;
  public process: any;
  public inputVar: any;
  public reputation: any = "";
  public productivity: any = "";
  public material: any = "";
  public waranty: any = "";
  public order: any = "";
  public delayindelivery: any = "";
  public description: any = "";
  public clearedby: any = "";
  public testedby: any = "";
  public snagcategory: any = "";
  public reportedstage: any = "";
  public engine_gear_no: any = "";
  public snagno: any = "";
  public partno: any = "";
  public processimage: string = "";
  words2 = [{ value: '' }];

  public mixerplanning:any={};
  constructor(public http: Http,private _route: ActivatedRoute, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    
  }
  ngOnInit() {
    var engineno={"enginesno":localStorage.getItem("enginesno"),"reporttype":localStorage.getItem("reporttype")};
    var meth = "post";
    var ur = "genaratereport";
    this.AjaxService.ajaxpost(engineno, meth, ur)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {        
          this.processimage =datas.message[0].defect_image;
          this.partno=datas.message[0].part_no;
          this.snagno=datas.message[0].id;
          this.engine_gear_no=datas.message[0].engine_s_no;
          this.reportedstage=localStorage.getItem("reporttype");
          this.stationname=datas.message[0].station_name;
          this.snagcategory=datas.message[0].snag_category;
          this.testedby=datas.message[0].tested_by;
          this.clearedby=datas.message[0].cleared_by;
          this.description=datas.message[0].description;      
          if(datas.message[0].delay_in_delivery == 1)
          {
            this.delayindelivery=true;
          }
          else
          {
            this.delayindelivery=false;
          }
          if(datas.message[0].waranty_replacement == 1)
          {
            this.waranty=true;
          }
          else
          {
            this.waranty=false;
          }
          if(datas.message[0].reputation_loss == 1)
          {
            this.reputation=true;
          }
          else
          {
            this.reputation=false;
          }
          if(datas.message[0].order_cancellation == 1)
          {
            this.order=true;
          }
          else
          {
            this.order=false;
          }
          if(datas.message[0].material_loss == 1)
          {
            this.material=true;
          }
          else
          {
            this.material=false;
          }
          if(datas.message[0].productivity_loss == 1)
          {
            this.productivity=true;
          }
          else
          {
            this.productivity=false;
          }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      this.getsnagcount('1');
  }
  getsnagcount(frm) {
    var data: { empid: string } = { empid:localStorage.getItem("respectiveperson") };
    var method = "post";
    var url = "employeesnag";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          // this.wip_partno=datas.data;

          // Build the chart
          Highcharts.chart('performancescale', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: 'Employee<br>Performance<br>',
                align: 'center',
                verticalAlign: 'middle',
                y: 40
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white'
                        }
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%'],
                    size: '110%'
                }
            },
            series: [{
                type: 'pie',
                name: 'Snag Count',
                innerSize: '50%',
                data: datas.data
            }]
        });
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
}