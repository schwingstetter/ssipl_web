import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnepointlessonreportComponent } from './onepointlessonreport.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const onepointlessonreportRoutes: Routes = [
  {
    path: '',
    component: OnepointlessonreportComponent,
    data: {
      breadcrumb: 'One Point Lesson Report' ,
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(onepointlessonreportRoutes),
    SharedModule 
  ],
  declarations: [OnepointlessonreportComponent]
})
export class OnepointlessonreportModule { }
