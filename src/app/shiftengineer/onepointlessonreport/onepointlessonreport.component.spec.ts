import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnepointlessonreportComponent } from './onepointlessonreport.component';

describe('OnepointlessonreportComponent', () => {
  let component: OnepointlessonreportComponent;
  let fixture: ComponentFixture<OnepointlessonreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnepointlessonreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnepointlessonreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
