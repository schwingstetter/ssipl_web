import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaintingprocessComponent } from './paintingprocess.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const paintngprocessRoutes: Routes = [
  {
    path: '',
    component: PaintingprocessComponent,
    data: {
      breadcrumb: 'Painting Process',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(paintngprocessRoutes),
    SharedModule   
  ],
  declarations: [PaintingprocessComponent]
})
export class PaintingprocessModule { }
