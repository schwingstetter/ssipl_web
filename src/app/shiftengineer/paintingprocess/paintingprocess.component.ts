import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
@Component({
  selector: 'app-paintingprocess',
  templateUrl: './paintingprocess.component.html',
  styleUrls: ['./paintingprocess.component.css']
})
export class PaintingprocessComponent implements OnInit {
  public contractorname: string = "";
  public contractor_name: string = "";
  private modalRef: NgbModalRef;
  closeResult: string;
  public workstationnames: string = "";
  public sno: string = "";
  public editpaintingprocess: string = "";
  public assemblystage: string = "";
  public partno: string = "";
  private getmixerparts: any;
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {

    this.toastr.setRootViewContainerRef(vcr);

  }

  ngOnInit() {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "get_contrators_name";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {

          if (datas.code == 201) {
            this.contractorname = datas.message;

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  this.getmixerpartno();
  
  }
  getmixerpartno() {
    var mixer_data: { switchcase: string } = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data, mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart.message;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      }
  getstage(event)
  {
    this.assemblystage=event;
  }
  getpartno(event)
  {
    this.partno=event;
  }
  getenginesno() {
    
    // this.sno="";
    if(this.assemblystage == undefined)
    {
      this.toastr.error("Select Assembly Stage","Warning");
    }
    else
    {
      
      var table = $('.datatable').DataTable();
      
    if (this.assemblystage == '24') {
      var data = { 'status': '0','partno':this.partno };
      var method = "post";
      var url = "serialno";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.sno = datas.message;
              setTimeout(() => {
                $('.datatable').DataTable();
              }, 1000);
             
            } 
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
   if(this.assemblystage == '25')
    {
      var table = $('.datatable').DataTable();
      var data = { 'status': '1','partno':this.partno };
      var method = "post";
      var url = "serialno";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.sno = datas.message;  
              if(this.sno.length>0)
              {
              this.contractor_name=datas.message[0].contractor_name;
              }
             
              
              setTimeout(() => {
                $('.datatable').DataTable();
              }, 1000);
               } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
     if(this.assemblystage == '26')
    {
      var table = $('.datatable').DataTable();
      var data = { 'status': '2','partno':this.partno };
      var method = "post";
      var url = "serialno";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.sno = datas.message;
              if(this.sno.length>0)
              {
                this.contractor_name=datas.message[0].contractor_name;
              }
              
            
              setTimeout(() => {
                $('.datatable').DataTable();
              }, 1000);
               } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
    table.destroy();
  }
  }
  editfunction(item)
  {
    console.log(item);
    this.editpaintingprocess = item;
  }
  updatepaintingprocess(form)
  {
    var data = form.value;
    var assemblystage = "assemblystage";
    data[assemblystage]=this.assemblystage;
    var method = "post";
    var url = "update_painting_process";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {         
            this.toastr.success(datas.message, datas.type);    
            this.getenginesno();   
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );
  }
  reloadPage() {
    this.ngOnInit();
  }
  
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
