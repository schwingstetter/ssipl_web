import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaintingprocessComponent } from './paintingprocess.component';

describe('PaintingprocessComponent', () => {
  let component: PaintingprocessComponent;
  let fixture: ComponentFixture<PaintingprocessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaintingprocessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaintingprocessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
