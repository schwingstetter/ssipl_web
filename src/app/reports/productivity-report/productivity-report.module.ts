import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProductivityReportComponent } from './productivity-report.component';
import { SharedModule } from '../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
export const productivityreportRoutes: Routes = [
  {
    path: '',
    component: ProductivityReportComponent,
    data: {
      breadcrumb: 'Productivity Report',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(productivityreportRoutes),
    SharedModule,
    AngularMultiSelectModule 
  ],
  declarations: [ProductivityReportComponent]
})
export class ProductivityReportModule { }
