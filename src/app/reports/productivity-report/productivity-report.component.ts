import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-productivity-report',
  templateUrl: './productivity-report.component.html',
  styleUrls: ['./productivity-report.component.css']
})
export class ProductivityReportComponent implements OnInit {
@ViewChild('tableex') tableex: ElementRef;
 
  dtOptions: any = {};
  public processimage: string = "";
  public choosprocesstype: string = "";
  public productiondurations: string = "";
  //dtOptions: any = {};
  private modalRef: NgbModalRef;
  public modeldata:any={};
  public workstation:any={};
  public technician:any={};
  public details:any={};
  closeResult: string;
  public selectedtechnician:string='';
  public selectedworkstation:string='';
  public dropdownSettings_technician = {};   
  public dropdownList_technician :{ emp_no: string, emp_no_name_role: string }[] = []; 
  public dropdownSettings_workstation = {};   
  public dropdownList_workstation :{ id: string, code_list: string }[] = [];
  public mindate:any={year: 2021, month: 1, day: 1};
  public maxdate:any={year: 2019, month: 12, day: 31};
  public hist_mindate:any={year: 2021, month: 1, day: 1};
  public hist_maxdate:any={year: 2019, month: 12, day: 31};
  choosewsmodel=[];
  model:any;
  models:any;
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }
  ngOnInit() {
   this.dropdownSettings_technician = {
        text: "Select a technician",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "emp_no",
        labelKey: "emp_no_name_role",
        noDataLabel: "No technician is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
       this.dropdownSettings_workstation = {
        text: "Select a Workstation",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "code_list",
        noDataLabel: "No work station is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    $('#tableex').DataTable();
    this.dtOptions = {
      // Declare the use of the extension in the dom parameter
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ]
    };
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

   // var lastday=this.lastday(yyyy,mm);
    //var lastdays=new Date(yyyy, mm +1, 0).getDate();
   this.mindate = { year: 2007, month: 1, day: 1 };
   this.maxdate = { year: yyyy, month: 12, day: 31 };
   this.hist_mindate = { year: 2007, month: 1, day: 1 };
   this.hist_maxdate = { year: yyyy, month: 12, day: 31 };
   this.dropdownList_technician=[];
   var data: { switchcase: string} = { switchcase: "gettechnician"};
   var method = "post";
   var url = "getdetails";
   this.AjaxService.ajaxpost(data, method, url)
     .subscribe(
       (datass) => {          
         if (datass.code == 201) {
           console.log(datass);
           if(datass.message.length>0)
           {
              this.dropdownList_technician.push({emp_no: "all", emp_no_name_role: "All"});
              for(let i=0;i<datass.message.length;i++)
             {
          this.dropdownList_technician.push({emp_no: datass.message[i]['emp_no'], emp_no_name_role: datass.message[i]['emp_no_name_role']});
             }
           }
          console.log(this.dropdownList_technician)
          // this.technician=datass;
         }
         else {
           this.toastr.error(datass.message, datass.type);
         }
       },
       (err) => {
         if (err.status == 403) {
           this.toastr.error(err._body);
           //alert(err._body);
         }
       }
     );
  };
  getselecttechnician(item:any)
  {
this.selectedtechnician=item.emp_no;
 //this.choosewsmodel=[];
 console.log(this.choosewsmodel)
  }
  getworkstationselected(item:any)
  {
   
   // this.choosewsmodel
 //  this.choosewsmodel = item;
    this.selectedworkstation=item.id;
    console.log(this.choosewsmodel)
    
  }
  export()
  {
    var table25=$('#tableex').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.tableex.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Productivity report.xlsx');
   $('#tableex').DataTable();
  }
  getworkstation(value){
    this.choosewsmodel=[];
    this.dropdownList_workstation=[];
    var data: { switchcase: string,id:string,category:string} = { switchcase: "getworkstation",id:this.choosprocesstype,category:value};
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
              if(datass.message.length>0)
           {
              this.dropdownList_workstation.push({id: "all", code_list: "All"});
              for(let i=0;i<datass.message.length;i++)
             {
               let tempname=datass.message[i]['operation_code']+'_'+datass.message[i]['list_of_operation'];
          this.dropdownList_workstation.push({id: datass.message[i]['id'], code_list:tempname });
             }
           }
        //    public dropdownList_workstation :{ id: string, code_list: string }[] = [];
            //this.workstation=datass;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
     // this.gettechnician();
  }
  getdetails(frm,add){
    var table=$('#tableex').DataTable();
    var data=frm.value;
    var value="switchcase";
    var workstation="workstation";
    var technician="technician";
    data[value]="getdetails";
    data[workstation]=this.selectedworkstation;
    data[technician]=this.selectedtechnician;
    var method = "post";
    var url = "productivityreport";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            console.log(data);
            this.details=datass.message;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
          table.destroy();
          setTimeout(() => {
            $('#tableex').DataTable();
          }, 1000);
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  getprocesstype(event)
  {
    this.choosewsmodel=[];
    this.dropdownList_workstation=[];
    this.choosprocesstype=event;
  }
  onChange(event)
  {
  //  alert(event);
  this.hist_mindate = { year: event.year, month: 1, day: 1 };
  this.hist_maxdate = { year: event.year, month: 12, day: 31 };
  }
  getduration(event)
  {
    this.productiondurations=event;
    this.model=[];
    this.models=[];
  }
}
