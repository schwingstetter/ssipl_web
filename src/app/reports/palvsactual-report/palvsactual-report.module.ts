import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PalvsactualReportComponent } from './palvsactual-report.component';
import { SharedModule } from '../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';

export const PalvsactualReportRoutes: Routes = [
  {
    path: '',
    component: PalvsactualReportComponent,
    data: {
      breadcrumb: 'Plan Vs Actual Report',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(PalvsactualReportRoutes),
    SharedModule 
  ],
  declarations: [PalvsactualReportComponent]
})
export class PalvsactualReportModule { }
