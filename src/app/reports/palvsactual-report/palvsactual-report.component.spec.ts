import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PalvsactualReportComponent } from './palvsactual-report.component';

describe('PalvsactualReportComponent', () => {
  let component: PalvsactualReportComponent;
  let fixture: ComponentFixture<PalvsactualReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PalvsactualReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PalvsactualReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
