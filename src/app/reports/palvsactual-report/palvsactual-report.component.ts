import { Component, OnInit, ViewContainerRef,ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-palvsactual-report',
  templateUrl: './palvsactual-report.component.html',
  styleUrls: ['./palvsactual-report.component.css']
})
export class PalvsactualReportComponent implements OnInit {
@ViewChild('exampletable') exampletable: ElementRef;
@ViewChild('exampletable1') exampletable1: ElementRef;
@ViewChild('exampletable2') exampletable2: ElementRef;

  dtOptions: any = {};
  public details:any=[];
  public actual:any=[];
  public planned:any=[];

  public choosep:string="0";
  public productiondurations:string="0";
  public propductmodel:any={};
  public workstation:any={};
public mindate:any={year: 2021, month: 1, day: 1};
public maxdate:any={year: 2019, month: 12, day: 31};
public hist_mindate:any={year: 2021, month: 1, day: 1};
public hist_maxdate:any={year: 2019, month: 12, day: 31};
constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
  this.toastr.setRootViewContainerRef(vcr);
}
 export()
{
   var table25=$('#exampletable').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.exampletable.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Plan Vs Actual report.xlsx');
   $('#exampletable').DataTable();
}
export1()
{
   var table25=$('#exampletable1').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.exampletable1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Plan Vs Actual report.xlsx');
   $('#exampletable1').DataTable();
}
export2()
{
   var table25=$('#exampletable2').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.exampletable2.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Plan Vs Actual report.xlsx');
   $('#exampletable2').DataTable();
}
  ngOnInit() {
    $('#exampletable').DataTable();
    $('#exampletable1').DataTable();
    this.dtOptions = {
      // Declare the use of the extension in the dom parameter
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ]
    };
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

   // var lastday=this.lastday(yyyy,mm);
    //var lastdays=new Date(yyyy, mm +1, 0).getDate();
   this.mindate = { year: 2007, month: 1, day: 1 };
   this.maxdate = { year: yyyy, month: 12, day: 31 };
   this.hist_mindate = { year: 2007, month: 1, day: 1 };
   this.hist_maxdate = { year: yyyy, month: 12, day: 31 };
  }; 
  isDisabled(date: NgbDateStruct) {
    const d = new Date(date.year, date.month - 1, date.day);
    return d.getDay() === 0 ;
  }
getdetails(frm,add)
{
 var table= $('#exampletable').DataTable();
  var table1=$('#exampletable1').DataTable();
  var data=frm.value;
  var method = "post";
  var url = "getplanactualdetails";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datass) => {          
        if (datass.code == 201) {   
          if(this.choosep == '1')
          {       
          console.log(datass.message.length);
          this.details=[];
          this.actual=[];
          this.planned=[];
          for(var i=0;i<datass.message.length;i++)
          {
            //alert(2);
           
              //alert(3);
              this.details[i]=datass.message[i].details;
              this.actual[i]=datass.message[i].actual;
              this.planned[i]=datass.message[i].planned;
        
            
          }
        }
        else if(this.choosep == '2')
         {
          this.details=datass.message;
        }
          console.log(this.details);
        //  this.details=datass.message;
        }
        else {
         // this.details=datass.message;
         // this.toastr.error(datass.message, datass.type);
        }
        table.destroy(); 
        setTimeout(() => {
          $('#exampletable').DataTable();
        }, 1000);
        table1.destroy(); 
        setTimeout(() => {
          $('#exampletable1').DataTable();
        }, 1000);
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
    );
}
gettype(event)
{
  this.choosep=event;
}
getduration(event)
{
  this.productiondurations=event;
}
onChange(event)
{
//  alert(event);
this.hist_mindate = { year: event.year, month: 1, day: 1 };
this.hist_maxdate = { year: event.year, month: 12, day: 31 };
}
}
