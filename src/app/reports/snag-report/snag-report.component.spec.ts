import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnagReportComponent } from './snag-report.component';

describe('SnagReportComponent', () => {
  let component: SnagReportComponent;
  let fixture: ComponentFixture<SnagReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnagReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnagReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
