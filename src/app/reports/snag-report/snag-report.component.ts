import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef  } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-snag-report',
  templateUrl: './snag-report.component.html',
  styleUrls: ['./snag-report.component.css']
})
export class SnagReportComponent implements OnInit {
@ViewChild('exdata') exdata: ElementRef;
  @ViewChild('exdata1') exdata1: ElementRef;
  @ViewChild('exdata2') exdata2: ElementRef;
  @ViewChild('exdata3') exdata3: ElementRef;
  
  
  private modalRef: NgbModalRef;
  dtOptions: any = {};
  model: any = "";
  stationname: any = "";
  type: any = "";
  reporttype: any = "";
  qc_inspector: any = "";
  from_date: any = "";
  to_date: any = "";
  choosep: any = "";
  choosecategory: any = "";
  closeResult: string;
  drummodel: any[];
  station: any[];
  choosemodels=[];
  choosews=[];
  public dropdownSettings_technician = {};   
  public dropdownList_technician :{ emp_no: string, emp_no_name_role: string }[] = []; 
  public dropdownSettings_model = {};   
  public dropdownList_model :{ id: string, drum_model: string }[] = []; 
  public dropdownList_mixermodel:{id:string,mixer_model:string}[]=[];
 public dropdownSettings_mixermodel = {};
 public dropdownList_ws :{ id: string, code_list: string }[] = [];

     
 public dropdownSettings_ws={};
  public snagchecklist: any = "";
  public monthly_occurance: any = "";
  public productiondurations: any = "";
  public index: any = "";
  public mindate:any={year: 2021, month: 1, day: 1};
  public maxdate:any={year: 2019, month: 12, day: 31};
  public hist_mindate:any={year: 2021, month: 1, day: 1};
  public hist_maxdate:any={year: 2019, month: 12, day: 31};
  public reworkchecklist=[];
  public delaychecklist=[];
  public choosemixermodels=[];
    public datemodelchange=false;
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    $('#ex1data').DataTable();
    $('#ex2data1').DataTable();
    $('#ex2data2').DataTable();
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };
    this.dropdownSettings_technician = {
      text: "Select a technician",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "singledropdowncustom",
      primaryKey: "emp_no",
      labelKey: "emp_no_name_role",
      noDataLabel: "No technician is available",
      enableSearchFilter: true,
      showCheckbox: false,
      singleSelection:true,
      disabled: false
    };
     this.dropdownSettings_model = {
        text: "Select a model no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "drum_model",
        noDataLabel: "No model no is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
      this.dropdownSettings_mixermodel= {
        text: "Select a model no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "mixer_model",
        noDataLabel: "No model no is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
       this.dropdownSettings_ws = {
        text: "Select a work station",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "code_list",
        noDataLabel: "No workstation is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    var data: { switchcase: string } = { switchcase: "getpdiengineslno" };
    var method = "post";
    var url = "getsopdrummodel";
    this.dropdownList_model=[];
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            if(datas.message.length>0)
            {
              this.dropdownList_model.push({id:"ALL", drum_model:"ALL"});
            }
            for(let i=0;i<datas.message.length;i++)
            {
            this.dropdownList_model.push({id: datas.message[i]['id'], drum_model: datas.message[i]['drum_model']});
            }
          //  this.dropdownList_model=datas.message;
         //   this.drummodel = datas.message;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

        var data: { switchcase: string } = { switchcase: "getmixermodelno" };
    var method = "post";
    var url = "getsnagmixermodel";
    this.dropdownList_mixermodel=[];
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            if(datas.message.length>0)
            {
              this.dropdownList_mixermodel.push({id:"ALL", mixer_model:"ALL"});
            }
            for(let i=0;i<datas.message.length;i++)
            {
            this.dropdownList_mixermodel.push({id: datas.message[i]['id'], mixer_model: datas.message[i]['mixer_model']});
            }
          //  this.dropdownList_model=datas.message;
         //   this.drummodel = datas.message;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );




      this.dropdownList_technician=[];
      var data: { switchcase: string} = { switchcase: "gettechnician"};
      var method = "post";
      var url = "getdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datass) => {          
            if (datass.code == 201) {
              console.log(datass);
              if(datass.message.length>0)
              {
                 this.dropdownList_technician.push({emp_no: "all", emp_no_name_role: "All"});
                 for(let i=0;i<datass.message.length;i++)
                {
             this.dropdownList_technician.push({emp_no: datass.message[i]['emp_no'], emp_no_name_role: datass.message[i]['emp_no_name_role']});
                }
              }
             console.log(this.dropdownList_technician)
             // this.technician=datass;
            }
            else {
              this.toastr.error(datass.message, datass.type);
            }
          },
          (err) => {
            if (err.status == 403) {
              this.toastr.error(err._body);
              //alert(err._body);
            }
          }
        );
  };
export()
  {
     var table25=$('#exdata').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.exdata.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Snag Report.xlsx');
   $('#exdata').DataTable();
  }
  /*export1()
  {
     var table25=$('#exdata1').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.exdata1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Snag Report.xlsx');
   $('#exdata').DataTable();
  }*/
  export1()
  {
     var table25=$('#exdata1').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.exdata1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Snag Report.xlsx');
   $('#exdata1').DataTable();
  }
  export2()
  {
      var table25=$('#exdata2').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.exdata2.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Snag Report.xlsx');
   $('#exdata2').DataTable();
  }
  export3()
  {
      var table25=$('#exdata3').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.exdata3.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Snag Report.xlsx');
   $('#exdata3').DataTable();
  }
  getworkstation(event) {
    this.choosecategory=event;
    var data: { switchcase: string,id:string,category:string} = { switchcase: "getworkstation",id:this.reporttype,category:event};
    var method = "post";
    var url = "getdetails";
    //alert(url);
    this.dropdownList_ws=[];
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            if(datas.message.length>0)
            {
          this.dropdownList_ws.push({ id: "ALL", code_list: "ALL" })
            }
            for(let i=0;i<datas.message.length;i++)
            {
         this.dropdownList_ws.push({ id: datas.message[i]['id'], code_list: datas.message[i]['operation_code']+'('+ datas.message[i]['list_of_operation']+')'})
            }
            this.station = datas.message;
            console.log(this.station);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  gettype(event)
  {
    this.choosews = [];
    this.type=event;
    this.choosep=event;
  }
  getreporttype(event)
  {
    this.choosemodels=[];
    this.choosews = [];
    this.choosemixermodels=[];
    this.reporttype=event;
  //  this.getworkstation();
  }
  getmodel(item:any) {
    this.model = item.id;
  }
  getstation(item:any) {
    this.stationname = item.id;
  }
  getduration(event)
  {
  this.datemodelchange=true;
    this.productiondurations=event;
    this.datemodelchange=true;
  }
  getdetails( reporttype, period) {
   
   
   
    var data: { switchcase: string, type: any, reporttype: any, drummodel: any, station: any, fromdate: any, todate: any ,choosecategory:any} = { switchcase: "getchecklistdetails", type: reporttype, reporttype: this.type, drummodel: this.model, station: this.stationname, fromdate: this.from_date, todate: this.to_date,choosecategory:this.choosecategory };
    var method = "post";
    var url = "snagreport";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
         // alert();
          console.log(datas)
          if (datas.code == 201) {
          //  alert(datas.code)
         //   if (datas.message.length > 0) {
              //   this.painter_supervisor = datas.message[0].painter_empname;
              //   this.qc_inspector = datas.message[0].qc_empname;
              //  // this.date = datas.message[0].created_at.substring(0, 10);
              //   this.paintersign =environment.file_url +"pdi_signatures/" + datas.message[0].paintersign;
              //   this.qcsign = environment.file_url +"pdi_signatures/" + datas.message[0].qcsign;
              //   this.painter_created_date = datas.message[0].painter_created_at.substring(0, 10);
              //   this.qc_created_date = datas.message[0].qc_created_at.substring(0, 10);
            
              this.monthly_occurance = datas.message.length;

             // this.index = datas.message[0].length;
              console.log(this.monthly_occurance);
              console.log(this.type)
              if(this.type=='1' || this.type==1)
              {
             //  alert(1);
                 var table = $('#ex1data').DataTable();
              this.snagchecklist=[];
              this.snagchecklist = datas.message;
                table.destroy();
                setTimeout(() => {
              $('#ex1data').DataTable();
             // $('#ex2data1').DataTable();
             //   $('#ex2data2').DataTable();
            }, 1000);
              }
              else if(this.type=='2' || this.type==2)
              {
              //   alert(2);
                 var table1 = $('#ex2data1').DataTable();
              this.delaychecklist=[];
              this.delaychecklist = datas.message;
              table1.destroy();
              setTimeout(() => {
            //  $('#ex1data').DataTable();
              $('#ex2data1').DataTable();
              //  $('#ex2data2').DataTable();
            }, 1000);
              }
              else
              {
               //  alert(3);
                var table2 = $('#ex2data2').DataTable();
                 this.reworkchecklist=[];
              this.reworkchecklist = datas.message;
               table2.destroy();
               setTimeout(() => {
            ///  $('#ex1data').DataTable();
             // $('#ex2data1').DataTable();
                $('#ex2data2').DataTable();
            }, 1000);
              }
             
              //table.destroy();
              //table1.destroy();
             // table2.destroy();
         //   }
          
            
            /*
            setTimeout(() => {
              $('#ex1data').DataTable();
              $('#ex2data1').DataTable();
                $('#ex2data2').DataTable();
            }, 1000);*/
            
          /*  setTimeout(() => {
              
            }, 1000);
           
            setTimeout(() => {
            
            }, 1000);*/
          } else {
            alert("else");
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

  }
  getfromdate(event) {
    this.from_date = event;
  }
  gettodate(event) {
    this.to_date = event;
  }
}
