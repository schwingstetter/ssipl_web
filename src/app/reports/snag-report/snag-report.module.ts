import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { SnagReportComponent } from './snag-report.component';
import { DataTablesModule } from 'angular-datatables';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
export const snagrportRoutes: Routes = [
  {
    path: '',
    component: SnagReportComponent,
    data: {
      breadcrumb: 'Snag Report',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(snagrportRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [SnagReportComponent]
})
export class SnagReportModule { }
