import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef  } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-wip-report',
  templateUrl: './wip-report.component.html',
  styleUrls: ['./wip-report.component.css']
})
export class WipReportComponent implements OnInit {
 @ViewChild('extab') extab: ElementRef;
  @ViewChild('extable') extable: ElementRef;
  dtOptions: any = {}; 
  fromdate: string = "";
  todate: string = "";
  partno: string = "";
  selectedpartno: string = "";
  details: string = "";
  status: string = "";
  type: string = "";
  public choosereports:string="";
//selectedpartno:string='';
 public dropdownList_partno:{ id: string, partno_mixer_model: string }[] = [];
 //public dropdownList_partno=[];
 public dropdownSettings_partno = {};   
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal,config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    // var today = new Date();
    // var dd = today.getDate();
    // var mm = today.getMonth() + 1; //January is 0!
    // var yyyy = today.getFullYear();
    // var lastdays=new Date(yyyy, mm +1, 0).getDate();
    // config.minDate = { year: yyyy, month: mm,day: 1};
    // config.maxDate = { year: yyyy, month: 11,day: 31};
  // var today = new Date();
   //this.current_quarter = Math.floor((today.getMonth() + 3) / 3);
   //this.startDate = new Date(2019, 0, 1);
 //  this.datepickermindate= { "year": (new Date()).getFullYear(), "month": ((new Date()).getMonth()), "day": 1 }
 // = new Date(2019, 11, 0);
  //this.endDate = new Date();
  }
  ngOnInit() {
    this.choosereports="";
    $('#extab').DataTable();
    $('#extable').DataTable();
    this.dropdownSettings_partno = {
        text: "Select a part no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "partno_mixer_model",
        noDataLabel: "No part no is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };
    var data: { switchcase: string } = { switchcase: "getpartno" };
      var method = "post";
      var url = "wipreport";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
             if(datas.message.length>0)
              {
                this.dropdownList_partno.push({id:"ALL" , partno_mixer_model: "ALL"});
                for(let i=0;i<datas.message.length;i++)
                {
                  let temp=datas.message[i]['part_no']+'('+datas.message[i]['mixer_model']+')';
          this.dropdownList_partno.push({id:datas.message[i]['id'] , partno_mixer_model: temp});
                }
              }
              this.partno = datas.message;       
            } else {
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  
  };
  getselectedpartno(item:any)
  {
this.selectedpartno=item.id;
  }
  export()
{
   var table25=$('#extab').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.extab.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'WIP report.xlsx');
   $('#extab').DataTable();
}
export1()
{
  var table25=$('#extable').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.extable.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'WIP report.xlsx');
   $('#extable').DataTable();
}
getdetails()
{
 var table= $('#extab').DataTable();
  var table1=$('#extable').DataTable();
  var data: { switchcase: string,partno:string,status:string,type:string} = { switchcase: "getwipdetails",partno:this.selectedpartno,status:this.status,type:this.type };
  var method = "post";
  var url = "wipreport";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if (datas.code == 201) {
          
          // for(var i=0;i<=datas.message.length;i++)
          // {
          //   if(datas.message[i].manufactured_on)
          //   {
          //   datas.message[i].manufactured_on=datas.message[i].manufactured_on.substring(0, 10);  
          //   }
          //   if(datas.message[i].painted_on)
          //   {
          //   datas.message[i].painted_on=datas.message[i].painted_on.substring(0, 10);
          //   }  
          //   if(datas.message[i].fg_date)
          //   {
          //   datas.message[i].fg_date=datas.message[i].fg_date.substring(0, 10);  
          //   }
          //   if(datas.message[i].dispatched_date)
          //   {
          //   datas.message[i].dispatched_date=datas.message[i].dispatched_date.substring(0, 10);  
          //   }
          
          // }   
          this.details = datas.message;  
          console
        } else {
        }
        table.destroy(); 
        setTimeout(() => {
          $('#extab').DataTable();
        }, 1000);
        table1.destroy(); 
        setTimeout(() => {
          $('#extable').DataTable();
        }, 1000);
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
}
/*get_partno(event)
{
this.selectedpartno = event;
}*/
getstatus(event)
{
  this.status = event;
}
getreporttype(event)
{
  this.type = event;
}
}
