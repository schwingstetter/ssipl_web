import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { WipReportComponent } from './wip-report.component';
import { DataTablesModule } from 'angular-datatables';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown'
export const wiprportRoutes: Routes = [
  {
    path: '',
    component: WipReportComponent,
    data: {
      breadcrumb: 'WIP Report',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(wiprportRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [WipReportComponent]
})
export class WipReportModule { }
