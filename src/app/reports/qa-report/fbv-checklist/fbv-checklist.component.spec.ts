import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbvChecklistComponent } from './fbv-checklist.component';

describe('FbvChecklistComponent', () => {
  let component: FbvChecklistComponent;
  let fixture: ComponentFixture<FbvChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbvChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbvChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
