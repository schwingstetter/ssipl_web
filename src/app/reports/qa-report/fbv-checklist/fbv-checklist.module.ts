import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FbvChecklistComponent } from './fbv-checklist.component';
import { SharedModule } from '../../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const fbvinspectionRoutes: Routes = [
  {
    path: '',
    component: FbvChecklistComponent,
    data: {
      breadcrumb: 'FBV Checklist',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,    
    DataTablesModule,
    RouterModule.forChild(fbvinspectionRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [FbvChecklistComponent]
})
export class FbvChecklistModule { }
