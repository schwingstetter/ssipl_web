import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-fbv-checklist',
  templateUrl: './fbv-checklist.component.html',
  styleUrls: ['./fbv-checklist.component.css']
})
export class FbvChecklistComponent implements OnInit {
  @ViewChild('table1') table1: ElementRef;
   @ViewChild('table2') table2: ElementRef;
  private modalRef: NgbModalRef;
  dtOptions: any = {};
  chassis_make: any = "";
  chassis_model: any = "";
  checklist_id: any = "";
  checklist_name: any = "";
  model: any = "";
  inspected_by: any = "";
  rectified_by: any = "";
  assy_inspected_by: any = "";
  assy_rectified_by: any = "";
  painter_supervisorsignature: any = "";
  qc_inspectorsignature: any = "";
  date: any = "";
  painter_supervisordate: any = "";
  qc_inspectordate: any = "";
  qcsign: any = "";
  shiftengineersign: any = "";
  defect_image: any = "";
  qc_created_date: any = "";
  painter_created_date: any = "";
  closeResult: string;
  custname: any[];
  chassis_no: any[];
  choosechassis=[];
  public pdichecklist: any = "";
  public pdidefect: any = "";
  public dropdownSettings_customer = {};   
  public dropdownList_customer = []; 
   public dropdownSettings_chassisno = {};   
  public dropdownList_chassisno = []; 
  public selectedcustomer:string="";
  public selected_chassisno:string="";
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    $('#example4').DataTable();
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };
     this.dropdownSettings_customer = {
        text: "Select a customer",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "custid_name",
        noDataLabel: "No customer is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
      this.dropdownSettings_chassisno = {
        text: "Select a chassis no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "chassisno",
        noDataLabel: "No chassis no available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    var data: { switchcase: string } = { switchcase: "getfbvcustomername" };
    var method = "post";
    var url = "engineno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            this.custname = datas.message;
            this.dropdownList_customer=datas.message;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }; 
  export()
  {
     var table25=$('#example41').DataTable();
   table25.destroy(); 
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'FBV Checklist.xlsx');
  $('#example41').DataTable();
  }
  export1()
  {
     var table25=$('#example4').DataTable();
   table25.destroy(); 
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table2.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'FBV Checklist.xlsx');
  $('#example4').DataTable();
  }
  getchassisno(item:any)
  {
    this.choosechassis=[];
    this.selectedcustomer=item.id;
    this.chassis_make="";
    this.chassis_model="";
    var data: { switchcase: string,custid:any } = { switchcase: "getcustchassis",custid:this.selectedcustomer };
    var method = "post";
    var url = "engineno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            this.chassis_no = datas.message;
            this.dropdownList_chassisno=datas.message;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getchassismakemodel(item:any)
  {
    this.selected_chassisno=item.id;
    var data: { switchcase: string,chassisno:any } = { switchcase: "getchassismakemodel",chassisno:this.selected_chassisno };
    var method = "post";
    var url = "engineno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            if(datas.message.length >0)
            {
              this.chassis_make = datas.message[0].chassismake;
              this.chassis_model = datas.message[0].chassismodel;
              this.checklist_id = datas.message[0].checklist_id;
              if(datas.message[0].checklist_id == 1)
              {
                this.checklist_name = "Mounting Checklist";
              }
              else if(datas.message[0].checklist_id == 2)
              {
                this.checklist_name = "Party Mounting";
              }
              else if(datas.message[0].checklist_id == 3)
              {
                this.checklist_name = "CHASSIS + BENZ";
              }
              else if(datas.message[0].checklist_id == 4)
              {
                this.checklist_name = "D 6 - 8 m³ & 10 m³ R 1";
              }
              else if(datas.message[0].checklist_id == 5)
              {
                this.checklist_name = "CHASSIS + 6 m³ TM";
              }
              else if(datas.message[0].checklist_id == 6)
              {
                this.checklist_name = "CHASSIS + 8 m³ TM";
              }
              else if(datas.message[0].checklist_id == 7)
              {
                this.checklist_name = "CHASSIS + 10 m³ TM";
              }
              else if(datas.message[0].checklist_id == 8)
              {
                this.checklist_name = "Mounting Pithampur - 8 m³";
              }
              else if(datas.message[0].checklist_id == 9)
              {
                this.checklist_name = "D 6 - 8 m³ R0";
              }
            }
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getchecklistdetails(checklistname)
  {
   
     if( this.selectedcustomer == '')
    {
      this.toastr.error("Select Customer Name", " Warning");
    }
    else if(this.selected_chassisno == '' )
    {
      this.toastr.error("Select ChassisNo", " Warning");
    }
    else
    {
    var table = $('#example4').DataTable();
    var data: { switchcase: string, checklist: any, chassisno: any } = { switchcase: "getfbvchecklist", checklist: this.checklist_id, chassisno: this.selected_chassisno };
    var method = "post";
    var url = "reports";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message.length > 0)
            {
              this.inspected_by = datas.message[0].inspected_by;
              this.rectified_by = datas.message[0].rectified_by;
              this.assy_rectified_by = datas.message[0].assy_rectified_by;
              this.shiftengineersign =environment.file_url +"public/fbv_signatures/" + datas.message[0].inspector_sign;
              this.qcsign = environment.file_url +"public/fbv_signatures/" + datas.message[0].mfg_rectifier_sign;
             // this.qcsign = datas.message[0].assy_rectified_by;
             var pdichecklist1=datas.message[0].checklist;
             
              this.pdichecklist = JSON.parse(pdichecklist1);
              console.log( this.pdichecklist);
            }
            table.destroy();
            setTimeout(() => {
              $('#example4').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      }     
  }

showimage(image){
  //  alert(image);
    this.defect_image = environment.file_url + image;
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  reset()
  {
    window.location.reload();
  }

}