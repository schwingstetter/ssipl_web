import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-drum-inspection',
  templateUrl: './drum-inspection.component.html',
  styleUrls: ['./drum-inspection.component.css']
})
export class DrumInspectionComponent implements OnInit {
@ViewChild('table1') table1: ElementRef;
  private modalRef: NgbModalRef;
  //dtOptions: any = {};
  drum_version: any = "";
  drum_serial_number: any = [];
  drumserialno: any = [];
  drum_date: any = "";
  drum_batch: any = "";
  noofcone: any = "";
  noofshell: any = "";
  rectified_by: any = "";
  inspected_by: any = "";
  fitter_name: any = "";
  welder_name: any = "";
  inspector_sign: any = "";
  rectifier_sign: any = ""; 
  fitter_sign: any = ""; 
  welder_sign: any = ""; 
  rectified_date: any = "";
  inspected_date: any = "";   
  tested_date: any = "";   
  fitter_tested_date: any = "";   
  welder_tested_date: any = "";   
  closeResult: string;
  drummodel: any[];
  columns: any[];
  tablecolumns: any[];
  public drumchecklist: any = "";
  public dropdownSettings_drummodel = {};   
  public dropdownSettingsserial = {};   
  public dropdownList_drummodel = [];
  public dropdownListserial: {drum_serial_number:string}[]=[];
  public cone_count=0;
  public shell_count=0;
  public showtable=false;
  dtElement: DataTableDirective;
  datatableElement: DataTableDirective;
  //dtOptions: DataTables.Settings = {};
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  public tabledata = [];
  public selected_drumid:string="";
  public selecteddrumserialno:string="";
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
   // $('.datatable').DataTable();
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };
    
    this.dropdownSettings_drummodel = {
      text: "Select a Drum Model",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "singledropdowncustom",
      primaryKey: "id",
      labelKey: "drum_model",
      noDataLabel: "No Drum Model available",
      enableSearchFilter: true,
      showCheckbox: false,
      singleSelection:true,
      disabled: false
    };
    this.dropdownSettingsserial={
       text: "Select a Serial no",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "singledropdowncustom",
      primaryKey: "drum_serial_number",
      labelKey: "drum_serial_number",
      noDataLabel: "No Serialno available",
      enableSearchFilter: true,
      showCheckbox: false,
      singleSelection:true,
      disabled: false
    }
    var data: { switchcase: string } = { switchcase: "getdrummodel" };
    var method = "post";
    var url = "engineno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            this.drummodel = datas.message;
            this.dropdownList_drummodel=datas.message;
          // for(var i=0;i<=datas.message.length;i++)
          // {
          // this.dropdownSettingsdrummodel = datas.message[0];
          // }
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }; 
   export()
  {
     //var table25=$('#exampletable').DataTable();
   //table25.destroy(); 
      const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table1.nativeElement);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'Drum Inspection report.xlsx');
    //  $('#exampletable').DataTable();
  }

  getdrumdetails(item:any) {
    this.dropdownListserial=[];
    this.selected_drumid=item.id;
    var table=$('.datatable').DataTable();
    this.drum_serial_number=[];
    var data: { switchcase: string, drumid: any } = { switchcase: "getdrumdetails", drumid: this.selected_drumid };
    var method = "post";
    var url = "engineno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
           
            this.drum_batch = datas.message[0].drum_batch;
            this.drum_date = datas.message[0].drum_date.substring(0, 10);
            this.drum_version = datas.message[0].drum_version;
            this.noofcone = datas.message[0].noofcone;
            this.noofshell = datas.message[0].noofshell;
            console.log(this.noofcone)
            for(var i=0;i<=datas.message.length;i++)
            {
            console.log(datas.message[i].drum_serial_number);
            this.dropdownListserial.push({'drum_serial_number': datas.message[i].drum_serial_number});
            }
            table.destroy(); 
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getdrumserialno(item:any)
  {
    this.selecteddrumserialno=item.drum_serial_number;
  }
  getdrumchecklistdetails()
  {    
    if(this.selected_drumid == '' )
    {
      this.toastr.error("Select Drum Model", " Warning");
    }
    else if(this.selecteddrumserialno == '')
    {
      this.toastr.error("Select Drum serial", " Warning");
    }
    else
    {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string, drumid: any } = { switchcase: "getdrumchecklist", drumid: this.selecteddrumserialno};
    var method = "post";
    var url = "reports";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) { 
            if(datas.message.length > 0)
            {  
              this.showtable=true;
              this.rectified_by=datas.message[0].rectified_by;
              this.inspected_by=datas.message[0].inspected_by;
              this.fitter_tested_date=datas.message[0].declare_date;
              this.welder_tested_date=datas.message[0].declare_date;
              this.fitter_name=datas.message[0].fitter_name;
              this.welder_name=datas.message[0].welder_name;
              this.inspected_date=datas.message[0].inspected_date.substring(0, 10);
              this.rectified_date=datas.message[0].rectified_date.substring(0, 10);
              this.cone_count=datas.message[0].noofcone;
              this.shell_count=datas.message[0].noofshell;
              this.inspector_sign=environment.file_url +"public/drum_signatures/" + datas.message[0].inspector_sign;
              this.rectifier_sign=environment.file_url +"public/drum_signatures/" + datas.message[0].rectifier_sign;
              this.fitter_sign=environment.file_url +"public/drum_signatures/" + datas.message[0].fitter_sign;
              this.welder_sign=environment.file_url +"public/drum_signatures/" + datas.message[0].welder_sign;
              // this.qcsign = datas.message[0].assy_rectified_by;
              var checklist1 = datas.message[0].checklist;
             //  var checklist1 = checklist.replace("/\//g", "");
               //this.pdichecklist = JSON.parse(checklist);
              this.drumchecklist = JSON.parse(checklist1);
              console.log(this.drumchecklist);
            }
            table.destroy(); 
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      }
  }
  reset()
    {
      window.location.reload();
    }
}
