import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DrumInspectionComponent } from './drum-inspection.component';
import { SharedModule } from '../../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
export const druminspectionRoutes: Routes = [
  {
    path: '',
    component: DrumInspectionComponent,
    data: {
      breadcrumb: 'Drum Inspection',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(druminspectionRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [DrumInspectionComponent]
})
export class DrumInspectionModule { }
