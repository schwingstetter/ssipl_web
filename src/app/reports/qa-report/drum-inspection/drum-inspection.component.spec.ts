import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumInspectionComponent } from './drum-inspection.component';

describe('DrumInspectionComponent', () => {
  let component: DrumInspectionComponent;
  let fixture: ComponentFixture<DrumInspectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumInspectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumInspectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
