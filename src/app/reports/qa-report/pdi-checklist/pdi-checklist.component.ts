import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-pdi-checklist',
  templateUrl: './pdi-checklist.component.html',
  styleUrls: ['./pdi-checklist.component.css']
})
export class PdiChecklistComponent implements OnInit {
  @ViewChild('table1') table1: ElementRef;
  @ViewChild('table2') table2: ElementRef;
  private modalRef: NgbModalRef;
  dtOptions: any = {};
  mixerslno: any = "";
  partno: any = "";
  model: any = "";
  painter_supervisor: any = "";
  qc_inspector: any = "";
  painter_supervisorsignature: any = "";
  qc_inspectorsignature: any = "";
  date: any = "";
  painter_supervisordate: any = "";
  qc_inspectordate: any = "";
  qcsign: any = "";
  paintersign: any = "";
  defect_image: any = "";
  qc_created_date: any = "";
  painter_created_date: any = "";
  closeResult: string;
  engineno: any[];
  public pdichecklist: any = "";
  public pdidefect: any = "";
  public selectedengineslno:string="";
    public dropdownSettings = {};   
  public dropdownList = [];
  productiondurations:string; 
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.productiondurations="";
    var table =$('#example').DataTable();
    var table2 =$('#example2').DataTable();
     this.dropdownSettings = {
        text: "Select a engine sl no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "engine_sl_no",
        labelKey: "engine_sl_no",
        noDataLabel: "No Engine sl no available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };
    var data: { switchcase: string } = { switchcase: "getpdiengineslno" };
    var method = "post";
    var url = "engineno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            this.engineno = datas.message;
            this.dropdownList=datas.message;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  export()
  {
     var table25=$('#example').DataTable();
   table25.destroy(); 
      const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table1.nativeElement);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'PDIchecklist report.xlsx');
      $('#example').DataTable();
  }
    export1()
  {
    var table25=$('#example2').DataTable();
   table25.destroy(); 
      const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table2.nativeElement);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'PDIchecklist Defects report.xlsx');
      $('#example2').DataTable();
  }
  getenginedetails(item:any) {
    this.selectedengineslno=item.engine_sl_no;
    var data: { switchcase: string, engineno: any } = { switchcase: "getenginedetails", engineno: item.engine_sl_no };
    var method = "post";
    var url = "engineno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            this.partno = datas.message[0].part_no;
            this.mixerslno = datas.message[0].mixer_s_no;
            this.model = datas.message[0].mixer_model;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getqadetails(checklistname) {
    console.log(this.selectedengineslno);
    console.log(checklistname);
    if(this.selectedengineslno == "" )
    {
      this.toastr.error("Select Engine Sl.No", " Warning");
    }
    else if( checklistname == "")
    {
      this.toastr.error("Select CheckList", " Warning");
    }
  
    else
    {
    var table=$('#example').DataTable();
    var data: { switchcase: string, checklist: any, engineno: any } = { switchcase: "getpdichecklist", checklist: checklistname, engineno: this.selectedengineslno };
    var method = "post";
    var url = "reports";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message.length > 0)
            {
              this.painter_supervisor = datas.message[0].painter_empname;
              this.qc_inspector = datas.message[0].qc_empname;
             // this.date = datas.message[0].created_at.substring(0, 10);
              this.paintersign =environment.file_url +"public/pdi_signatures/" + datas.message[0].paintersign;
              this.qcsign = environment.file_url +"public/pdi_signatures/" + datas.message[0].qc_sign;
              this.painter_created_date = datas.message[0].painter_created_at.substring(0, 10);
              this.qc_created_date = datas.message[0].qc_created_at.substring(0, 10);
              this.pdichecklist = JSON.parse(datas.message[0].checklist);
            }
           
            table.destroy(); 
            setTimeout(() => {
              $('#example').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      }
  }
  getdefects(checklistname)
  {
    var table = $('#example2').DataTable();
    var data: { switchcase: string, checklist: any, engineno: any } = { switchcase: "getpdidefect", checklist: checklistname, engineno: this.selectedengineslno};
    var method = "post";
    var url = "reports";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.pdidefect = datas.message;
            // for(var i=0;i<=datas.message.length;i++)
            // {
            //   datas.message[i].created_at=datas.message[i].created_at.substring(0, 10);
            // }
           
            table.destroy();
            setTimeout(() => {
              $('#example2').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  showimage(image){
    //  alert(image);
      this.defect_image = environment.file_url +"public/pdi_defectimages/" +  image;
    };
    openbulkupload(bulkupload) {
      this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    };
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }
    reset()
    {
      
      window.location.reload();
    }
}
