import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PdiChecklistComponent } from './pdi-checklist.component';
import { SharedModule } from '../../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const pdichecklistRoutes: Routes = [
  {
    path: '',
    component: PdiChecklistComponent,
    data: {
      breadcrumb: 'PDI Checklist',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(pdichecklistRoutes),
    SharedModule,
    AngularMultiSelectModule 
  ],
  declarations: [PdiChecklistComponent]
})
export class PdiChecklistModule { }
