import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdiChecklistComponent } from './pdi-checklist.component';

describe('PdiChecklistComponent', () => {
  let component: PdiChecklistComponent;
  let fixture: ComponentFixture<PdiChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdiChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdiChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
