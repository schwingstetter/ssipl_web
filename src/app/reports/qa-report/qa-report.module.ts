import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { QaReportComponent } from './qa-report.component';
import { CommonModule } from '@angular/common';
import { qareportsRoutes } from './qa-report.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(qareportsRoutes),
  ],
  declarations: [QaReportComponent]
})
export class QaReportModule { }
