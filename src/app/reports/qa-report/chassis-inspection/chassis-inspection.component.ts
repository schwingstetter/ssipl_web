import { Component, OnInit, ViewContainerRef,ViewChild, ElementRef  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AjaxService } from '../../../ajax.service';
import { environment } from '../../../../environments/environment';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
 import * as XLSX from 'xlsx';
@Component({
  selector: 'app-chassis-inspection',
  templateUrl: './chassis-inspection.component.html',
  styleUrls: ['./chassis-inspection.component.css']
})
export class ChassisInspectionComponent implements OnInit {
 @ViewChild('table1') table1: ElementRef;
  //dtOptions: any = {};
  public dropdownSettings = {};   
  public dropdownSettingschassisno = {};   
  public dropdownList = [];
  public dropdownListchassisno=[];
  public custid: String = '0';
  public chassisno: String= '0';
  public supervisorsign: String= "";
  public makename: string="";
  public formclicked=false;
  public qcinspectormodel:string="";
   public drivermobileno:string="";
  dtElement: DataTableDirective;
  datatableElement: DataTableDirective;
  //dtOptions: DataTables.Settings = {};
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  public tabledata = [];
  choosechassisnomodel=[];
  constructor(public http: Http, private toastyService: ToastyService, public AjaxService:AjaxService,public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.dropdownSettings = {
        text: "Select a customer",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "custname_with_id",
        noDataLabel: "No customers available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
      this.dropdownSettingschassisno={
         text: "Select a Chassis no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "chassisno",
        noDataLabel: "No Chassis available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      }
   //$('.datatable').DataTable();
   this.getcustlistall();
    /*this.dtOptions = {
      // Declare the use of the extension in the dom parameter
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ]
    };*/
  }; 
   export()
{
   var table25=$('.datatable').DataTable();
   table25.destroy(); 
  console.log(this.table1.nativeElement);
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Chassis inspection.xlsx');
   $('.datatable').DataTable();
}
   oncustomerSelect(item:any){
     console.log(item.id)
     this.custid = item.id;
      this.makename="";
      this.formclicked=false;
      this.choosechassisnomodel=[];
     this.getchassisnoall(item.id)
  }
  OncustomerDeSelect(item:any){
    this.dropdownList = [];
     this.custid = '0';
     this.chassisno = '0';
     this.makename="";
     this.formclicked=false;
     
    this.getchassisnoall(item.id)
  }
  onchassisSelect(item:any){
     console.log(item.id)
    
     this.chassisno = item.id;
      this.getmakename(item.id);
this.formclicked=false;
     // this.getchassisinspectiondetails(this.custid , this.chassisno);
  }
  OnchassisDeSelect(item: any){
    this.dropdownListchassisno = [];
    this.chassisno = '0';
     this.makename="";
this.formclicked=false;
  //  this.getchassisinspectiondetails(this.custid, this.chassisno);
  }
  /*onSelectAll(items: any){
      // console.log(items);
  }
  onDeSelectAll(items: any){
    //  console.log(items);
  }*/
  getchassisinspectiondetails()
  {
    console.log(this.custid);
    console.log(this.chassisno);
    if(this.custid == '0' )
    {
      this.toastr.error("Select  Customer Name", " Warning");
    }
    else if(this.chassisno == '0')
    {
      this.toastr.error("Select Chassis No", " Warning");
    }
    else
    {
     var table=$('.datatable').DataTable();
    this.formclicked=true;
     var data: { customerid: String, chassisno: String } = { customerid: this.custid, chassisno: this.chassisno };
      var method = "post";
      var url = "getchassisinspection_chassisdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
             this.supervisorsign= environment.file_url+'public/chassis_signatures/'+datas.data[0]['driver_sign'];
           this.qcinspectormodel=datas.data[0]['name'];
           this.drivermobileno=datas.data[0]['driver_mobile'];
            if(datas.data[0]['ch_checklist'].length>0)
            {
              this.tabledata=JSON.parse(datas.data[0]['ch_checklist']);
            }
          
            table.destroy(); 
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
               //this.dropdownListchassisno=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
        }
  }
  getmakename(chassisid)
  {
var data: { chassisno: String } = { chassisno: chassisid };
      var method = "post";
      var url = "getchassisinspection_chassismake";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            this.makename=datas.data[0]['chassismake'];
               //this.dropdownListchassisno=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  getchassisnoall(custid)
  {
      var data: { customerid: string } = { customerid: custid };
      var method = "post";
      var url = "getchassisinspection_chassisno";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
               this.dropdownListchassisno=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
getcustlistall()
{
 
   var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "getchassisinspection_cust";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
               this.dropdownList=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
}
reset()
    {
      
      window.location.reload();
    }
}