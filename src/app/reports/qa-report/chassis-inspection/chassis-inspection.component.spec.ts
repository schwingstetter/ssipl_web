import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChassisInspectionComponent } from './chassis-inspection.component';

describe('ChassisInspectionComponent', () => {
  let component: ChassisInspectionComponent;
  let fixture: ComponentFixture<ChassisInspectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChassisInspectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChassisInspectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
