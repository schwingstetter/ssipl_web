import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ChassisInspectionComponent } from './chassis-inspection.component';
import { SharedModule } from '../../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
export const chassisinspectionRoutes: Routes = [
  {
    path: '',
    component: ChassisInspectionComponent,
    data: {
      breadcrumb: 'Chassis Inspection',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(chassisinspectionRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [ChassisInspectionComponent]
})
export class ChassisInspectionModule { }