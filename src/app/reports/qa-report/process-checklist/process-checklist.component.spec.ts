import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessChecklistComponent } from './process-checklist.component';

describe('ProcessChecklistComponent', () => {
  let component: ProcessChecklistComponent;
  let fixture: ComponentFixture<ProcessChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
