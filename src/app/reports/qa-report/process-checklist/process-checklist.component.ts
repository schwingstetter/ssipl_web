import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-process-checklist',
  templateUrl: './process-checklist.component.html',
  styleUrls: ['./process-checklist.component.css']
})
export class ProcessChecklistComponent implements OnInit {
  @ViewChild('table1') table1: ElementRef;
  private modalRef: NgbModalRef;
  dtOptions: any = {};
  mixerslno: any = "";
  partno: any = "";
  model: any = "";
  prd_cleared_date: any = "";
  mix_color: any = "";
  painting_date: any = "";
  fg_date: any = "";
  customer_name: any = "";
  contractor_name: any = "";
  painter_supervisor: any = "";
  qc_inspector: any = "";
  contractor_empname: any = "";
  supervisor_empname: any = "";
  contractor_createddate: any = "";
  supervisor_createddate: any = "";
  contractorsign: any = "";
  supervisorsign: any = "";
  closeResult: string;
  engineno: any[];
  public processchecklist: any = "";
  public pdidefect: any = "";
    public dropdownSettings = {};   
  public dropdownList = [];
  public selected_engineslno:any="";
  checklistnames:string; 
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.checklistnames="";
  var table=  $('#example3').DataTable();
   this.dropdownSettings = {
        text: "Select a engine sl no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "engine_sl_no",
        labelKey: "engine_sl_no",
        noDataLabel: "No Engine sl no available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };
    var data: { switchcase: string } = { switchcase: "getprocessengineslno" };
    var method = "post";
    var url = "engineno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            this.engineno = datas.message;
            this.dropdownList= datas.message;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getenginedetails(item:any) {
    this.selected_engineslno=item.engine_sl_no;
    var data: { switchcase: string, engineno: any } = { switchcase: "getenginedetails", engineno: item.engine_sl_no };
    var method = "post";
    var url = "engineno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            this.partno = datas.message[0].part_no;
            this.mixerslno = datas.message[0].mixer_s_no;
            this.model = datas.message[0].mixer_model;
            this.prd_cleared_date = datas.message[0].prd_cleared_date.substring(0, 10);
            this.mix_color = datas.message[0].mix_color;
            this.painting_date = datas.message[0].painting_date.substring(0, 10);
            this.fg_date = datas.message[0].fg_date.substring(0, 10);
            this.customer_name = datas.message[0].customer_name;
            this.contractor_name = datas.message[0].contractor_name;
         
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  export()
{
   ////var table25=$('#example3').DataTable();
  // table25.destroy(); 
  console.log(this.table1.nativeElement);
   const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Process checklist report.xlsx');
//  $('#example3').DataTable();
}
  getqadetails(checklistname, engineno) {
   var table =  $('#example3').DataTable();
   console.log(checklistname);
   console.log(this.selected_engineslno);
   if(this.selected_engineslno == ""  )
   {
    this.toastr.error("Select Engine Sl.No", " Warning");
   }
   else if(checklistname == "")
   {
    this.toastr.error("Select CheckList", " Warning");
   }
   else
   {
    var data: { switchcase: string, checklist: any, engineno: any } = { switchcase: "getprocesschecklist", checklist: checklistname, engineno: this.selected_engineslno };
    var method = "post";
    var url = "reports";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message.length > 0)
            {
                this.contractor_empname = datas.message[0].contractor_empname;
                this.supervisor_empname = datas.message[0].supervisor_empname;
                this.contractor_createddate=datas.message[0].c_created;
                this.supervisor_createddate=datas.message[0].s_created;
                   this.contractorsign = environment.file_url +"public/process_signatures/" + datas.message[0].contractorsign;
            this.supervisorsign = environment.file_url +"public/process_signatures/" + datas.message[0].supervisor_sign;
            console.log(this.contractorsign);
            console.log(this.supervisorsign);
                this.processchecklist = JSON.parse(datas.message[0].checklist);
                console.log(this.processchecklist);
            }
          table.destroy();
            setTimeout(() => {
              $('#example3').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      }
  }
  // getdefects(checklistname, engineno)
  // {
  //   var table =  $('#example').DataTable();
  //   var data: { switchcase: string, checklist: any, engineno: any } = { switchcase: "getpdidefect", checklist: checklistname, engineno: engineno };
  //   var method = "post";
  //   var url = "reports";
  //   this.AjaxService.ajaxpost(data, method, url)
  //     .subscribe(
  //       (datas) => {
  //         if (datas.code == 201) {
  //           this.pdidefect = datas.message;
  //           // for(var i=0;i<=datas.message.length;i++)
  //           // {
  //           //   datas.message[i].created_at=datas.message[i].created_at.substring(0, 10);
  //           // }
           
  //           table.destroy();
  //           setTimeout(() => {
  //             $('#example').DataTable();
  //           }, 1000);
  //         } else {
  //         }
  //       },
  //       (err) => {
  //         if (err.status == 403) {
  //           alert(err._body);
  //         }
  //       }
  //     );
  // }
  showimage(image){
    //  alert(image);
     // this.defect_image = environment.file_url + image;
    };
    openbulkupload(bulkupload) {
      this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    };
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }
    reset()
    {
      window.location.reload();
    }
}
