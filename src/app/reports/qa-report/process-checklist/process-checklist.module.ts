import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProcessChecklistComponent } from './process-checklist.component';
import { SharedModule } from '../../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
export const processchecklistRoutes: Routes = [
  {
    path: '',
    component: ProcessChecklistComponent,
    data: {
      breadcrumb: 'Process Checklist',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(processchecklistRoutes),
    SharedModule,
    AngularMultiSelectModule 
  ],
  declarations: [ProcessChecklistComponent]
})
export class ProcessChecklistModule { }
