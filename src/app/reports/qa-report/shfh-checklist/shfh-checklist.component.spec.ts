import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShfhChecklistComponent } from './shfh-checklist.component';

describe('ShfhChecklistComponent', () => {
  let component: ShfhChecklistComponent;
  let fixture: ComponentFixture<ShfhChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShfhChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShfhChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
