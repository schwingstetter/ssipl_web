import { Component, OnInit, ViewContainerRef,ViewChild, ElementRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AjaxService } from '../../../ajax.service';
import { json } from 'd3';
 import * as XLSX from 'xlsx';
 import { environment } from './../../../../environments/environment';
@Component({
  selector: 'app-shfh-checklist',
  templateUrl: './shfh-checklist.component.html',
  styleUrls: ['./shfh-checklist.component.css']
})
export class ShfhChecklistComponent implements OnInit {
   @ViewChild('generalinstable') table1: ElementRef;
   @ViewChild('hydraulictable') table2: ElementRef;
   @ViewChild('assemblychecklist2') table3: ElementRef;
   @ViewChild('fittertable') table4: ElementRef;
   @ViewChild('weldertable1') tablewelder: ElementRef;
   @ViewChild('snagassembly') table6: ElementRef;
   @ViewChild('functionaltable') table7: ElementRef;
   @ViewChild('vcchecklisttable') table10: ElementRef;
    
  dtOptions: any = {};
  public workorder_no: string = "";
  public mixer_s_no: string = "";
  public forminvalid:boolean=true;
  public engine_gearno: string ="";
  public waterpump_hydcombution: string = "";
  public part_no: string = "";
  public sh_fh_created_at: string = ""; 
  public isgeneral = true;
  public isassembly = true;
  public issnag = true;
  public isfunctional = true;
  private modalRef: NgbModalRef;
  closeResult: string;
  public dropdownSettings = {};   
  public dropdownList = []; 
  public submitbuttonclicked:boolean = false;
  public showerror:boolean = false;
  public generalinspectiontabledata = [];
  public fittertable :{ list_of_operation: string, emp_no: string, name: string }[]=[];
  public weldertable :{ list_of_operation: string, emp_no: string, name: string }[]=[];
  public hydraulicombinationdata = [];
  public shiftengineername : string ="";
  public recfiername : string ="";
  public assemby_insname : string =""; 
  public ins_checklistdata=[];
  public vcchecklistdata=[];
  public snaginspectiondata=[];
  public enginefunctionaldata=[];
  public functionaltesteddate : string ="";
  public functionaltestedbyname : string ="";
  public rectifiedbydate : string ="";
  public rectifiedbyname : string ="";
  public func_engineerdate : string ="";
  public func_engineername : string ="";
  public defect_image:any;
  public assembler_sign:any;
  public assembler2_sign:any;
  public qc_sign:any;
  //public hydrauliccombodata:
  public hydrauliccombodata: { component_name: string, Make: string, Model: string, Capacity: string, SLNO: string }[] = [];
  constructor(public http: Http, private toastyService: ToastyService,public AjaxService:AjaxService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    $('.datatable').DataTable();
    this.dtOptions = {
      // Declare the use of the extension in the dom parameter
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ]
    };
    this.dropdownSettings = {
        text: "Select a engine sl no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "engine_gear_no",
        labelKey: "engine_gear_no",
        noDataLabel: "No Engine sl no available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
      this.getengineserialno();
  };
  export()
{
   var table25=$('#generalinstable').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'General inspection.xlsx');
  $('#generalinstable').DataTable();
}
export1(){
   var table25=$('#hydraulictable').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table2.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Hydraulic Combination.xlsx');
  $('#hydraulictable').DataTable();
  
}
export2()
{
  var table25=$('#assemblychecklist2').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table3.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Assembly Checklist inspection.xlsx');
   $('#assemblychecklist2').DataTable();
}
export3()
{
  
   //console.log(tablewelder)
  var table25=$('#weldertable').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.tablewelder.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Welder details.xlsx');
   $('#weldertable').DataTable();
}
export4()
{
   var table25=$('#fittertable').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table4.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Fitter details.xlsx');
  $('#fittertable').DataTable();
}
export5()
{
   var table25=$('#snagassembly').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table6.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Snag details.xlsx');
  $('#snagassembly').DataTable();
}
export6()
{
   var table25=$('#functionaltable').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table7.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Functional details.xlsx');
   $('#functionaltable').DataTable();
}
export10()
{
  var table25=$('#vcchecklisttable').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table10.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'VC Checklist details.xlsx');
   $('#vcchecklisttable').DataTable();
}
   onengineSelect(item:any){
     this.showerror=false;
     this.forminvalid=false;
     this.getenginedetails(item.engine_gear_no);
     this.engine_gearno=item.engine_gear_no;
   }
   showtable()
   {
     
     if(this.forminvalid==false)
     {
       this.generalinspectiontabledata=[];
       this.hydrauliccombodata=[];
       this.ins_checklistdata=[];
       this.snaginspectiondata=[];
       this.enginefunctionaldata=[];
       this.fittertable=[];
       this.weldertable=[];
       this.showerror=false;
       this.submitbuttonclicked=true;
       this.getsnag_shfhreport();
       this.getgeneralinspectiondetails();
       this.getengineassemblyhydraulic();
       this.getengineassemblyinspectiondetails();
       this.getenginefunctionaldetails();
       this.getfitterdetails();
       this.getwelderdetails();
     }
     else{
       this.showerror=true;
      }
   }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addNew) {
  //  alert();
 
  var table6=$('.datatable6').DataTable();
  table6.destroy();
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    
            setTimeout(() => {
              $('.datatable6').DataTable();
            }, 1000);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  open1(addNew) {
   // alert(1)
    var table5=$('.datatable5').DataTable();
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
      table5.destroy(); 
            setTimeout(() => {
              $('.datatable5').DataTable();
            }, 1000);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  getengineserialno()
  {
     var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "getengineserialno_shfhreport";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
               this.dropdownList=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  getenginedetails(qacompid)
  {
    var data: { switchcase: string, qacompid: string } = { switchcase: "get", qacompid: qacompid };
      var method = "post";
      var url = "getengineserialdetail_shfhreport";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              // this.dropdownList=datas.data;
             this.mixer_s_no=datas.data[0]['mixer_s_no'];
             this.workorder_no=datas.data[0]['workorder_no'];
             this.waterpump_hydcombution=datas.data[0]['waterpump_hydcombution'];
             this.part_no=datas.data[0]['part_no'];
             this.sh_fh_created_at=datas.data[0]['sh_fh_created_at'];
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
getgeneralinspectiondetails()
{
  // this.generalinspectiontabledata=[];
  //  var table=$('.datatable0').DataTable();
  //  table.destroy(); 
  //  var data: { switchcase: string, engineserialno: string } = { switchcase: "get", engineserialno: this.engine_gearno };
  //     var method = "post";
  //     var url = "getengineserialgenins_shfhreport";
  //     this.AjaxService.ajaxpost(data, method, url)
  //       .subscribe(
  //         (datas) => {
  //             // this.dropdownList=datas.data;
  //             if(datas.data.length>0)
  //             {
  //               this.shiftengineername=datas.data[0]['shiftenginnername'];
  //               this.recfiername=datas.data[0]['assemblername'];
  //            //   const samplearray=(JSON.parse(datas.data[0]['assembler_defeat'].tostring())).concat(JSON.parse(datas.data[0]['testing_defeat']));
  //  const samplearray1=(JSON.parse(datas.data[0]['assembler_defeat']));
  // const samplearray2=(JSON.parse(datas.data[0]['testing_defeat']));
  // const samplearray=samplearray1.concat(samplearray2);
  //                 this.generalinspectiontabledata = samplearray;
  //                 console.log(this.generalinspectiontabledata)
  //             }
              
  //           setTimeout(() => {
  //             $('.datatable0').DataTable(
  //             /*  {
  //                  dom: 'Bfrtip',
  //                  buttons: [
         
  //           $.extend( true, {}, buttonCommon, {
  //               extend: 'excelHtml5'
  //           } )
  //       ]
  //               }*/
  //             );
  //           }, 1000);
  //           console.log(this.generalinspectiontabledata)
  //         },
  //         (err) => {
  //           if (err.status == 403) {
  //             alert(err._body);
  //           }
  //         }
  //       );
  var table = $('.datatable0').DataTable();
  table.destroy(); 
    var data: { switchcase: string, engineno: any } = { switchcase: "general_report", engineno: this.engine_gearno};
    var method = "post";
    var url = "reports";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.generalinspectiontabledata = datas.message;
            // for(var i=0;i<=datas.message.length;i++)
            // {
            //   datas.message[i].created_at=datas.message[i].created_at.substring(0, 10);
            // }
           
            
            setTimeout(() => {
              $('.datatable0').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => { 
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
}
showimage(image){
  //  alert(image);
    this.defect_image = environment.file_url +"public/shfh_defectimages/" +  image;
  }
getengineassemblyhydraulic()
{
  var table1=$('.datatable1').DataTable();
  this.hydrauliccombodata=[];
  table1.destroy(); 
  var data: { mixer_s_no: string, engineno: string } = { mixer_s_no: this.mixer_s_no, engineno: this.engine_gearno};
      var method = "post";
      var url = "getengineassemblyhydraulic_shfhreport";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
         //   this.assemby_insname = datas.data['inspection_points'][0]['ins_name'];
            // this.hydraulicombinationdata = datas.data[0];
            if(datas.data.length>0)
            {
             // component_name: string, Make: string, Model: string, Capacity: string, SLNO: string
              this.hydrauliccombodata.push({ "component_name": 'Engine', "Make": datas.data[0]['engine_make'], "Model": datas.data[0]['engine_model'], "Capacity": datas.data[0]['engine_capacity'],"SLNO":datas.data[0]['engine_sl_no'] });
              this.hydrauliccombodata.push({ "component_name": 'Gear Box', "Make": datas.data[0]['gearbox_make'], "Model": datas.data[0]['gearbox_model'], "Capacity": datas.data[0]['gearbox_capacity'],"SLNO":datas.data[0]['gear_box_sl_no'] });
              this.hydrauliccombodata.push({ "component_name": 'Hydraulic Pump', "Make": datas.data[0]['hydpump_make'], "Model": datas.data[0]['hydpump_model'], "Capacity": datas.data[0]['hydpump_capacity'],"SLNO":datas.data[0]['hyd_pump_s_no'] });
              this.hydrauliccombodata.push({ "component_name": 'Hydraulic Motor', "Make": datas.data[0]['hydmotor_make'], "Model": datas.data[0]['hydmotor_model'], "Capacity": datas.data[0]['hydmotor_capcity'],"SLNO":datas.data[0]['hyd_motor_s_no'] });
              this.hydrauliccombodata.push({ "component_name": 'Water Pump', "Make": datas.data[0]['waterpump_make'], "Model": datas.data[0]['waterpump_model'], "Capacity": datas.data[0]['waterpump_capacity'],"SLNO":datas.data[0]['mixer_s_no'] });
            }
             
             console.log(datas.data)
             console.log(this.hydraulicombinationdata)
         //    this.ins_checklistdata = JSON.parse(datas.data['inspection_points'][0]['ins_checklist']);
             
            setTimeout(() => {
              $('.datatable1').DataTable();
            }, 1000);
            
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
        
}

getengineassemblyinspectiondetails()
{
  // var table1=$('.datatable1').DataTable();
   var table2=$('.datatable2').DataTable();
   table2.destroy();
   var table10=$('.datatable10').DataTable();
    table10.destroy();
  var data: { mixer_s_no: string, engineno: string } = { mixer_s_no: this.mixer_s_no, engineno: this.engine_gearno};
      var method = "post";
      var url = "getengineassemblyinspectiondetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            this.assemby_insname = datas.data[0]['ins_name'];
         //    this.hydraulicombinationdata = datas.data['hydraulic_combination'][0];
         let testarray=[];
         testarray = JSON.parse(datas.data[0]['ins_checklist']);
             this.ins_checklistdata = testarray;
            // console.log(this.ins_checklistdata)
             let testarray2 = [];
             console.log(datas.data[0]['vc_checklist'])
              testarray2 = JSON.parse(datas.data[0]['vc_checklist']);
             this.vcchecklistdata = testarray2;
            setTimeout(() => {
              $('.datatable2').DataTable();
              $('.datatable10').DataTable();
            }, 1000);
            
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
}
getsnag_shfhreport()
{
  this.snaginspectiondata=[];
   var table3=$('.datatable3').DataTable();
    table3.destroy(); 
   var data: { engineno: string } = { engineno: this.engine_gearno };
      var method = "post";
      var url = "getsnag_shfhreport";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              // this.dropdownList=datas.data;
                  this.snaginspectiondata = datas.data;
             
            setTimeout(() => {
              $('.datatable3').DataTable();
            }, 1000);
            //console.log(this.generalinspectiontabledata)
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
}
getenginefunctionaldetails()
{
  var table4=$('.datatable4').DataTable();
    table4.destroy(); 
   var data: { engineno: string } = { engineno: this.engine_gearno };
      var method = "post";
      var url = "getenginefunctionaldetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              // this.dropdownList=datas.data;
              if(datas.data.length>0)
              {


  this.functionaltesteddate = datas.data[0]['assembler_created_at'];
  this.functionaltestedbyname = datas.data[0]['assembler1name'];
  this.rectifiedbydate = datas.data[0]['assembler2_created_at'];
  this.rectifiedbyname = datas.data[0]['assembler2name'];
  this.func_engineerdate = datas.data[0]['qc_created_at'];
  this.func_engineername = datas.data[0]['qcname'];
  this.assembler_sign = datas.data[0]['assembler_sign'];
  this.assembler2_sign = datas.data[0]['assembler2_sign'];
  this.qc_sign=datas.data[0]['qc_sign'];
  console.log(datas.data[0]['vc_checklist']);
  this.enginefunctionaldata = JSON.parse(datas.data[0]['vc_checklist']);

              }
                  
            
            setTimeout(() => {
              $('.datatable4').DataTable();
            }, 1000);
            //console.log(this.generalinspectiontabledata)
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
}
getfitterdetails()
{
  this.fittertable=[];
  var table5=$('.datatable5').DataTable();
   table5.destroy(); 
   var data: { workorderno: string } = { workorderno: this.workorder_no };
      var method = "post";
      var url = "getfitterdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              // this.dropdownList=datas.data;
              if(datas.data.length>0)
              {
                  for(var i=0;i<datas.data.length;i++)
                  {
                    this.fittertable.push({ "list_of_operation": datas.data[i][0]['list_of_operation'], "emp_no": datas.data[i][0]['emp_no'], "name": datas.data[i][0]['name'] });
                  }
              }

              //this.hydrauliccombodata.push({ "component_name": 'Engine', "Make": datas.data[0]['engine_make'], "Model": datas.data[0]['engine_model'], "Capacity": datas.data[0]['engine_capacity'],"SLNO":datas.data[0]['engine_sl_no'] });
                  //this.fittertable = datas.data;
                
             
            setTimeout(() => {
              $('.datatable5').DataTable();
            }, 1000);
            //console.log(this.generalinspectiontabledata)
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
}
getwelderdetails()
{
  this.weldertable=[];
  var table6=$('.datatable6').DataTable();
   table6.destroy(); 
   var data: { workorderno: string } = { workorderno: this.workorder_no };
      var method = "post";
      var url = "getwelderdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              // this.dropdownList=datas.data;
               //   this.weldertable = datas.data;
               if(datas.data.length>0)
               {
                   for(var i=0;i<datas.data.length;i++)
                   {
                     this.weldertable.push({ "list_of_operation": datas.data[i][0]['list_of_operation'], "emp_no": datas.data[i][0]['emp_no'], "name": datas.data[i][0]['name'] });
                   }
               }
                 
            setTimeout(() => {
              $('.datatable6').DataTable();
            }, 1000);
            //console.log(this.generalinspectiontabledata)
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
}

reset()
    {
      
      window.location.reload();
    }
}
