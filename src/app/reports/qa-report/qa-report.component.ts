import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qa-report',
  template: '<router-outlet></router-outlet>'
})
export class QaReportComponent implements OnInit {

  constructor() { }

  ngOnInit() {   
  }

}
