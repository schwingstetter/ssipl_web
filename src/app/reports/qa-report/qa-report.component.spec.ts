import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QaReportComponent } from './qa-report.component';

describe('QaReportComponent', () => {
  let component: QaReportComponent;
  let fixture: ComponentFixture<QaReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QaReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QaReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
