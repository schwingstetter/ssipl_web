import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ModelwisesopReportComponent } from './modelwisesop-report.component';
import { SharedModule } from '../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
export const modelwisesopReportRoutes: Routes = [
  {
    path: '',
    component: ModelwisesopReportComponent,
    data: {
      breadcrumb: 'Model Wise SOP Report',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(modelwisesopReportRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [ModelwisesopReportComponent]
})
export class ModelwisesopReportModule { }
