import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelwisesopReportComponent } from './modelwisesop-report.component';

describe('ModelwisesopReportComponent', () => {
  let component: ModelwisesopReportComponent;
  let fixture: ComponentFixture<ModelwisesopReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelwisesopReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelwisesopReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
