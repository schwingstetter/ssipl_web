import { Component, OnInit, ViewContainerRef,ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { environment } from './../../../environments/environment';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-modelwisesop-report',
  templateUrl: './modelwisesop-report.component.html',
  styleUrls: ['./modelwisesop-report.component.css']
})
export class ModelwisesopReportComponent implements OnInit {
  public processimage: string = "";
  dtOptions: any = {};
  private modalRef: NgbModalRef;
  public modeldata:any={};
  public workstation:any={};
  public details:any={};
  public rev:any="";
  public rev_date:any="";
  public ppe:any="";
  public preparedby:any="";
  public approvedby:any="";
  public checkedby:any="";
  public choosmod:any="";
  closeResult: string;
   public dropdownList_model = []; 
   public dropdownSettings_model = {};  
   public selectedmodel:string=''; 
   public choosetype:string=''; 
   public dropdownList_workstation=[];
   public dropdownSettings_workstation={};
   public selectedworkstation:string='';
   formerror=false;
  @ViewChild('example') example: ElementRef;
  choosemodel=[];
  choosewsmodel=[];
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }
  ngOnInit() {
    $('#example').DataTable();
    this.dropdownSettings_model = {
        text: "Select a model",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "partno_model",
        noDataLabel: "No partno is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
      this.dropdownSettings_workstation = {
        text: "Select a workstation",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "workstation_operation_code",
        noDataLabel: "No Workstation is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };

  }; 
  getselectedmodel(item:any)
  {
    this.selectedmodel=item.id;
  }
  getselectedworkstation(item:any)
  {
    this.selectedworkstation=item.id
  }
  export()
{
   var table25=$('#example').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.example.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Model Wise SOP report.xlsx');
   $('#example').DataTable();
}
  getmodel(event)
  {
    var table=$('#example').DataTable();
    this.choosetype=event;
    this.choosemodel=[];
    var data: { switchcase: string,id:string} = { switchcase: "checkproduction",id:event};
    var method = "post";
    var url = "getsopmodel";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {     
        
          if (datass.code == 201) {
            this.modeldata=datass;
            this.dropdownList_model=datass.message;
            //this.details="";
            this.rev="";
            this.rev_date="";
            this.ppe="";
            this.preparedby="";
            this.checkedby="";
            this.approvedby=""; 
            table.destroy();   
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
     // this.getworkstation(event);
     
  }
  getworkstation(value){
    this.choosewsmodel=[];
     var data: { switchcase: string,id:string,category:string} = { switchcase: "getworkstation",id:this.choosetype,category:value};    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            this.dropdownList_workstation=datass.message;
            this.workstation=datass;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  getdetails(frm:NgForm)
  {
    var table=$('#example').DataTable();
    var data=frm.value;
    var method = "post";
    var url = "getsopdetail";
    var selectedWorkStation="selectedWorkStation";
    var selectedModel="selectedModel";
   // data[switchcase] = value;
    data[selectedWorkStation] = this.selectedworkstation;
    data[selectedModel] = this.selectedmodel;
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            this.details=datass.message.sop;
            this.rev=datass.message.productvariant[0].rev_no;
            this.rev_date=datass.message.productvariant[0].rev_date;
            this.ppe=datass.message.productvariant[0].PPE;
            this.preparedby=datass.message.productvariant[0].prepared_by;
            this.checkedby=datass.message.productvariant[0].Checked_by;
            this.approvedby=datass.message.productvariant[0].approved_by;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
          table.destroy();
          setTimeout(() => {
            $('#example').DataTable();
          }, 1000);
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  showimage(image){
    //  alert(image);
      this.processimage = environment.file_url + image;
    };
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }
    open(addNew) {
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
      //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
    };
    openbulkupload(bulkupload) {
      this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    };
}
