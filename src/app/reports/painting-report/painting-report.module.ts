import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { PaintingReportComponent } from './painting-report.component';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
export const paintingportRoutes: Routes = [
  {
    path: '',
    component: PaintingReportComponent,
    data: {
      breadcrumb: 'Painting Report',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(paintingportRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [PaintingReportComponent]
})
export class PaintingReportModule { }
