import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef  } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-painting-report',
  templateUrl: './painting-report.component.html',
  styleUrls: ['./painting-report.component.css']
})
export class PaintingReportComponent implements OnInit {
  @ViewChild('table1') table1: ElementRef;
  dtOptions: any = {};
  public selectedmodelno:string='';
  public propductmodel:any={};
  public contractmaster:any={};
  public dropdownSettings = {};  
   
  //public dropdownList = [];
  public dropdownList :{ id: string, partno_mixer_model: string }[] = []; 
  public details:string = "";
  public statuss:string="";
  public dropdownSettingscontractor = {};   
  //public dropdownListcontractor = [];
  public dropdownListcontractor:{ id: string, contrctor_name_id: string }[] = [];
  public selectedcontractor:string="";
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
     /* var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
  this.from_mindate ={year: yyyy, month: 1, day: 1};
  this.from_maxdate ={year: yyyy, month: 12, day: 31};
  this.to_mindate ={year: yyyy, month: 1, day: 1};
  this.to_maxdate ={year: yyyy, month: 12, day: 31};*/
  }
  public durations:string="";
  ngOnInit() {
    this.durations="";
    // $('.datatable').DataTable();
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };
    $('#exampletable').DataTable();
      this.dropdownSettings = {
        text: "Select a mixer model",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "partno_mixer_model",
        noDataLabel: "No mixer models available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
      this.dropdownSettingscontractor = {
        text: "Select a Contractor",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "contrctor_name_id",
        noDataLabel: "No Contractors available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    this.getdetails();
    this.getcontractmaster();

  }
  export()
  {
    var table25=$('#exampletable').DataTable();
    table25.destroy(); 
   const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table1.nativeElement);
   const wb: XLSX.WorkBook = XLSX.utils.book_new();
   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
   XLSX.writeFile(wb, 'Painting report.xlsx');
    $('#exampletable').DataTable();
  }
  onmixermodelselect(item:any)
  {
    this.selectedmodelno=item.id;
  }
  oncontractorselect(item:any)
  {
    this.selectedcontractor=item.id;
  }
  getdetails()
  {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mixerassemblymaster";
    this.dropdownList=[];
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
         //   this.propductmodel = datas;
        
         if(datas.message.length>0)
         {
this.dropdownList.push({"id": "All","partno_mixer_model":"All"})
         }
         for(let i=0;i<datas.message.length;i++ )
         {
this.dropdownList.push({"id":datas.message[i]['id'],"partno_mixer_model":datas.message[i]['partno_mixer_model']})
         }
       
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  getcontractmaster(){
    this.dropdownListcontractor=[];
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "getcontractdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
           // this.contractmaster = datas;
           if(datas.message.length>0)
           {
            this.dropdownListcontractor.push({ id: "All", contrctor_name_id: "All" });
            for(let k=0;k<datas.message.length;k++)
            {
              this.dropdownListcontractor.push({ id: datas.message[k]['id'], contrctor_name_id: datas.message[k]['contrctor_name_id'] });
            }
           }
           //this.dropdownListcontractor=datas.message;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getproduction(frm:NgForm){
    var table= $('#exampletable').DataTable();
    var data = frm.value;
    var switchcase = "switchcase";
    var selectedmodel = "selectedmodel";
    var selectedcontractor = "selectedcontractor";
    var value = "getpaintedreport";
    data[switchcase] = value;
    data[selectedmodel] = this.selectedmodelno;
    data[selectedcontractor] = this.selectedcontractor;
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            this.details=datass.message;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
          table.destroy(); 
          setTimeout(() => {
            $('#exampletable').DataTable();
          }, 1000);
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }

}
