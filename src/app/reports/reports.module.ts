import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { CommonModule } from '@angular/common';
import { reportsRoutes } from './reports.routing';
import { SharedModule } from '../shared/shared.module';
//import { ExportAsModule } from 'ngx-export-as';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(reportsRoutes),
  //  ExportAsModule
  ],
  declarations: [ReportsComponent]
})
export class ReportsModule { }
