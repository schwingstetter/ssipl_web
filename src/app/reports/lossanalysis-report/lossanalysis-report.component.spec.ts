import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LossanalysisReportComponent } from './lossanalysis-report.component';

describe('LossanalysisReportComponent', () => {
  let component: LossanalysisReportComponent;
  let fixture: ComponentFixture<LossanalysisReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LossanalysisReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LossanalysisReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
