import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LossanalysisReportComponent } from './lossanalysis-report.component';
import { SharedModule } from '../../shared/shared.module';

export const lossanalysisReportRoutes: Routes = [
  {
    path: '',
    component: LossanalysisReportComponent,
    data: {
      breadcrumb: 'Loss Analysis Report',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(lossanalysisReportRoutes),
    SharedModule     
  ],
  declarations: [LossanalysisReportComponent]
})
export class LossanalysisReportModule { }
