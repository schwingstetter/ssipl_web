import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlnoTrackingComponent } from './slno-tracking.component';

describe('SlnoTrackingComponent', () => {
  let component: SlnoTrackingComponent;
  let fixture: ComponentFixture<SlnoTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlnoTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlnoTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
