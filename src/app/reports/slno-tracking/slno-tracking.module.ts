import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { SlnoTrackingComponent } from './slno-tracking.component';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AjaxService } from '../../ajax.service';
export const slnorportRoutes: Routes = [
  {
    path: '',
    component: SlnoTrackingComponent,
    data: {
      breadcrumb: 'SL No Tracking',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(slnorportRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [SlnoTrackingComponent]
})
export class SlnoTrackingModule { 

  
}
