import { Component, OnInit, ViewContainerRef,ViewChild, ElementRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AjaxService } from '../../ajax.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-slno-tracking',
  templateUrl: './slno-tracking.component.html',
  styleUrls: ['./slno-tracking.component.css']
})
export class SlnoTrackingComponent implements OnInit {
@ViewChild('table1') table1: ElementRef;
@ViewChild('table2') table2: ElementRef;
@ViewChild('table3') table3: ElementRef;
  dtOptions: any = {};
  public dropdownSettings = {};   
  public dropdownList:{ basedon: string, created_at: string, id: string , list_of_operation: string, operation_code: string, process_category:string, process_flow:string, product_group: string,updated_at: string }[] = [];
  /*basedon: "1"
created_at: null
id: 50
list_of_operation: "Cone 1 spiral fixing & welding"
operation_code: "FAB 08"
process_category: "8"
process_flow: "A,B,C,D,E"
product_group: 1
updated_at: null*/
  public dropdownSettings1 = {};   
  public dropdownListslno = [];
  public selected_work_order: string = "";
  public selected_product_type: string = "";
  public selected_slno: string ="";
  public tabledata=[];
  public tabledata1=[];
  public tabledata2=[];
  drumslnomodel=[];
  formerror=false;
  chooswss=[];
  drumslnotable:boolean=false;
  public choosep:string;
  //public chooswss:any={};
  //public drumslnomodel:any={};
  constructor(public http: Http,public AjaxService:AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.choosep="";
    $('.datatable').DataTable();
      $('.datatable1').DataTable();
       $('.datatable2').DataTable();
    this.dropdownSettings1 = {
        text: "Select a Sl no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "slno",
        labelKey: "slno",
        noDataLabel: "No Sl no is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
      this.dropdownSettings = {
        text: "Select a workstation",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "operation_code",
        noDataLabel: "No workstation is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
   /* this.dtOptions = {
      // Declare the use of the extension in the dom parameter
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ]
    };*/
  }; 
  export()
  {
      var table25=$('#table1').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Slno tracking.xlsx');
   $('#table1').DataTable();
  }
  export1()
  {
       var table25=$('#table2').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table2.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Slno tracking.xlsx');
   $('#table2').DataTable();
  }
  export2()
  {
       var table25=$('#table3').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table3.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Slno tracking.xlsx');
   $('#table3').DataTable();
  }
  showtable()
  {
    var table=$('.datatable').DataTable();
    var table1=$('.datatable1').DataTable();
    var table2=$('.datatable2').DataTable();
     
    var data: { producttype: string, workstation: string, slno:string } = { producttype: this.selected_product_type ,workstation: this.selected_work_order, slno:this.selected_slno };
      var method = "post";
      var url = "getslnotrackingdata";
      
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            switch(this.selected_product_type) 
            {
                case '1':
                this.drumslnotable=true;
                  this.tabledata = datas.data;
                                table.destroy(); 
                          setTimeout(() => {
                            $('.datatable').DataTable();
                          }, 1000);
                  break;
                case '2':
                 this.drumslnotable=false;
                  this.tabledata1 = datas.data;
                                table1.destroy(); 
                          setTimeout(() => {
                            $('.datatable1').DataTable();
                          }, 1000);
                  break;
              case '3':
               this.drumslnotable=false;
                    this.tabledata2 = datas.data;
                    table2.destroy(); 
                          setTimeout(() => {
                            $('.datatable2').DataTable();
                          }, 1000);
                  break;
            }  
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  onworkstationselect(item:any)
  {
      this.selected_work_order = item.id;
     // this.selected_product_type=event.target.value;

  }
  onslnoselect(item:any)
  {
    this.selected_slno = item.slno;
  }
  getslno()
  {
this.dropdownListslno=[];
this.drumslnomodel=[];
var data: { producttype: string } = { producttype: this.selected_product_type };
      var method = "post";
      var url = "getslnolist";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
               this.dropdownListslno = datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
getworkstations(event)
{
//console.log(event.target.value);
//this.chooswss={};
//this.drumslnomodel={};
this.selected_product_type=event.target.value;
this.getslno();
this.dropdownList=[];
this.chooswss=[];
var data: { producttype: string } = { producttype: event.target.value };
      var method = "post";
      var url = "getworkstations";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if(datas.data.length>0)
            {
             
              this.dropdownList.push({basedon: "", created_at: "", id: '0' , list_of_operation: "", operation_code: "All", process_category:"", process_flow:"", product_group: "",updated_at: ""});
             for(let i=0;i<datas.data.length;i++)
             {
this.dropdownList.push({basedon: datas.data[i]['basedon'], created_at: datas.data[i]['created_at'], id: datas.data[i]['id'] , list_of_operation: datas.data[i]['list_of_operation'], operation_code: datas.data[i]['operation_code'], process_category:datas.data[i]['process_category'], process_flow:datas.data[i]['process_flow'], product_group: datas.data[i]['product_group'],updated_at: datas.data[i]['updated_at']});
             }
             // this.dropdownList.push(datas.data)
            }
              // this.dropdownList=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
}


}
