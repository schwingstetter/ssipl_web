import { Routes } from '@angular/router';


export const reportsRoutes: Routes = [
    {
        path: '',
        data: {
            /* breadcrumb: 'Reports Management', */
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'productionreport',
                loadChildren: './production-report/production-report.module#ProductionReportModule'
            },{
                path: 'rawmaterialconsumption',
                loadChildren: './rawmaterialconsume-report/rawmaterialconsume-report.module#RawmaterialconsumeReportModule'
            },{
                path: 'paintingreport',
                loadChildren: './painting-report/painting-report.module#PaintingReportModule'
            },{
                path: 'wipreport',
                loadChildren: './wip-report/wip-report.module#WipReportModule'
            },{
                path: 'productivityreport',
                loadChildren: './productivity-report/productivity-report.module#ProductivityReportModule'
            }, {
                path: 'machineruntimereport',
                loadChildren: './machineruntime-report/machineruntime-report.module#MachineruntimeReportModule'
            },{
                path: 'materialconsumptionreport',
                loadChildren: './materialconsume-report/materialconsume-report.module#MaterialconsumeReportModule'
            }, {
                path: 'qareports',
                loadChildren: './qa-report/qa-report.module#QaReportModule'
            }, {
                path: 'planvsactualproduction',
                loadChildren: './palvsactual-report/palvsactual-report.module#PalvsactualReportModule'
            }, {
                path: 'lossanalysis',
                loadChildren: './lossanalysis-report/lossanalysis-report.module#LossanalysisReportModule'
            },{
                path: 'slnotracking',
                loadChildren: './slno-tracking/slno-tracking.module#SlnoTrackingModule'
            },
            {
                path: 'snagreport',
                loadChildren: './snag-report/snag-report.module#SnagReportModule'
            },{
                path: 'mixerreport',
                loadChildren: './mixer-report/mixer-report.module#MixerReportModule'
            },{
                path: 'productdeliveryreport',
                loadChildren: './productdelivery-report/productdelivery-report.module#ProductdeliveryReportModule'
            },{
                path: 'modelwisesopreport',
                loadChildren: './modelwisesop-report/modelwisesop-report.module#ModelwisesopReportModule'
            },                 
                         
        ]
    }
];