import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixerReportComponent } from './mixer-report.component';

describe('MixerReportComponent', () => {
  let component: MixerReportComponent;
  let fixture: ComponentFixture<MixerReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixerReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixerReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
