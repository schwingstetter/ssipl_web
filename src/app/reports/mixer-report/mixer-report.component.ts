import { Component, OnInit, ViewContainerRef,ViewChild, ElementRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AjaxService } from '../../ajax.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-mixer-report',
  templateUrl: './mixer-report.component.html',
  styleUrls: ['./mixer-report.component.css']
})
export class MixerReportComponent implements OnInit {

  dtOptions: any = {};
 
  @ViewChild('mixertable') mixertable: ElementRef;
  //  public fromdatemodel:any;
  public from_mindate : any;
  public from_maxdate : any;
  public to_mindate : any;
  public to_maxdate : any;
  public todatemodel:any;
  public fromdatemodel:any;
  public productiondurations:string="";
  public selectedpartno="";
  public dropdownSettings = {};   
  public dropdownList = [];
  public tabledata=[];
  fromerror=false;
  
  constructor(public http: Http,public AjaxService:AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
      var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
  this.from_mindate ={year: yyyy, month: 1, day: 1};
  this.from_maxdate ={year: yyyy, month: 12, day: 31};
  this.to_mindate ={year: yyyy, month: 1, day: 1};
  this.to_maxdate ={year: yyyy, month: 12, day: 31};
  }
  ngOnInit() {
    
    $('.datatable1').DataTable();
    this.dtOptions = {
      // Declare the use of the extension in the dom parameter
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ]
    };
    this.dropdownSettings = {
        text: "Select a part no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "part_no",
        noDataLabel: "No part no is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
      this.getpartno();
  }; 
   export()
{
   var table25=$('#mixertable').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.mixertable.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Mixer report.xlsx');
   $('#mixertable').DataTable();
}
  fromdatechanged(event)
  {
    this.to_mindate=event;

    this.to_maxdate ={year: this.to_mindate.year, month: 12, day: 31};
  }
  onpartnoselect(item:any){
   
     console.log(item.id);
    this.selectedpartno=item.id;
   }
   reortDuratioChange(event)
   {
      console.log(event.target.value)
      if(event.target.value==1)//Historical
      {
            var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
  this.from_mindate ={year: 2007, month: 1, day: 1};
  this.from_maxdate ={year: yyyy, month: 12, day: 31};
  this.to_mindate ={year: 2007, month: 1, day: 1};
  this.to_maxdate ={year: yyyy, month: 12, day: 31};
      }
      else{
 var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
  this.from_mindate ={year: 2007, month: 1, day: 1};
  this.from_maxdate ={year: yyyy, month: 12, day: 31};
  this.to_mindate ={year: 2007, month: 1, day: 1};
  this.to_maxdate ={year: yyyy, month: 12, day: 31};
      }
   }
   showtable()
   {
     console.log(this.fromdatemodel);
     if((!this.fromdatemodel.year) && (!this.todatemodel.year) && (this.productiondurations=='') && (this.selectedpartno==''))
     {
      this.toastr.error("Warning","empty");
     }
     else{
var table=$('.datatable1').DataTable();
     let fromdate:string=this.fromdatemodel.year+'-'+this.fromdatemodel.month+'-'+this.fromdatemodel.day;
     let todate:string=this.todatemodel.year+'-'+this.todatemodel.month+'-'+this.todatemodel.day;
  var data: { switchcase: string,fromdate:string,todate:string,calendartype:string,partno:string } = { switchcase: "get",fromdate:fromdate,todate:todate,calendartype:this.productiondurations,partno:this.selectedpartno };
      var method = "post";
      var url = "getmixerreport";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
               this.tabledata=datas.data;
                 table.destroy(); 
            setTimeout(() => {
              $('.datatable1').DataTable();
            }, 1000);
              
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
     }
      
   }
  getpartno()
  {
 var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "getpartno_mixerreport";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
               this.dropdownList=datas.data;
               console.log(this.dropdownList)
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
}
