import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { MixerReportComponent } from './mixer-report.component';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
export const mixerreportRoutes: Routes = [
  {
    path: '',
    component: MixerReportComponent,
    data: {
      breadcrumb: 'Mixer Report',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(mixerreportRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [MixerReportComponent]
})
export class MixerReportModule { }
