import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var Morris: any;

@Component({
  selector: 'app-materialconsume-report',
  templateUrl: './materialconsume-report.component.html',
  styleUrls: ['./materialconsume-report.component.css']
})
export class MaterialconsumeReportComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      Morris.Area({
        element: 'area-example',
        data: [
          { y: '2006', a: 100, b: 90 },
          { y: '2007', a: 75, b: 65 },
          { y: '2008', a: 50, b: 40 },
          { y: '2009', a: 75, b: 65 },
          { y: '2010', a: 50, b: 40 },
          { y: '2011', a: 75, b: 65 },
          { y: '2012', a: 100, b: 90 }
        ],
        xkey: 'y',
        resize: true,
        redraw: true,
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        lineColors: ['#93EBDD', '#64DDBB']
      });
    }, 1);    
  }

}
