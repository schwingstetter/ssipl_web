import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialconsumeReportComponent } from './materialconsume-report.component';

describe('MaterialconsumeReportComponent', () => {
  let component: MaterialconsumeReportComponent;
  let fixture: ComponentFixture<MaterialconsumeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialconsumeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialconsumeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
