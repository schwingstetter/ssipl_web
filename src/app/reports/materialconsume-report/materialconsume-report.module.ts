import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MaterialconsumeReportComponent } from './materialconsume-report.component';
import { SharedModule } from '../../shared/shared.module';

export const materialconsumereportRoutes: Routes = [
  {
    path: '',
    component: MaterialconsumeReportComponent,
    data: {
      breadcrumb: 'Material Consume Report',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(materialconsumereportRoutes),
    SharedModule
  ],
  declarations: [MaterialconsumeReportComponent]
})
export class MaterialconsumeReportModule { }
