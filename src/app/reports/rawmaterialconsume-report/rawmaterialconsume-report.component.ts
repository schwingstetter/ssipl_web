// import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef } from '@angular/core';
// import { NgForm } from '@angular/forms';
// //import { Http, Headers, HttpModule } from "@angular/http";
// import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import swal from 'sweetalert2';
// import { AjaxService } from '../../ajax.service';
// import { AuthService } from '../../auth.service';
// import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
// import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
// import * as $ from 'jquery';
// import 'datatables.net';
// import 'datatables.net-bs4';
// import { ToastsManager } from 'ng2-toastr/ng2-toastr';
// import { Router } from '@angular/router'
// import { json } from 'd3';
// import { decode } from 'punycode';
// import { environment } from './../../../environments/environment';
// import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';
// import * as XLSX from 'xlsx';
// //import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
// @Component({
//   selector: 'app-rawmaterialconsume-report',
//   templateUrl: './rawmaterialconsume-report.component.html',
//   styleUrls: ['./rawmaterialconsume-report.component.css']
// })
// export class RawmaterialconsumeReportComponent implements OnInit {
//   @ViewChild('table1') table1: ElementRef;
// @ViewChild('table2') table2: ElementRef;
//   dtOptions: any = {};
//   partno: string = "";
//   startDate: Date ;
//   endDate:Date;
//   fromdate: string ;
//   todate:string;
//   scrap_weight:string;
//   scrap_percentage:string;
//   searchpartno: string = "";
//   rmdetails: string ="";
//   choosep: string ="";
//   runningmtrdetails: string ="";
//   productiondurations: string ="";
//   datepickermindate :any;
//   public mindate:any={year: 2021, month: 1, day: 1};
// public maxdate:any={year: 2019, month: 12, day: 31};
// public hist_mindate:any={year: 2021, month: 1, day: 1};
// public hist_maxdate:any={year: 2019, month: 12, day: 31};

// public dropdownSettings_partno = {};   
 
//    public dropdownList_partno:{ id: string, partno_description: string }[] = [];
//   public selectedpartno:string="";

//   constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal,config: NgbDatepickerConfig, private calendar: NgbCalendar) {
//     this.toastr.setRootViewContainerRef(vcr);
//     // var today = new Date();
//     // var dd = today.getDate();
//     // var mm = today.getMonth() + 1; //January is 0!
//     // var yyyy = today.getFullYear();
//     // var lastdays=new Date(yyyy, mm +1, 0).getDate();
//     // config.minDate = { year: yyyy, month: mm,day: 1};
//     // config.maxDate = { year: yyyy, month: 11,day: 31};
//    var today = new Date();
//    //this.current_quarter = Math.floor((today.getMonth() + 3) / 3);
//    this.startDate = new Date(2019, 0, 1);
//  //  this.datepickermindate= { "year": (new Date()).getFullYear(), "month": ((new Date()).getMonth()), "day": 1 }
//  // = new Date(2019, 11, 0);
//   //this.endDate = new Date();
//   }

//   ngOnInit() {
//     $('#tableexample').DataTable();
//     $('#tableexample1').DataTable();
//     // this.dtOptions = {
//     //   // Declare the use of the extension in the dom parameter
//     //   dom: 'Bfrtip',
//     //   // Configure the buttons
//     //   buttons: [
//     //     'excel',
//     //   ]
//     // };
//        this.dropdownSettings_partno = {
//         text: "Select a part no",
//         selectAllText: 'Select All',
//         unSelectAllText: 'UnSelect All',
//         classes: "singledropdowncustom",
//         primaryKey: "id",
//         labelKey: "partno_description",
//         noDataLabel: "No part no is available",
//         enableSearchFilter: true,
//         showCheckbox: false,
//         singleSelection:true,
//         disabled: false
//       };
//     var today = new Date();
//     var dd = today.getDate();
//     var mm = today.getMonth() + 1; //January is 0!
//     var yyyy = today.getFullYear();

//    // var lastday=this.lastday(yyyy,mm);
//     //var lastdays=new Date(yyyy, mm +1, 0).getDate();
//    this.mindate = { year: yyyy, month: 1, day: 1 };
//    this.maxdate = { year: yyyy, month: 12, day: 31 };
//    this.hist_mindate = { year: 2019, month: 1, day: 1 };
//    this.hist_maxdate = { year: yyyy, month: 12, day: 31 };
//       var data: { switchcase: string } = { switchcase: "getpartno" };
//       var method = "post";
//       var url = "cncsegment";
//       this.dropdownList_partno=[];
//      //  this.dropdownList_workstation=[];
//       this.AjaxService.ajaxpost(data, method, url)
//         .subscribe(
//           (datas) => {
//             if (datas.code == 201) {
//             if(datas.message.length>0)
//               {
//                 this.dropdownList_partno.push({id:"ALL" , partno_description: "ALL"});
//                 for(let i=0;i<datas.message.length;i++)
//                 {
//                   let temp=datas.message[i]['raw_partno'] + "(" +datas.message[i]['description'] + ")";
//                 this.dropdownList_partno.push({id:datas.message[i]['id'] , partno_description: temp});
//                 }
//               }
             
//              // this.partno = datas.message;       
//             } else {
//             }
//           },
//           (err) => {
//             if (err.status == 403) {
//               alert(err._body);
//             }
//           }
//         );
      
    
//   }
//   getselectedpartno(item:any)
//   {
//     this.selectedpartno=item.id;
//     this.get_partno(item.id);
//   }
//    export()
//   {
//       var table25=$('#tableexample').DataTable();
//    table25.destroy(); 
//   const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table1.nativeElement);
//   const wb: XLSX.WorkBook = XLSX.utils.book_new();
//   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
//   XLSX.writeFile(wb, 'Raw material consumption report.xlsx');
//    $('#tableexample').DataTable();
//   }
//   export1()
//   {
//        var table25=$('#tableexample1').DataTable();
//    table25.destroy(); 
//   const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table2.nativeElement);
//   const wb: XLSX.WorkBook = XLSX.utils.book_new();
//   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
//   XLSX.writeFile(wb, 'Raw material consumption report.xlsx');
//    $('#tableexample1').DataTable();
//   }
//   getduration(event)
//   {
//     this.productiondurations=event;
//   }
//   getmodel(event)
//   {
   
//     this.choosep=event;
//     console.log(this.choosep);
//   }
//   onChange(event)
//   {
//   //  alert(event);
//   this.hist_mindate = { year: event.year, month: 1, day: 1 };
//   this.hist_maxdate = { year: event.year, month: 12, day: 31 };
//   }
//   getdetails()
//   {
//    var table = $('#tableexample').DataTable();
//    var table1 = $('#tableexample1').DataTable();
//     var data: { switchcase: string,partno:string,fromdate:string,todate:string } = { switchcase: "getrmdetails",partno:this.searchpartno,fromdate:this.fromdate,todate:this.todate };
//     var method = "post";
//     var url = "rmreport";
//     this.AjaxService.ajaxpost(data, method, url)
//       .subscribe(
//         (datas) => {
//           if (datas.code == 201) {            
//             this.rmdetails = datas.message;  
//             for(var i=0;i<datas.message.length;i++)    
//             {
//               datas.message[i].create_at=datas.message[i].create_at.substring(0, 10);  
//             }
//           } else {
//           }
//           table.destroy(); 
//           setTimeout(() => {
//             $('#tableexample').DataTable();
//           }, 1000);
//           table1.destroy(); 
//           setTimeout(() => {
//             $('#tableexample1').DataTable();
//           }, 1000);
//         },
//         (err) => {
//           if (err.status == 403) {
//             alert(err._body);
//           }
//         }
//       );
//       this.getrunningmtrdetails();
//   }
//   getrunningmtrdetails()
//   {
//     var table = $('#tableexample1').DataTable();
//     var data: { switchcase: string,partno:string,fromdate:string,todate:string } = { switchcase: "getrunnungmtrdetails",partno:this.searchpartno,fromdate:this.fromdate,todate:this.todate };
//     var method = "post";
//     var url = "rmreport";
//     this.AjaxService.ajaxpost(data, method, url)
//       .subscribe(
//         (datas) => {
//           if (datas.code == 201) {
            
//             this.runningmtrdetails = datas.message;  
//             for(var i=0;i<datas.message.length;i++) 
//             {
//               datas.message[i].create_at=datas.message[i].create_at.substring(0, 10);  
//             }
//           } else {
//           }
//           table.destroy(); 
//           setTimeout(() => {
//             $('#tableexample1').DataTable();
//           }, 1000);
//         },
//         (err) => {
//           if (err.status == 403) {
//             alert(err._body);
//           }
//         }
//       );
//   }
//   get_partno(id)
//   {
//     this.searchpartno = id;
//   }
//   getfromdate(event)
//   {
//     this.fromdate = event;
//   }
//   gettodate(event)
//   {
//     this.todate = event;
//   }
// }
import { Component, OnInit, ViewContainerRef, Input,ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
//import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'round'})
export class RoundPipe implements PipeTransform {
    /**
     *
     * @param value
     * @returns {number}
     */
    transform(value: number): number {
        return Math.round(value);
    }
}
@Component({
  selector: 'app-rawmaterialconsume-report',
  templateUrl: './rawmaterialconsume-report.component.html',
  styleUrls: ['./rawmaterialconsume-report.component.css']
})
export class RawmaterialconsumeReportComponent implements OnInit {
  @ViewChild('table1') table1: ElementRef;
@ViewChild('table2') table2: ElementRef;
  dtOptions: any = {};
  partno: string = "";
  startDate: Date ;
  endDate:Date;
  fromdate: string ;
  todate:string;
  scrap_weight:string;
  scrap_percentage:string;
  searchpartno: string = "";
  rmdetails: string ="";
  choosep: string ="";
  runningmtrdetails: string ="";
  productiondurations: string ="";
  datepickermindate :any;
  public mindate:any={year: 2021, month: 1, day: 1};
public maxdate:any={year: 2019, month: 12, day: 31};
public hist_mindate:any={year: 2021, month: 1, day: 1};
public hist_maxdate:any={year: 2019, month: 12, day: 31};

public dropdownSettings_partno = {};   
 
   public dropdownList_partno:{ id: string, partno_description: string }[] = [];
  public selectedpartno:string="";
choosepartno=[];
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal,config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    // var today = new Date();
    // var dd = today.getDate();
    // var mm = today.getMonth() + 1; //January is 0!
    // var yyyy = today.getFullYear();
    // var lastdays=new Date(yyyy, mm +1, 0).getDate();
    // config.minDate = { year: yyyy, month: mm,day: 1};
    // config.maxDate = { year: yyyy, month: 11,day: 31};
   var today = new Date();
   //this.current_quarter = Math.floor((today.getMonth() + 3) / 3);
   this.startDate = new Date(2019, 0, 1);
 //  this.datepickermindate= { "year": (new Date()).getFullYear(), "month": ((new Date()).getMonth()), "day": 1 }
 // = new Date(2019, 11, 0);
  //this.endDate = new Date();
  }

  ngOnInit() {
   // $('#tableexample').DataTable();
   // $('#tableexample1').DataTable();
   
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };
       this.dropdownSettings_partno = {
        text: "Select a part no",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "partno_description",
        noDataLabel: "No part no is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

   // var lastday=this.lastday(yyyy,mm);
    //var lastdays=new Date(yyyy, mm +1, 0).getDate();
   this.mindate = { year: 2017, month: 1, day: 1 };
   this.maxdate = { year: yyyy, month: 12, day: 31 };
   this.hist_mindate = { year: 2017, month: 1, day: 1 };
   this.hist_maxdate = { year: yyyy, month: 12, day: 31 };
      var data: { switchcase: string } = { switchcase: "getpartno" };
      var method = "post";
      var url = "cncsegment";
      this.dropdownList_partno=[];
      this.choosepartno=[];
     //  this.dropdownList_workstation=[];
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
            if(datas.message.length>0)
              {
                this.dropdownList_partno.push({id:"ALL" , partno_description: "ALL"});
                for(let i=0;i<datas.message.length;i++)
                {
                  let temp=datas.message[i]['raw_partno'] + "(" +datas.message[i]['description'] + ")";
                this.dropdownList_partno.push({id:datas.message[i]['id'] , partno_description: temp});
                }
              }
             
             // this.partno = datas.message;       
            } else {
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
      
    
  }
  getselectedpartno(item:any)
  {
    this.selectedpartno=item.id;
    this.get_partno(item.id);
  }
   export()
  {
      var table25=$('#tableexample').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Raw material consumption report.xlsx');
   $('#tableexample').DataTable();
  }
  export1()
  {
       var table25=$('#tableexample1').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table2.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Raw material consumption report.xlsx');
   $('#tableexample1').DataTable();
  }
  getduration(event)
  {
    this.productiondurations=event;
  }
  getmodel(event)
  {
    this.choosep=event;
   // console.log(this.choosep);
  /*var table13= $('.datatable2').DataTable();
  table13.destroy();
  $('.datatable2').DataTable();
*/
if(this.choosep=='1')
{
var table13= $('#tableexample').DataTable();
  table13.destroy();
 setTimeout(() => {
           $('#tableexample').DataTable();
          }, 400);
}
else{
var table13= $('#tableexample1').DataTable();
  table13.destroy();
  $('#tableexample1').DataTable();

setTimeout(() => {
           $('#tableexample1').DataTable();
          }, 400);


}
  // $('#tableexample1').DataTable();
  //$('#tableexample').DataTable();
  }
  onChange(event)
  {
  //  alert(event);
  this.hist_mindate = { year: event.year, month: 1, day: 1 };
  this.hist_maxdate = { year: event.year, month: 12, day: 31 };
  }
  getdetails()
  {
   var table = $('#tableexample').DataTable();
  // var table1 = $('#tableexample1').DataTable();
    var data: { switchcase: string,partno:string,fromdate:string,todate:string } = { switchcase: "getrmdetails",partno:this.searchpartno,fromdate:this.fromdate,todate:this.todate };
    var method = "post";
    var url = "rmreport";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {            
            this.rmdetails = datas.message;  
            for(var i=0;i<datas.message.length;i++)    
            {
              datas.message[i].create_at=datas.message[i].create_at.substring(0, 10);  
            }
          } else {
          }
        
         table.destroy(); 
          setTimeout(() => {
            $('#tableexample').DataTable();
          }, 1000);
         
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getrunningmtrdetails();
  }
  getrunningmtrdetails()
  {
    var table = $('#tableexample1').DataTable();
    var data: { switchcase: string,partno:string,fromdate:string,todate:string } = { switchcase: "getrunnungmtrdetails",partno:this.searchpartno,fromdate:this.fromdate,todate:this.todate };
    var method = "post";
    var url = "rmreport";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            
            this.runningmtrdetails = datas.message;  
            for(var i=0;i<datas.message.length;i++) 
            {
              datas.message[i].create_at=datas.message[i].create_at.substring(0, 10);  
            }
          } else {
          }
          table.destroy(); 
          setTimeout(() => {
            $('#tableexample1').DataTable();
          }, 1000);
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  get_partno(id)
  {
    this.searchpartno = id;
  }
  getfromdate(event)
  {
    this.fromdate = event;
  }
  gettodate(event)
  {
    this.todate = event;
  }
}