import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RawmaterialconsumeReportComponent } from './rawmaterialconsume-report.component';
import { SharedModule } from '../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
//import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
export const rawmaterialroutes: Routes = [
  {
    path: '',
    component: RawmaterialconsumeReportComponent,
    data: {
      breadcrumb: 'Raw Material Consumption Report',
    status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(rawmaterialroutes),
    SharedModule,
    AngularMultiSelectModule 
   //OwlDateTimeModule, 
  //  OwlNativeDateTimeModule
  ],
  declarations: [[RawmaterialconsumeReportComponent]]
  
})
export class RawmaterialconsumeReportModule  { }
