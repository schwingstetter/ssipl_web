import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawmaterialconsumeReportComponent } from './rawmaterialconsume-report.component';

describe('RawmaterialconsumeReportComponent', () => {
  let component: RawmaterialconsumeReportComponent;
  let fixture: ComponentFixture<RawmaterialconsumeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawmaterialconsumeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawmaterialconsumeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
