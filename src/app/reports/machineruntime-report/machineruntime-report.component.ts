import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var Morris: any;

@Component({
  selector: 'app-machineruntime-report',
  templateUrl: './machineruntime-report.component.html',
  styleUrls: ['./machineruntime-report.component.css']
})
export class MachineruntimeReportComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      Morris.Donut({
        element: 'donut-example',
        redraw: true,
        data: [          
          { label: 'Run Time', value: 50 },
          { label: 'Down Time', value: 20 }
        ],
        colors: ['#34495E', '#FF9F55']
      });
    }, 1);
  }

}
