import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineruntimeReportComponent } from './machineruntime-report.component';

describe('MachineruntimeReportComponent', () => {
  let component: MachineruntimeReportComponent;
  let fixture: ComponentFixture<MachineruntimeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineruntimeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineruntimeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
