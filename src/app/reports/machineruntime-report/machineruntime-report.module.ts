import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineruntimeReportComponent } from './machineruntime-report.component';
import { SharedModule } from '../../shared/shared.module';

export const machineruntimereportRoutes: Routes = [
  {
    path: '',
    component: MachineruntimeReportComponent,
    data: {
      breadcrumb: 'Machine Runtime Report',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(machineruntimereportRoutes),
    SharedModule  
  ],
  declarations: [MachineruntimeReportComponent]
})
export class MachineruntimeReportModule { }
