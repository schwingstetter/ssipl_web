import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductdeliveryReportComponent } from './productdelivery-report.component';

describe('ProductdeliveryReportComponent', () => {
  let component: ProductdeliveryReportComponent;
  let fixture: ComponentFixture<ProductdeliveryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductdeliveryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductdeliveryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
