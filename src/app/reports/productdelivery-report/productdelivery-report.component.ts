import { Component, OnInit, ViewContainerRef, ViewChild, ElementRef  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AjaxService } from '../../ajax.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-productdelivery-report',
  templateUrl: './productdelivery-report.component.html',
  styleUrls: ['./productdelivery-report.component.css']
})
export class ProductdeliveryReportComponent implements OnInit {
@ViewChild('table1') table1: ElementRef;
  dtOptions: any = {};
  public showtableboolean:boolean=false;
   public dropdownSettings = {};   
    public dropdownList = [];
    public dropdownListstage=[];
    public dropdownSettingsstage={};
    public from_mindate : any;
  public from_maxdate : any;
  public to_mindate : any;
  public to_maxdate : any;
  public todatemodel:any;
 public fromdatemodel:any;
  public selected_partno:string="";
  public selected_fromstage:string="";
  public selected_tostage:string="";
  public selected_fromdate:string="";
public selected_todate:string="";
public productiondurations:string="";
  public choosep:string = "Mixer Module";
  public tabledata=[];
  constructor(public http: Http,public AjaxService:AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    $('.datatable').DataTable();
    this.getpartno();
    this.getstagelist();
      var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
  this.from_mindate ={year: 2007, month: 1, day: 1};
  this.from_maxdate ={year: yyyy, month: 12, day: 31};
  this.to_mindate ={year: 2007, month: 1, day: 1};
  this.to_maxdate ={year: yyyy, month: 12, day: 31};
    /*this.dtOptions = {
      // Declare the use of the extension in the dom parameter
      dom: 'Bfrtip',
      // Configure the buttons
      buttons: [
        'excel',
      ]
    };*/
      this.dropdownSettings = {
        text: "Select a part no or mixer model",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "partno_with_model",
        noDataLabel: "No data available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
       this.dropdownSettingsstage = {
        text: "Select a workstation",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "operation_code",
        noDataLabel: "No workstation is available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
  }; 
  export()
{
   var table25=$('#table1').DataTable();
   table25.destroy(); 
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Product Delivery report.xlsx');
   $('#table1').DataTable();
}
   fromdatechanged(event)
  {
    this.to_mindate=event;
  }
   reortDuratioChange(event)
   {
      console.log(event.target.value)
      if(event.target.value==1)//Historical
      {
            var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
  this.from_mindate ={year: 2019, month: 1, day: 1};
  this.from_maxdate ={year: yyyy, month: 12, day: 31};
  this.to_mindate ={year: 2019, month: 1, day: 1};
  this.to_maxdate ={year: yyyy, month: 12, day: 31};
      }
      else{
 var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
  this.from_mindate ={year: yyyy, month: 1, day: 1};
  this.from_maxdate ={year: yyyy, month: 12, day: 31};
  this.to_mindate ={year: yyyy, month: 1, day: 1};
  this.to_maxdate ={year: yyyy, month: 12, day: 31};
      }
   }
  getpartno()
  {
this.dropdownList=[];
var data: { engineno: string } = { engineno: '0' };
      var method = "post";
      var url = "getpartno_productdeliveryreport";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
               this.dropdownList = datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  onfromstageselect(item:any)
  {
    console.log(item.id);
     this.selected_fromstage = item.id;
  
  }
  ontostageselect(item:any)
  {
    console.log(item.id);
    this.selected_tostage=item.id;
  }

  getstagelist()
  {
    this.dropdownList=[];
var data: { producttype: string } = { producttype: '2' };
      var method = "post";
      var url = "getworkstations";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
          /*  if(datas.data.length>0)
            {
              this.dropdownList.push({basedon: "", created_at: "", id: '0' , list_of_operation: "", operation_code: "All", process_category:"", process_flow:"", product_group: "",updated_at: ""});
             for(let i=0;i<datas.data.length;i++)
             {
this.dropdownList.push({basedon: datas.data[i]['basedon'], created_at: datas.data[i]['created_at'], id: datas.data[i]['id'] , list_of_operation: datas.data[i]['list_of_operation'], operation_code: datas.data[i]['operation_code'], process_category:datas.data[i]['process_category'], process_flow:datas.data[i]['process_flow'], product_group: datas.data[i]['product_group'],updated_at: datas.data[i]['updated_at']});
             }
            
            }*/
              this.dropdownListstage=datas.data;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
onpartnoselect(item:any)
{
console.log(item.id);
this.selected_partno = item.id;

 /*public todatemodel:any;
 public fromdatemodel:any;
  
  public selected_fromstage:string="";
  public selected_tostage:string="";
  public selected_fromdate:string="";
public selected_todate:string="";*/
}
showtable()
{
  this.showtableboolean=true;
  var table=$('#table1').DataTable();
     let fromdate:string=this.fromdatemodel.year+'-'+this.fromdatemodel.month+'-'+this.fromdatemodel.day;
     let todate:string=this.todatemodel.year+'-'+this.todatemodel.month+'-'+this.todatemodel.day;
  var data: { switchcase: string,fromdate:string,todate:string,calendartype:string,partno:string,fromstage:string,tostage:string } = { switchcase: "get",fromdate:fromdate,todate:todate,calendartype:this.productiondurations,partno:this.selected_partno,fromstage:this.selected_fromstage,tostage:this.selected_tostage };
      var method = "post";
      var url = "get_productivityreport";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
               this.tabledata=datas.data;
                 table.destroy(); 
            setTimeout(() => {
              $('#table1').DataTable();
            }, 1500);
              
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
}
}
