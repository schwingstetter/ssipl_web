import { Component, OnInit, ViewContainerRef,ViewChild, ElementRef, HostListener } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-production-report',
  templateUrl: './production-report.component.html',
  styleUrls: ['./production-report.component.css']
})
export class ProductionReportComponent implements OnInit {
@ViewChild('table1') table1: ElementRef;
@ViewChild('table2') table2: ElementRef;
@ViewChild('table3') table3: ElementRef;
@ViewChild('table4') table4: ElementRef;
@ViewChild('table5') table5: ElementRef;
@HostListener('click')
  dtOptions: any = {};
  public selectedworkstation:string="";
  public selectedmodel:string="";
  public details:string="";
  public chasismodel:string="";
  public choosep:string="0";
  public durations:string="0";
  public propductmodel:any={};
  public workstation:any={};
public mindate:any={year: 2021, month: 1, day: 1};
public maxdate:any={year: 2019, month: 12, day: 31};
public hist_mindate:any={year: 2021, month: 1, day: 1};
public hist_maxdate:any={year: 2019, month: 12, day: 31};
 public dropdownSettings_workstation = {};   
  //public dropdownList_workstation = [];
  public dropdownList_workstation:{ id: string, workstation_operation_code: string }[] = [];
  public dropdownSettings_model={};
 // public dropdownList_model=[];
 chooseworkstations=[];
 choosemodels=[];
  public dropdownList_model:{ id: string, partno_model: string }[] = [];
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    $('#exampletable').DataTable();
    // this.dtOptions = {
    //   // Declare the use of the extension in the dom parameter
    //   dom: 'Bfrtip',
    //   // Configure the buttons
    //   buttons: [
    //     'excel',
    //   ]
    // };
     this.dropdownSettings_workstation = {
        text: "Select a workstation",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "workstation_operation_code",
        noDataLabel: "No workstations available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
      this.dropdownSettings_model= {
        text: "Select a model",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "singledropdowncustom",
        primaryKey: "id",
        labelKey: "partno_model",
        noDataLabel: "No Models available",
        enableSearchFilter: true,
        showCheckbox: false,
        singleSelection:true,
        disabled: false
      };
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

   // var lastday=this.lastday(yyyy,mm);
    //var lastdays=new Date(yyyy, mm +1, 0).getDate();
   this.mindate = { year: 2007, month: 1, day: 1 };
   this.maxdate = { year: yyyy, month: 12, day: 31 };
   this.hist_mindate = { year: 2007, month: 1, day: 1 };
   this.hist_maxdate = { year: yyyy, month: 12, day: 31 };
   this.getchassismodel();
  }; 
   export()
{
  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table1.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Production report.xlsx');
}
   export1()
{
  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table2.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Production report.xlsx');
}
export2()
{
  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table3.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Production report.xlsx');
}
export3()
{
   const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table4.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Production report.xlsx');
}
export4()
{
   const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table5.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, 'Production report.xlsx');
}
  isDisabled(date: NgbDateStruct) {
    const d = new Date(date.year, date.month - 1, date.day);
    return d.getDay() === 0 ;
  }
  onworkstationselect(item:any)
  {
    this.selectedworkstation=item.id;
  }
  onmodelselect(item:any)
  {
    this.selectedmodel=item.id;
  }
  checkproduction(value){
    this.chooseworkstations=[];
    this.dropdownList_workstation=[];
    this.dropdownList_model=[];
    this.choosemodels=[];
    this.choosep="";
    this.choosep = value;
   // console.log(this.choosep);

   this.details="";
   if(this.choosep == '3' || this.choosep == '4')
   {
    var data: { switchcase: string,id:string} = { switchcase: "checkproduction",id:value};
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            this.propductmodel=datass.message;
         //   this.dropdownList_model=datass.message;

             if(datass.message.length>0)
            {
              this.dropdownList_model=[];
              this.dropdownList_model.push({id: "All", partno_model: "All"});
              for(let i=0;i<datass.message.length;i++)
              {
                console.log(datass.message[i])
                console.log(datass.message[i]['partno_model'])
              this.dropdownList_model.push({id: datass.message[i]['id'], partno_model: datass.message[i]['partno_model']});
              }
              console.log(this.dropdownList_model);
            }
            else{
              this.dropdownList_model=[];
            }

          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      }
    //  this.getworkstation(value);
  }
  onChange(event)
  {
  //  alert(event);
  this.hist_mindate = { year: event.year, month: 1, day: 1 };
  this.hist_maxdate = { year: event.year, month: 12, day: 31 };
  }
  getworkstation(value){
    this.chooseworkstations=[];
    var data: { switchcase: string,id:string,category:string} = { switchcase: "getworkstation",id:this.choosep,category:value};
    var method = "post";
    var url = "getdetails";
    this.dropdownList_workstation=[];
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            this.workstation=datass;
             if(datass.message.length>0)
            {
              
              this.dropdownList_workstation.push({id: "All", workstation_operation_code: "All"});
              for(let i=0;i<datass.message.length;i++)
              {
              this.dropdownList_workstation.push({id: datass.message[i]['id'], workstation_operation_code:datass.message[i]['workstation_operation_code']});
              }
            }
            else{
              this.dropdownList_workstation=[];
            }
           // this.dropdownList_workstation=datass.message;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      
  }
  getchassismodel()
  {
    var data: { switchcase: string} = { switchcase: "getchassismodel"};
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            this.chasismodel=datass.message;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  getproduction(frm:NgForm){

    
   var table= $('#exampletable').DataTable();
   var table11= $('#exampletable1').DataTable();
   var table12= $('#exampletable2').DataTable();
   var table13= $('#exampletable3').DataTable();
   var table14= $('#exampletable4').DataTable();
    var data = frm.value;
    var switchcase = "switchcase";
    var selectedWorkStation="selectedWorkStation";
    var selectedModel="selectedModel";
    var value = "getproductionvalues";
    data[switchcase] = value;
    data[selectedWorkStation] = this.selectedworkstation;
    data[selectedModel] = this.selectedmodel;
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            console.log(datass);
            this.details=datass.message;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
          table.destroy(); 
          setTimeout(() => {
            $('#exampletable').DataTable();
          }, 1000);
          table11.destroy(); 
          setTimeout(() => {
            $('#exampletable1').DataTable();
          }, 1000);
          table12.destroy(); 
          setTimeout(() => {
            $('#exampletable2').DataTable();
          }, 1000);
          table13.destroy(); 
          setTimeout(() => {
            $('#exampletable3').DataTable();
          }, 1000);
          table14.destroy(); 
          setTimeout(() => {
            $('#exampletable4').DataTable();
          }, 1000);
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  
  durationtype(event)
  {
    this.durations=event;
  }
  reloadpage()
  {
    this.ngOnInit();
  
  }
}
