import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProductionReportComponent } from './production-report.component';
import { SharedModule } from '../../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
export const productiontreportRoutes: Routes = [
  {
    path: '',
    component: ProductionReportComponent,
    data: {
      breadcrumb: 'Production Report',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(productiontreportRoutes),
    SharedModule,
    AngularMultiSelectModule  
  ],
  declarations: [ProductionReportComponent]
})
export class ProductionReportModule { }
