import { Routes } from '@angular/router';
import { ProductionPlanComponent } from "./production-plan.component";

export const productionRoutes: Routes = [{
    path: '',
    component: ProductionPlanComponent,
    data: {
        breadcrumb: "Production Plan"
    }
}];
