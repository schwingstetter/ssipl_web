import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ProductionPlanComponent } from './production-plan.component';
import { productionRoutes } from './production-plan.routing';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(productionRoutes),
    SharedModule
  ],
  declarations: [ProductionPlanComponent]
})
export class ProductionPlanModule { }
