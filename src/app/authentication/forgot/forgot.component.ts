import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import {AjaxService} from '../../ajax.service';
import { AuthService } from '../../auth.service';
import swal from 'sweetalert2';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html'
})
export class ForgotComponent implements OnInit {

  public form: FormGroup;
  constructor(private fb: FormBuilder, private router: Router,public toastr: ToastsManager,private AjaxService: AjaxService, private toastyService: ToastyService, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.form = this.fb.group ( {
      uname: [null , Validators.compose ( [ Validators.required ] )]
    } );
  }
 generateforgotpassword(frm) {    
   
    var data={ email: frm.form.controls.email.value }
    var method= "post";
    var url="forgotpassword";
    this.AjaxService.ajaxpost(data, method, url)

    .subscribe(
      (datas) => {
     //   alert(datas.responsecode)
   if(datas.status=='success')
   {
  //  this.toastr.success(datas.Message);
      setTimeout(() => {
                           this.router.navigate ( [ '/' ] );
                          }, 1000);
   
   }
   {
   //  alert('error');
 //this.toastr.error(datas.Message,'error');
   }
       
     
      },
      (err) => {
        if (err.status == 403) {          
        //  this.errors = err._body;
           this.toastr.error(err._body,'error');
            //this.modalRef.close();   
          frm.resetForm();          
        }
      }
    );
    //this.router.navigate(["/dashboard"]);    
  }
  onSubmit() {
    this.router.navigate ( [ '/' ] );
  }
}
