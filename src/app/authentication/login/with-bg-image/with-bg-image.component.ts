import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from './../../../auth.service';
import swal from 'sweetalert2';
import { global } from "../../../shared/global";

@Component({
  selector: 'app-with-bg-image',
  templateUrl: './with-bg-image.component.html',
  providers: [AjaxService, AuthService,global]
})
export class WithBgImageComponent implements OnInit {
  public profileobj:any ="";
  public errors:string="";
  public menunames:string="";

  constructor(private globalvariable: global,private router: Router, private routes: ActivatedRoute, private AjaxService: AjaxService, private auth: AuthService) { }

  ngOnInit() {
   // console.log("global "+this.globalvariable.empid);
    console.log(localStorage.length);
    if(localStorage.length>0)//Check if user has already logged in
    {
  this.getmenuaccess();    
  this.router.navigate(["/dashboard"]);
    }
   
    //this.router.navigate(["/dashboard"]);
  }
  forgotpwd()
  {
   // alert();
    this.router.navigate(["/authentication/forgot"]);
  }
  checkUser(frm) {    
   
    var data={ email: frm.form.controls.email.value,password: frm.form.controls.password.value }
    var method= "post";
    var url="login";
    this.AjaxService.ajaxpost(data, method, url)

    .subscribe(
      (datas) => {
      
       this.globalvariable.empid = datas[0].emp_id;
       this.globalvariable.role = datas[0].role;
       this.globalvariable.name = datas[0].name;
       this.getmenuaccess();
        this.auth.sendToken(this.globalvariable.role,this.globalvariable.empid,'0',this.globalvariable.name);
        this.router.navigate(["/dashboard"]);
      },
      (err) => {
        if (err.status == 403) {          
          this.errors = err._body;
          frm.resetForm();          
        }
      }
    );
    //this.router.navigate(["/dashboard"]);    
  }
getmenuaccess()
{
  var data={"role":this.globalvariable.role};
  var method = "post";
  var url = "rolemenu";
  this.AjaxService.ajaxpost(data, method, url)
    .toPromise()
    .then(
      (datas) => {
        if (datas.code == 201) {
        this.globalvariable.menunames=datas.message;
        } else {
        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
}
}
