import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mountingarrangement',
  templateUrl: './mountingarrangement.component.html',
  styleUrls: ['./mountingarrangement.component.css']
})
export class MountingarrangementComponent implements OnInit {

  public mountingmaster: string = "";
  public editmounting: string = "";
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  modelno: any;

  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mountingarrangement";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mountingmaster = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getmodelno();
  }
  getmodelno()
  {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mountingidno";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.modelno = datas.message;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  reloadPage() {
    this.ngOnInit();
  }
  addmountinginternal(form: NgForm, addnew) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "mountingarrangement";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();
            this.modalRef.close();         
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  editfunction(item) {
    this.editmounting = item;
  };
  editmountinginternal(editfrm, edit) {
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "mountingarrangement";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.ngOnInit();
            this.modalRef.close();         
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  downloadFile() {

    return this.http
      .get('assets/uploads/mountingarrangements.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'mountingarrangements.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  };
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;

    let fileList: FileList = event.target.files;
    this.file_name = fileList;
  }
  filesubmit(bulkupload) {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({ headers: headers });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/

      var data = formData;
      var method = "post";
      var url = "upload_mountingarrangement";
      var i: number;
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for (i = 0; i < data.length; i++) {
              if (data[i].code == 201) {
                this.toastr.success(data[i].message, 'Success!');
              } else {
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.reloadPage();
            this.modalRef.close();         
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };
  deletemounting(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "mountingarrangement";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  response.message,
                  'success'
                )
                this.ngOnInit();
              }
              else {
                swal(
                  'Deleted!',
                  response.message,
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
        swal(
          'Deleted!',
          'Your file is safe.',
          'error'
        )
      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };

}
