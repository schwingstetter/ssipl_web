import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MountingarrangementComponent } from './mountingarrangement.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const mountingRoutes: Routes = [
  {
    path: '',
    component: MountingarrangementComponent,
    data: {
      breadcrumb: 'Mounting Arrangement',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mountingRoutes),
    SharedModule
  ],
  declarations: [MountingarrangementComponent]
})
export class MountingarrangementModule { }
