import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MountingarrangementComponent } from './mountingarrangement.component';

describe('MountingarrangementComponent', () => {
  let component: MountingarrangementComponent;
  let fixture: ComponentFixture<MountingarrangementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MountingarrangementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MountingarrangementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
