import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumtypesComponent } from './drumtypes.component';

describe('DrumtypesComponent', () => {
  let component: DrumtypesComponent;
  let fixture: ComponentFixture<DrumtypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumtypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumtypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
