import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumtypesComponent } from './drumtypes.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const DrumTypesRoutes: Routes = [
  {
    path: '',
    component: DrumtypesComponent,
    data: {
      breadcrumb: 'Drum Master',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DrumTypesRoutes),
    SharedModule
  ],
  declarations: [DrumtypesComponent]
})
export class DrumtypesModule { }
