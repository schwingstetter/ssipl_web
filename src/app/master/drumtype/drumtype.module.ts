import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumtypeComponent } from './drumtype.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { drumtypeRoutes } from './drumtype.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(drumtypeRoutes),
    SharedModule
  ],
  declarations: [DrumtypeComponent]
})
export class DrumtypeModule { }
