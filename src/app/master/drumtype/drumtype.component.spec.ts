import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumtypeComponent } from './drumtype.component';

describe('DrumtypeComponent', () => {
  let component: DrumtypeComponent;
  let fixture: ComponentFixture<DrumtypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumtypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
