import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubassybomComponent } from './subassybom.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

 
export const subassybomroutes: Routes = [
  {
    path: '',
    component: SubassybomComponent,
    data: {
      breadcrumb: 'Sub Assembly BOM',
      status: true
    }
  }
];

@NgModule({
  imports: [   
    CommonModule,
    RouterModule.forChild(subassybomroutes),
    SharedModule,
    
  ],
  declarations: [SubassybomComponent]
})
export class SubassybomModule { }
