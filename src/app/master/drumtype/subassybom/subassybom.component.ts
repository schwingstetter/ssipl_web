import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-subassybom',
  providers: [AjaxService, AuthService],
  templateUrl: './subassybom.component.html',
  styleUrls: ['./subassybom.component.css'],
})
export class SubassybomComponent implements OnInit {

  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;

  public assy_headers:any={};
  public assyheaders:any={};
  menuaccess:string[] = new Array();
  public bulkmixermodel:string='';
  public options = [];
  public optionsMap = {1:true,2:false,3:true};
  public optionsChecked = [];
  public order:any={};
  public tags=[];
  public checked_headers:any={};
  public cdata:any=false;
  public search:any = '';

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "drummaster";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.assy_headers = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getassyheader();
  }
  getassyheader(){
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "drumheader";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.assyheaders = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };  
   reloadPage() {
     this.ngOnInit();
  }
  checkValue(option,options){
    if(this.menuaccess.length == 0){
      this.menuaccess.push(option);
    }else{
      if(options == false){
        this.menuaccess.splice(this.menuaccess.indexOf(option),1);
        console.log("new deleted numbers is : " + this.menuaccess );  
      }else{
        var length = this.menuaccess.push(option);
        console.log("new numbers is : " + this.menuaccess );
      }
      console.log(this.menuaccess);
    }
  };
  addsubassy(form: NgForm) {
    var data = form.value;
    var menu = "options";
    var menus = this.menuaccess;
    data[menu]=menus;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "drumsubassy";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){ 
            form.resetForm();           
            this.ngOnInit();
            this.toastr.success(datas.message, datas.type);
          }else{  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );   
  };
  changeCheckbox(tags, i) {
    if (tags) {
      this.tags[i].checked = !this.tags[i].checked;
    }
  };
  clearvalue(){
    $('input[type="checkbox"]').removeAttr('checked');
    this.menuaccess=[]; 
  };
  getassyvalue(form: NgForm,value){
    //$('.headersnos').prop('checked',false);
    this.menuaccess=[];
    var data: { switchcase: string,id:string } = { switchcase: "get",id:value };
    var method = "post";
    var url = "drumsubassy";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message[0]){
              var headers = datas.message[0].headerno.split(',');
              for(var i=0; i < headers.length; i++){              
                var e2 = <HTMLTableElement>document.getElementById(headers[i]);
                e2.setAttribute('checked', 'true');
                this.menuaccess.push(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  reset(frm:NgForm){
    frm.resetForm();
    this.menuaccess=[]; 
    this.ngOnInit();
  }
  open(addNew) {
    this.ngOnInit();
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    /*this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });*/
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  downloadFile() {

    return this.http
      .get('assets/uploads/subassybom.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'subassybom.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  }
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
   this.file_name=fileList;
  };
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      formData.append('drummodelno',this.bulkmixermodel)
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      let options = new RequestOptions({ headers: headers });
     

      var data = formData;
      var method = "post";
      var url = "uploadfileassybom";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for(i=0;i<data.length;i++){
              if(data[i].code==201){
                this.toastr.success(data[i].message, 'Success!');                               
                this.modalRef.close();
              }else{
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.reloadPage(); 
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };

}
