import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubassybomComponent } from './subassybom.component';

describe('SubassybomComponent', () => {
  let component: SubassybomComponent;
  let fixture: ComponentFixture<SubassybomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubassybomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubassybomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
