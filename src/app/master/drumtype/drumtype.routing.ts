import { Routes } from '@angular/router';


export const drumtypeRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'drumtypes',
                loadChildren: './drumtypes/drumtypes.module#DrumtypesModule'
            },{
                path: 'subassybom',
                loadChildren: './subassybom/subassybom.module#SubassybomModule'
            } 
        ]
    }
];
