import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobsheetComponent } from './jobsheet.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const jobsheetRoutes: Routes = [
  {
    path: '',
    component: JobsheetComponent,
    data: {
      breadcrumb: 'Assembly Job Sheet',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(jobsheetRoutes),
    SharedModule
  ],
  declarations: [JobsheetComponent]
})
export class JobsheetModule { }
