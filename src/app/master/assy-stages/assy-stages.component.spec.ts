import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssyStagesComponent } from './assy-stages.component';

describe('AssyStagesComponent', () => {
  let component: AssyStagesComponent;
  let fixture: ComponentFixture<AssyStagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssyStagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssyStagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
