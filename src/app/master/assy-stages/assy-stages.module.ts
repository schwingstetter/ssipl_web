import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssyStagesComponent } from './assy-stages.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const assystagesRoutes: Routes = [
  {
    path: '',
    component: AssyStagesComponent,
    data: {
      breadcrumb: 'Assembly Stages',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(assystagesRoutes),
    SharedModule
  ],
  declarations: [AssyStagesComponent]
})
export class AssyStagesModule { }
