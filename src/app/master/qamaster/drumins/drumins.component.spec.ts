import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DruminsComponent } from './drumins.component';

describe('DruminsComponent', () => {
  let component: DruminsComponent;
  let fixture: ComponentFixture<DruminsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DruminsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DruminsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
