import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DruminsComponent } from './drumins.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';


export const druminsRoutes: Routes = [
  {
    path: '',
    component: DruminsComponent,
    data: {
      breadcrumb: 'Drum Inspection',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(druminsRoutes),
    SharedModule
  ],
  declarations: [DruminsComponent]
})
export class DruminsModule { }
