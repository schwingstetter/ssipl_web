import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChassisinsComponent } from './chassisins.component';

describe('ChassisinsComponent', () => {
  let component: ChassisinsComponent;
  let fixture: ComponentFixture<ChassisinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChassisinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChassisinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
