import { NgModule } from '@angular/core';
import { NgSwitch } from '@angular/common';
import { NgSwitchCase } from '@angular/common';
import { CommonModule } from '@angular/common';
import { ChassisinsComponent } from './chassisins.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const chassisinsRoutes: Routes = [
  {
    path: '',
    component: ChassisinsComponent,
    data: {
      breadcrumb: 'FBV Checklist',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(chassisinsRoutes),
    SharedModule
  ],
  declarations: [ChassisinsComponent]
})
export class ChassisinsModule { }
