import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PdichecklistComponent } from './pdichecklist.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const pdichecklistRoutes: Routes = [
  {
    path: '',
    component: PdichecklistComponent,
    data: {
      breadcrumb: 'PDI Checklist',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(pdichecklistRoutes),
    SharedModule
  ],
  declarations: [PdichecklistComponent]
})
export class PdichecklistModule { }
