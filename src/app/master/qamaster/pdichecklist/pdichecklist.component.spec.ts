import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdichecklistComponent } from './pdichecklist.component';

describe('PdichecklistComponent', () => {
  let component: PdichecklistComponent;
  let fixture: ComponentFixture<PdichecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdichecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdichecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
