import { Routes } from '@angular/router';

export const qamasterRoutes: Routes = [
    {
        path: '',
        data: {
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'drumins',
                loadChildren: './drumins/drumins.module#DruminsModule'
            }, {
                path: 'processchecklist',
                loadChildren: './processchecklist/processchecklist.module#ProcesschecklistModule'
            },{
                path: 'pdichecklist',
                loadChildren: './pdichecklist/pdichecklist.module#PdichecklistModule'
            }, {
                path: 'shfhchecklist',
                loadChildren: './shfhchecklist/shfhchecklist.module#ShfhchecklistModule'
            }, {
                path: 'fbvchecklist',
                loadChildren: './chassisins/chassisins.module#ChassisinsModule'
            }, {
                path: 'chassisins',
                loadChildren: './fbvchecklist/fbvchecklist.module#FbvchecklistModule'
            }
        ]
    }
];