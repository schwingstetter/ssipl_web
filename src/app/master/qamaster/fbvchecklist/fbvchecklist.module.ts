import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FbvchecklistComponent } from './fbvchecklist.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const fdichecklistRoutes: Routes = [
  {
    path: '',
    component: FbvchecklistComponent,
    data: {
      breadcrumb: 'Chassis Inspection',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(fdichecklistRoutes),
    SharedModule
  ],
  declarations: [FbvchecklistComponent]
})
export class FbvchecklistModule { }
