import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbvchecklistComponent } from './fbvchecklist.component';

describe('FbvchecklistComponent', () => {
  let component: FbvchecklistComponent;
  let fixture: ComponentFixture<FbvchecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbvchecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbvchecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
