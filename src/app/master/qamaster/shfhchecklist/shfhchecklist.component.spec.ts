import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShfhchecklistComponent } from './shfhchecklist.component';

describe('ShfhchecklistComponent', () => {
  let component: ShfhchecklistComponent;
  let fixture: ComponentFixture<ShfhchecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShfhchecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShfhchecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
