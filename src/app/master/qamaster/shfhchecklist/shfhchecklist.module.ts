import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShfhchecklistComponent } from './shfhchecklist.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const shfhchecklistRoutes: Routes = [
  {
    path: '',
    component: ShfhchecklistComponent,
    data: {
      breadcrumb: 'FH/SH Checklist',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(shfhchecklistRoutes),
    SharedModule
  ],
  declarations: [ShfhchecklistComponent]
})
export class ShfhchecklistModule { }
