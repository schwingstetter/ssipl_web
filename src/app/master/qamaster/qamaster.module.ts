import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QamasterComponent } from './qamaster.component'; 
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { qamasterRoutes } from './qamaster.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(qamasterRoutes),
    SharedModule
  ],
  declarations: [QamasterComponent]
})
export class QamasterModule { }
