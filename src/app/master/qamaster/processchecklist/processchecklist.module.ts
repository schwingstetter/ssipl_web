import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcesschecklistComponent } from './processchecklist.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const processchecklistRoutes: Routes = [
  {
    path: '',
    component: ProcesschecklistComponent,
    data: {
      breadcrumb: 'Process Checklist',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(processchecklistRoutes),
    SharedModule
  ],
  declarations: [ProcesschecklistComponent]
})
export class ProcesschecklistModule { }
