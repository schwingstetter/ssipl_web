import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcesschecklistComponent } from './processchecklist.component';

describe('ProcesschecklistComponent', () => {
  let component: ProcesschecklistComponent;
  let fixture: ComponentFixture<ProcesschecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcesschecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcesschecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
