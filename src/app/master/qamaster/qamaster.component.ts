import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qamaster',
  template: '<router-outlet></router-outlet>'
})
export class QamasterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
