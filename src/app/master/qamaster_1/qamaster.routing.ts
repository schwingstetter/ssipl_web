import { Routes } from '@angular/router';


export const qamasterRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'druminspection',
                loadChildren: './druminspection/druminspection.module#DruminspectionModule'
            }/* ,{
                path: 'pdichecklist',
                loadChildren: './pdichecklist/pdichecklist.module#PdichecklistModule'
            },{
                path: 'sh-fhchecklist',
                loadChildren: './sh-fhchecklist/sh-fhchecklist.module#ShFhchecklistModule'
            },{
                path: 'chassisins',
                loadChildren: './chassisins/chassisins.module#ChassisinsModule'
            },{
                path: 'fbvchecklist',
                loadChildren: './fbvchecklist/fbvchecklist.module#FbvchecklistModule'
            } */
        ]
    }
];
