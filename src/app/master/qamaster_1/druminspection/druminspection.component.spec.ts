import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DruminspectionComponent } from './druminspection.component';

describe('DruminspectionComponent', () => {
  let component: DruminspectionComponent;
  let fixture: ComponentFixture<DruminspectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DruminspectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DruminspectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
