import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DruminspectionComponent } from './druminspection.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DruminspectionComponent]
})
export class DruminspectionModule { }
