import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FabprocessComponent } from './fabprocess.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const fabprocessRoutes: Routes = [
  {
    path: '',
    component: FabprocessComponent,
    data: {
      breadcrumb: 'Fabrication Process Flow',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(fabprocessRoutes),
    SharedModule
  ],
  declarations: [FabprocessComponent]
})
export class FabprocessModule { }
