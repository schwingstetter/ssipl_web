import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabprocessComponent } from './fabprocess.component';

describe('FabprocessComponent', () => {
  let component: FabprocessComponent;
  let fixture: ComponentFixture<FabprocessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabprocessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabprocessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
