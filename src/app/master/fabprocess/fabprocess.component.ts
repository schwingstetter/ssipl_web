import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-fabprocess',
  templateUrl: './fabprocess.component.html',
  styleUrls: ['./fabprocess.component.css']
})
export class FabprocessComponent implements OnInit {
  file_name: FileList;
  public fabprocess: string = "";
  public processimage: string = "";
  public editfabprocess: string = "";
  file_names:FileList;
  private modalRef: NgbModalRef;
  closeResult: string;

  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() { 
    var table=$('.datatable').DataTable();   
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "fabprocess";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.fabprocess = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }  
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;

    let fileList: FileList = event.target.files;
    this.file_name = fileList;
  }
  addfabprocess(form: NgForm, addnew) {     
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      let extension: string = form.value.fabprocesschart.split('.').pop().toLowerCase();
      if ($.inArray(extension, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
        formData.append('file', file);
        formData.append('fabprocessflow', form.value.fabprocessflow);
        formData.append('switchcase', "insert");
        var method = "post";
        var url = "fabprocess";
        this.AjaxService.ajaxpost(formData, method, url)
          .subscribe(
            (datas) => {
              if (datas.code == 201) {
                this.toastr.success(datas.message, datas.type);
                this.ngOnInit();
                this.modalRef.close();                   
              }
              else {
                this.toastr.error(datas.message, datas.type);
              }
            },
            (err) => {
              if (err.status == 403) {
                this.toastr.error(err._body);
              }
            }
          );
      } else {
        this.toastr.error('Process Flow Chart should be an Image file', 'Error!');
      }
    }    
  };
  showimage(image){
    this.processimage = environment.file_url + image;
  };
  editfunction(item) {
    this.editfabprocess = item;
  };

  fileUploads(events) {
    let fileList: FileList = events.target.files;
    this.file_names = fileList;
  }
  editfabproces(editfrm) {
    if(editfrm.form.valid){
      if(this.file_names){
        if (this.file_names.length > 0) {
          let files: File = this.file_names[0];
          let formData: FormData = new FormData();
          let extension: string = editfrm.value.fabprocesschartss.split('.').pop().toLowerCase();
          if ($.inArray(extension, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
            formData.append('files', files);
            formData.append('fabprocessflow', editfrm.value.fabprocessflows);
            formData.append('id', editfrm.value.fid);
            formData.append('existing_image', editfrm.value.fabprocesschartss);
            formData.append('switchcase', "edit");
            formData.append('extension', extension);
            var method = "post";
            var url = "fabprocess";
            this.AjaxService.ajaxpost(formData, method, url)
              .subscribe(
                (datas) => {
                  if (datas.code == 201) {
                    this.toastr.success(datas.message, datas.type);
                    this.ngOnInit();
                    this.modalRef.close();               
                  }
                  else {
                    this.toastr.error(datas.message, datas.type);
                  }
                  this.ngOnInit();
                },
                (err) => {
                  if (err.status == 403) {
                    this.toastr.error(err._body);
                    this.ngOnInit();
                  }
                }
              );
          } else {
            this.toastr.error('Process Flow Chart should be an Image file', 'Error!');
          }
        }
      }else{
        var data = editfrm.value;
        var switchcase = "switchcase";
        var value = "edit";
        data[switchcase] = value;
        var method = "post";
        var url = "fabprocess";
        this.AjaxService.ajaxpost(data, method, url)
          .subscribe(
            (datas) => {
              if (datas.code == 201) {
                this.toastr.success(datas.message, datas.type);
                this.modalRef.close();
              }
              else {
                this.toastr.error(datas.message, datas.type);
              }
              this.ngOnInit();
            },
            (err) => {
              if (err.status == 403) {
                this.toastr.error(err._body);
                this.ngOnInit();
              }
            }
          );
      } 
    }else{
      this.toastr.error("All fields are Mandatory");
    }
  };  
  deletefabprocess(id, process_image) {
    var data: { switchcase: string, id: any, process_image: string } = { switchcase: "delete", id: id, process_image: process_image };
    var method = "post";
    var url = "fabprocess";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                this.ngOnInit();
              }
              else {
               
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
       
        
      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
}
