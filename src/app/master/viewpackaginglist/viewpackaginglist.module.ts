import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewpackaginglistComponent } from './viewpackaginglist.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const viewpackagingRoutes: Routes = [
  {
    path: '',
    component: ViewpackaginglistComponent,
    data: {
      breadcrumb: 'View Packaging',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(viewpackagingRoutes),
    SharedModule
  ],
  declarations: [ViewpackaginglistComponent]
})
export class ViewpackaginglistModule { }
