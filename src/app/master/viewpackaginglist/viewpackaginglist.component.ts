import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-viewpackaginglist',
  providers: [AjaxService, AuthService],
  templateUrl: './viewpackaginglist.component.html',
  styleUrls: ['./viewpackaginglist.component.css']
})
export class ViewpackaginglistComponent implements OnInit {

  private modalRef: NgbModalRef;
  closeResult: string;
  public mountingheaders:any={};
  public editmountingheader:any={};
  public packagingdetails:any={};
  file_name: FileList;
  
  

  constructor(public http: Http, private AjaxService: AjaxService,private myRoute: Router, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "getpackaginglist" };
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          console.log(datas);
          if (datas.code == 201) {
            this.packagingdetails = datas;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getdetails(item){
    this.myRoute.navigate(['/planner/viewpackage', { id:item}]);
  }

}
