import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpackaginglistComponent } from './viewpackaginglist.component';

describe('ViewpackaginglistComponent', () => {
  let component: ViewpackaginglistComponent;
  let fixture: ComponentFixture<ViewpackaginglistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpackaginglistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpackaginglistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
