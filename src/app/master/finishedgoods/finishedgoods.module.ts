import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinishedgoodsComponent } from './finishedgoods.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const finishedgodsRoutes: Routes = [
  {
    path: '',
    component: FinishedgoodsComponent,
    data: {
      breadcrumb: 'Mixer Assembly',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(finishedgodsRoutes),
    SharedModule
  ],
  declarations: [FinishedgoodsComponent]
})
export class FinishedgoodsModule { }
