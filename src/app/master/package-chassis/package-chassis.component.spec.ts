import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageChassisComponent } from './package-chassis.component';

describe('PackageChassisComponent', () => {
  let component: PackageChassisComponent;
  let fixture: ComponentFixture<PackageChassisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageChassisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageChassisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
