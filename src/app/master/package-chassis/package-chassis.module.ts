import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackageChassisComponent } from './package-chassis.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const packagechassisRoutes: Routes = [
  {
    path: '',
    component: PackageChassisComponent,
    data: {
      breadcrumb: 'Packaging Chassis',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,    
    RouterModule.forChild(packagechassisRoutes),
    SharedModule
  ],
  declarations: [PackageChassisComponent]
})
export class PackageChassisModule { }
