import { Component, OnInit, ViewContainerRef} from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Component({
  selector: 'app-employee',
  providers: [AjaxService, AuthService],
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']  
})
export class EmployeeComponent implements OnInit {

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "";
  public sortOrder: string = "desc";

  public switchcase:string = "";
  public id:number = 10;
  public employees:string="";
  public employeesnags:string="";
  public contractorname:string="";
  public editemployee:any = { };
  arrayBuffer: any;
  file: File;
  auditPhotoUploader: FileUploader;
  public RequestOptions:any;
  file_name:FileList;
  position: string = 'bottom-right';
  title: string;
  msg: string;
  isavailable: string;
  showClose: boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: string = 'default';
  closeOther: boolean = false;
  tenentIDFileName:any;
  userrole :any;
  public form:any;

  public show:boolean = false;
  public buttonName:any = 'Show';
  public datass:any; 


  closeResult: string;
  dtElement: DataTableDirective;
  datatableElement: DataTableDirective;

  //dtOptions: DataTables.Settings = {};
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  private modalRef: NgbModalRef;
  DataArray: any=[];

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {

    this.toastr.setRootViewContainerRef(vcr);
      this.auditPhotoUploader = new FileUploader({
    });
  }
  ngOnInit(): void {
    this.getEmployee(); 
  }
  getEmployee(){
    var table=$('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "employee";
    this.AjaxService.ajaxpost(data, method, url)
      .toPromise()
      .then(
        (datas) => {
          if (datas.code == 201) {
            this.employees = datas.message;
            //this.rerender();
            table.destroy(); 
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getrole();
  }
  reset(template)
  {
   this.userrole='';
   this.getEmployee();
  }
  contractor(event)
  {
if(event == '11')
{
  this.isavailable = '1';
  var data: { switchcase: string } = { switchcase: "get" };
  var method = "post";
  var url = "getcontratorsname";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {

        if(datas.code == 201){
          this.contractorname = datas.message;
       
        }else{

        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
}
else
{
  this.isavailable = '0';
}
  }
  getrole()
{
  var data: { switchcase: string } = { switchcase: "get" };
  var method = "post";
  var url = "usersrole";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {

        if(datas.code == 201){
          this.userrole = datas.message;
       
        }else{

        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    );
}
  reloadPage() {
     this.ngOnInit();
  }
  addemployee(form: NgForm,add){
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method= "post";
    var url="employee";
    this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if(datas.code==201){
          this.toastr.success(datas.message, datas.type);
          form.resetForm();
          this.modalRef.close();
          this.ngOnInit();
          //add.hide();
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }    
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
    );
  };
  editfunction(item) {
    this.editemployee=item;
    if(this.editemployee.emp_designation == '11')
    {
      this.isavailable = '1';
      this.contractor('11');
    }
    else
    {this.isavailable = '0';

    }
    this.getrole();
  };
  employeesnag(item) {
    var table=$('#example').DataTable();
    var data: { switchcase: string,userid : string } = { switchcase: "getsnagdetails",userid:item };
    var method = "post";
    var url = "employee";
    this.AjaxService.ajaxpost(data, method, url)
      .toPromise()
      .then(
        (datas) => {
          if (datas.code == 201) {
            this.employeesnags = datas.message;
           table.destroy(); 
           setTimeout(() => {
             $('#example').DataTable();
           }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  editemployees(editfrm){
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "employee";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);    
            //edit.hide();
            this.reloadPage();
            this.modalRef.close();        
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //this.toastr.success('Hello world!', 'Toastr fun!');
  };
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
   this.file_name=fileList;
  };
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({ headers: headers });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/

      var data = formData;
      var method = "post";
      var url = "uploadfiles";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for(i=0;i<data.length;i++){
              if(data[i].code==201){
                this.toastr.success(data[i].message, 'Success!');                               
                this.modalRef.close();
              }else{
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.reloadPage(); 
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };
  downloadFile() {

    return this.http
      .get('assets/uploads/Employeemaster.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'Employeemaster.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addNew) {
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  onSearchChange(e){    
    /* if (e.which === 32 && !e.target.value.length)
      e.preventDefault(); */
  }
}



// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/master/employee/employee.component.ts