import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeComponent } from './employee.component';
import { SharedModule } from '../../shared/shared.module'; 
import { FileUploadModule } from 'ng2-file-upload';
import { DataTablesModule } from 'angular-datatables';
//Import toast module
/* import { ToastModule } from 'ng2-toastr/ng2-toastr'; */
//import { DataTablesModule } from 'angular-datatables';

export const EmployeeRoutes: Routes = [
  {
    path: '',
    component: EmployeeComponent,
    data: {
      breadcrumb: 'Employee Master',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EmployeeRoutes),
    SharedModule,
    FileUploadModule,
    DataTablesModule
    //DataTablesModule
    /* ToastModule.forRoot() */
  ],
  declarations: [EmployeeComponent]
})
export class EmployeeModule { }
