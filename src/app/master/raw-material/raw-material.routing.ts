import { Routes } from '@angular/router';


export const RawmaterialRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'rawmaterialdetails',
                loadChildren: './rawmaterialdetails/rawmaterialdetails.module#RawmaterialdetailsModule'
            },{
                path: 'conedetails',
                loadChildren: './conedetails/conedetails.module#ConedetailsModule'

            }  ,
            {
                path: 'conemapdetails',
                loadChildren: './conemapping/conemapping.module#ConemappingModule'

            }  
        ]
    }
];
