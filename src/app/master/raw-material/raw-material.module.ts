import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { RawMaterialComponent } from './raw-material.component';
import { RawmaterialRoutes } from './raw-material.routing';




@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RawmaterialRoutes),
    SharedModule
  ],
  declarations: [RawMaterialComponent]
})
export class RawMaterialModule { }
