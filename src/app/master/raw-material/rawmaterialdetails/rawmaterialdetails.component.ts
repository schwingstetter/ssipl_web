import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from "../../../ajax.service";
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-rawmaterialdetails',
  templateUrl: './rawmaterialdetails.component.html',
  styleUrls: ['./rawmaterialdetails.component.css']
})
export class RawmaterialdetailsComponent implements OnInit {
  public rawmaterials:string="";
  public editrawmatterial:string="";
  private modalRef: NgbModalRef;
  closeResult: string;
  public closeOnConfirm: boolean;
  file_name: FileList;
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) { 
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "rawmateraildetail";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.rawmaterials = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
          setTimeout(() => {
            (<any>$('.datatable')).DataTable();
          }, 1000);
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );     
  };  

  reloadPage() {
    this.ngOnInit();
  }
  addrawmaterial(form: NgForm, addNew) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "rawmateraildetail";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();
            this.modalRef.close();   
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );
  };
  editfunction(item) {
    this.editrawmatterial = item;
  };  
  editrawmaterial(editfrm, edit) {
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "rawmateraildetail";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.reloadPage();
            this.modalRef.close();   
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    //this.toastr.success('Hello world!', 'Toastr fun!');
  };
  deletedrawmaterial(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "rawmateraildetail";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  response.message,
                  'success'
                )
                this.ngOnInit();
              }
              else {
                swal(
                  // {title: 'Alert',
                  // text: "Raw material Detail is is not Deleted,kindly checking RM mapping ",
                  // type: 'warning',
                  
                  // confirmButtonColor: '#3085d6',}
                  'Not Deleted!',
                  response.message,
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
      
      }
    });
  };
  downloadFile() {

    return this.http
      .get('assets/uploads/rawmaterialdetails.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'rawmaterialdetails.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  };
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;

    let fileList: FileList = event.target.files;
    this.file_name = fileList;
  }
  filesubmit(bulkupload) {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({ headers: headers });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/

      var data = formData;
      var method = "post";
      var url = "rawmaterial_uploadfiles";
      var i: number;
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for (i = 0; i < data.length; i++) {
              if (data[i].code == 201) {
                this.toastr.success(data[i].message, 'Success!');                
              } else {
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.reloadPage();
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew,{ backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };

}
