import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawmaterialdetailsComponent } from './rawmaterialdetails.component';

describe('RawmaterialdetailsComponent', () => {
  let component: RawmaterialdetailsComponent;
  let fixture: ComponentFixture<RawmaterialdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawmaterialdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawmaterialdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
