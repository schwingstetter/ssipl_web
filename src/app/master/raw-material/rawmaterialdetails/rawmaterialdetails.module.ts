import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RawmaterialdetailsComponent } from './rawmaterialdetails.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const rawmaterialdetailroutes: Routes = [
  {
    path: '',
    component: RawmaterialdetailsComponent,
    data: {
      breadcrumb: 'Raw Material Details',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(rawmaterialdetailroutes),  ],
  declarations: [RawmaterialdetailsComponent]
})
export class RawmaterialdetailsModule { }
