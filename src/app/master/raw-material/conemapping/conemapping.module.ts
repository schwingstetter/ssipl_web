import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConemappingComponent } from './conemapping.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const conemappingroutes: Routes = [
  {
    path: '',
    component: ConemappingComponent,
    data: {
      breadcrumb: 'RM Cone Mapping',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(conemappingroutes),  ],
  declarations: [ConemappingComponent]
})
export class ConemappingModule { }
