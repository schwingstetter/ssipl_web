import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConemappingComponent } from './conemapping.component';

describe('ConemappingComponent', () => {
  let component: ConemappingComponent;
  let fixture: ComponentFixture<ConemappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConemappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConemappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
