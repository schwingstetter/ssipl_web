import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from './../../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-conemapping',
  templateUrl: './conemapping.component.html',
  styleUrls: ['./conemapping.component.css']
})
export class ConemappingComponent implements OnInit {
  public checklist: string = "";
 public drummodel: string = "";
 public type: string = "";
 typeofcone:string;
  assignto:string;
  public editchecklist: any = "";
  public RequestOptions: any;
  public options: any;
  public options1: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  conecategory:string[] = new Array();
  shellcategory:string[] = new Array();
  istype:string;
  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.getconemapping();   
    this.getmodelno();
  };
  getconemapping(){
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "conemapping";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.checklist = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  getpartandmodelno(event)
  {

    var data: { switchcase: string , id: number} = { switchcase: "get" , id:event};
    var method = "post";
    var url = "conemappingpart";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
      //    alert(datas.code);
          if(datas.code == 201){           
           this.options=datas.message;
           setTimeout(() => {
            $('.datatable').DataTable();
          }, 1000);
           }else{
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
     
  }
  getmodelno()
  {
    var data1: { switchcase: string } = { switchcase: "get" };
    var method1 = "post";
    var url1 = "conemappingmodel";
    //alert(url);
    this.AjaxService.ajaxpost(data1, method1, url1)
      .subscribe(
        (datas1) => {
      //    alert(datas.code);
          if(datas1.code == 201){
           this.options1=datas1.message;
           setTimeout(() => {
            $('.datatable').DataTable();
          }, 1000);
           }else{
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  reset(frm:NgForm)
  {
    frm.resetForm();
  }
  reloadPage() {
    this.getconemapping();
  };
  addconemapping(form: NgForm) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "conemapping";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reset(form);
            this.reloadPage();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  ontypeselected(event)
  {
    this.type=event;
    this.getpartandmodelno(event);
  }
  onOptionsSelected(event)
  {
    this.conecategory=Array();
    this.shellcategory=Array();
    this.drummodel=event;
    var data = {"drummodel":event};
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "getconedetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
         //  this.types=[];
            this.typeofcone="";
            if(datas.message.length >0)
            {
              if(datas.message[0].product_group == 3)
              {
                this.istype = '2';                
              }
              else
              {
                this.istype = '1';
              }
              for(var i=1;i<=datas.message[0].noofshell;i++)
              {
                this.shellcategory.push("Shell" + ' ' + (+(i)));
              }
              for (var i = 1; i <= datas.message[0].noofcone; i++) {
                this.conecategory.push("Cone" + ' ' +(+(i)));
              }         
            }
          console.log(this.istype);
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  }
  onconeselected(event)
  {
    console.log(event);
  
    var data = {"drummodel":this.drummodel,"conecategory":event.trim()};
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "getconemappingtype";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message)
            { 
              if(datas.message[0].cone1)
              {
                this.typeofcone=datas.message[0].cone1;
                this.editchecklist.cone_type=datas.message[0].cone1;
              }
              if(datas.message[0].cone2)
              {
                this.typeofcone=datas.message[0].cone2;
                this.editchecklist.cone_type=datas.message[0].cone2;
              }
              if(datas.message[0].cone3)
              {
                this.typeofcone=datas.message[0].cone3;
                this.editchecklist.cone_type=datas.message[0].cone3;
              }
              if(datas.message[0].cone4)
              {
                this.typeofcone=datas.message[0].cone4;
                this.editchecklist.cone_type=datas.message[0].cone4;
              }
            
            }
            this.reloadPage();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  }
  editfunction(item) {    
    console.log(item);
    this.editchecklist = item;
    this.assignto=item.assign_to;
    this.onOptionsSelected(item.model);
    this.ontypeselected(item.assign_to);
  };
  editchecklists(editfrm, edit) {
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "conemapping";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.ngOnInit();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  deletechecklist(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "conemapping";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your Data has been deleted.',
                  'success'
                )
                this.ngOnInit();
              }
              else {
                swal(
                  'Deleted!',
                  'Your Data is safe.',
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  }; 
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };

}