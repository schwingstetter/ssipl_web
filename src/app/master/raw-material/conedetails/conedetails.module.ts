import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConedetailsComponent } from './conedetails.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const conedetailsroutes: Routes = [
  {
    path: '',
    component: ConedetailsComponent,
    data: {
      breadcrumb: 'Cone Details',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(conedetailsroutes),  ],
  declarations: [ConedetailsComponent]
})
export class ConedetailsModule { }
