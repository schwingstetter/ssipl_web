import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConedetailsComponent } from './conedetails.component';

describe('ConedetailsComponent', () => {
  let component: ConedetailsComponent;
  let fixture: ComponentFixture<ConedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
