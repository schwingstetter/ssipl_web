import { Routes } from '@angular/router';


export const masterRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'contract',
                loadChildren: './contract/contract.module#ContractModule'
            }, {
                path: 'employee',
                loadChildren: './employee/employee.module#EmployeeModule'
            }, {
                path: 'drumtype',
                loadChildren: './drumtype/drumtype.module#DrumtypeModule'
            }, {
                path: 'fabstages',
                loadChildren: './fab-stages/fab-stages.module#FabStagesModule'
            }, {
                path: 'rawmaterial',
                loadChildren: './raw-material/raw-material.module#RawMaterialModule'
            }, {
                path: 'druminternal',
                loadChildren: './drum-internal/drum-internal.module#DrumInternalModule'
            }, {
                path: 'assystages',
                loadChildren: './assy-stages/assy-stages.module#AssyStagesModule'
            }, {
                path: 'assyinternal',
                loadChildren: './assy-internal/assy-internal.module#AssyInternalModule'
            }, {
                path: 'delayrework',
                loadChildren: './delay-rework/delay-rework.module#DelayReworkModule'
            }, {
                path: 'mixermaster',
                loadChildren: './mixerassemblymaster/mixerassemblymaster.module#MixerassemblymasterModule'
            }, {
                path: 'snagmaster',
                loadChildren: './snagmaster/snagmaster.module#SnagmasterModule'
            }, {
                path: 'qamaster',
                loadChildren: './qamaster/qamaster.module#QamasterModule'
            }, {
                path: 'fabprocess',
                loadChildren: './fabprocess/fabprocess.module#FabprocessModule'
            }, {
                path: 'fingoods',
                loadChildren: './finishedgoods/finishedgoods.module#FinishedgoodsModule'
            }, {
                path: 'jobsheet',
                loadChildren: './jobsheet/jobsheet.module#JobsheetModule'
            }, {
                path: 'sopmaster',
                loadChildren: './sopmaster/sopmaster.module#SopmasterModule'
            },
            {
                path: 'sopdrum',
                loadChildren: './sopdrum/sopdrum.module#SopdrumModule'
            }, {
                path: 'mountingarrangement',
                loadChildren: './mountingbom/mountingbom.module#MountingbomModule'
            },{
                path: 'mounting',
                loadChildren: './mountingarrangement/mountingarrangement.module#MountingarrangementModule'
            },{
                path: 'storesmaster',
                loadChildren: './storesmaster/storesmaster.module#StoresmasterModule'
            },{
                path: 'forgotpwd',
                loadChildren: './forgotpwd/forgotpwd.module#ForgotpwdModule'
            },{
                path: 'packagingarrangement',
                loadChildren: './packaging/packaging.module#PackagingModule'
            },{
                path: 'packagechassis',
                loadChildren: './package-chassis/package-chassis.module#PackageChassisModule'
            },{
                path: 'packagemapping',
                loadChildren: './package-mapping/package-mapping.module#PackageMappingModule'
            },{
                path: 'packaginglist',
                loadChildren: './packaginglist/packaginglist.module#PackaginglistModule'
            },{
                path: 'viewpackaginglist',
                loadChildren: './viewpackaginglist/viewpackaginglist.module#ViewpackaginglistModule'
            },{
                path: 'viewpackage',
                loadChildren: './viewpackaglists/viewpackaglists.module#ViewpackaglistsModule'
            },{
                path: 'torque',
                loadChildren: './torque/torque.module#TorqueModule'
            }  ,{
                path: 'employeeperformance',
                loadChildren: './employeeperformance/employeeperformance.module#EmployeeperformanceModule'
            }  
        ]
    }
];

//allinone 730s