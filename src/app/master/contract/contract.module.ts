import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContractComponent } from './contract.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
export const contractRoutes: Routes = [
  {
    path: '',
    component: ContractComponent,
    data: {
      breadcrumb: 'Contract Master',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(contractRoutes),
    SharedModule
  ],
  declarations: [ContractComponent]
})
export class ContractModule { }
