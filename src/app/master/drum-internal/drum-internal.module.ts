import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumInternalComponent } from './drum-internal.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { druminternalRoutes } from './drum-internal.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(druminternalRoutes),
    SharedModule
  ],
  declarations: [DrumInternalComponent]
})
export class DrumInternalModule { }
