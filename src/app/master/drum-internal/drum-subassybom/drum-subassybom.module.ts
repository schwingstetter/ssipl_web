import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumSubassybomComponent } from './drum-subassybom.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const drumassyRoutes: Routes = [
  {
    path: '',
    component: DrumSubassybomComponent,
    data: {
      breadcrumb: 'Drum Sub Assy BOM',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(drumassyRoutes),
    SharedModule
  ],
  declarations: [DrumSubassybomComponent]
})
export class DrumSubassybomModule { }
