import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumSubassybomComponent } from './drum-subassybom.component';

describe('DrumSubassybomComponent', () => {
  let component: DrumSubassybomComponent;
  let fixture: ComponentFixture<DrumSubassybomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumSubassybomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumSubassybomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
