import { Routes } from '@angular/router';

export const druminternalRoutes: Routes = [
    {
        path: '',
        data: {
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'drumheader',
                loadChildren: './drum-header/drum-header.module#DrumHeaderModule'
            },{
                path: 'drumsubassy',
                loadChildren: './drum-subassybom/drum-subassybom.module#DrumSubassybomModule'
            }
        ]
    }
];
