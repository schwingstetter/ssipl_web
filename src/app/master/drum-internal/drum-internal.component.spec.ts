import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumInternalComponent } from './drum-internal.component';

describe('DrumInternalComponent', () => {
  let component: DrumInternalComponent;
  let fixture: ComponentFixture<DrumInternalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumInternalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumInternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
