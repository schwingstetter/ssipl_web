import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumHeaderComponent } from './drum-header.component';

describe('DrumHeaderComponent', () => {
  let component: DrumHeaderComponent;
  let fixture: ComponentFixture<DrumHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
