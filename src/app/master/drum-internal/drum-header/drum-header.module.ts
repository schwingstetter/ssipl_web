import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumHeaderComponent } from './drum-header.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const drumheaderRoutes: Routes = [
  {
    path: '',
    component: DrumHeaderComponent,
    data: {
      breadcrumb: 'Drum Header',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(drumheaderRoutes),
    SharedModule
  ],
  declarations: [DrumHeaderComponent]
})
export class DrumHeaderModule { }
