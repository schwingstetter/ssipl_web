import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixerassemblymasterComponent } from './mixerassemblymaster.component';

describe('MixerassemblymasterComponent', () => {
  let component: MixerassemblymasterComponent;
  let fixture: ComponentFixture<MixerassemblymasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixerassemblymasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixerassemblymasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
