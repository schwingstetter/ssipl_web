import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MixertypemasterComponent } from './mixertypemaster.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const mixerassemblyroutes: Routes = [
  {
    path: '',
    component: MixertypemasterComponent,
    data: {
      breadcrumb: 'Mixer Assembly Master',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mixerassemblyroutes),
    SharedModule
  ],
  declarations: [MixertypemasterComponent]
})
export class MixertypemasterModule { }
