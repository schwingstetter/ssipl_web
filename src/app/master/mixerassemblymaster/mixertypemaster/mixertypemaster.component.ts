import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-mixertypemaster',
  templateUrl: './mixertypemaster.component.html',
  styleUrls: ['./mixertypemaster.component.css']
})
export class MixertypemasterComponent implements OnInit {
  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "";
  public sortOrder: string = "desc";
  public sot: any = "";
  public switchcase: string = "";
  public id: number = 10;
  public employees: string = "";
  public editemployee: any = {};
  arrayBuffer: any;
  file: File;
  auditPhotoUploader: FileUploader;
  public RequestOptions: any;
  file_name: FileList;
  position: string = 'bottom-right';
  title: string;
  msg: string;
  cum3value:string;
  showClose: boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: string = 'default';
  closeOther: boolean = false;
  tenentIDFileName: any;
  public show: boolean = false;
  public buttonName: any = 'Show';
  dtOptions: DataTables.Settings = {};
  private modalRef: NgbModalRef;
  closeResult: string;
  public engine: any = "";
  public gearbox: any = "";
  public hydpump: any = "";
  public hydmotor: any = "";
  public waterpump: any = "";
  public enginemodels: any = "";
  public gearboxmodels: any = "";
  public hydpumpmodels: any = "";
  public hydmotormodels: any = "";
  public waterpumpmodels: any = "";
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);

  }
  ngOnInit() {
    this.getmixerassembly();
    this.getmodelmake();
    /* setTimeout(() => {
      (<any>$('.datatable')).DataTable();
    }, 2000);   */
  }
  getmixerassembly(){
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mixerassemblymaster";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.employees = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmodelmake() {
    var data = { "engine_model": "Engine Model" };
    var method = "post";
    var switchcase = "switchcase";
    var value = "engine";
    data[switchcase] = value;
    var url = "mixerassemblymastermodelmake";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {

              this.engine = datas.message;
            }
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    var data1 = { "gear_model": "Gear Box Model" };
    var method = "post";
    var switchcase = "switchcase";
    var value = "gear";
    data1[switchcase] = value;
    var url = "mixerassemblymastermodelmake";
    this.AjaxService.ajaxpost(data1, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {
              this.gearbox = datas.message;

            }

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    var data2 = { "hydpump_model": "Hyd. Pump Model" };
    var method = "post";
    var switchcase = "switchcase";
    var value = "hydpump";
    data2[switchcase] = value;
    var url = "mixerassemblymastermodelmake";
    this.AjaxService.ajaxpost(data2, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {
              this.hydpump = datas.message;
            }

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    var data3 = { "hydmotor_model": "Hyd. Motor Model" };
    var method = "post";
    var switchcase = "switchcase";
    var value = "hydmotor";
    data3[switchcase] = value;
    var url = "mixerassemblymastermodelmake";
    this.AjaxService.ajaxpost(data3, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {
              this.hydmotor = datas.message;
            }

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    var data4 = { "waterpump_model": "Water pump Model" };
    var method = "post";
    var switchcase = "switchcase";
    var value = "waterpump";
    data4[switchcase] = value;
    var url = "mixerassemblymastermodelmake";
    this.AjaxService.ajaxpost(data4, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for (let i = 0; i < datas.message.length; i++) {
              this.waterpump = datas.message;
            }

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  onOptionsSelected(event) {
    var data = { "make": event, "model": "1" };
    var method = "post";
    var url = "getmake";
    var i: number;

    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {

            this.enginemodels = datas.message;


          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getcum3(event) {
    if (event.length == 3) {
      this.cum3value=event.charAt(2);
    }
  }
  getcum3edit(event){
    if (event.length == 3) {
      this.editemployee.waterpump_cum3=event.charAt(2);
    }
  }
  onOptionsSelected1(event) {

    var data = { "make": event, "model": "2" };
    var method = "post";
    var url = "getmake";
    var i: number;

    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {

            this.gearboxmodels = datas.message;


          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  onOptionsSelected2(event) {

    var data = { "make": event, "model": "3" };
    var method = "post";
    var url = "getmake";
    var i: number;

    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {

            this.hydpumpmodels = datas.message;


          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  onOptionsSelected3(event) {

    var data = { "make": event, "model": "4" };
    var method = "post";
    var url = "getmake";
    var i: number;

    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {

            this.hydmotormodels = datas.message;


          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  editfunction(item) {
    this.editemployee = item;
    this.onOptionsSelected(item.engine_make);
    this.onOptionsSelected1(item.gearbox_make);
    this.onOptionsSelected2(item.hydpump_make);
    this.onOptionsSelected3(item.hydmotor_make);
  };
  editemployees(editfrm, edit) {
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "mixerassemblymaster";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.modalRef.close();
            this.reloadPage();
            this.toastr.success(datas.message, datas.type);
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    //this.toastr.success('Hello world!', 'Toastr fun!');
  };
  timechanged(newTime) {
    var minutes = (+newTime.hour) * 60 + (+newTime.minute);  
    this.sot=minutes;
    console.log(this.sot);
    }
  addmixerassembly(form: NgForm, add) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    var time = "time";
    data[switchcase] = value;
    data[time] = this.sot;
    var method = "post";
    var url = "mixerassemblymaster";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();
            this.modalRef.close();

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  reloadPage() {
    // Solution 1:   
    // this.router.navigate('localhost:4200/new');

    // Solution 2:
    //this.ngOnInit();
    this.getmixerassembly();
  }
  deletemixerassembly(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "mixerassemblymaster";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Deleted!',
                  response.message,
                  'success'
                )
              }
              else if (response.code == 202)  {
                swal(
                  // {
                  // title: 'Alert',
                  // text: " mixer is  not Deleted,kindly checking the workorder and CM variance",
                  // type: 'warning',  
                  // confirmButtonColor: '#3085d6', }
                  'Not Deleted!',
                  response.message,
                  'error'

                  )
               

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.reloadPage();
      }
    });
  };
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
  }
  filesubmit(bulkupload) {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({ headers: headers });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/

      var data = formData;
      var method = "post";
      var url = "uploadfilesmixerassembly";
      var i: number;
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for (i = 0; i < data.length; i++) {
              if (data[i].code == 201) {
                this.toastr.success(data[i].message, 'Success!');
                this.modalRef.close();
              } else {
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.reloadPage();
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  }
  downloadFile() {

    return this.http
      .get('assets/uploads/Mixerassembly_master.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'Mixerassembly_master.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  reset(form:NgForm){
    form.resetForm();
  }
}
