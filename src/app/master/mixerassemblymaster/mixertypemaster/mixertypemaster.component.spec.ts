import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixertypemasterComponent } from './mixertypemaster.component';

describe('MixertypemasterComponent', () => {
  let component: MixertypemasterComponent;
  let fixture: ComponentFixture<MixertypemasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixertypemasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixertypemasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
