import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MixerassemblymasterComponent } from './mixerassemblymaster.component'; 
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { mixertypeRoutes } from './mixerassemblymaster.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mixertypeRoutes),
      ],
  declarations: [MixerassemblymasterComponent]
})
export class MixerassemblymasterModule { }
