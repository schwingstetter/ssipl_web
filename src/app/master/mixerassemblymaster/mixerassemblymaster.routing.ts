import { Routes } from '@angular/router';


export const mixertypeRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'mixertypemaster',
                loadChildren: './mixertypemaster/mixertypemaster.module#MixertypemasterModule'
            },{
                path: 'hydrauliccombination',
                loadChildren: './hydrauliccombination/hydrauliccombination.module#HydrauliccombinationModule'

            },{
                path: 'distancecorrelation',
                loadChildren: './distanccecorrelation/distanccecorrelation.module#DistanccecorrelationModule'
            },{
                path: 'subassy',
                loadChildren: './sub-assy/sub-assy.module#SubAssyModule'
            },
            {
                path:'torquemixer',
                loadChildren:'./../torque/torque.module#TorqueModule'
            }
            
        ]
    }
];
