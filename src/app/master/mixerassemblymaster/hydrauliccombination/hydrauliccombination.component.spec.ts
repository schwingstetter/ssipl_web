import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HydrauliccombinationComponent } from './hydrauliccombination.component';

describe('HydrauliccombinationComponent', () => {
  let component: HydrauliccombinationComponent;
  let fixture: ComponentFixture<HydrauliccombinationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HydrauliccombinationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HydrauliccombinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
