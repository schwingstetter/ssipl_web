import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HydrauliccombinationComponent } from './hydrauliccombination.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const hydraulicroutes: Routes = [
  {
    path: '',
    component: HydrauliccombinationComponent,
    data: {
      breadcrumb: 'Hydraulic Combination',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(hydraulicroutes),
    SharedModule
  ],
  declarations: [HydrauliccombinationComponent]
})
export class HydrauliccombinationModule { }
