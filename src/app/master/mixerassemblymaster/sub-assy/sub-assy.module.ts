import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubAssyComponent } from './sub-assy.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const subassemblyroutes: Routes = [
  {
    path: '',
    component: SubAssyComponent,
    data: {
      breadcrumb: 'Sub Assembly Master',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(subassemblyroutes),
    SharedModule
  ],
  declarations: [SubAssyComponent]
})
export class SubAssyModule { }
