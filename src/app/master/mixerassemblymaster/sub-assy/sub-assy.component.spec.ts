import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubAssyComponent } from './sub-assy.component';

describe('SubAssyComponent', () => {
  let component: SubAssyComponent;
  let fixture: ComponentFixture<SubAssyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubAssyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubAssyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
