import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DistanccecorrelationComponent } from './distanccecorrelation.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const distanceroutes: Routes = [
  {
    path: '',
    component: DistanccecorrelationComponent,
    data: {
      breadcrumb: 'Distance Correlation',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(distanceroutes),
    SharedModule
  ],
  declarations: [DistanccecorrelationComponent]
})
export class DistanccecorrelationModule { }
