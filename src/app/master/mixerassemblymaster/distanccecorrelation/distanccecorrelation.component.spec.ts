import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistanccecorrelationComponent } from './distanccecorrelation.component';

describe('DistanccecorrelationComponent', () => {
  let component: DistanccecorrelationComponent;
  let fixture: ComponentFixture<DistanccecorrelationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistanccecorrelationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistanccecorrelationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
