import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { resetFakeAsyncZone } from '@angular/core/testing';

@Component({
  selector: 'app-fab-stages',
  providers: [AjaxService, AuthService],
  templateUrl: './fab-stages.component.html',
  styleUrls: ['./fab-stages.component.css']
})

export class FabStagesComponent implements OnInit {
  options = ['Fabrication','Assembly'];
  optionSelected: any = "";

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "";
  public sortOrder: string = "desc";
  hours :any;
  minutes :any;
  second :any;
  public switchcase:string = "";
  public id:number = 10;
  public taskassignment:string="";
  public drummodel:string="";
  public edittask:any = { };
  public edittime:any = { };
  arrayBuffer: any;
  file: File;
  auditPhotoUploader: FileUploader;
  public RequestOptions:any;
  file_name:FileList;
  position: string = 'bottom-right';
  title: string;
  msg: string;
  showClose: boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: any;
  closeOther: boolean = false;
  tenentIDFileName:any;
  public show:boolean = false;
  public buttonName:any = 'Show';
  private modalRef: NgbModalRef;
  closeResult: string;
  seconds = true;
  workstationnames:any;
  operationcodes:any;
  opcode:any;
  drumcubic:any;
  drum:any;
  drumcubicvalue:any;
  dividedtime:any;
  timerequiredinminutes:any;
  timerequiredmin:any;
  assydividedtime:any;
  dividedtime1:any;
  dividedtime17:any;
  dividedtime33:any;
  dividedtim50:any;
  dividedtime67:any;
  dividedtime83:any;
  dividedtime100:any;
  dividedtime14:any;
  dividedtime29:any;
  dividedtime43:any;
  dividedtime57:any;
  dividedtime71:any;
  dividedtime86:any;
  dividedtime13:any ;
  dividedtime25:any;
  dividedtime38:any ;
  dividedtime50:any;
  dividedtime63:any ;
  dividedtime75:any ;
  dividedtime88:any;
  assydividedtime1:any;
  isavailable:any;
  dividevalue:any='4';
stationcodename = 'Fabrication';
stationcodname = 'Assembly';
drummodelid :any;
  dtOptions: DataTables.Settings = {};
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);

   }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "taskassignmenttargets";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
            this.taskassignment = datas.message;
            table.destroy();
            setTimeout(() => { 
              $('.datatable').DataTable();
            }, 1000);
          }else{
          }          
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      var data1: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "conemappingmodel";
      //alert(url);
      this.AjaxService.ajaxpost(data1, method, url)
        .subscribe(
          (datas) => {
        //    alert(datas.code);
            if(datas.code == 201){
             this.drummodel=datas.message;
            
             }else{
    
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  timechanged(newTime) {

if(this.optionSelected == 1)
{ 
var minutes = (+newTime.hour) * 60 + (+newTime.minute);  
this.timerequiredinminutes = minutes;
this.dividedtime100=minutes;
this.edittask.time_required=minutes;
this.dividedtime=minutes / this.dividevalue;
console.log(this.dividedtime);
console.log(this.dividevalue);
if(this.dividevalue == 1)
{
  this.dividedtime25 = Math.trunc((minutes * 25) / 100);
  this.edittask.t_25 = Math.trunc((minutes * 25) / 100);
  this.dividedtime50 = Math.trunc((minutes * 50) / 100);
  this.edittask.t_50 = Math.trunc((minutes * 50) / 100);
  this.dividedtime75 = Math.trunc((minutes * 75) / 100);  
  this.edittask.t_75 = Math.trunc((minutes * 75) / 100);
}
if(this.dividevalue == 6)
{
  this.dividedtime17 = Math.trunc((minutes * 17) / 100);
  this.edittask.t_17 = Math.trunc((minutes * 17) / 100);
  this.dividedtime33 = Math.trunc((minutes * 33) / 100);
  this.edittask.t_33 = Math.trunc((minutes * 33) / 100);
  this.dividedtime50 = Math.trunc((minutes * 50) / 100);
  this.edittask.t_50 = Math.trunc((minutes * 50) / 100);
  this.dividedtime67 = Math.trunc((minutes * 67) / 100);
  this.edittask.t_67 = Math.trunc((minutes * 67) / 100);
  this.dividedtime83 = Math.trunc((minutes * 83) / 100);
  this.edittask.t_83 = Math.trunc((minutes * 83) / 100);
}
if(this.dividevalue == 7)
{
  this.dividedtime14 = Math.trunc((minutes * 14) / 100);
  this.edittask.t_14 = Math.trunc((minutes * 14) / 100);
  this.dividedtime29 = Math.trunc((minutes * 29) / 100);
  this.edittask.t_29 = Math.trunc((minutes * 29) / 100);
  this.dividedtime43 = Math.trunc((minutes * 43) / 100);
  this.edittask.t_43 = Math.trunc((minutes * 43) / 100);
  this.dividedtime57 = Math.trunc((minutes * 57) / 100);
  this.edittask.t_57 = Math.trunc((minutes * 57) / 100);
  this.dividedtime71 = Math.trunc((minutes * 71) / 100);
  this.edittask.t_71 = Math.trunc((minutes * 71) / 100);
  this.dividedtime86 = Math.trunc((minutes * 86) / 100);
  this.edittask.t_86 = Math.trunc((minutes * 86) / 100);
}
if(this.dividevalue == 8)
{
  this.dividedtime13 = Math.trunc((minutes * 13) / 100);
  this.edittask.t_13 = Math.trunc((minutes * 13) / 100);
  this.dividedtime25 = Math.trunc((minutes * 25) / 100);
  this.edittask.t_25 = Math.trunc((minutes * 25) / 100);
  this.dividedtime38 = Math.trunc((minutes * 38) / 100);
  this.edittask.t_38 = Math.trunc((minutes * 38) / 100);
  this.dividedtime50 = Math.trunc((minutes * 50) / 100);
  this.edittask.t_50 = Math.trunc((minutes * 50) / 100);
  this.dividedtime63 = Math.trunc((minutes * 63) / 100);
  this.edittask.t_63 = Math.trunc((minutes * 63) / 100);
  this.dividedtime75 = Math.trunc((minutes * 75) / 100);
  this.edittask.t_75 = Math.trunc((minutes * 75) / 100);
  this.dividedtime88 = Math.trunc((minutes * 88) / 100);
  this.edittask.t_88 = Math.trunc((minutes * 88) / 100);
}
}
else if(this.optionSelected == 2)
{
  var minutes = (+newTime.hour) * 60 + (+newTime.minute);  
  this.timerequiredinminutes = minutes;
  this.dividedtime100=minutes;
  this.edittask.time_required=minutes;
  this.dividedtime=minutes / this.dividevalue;
  this.dividedtime50 = Math.trunc((minutes * 50) / 100);
  this.edittask.t_50 = Math.trunc((minutes * 50) / 100);
}
else //fab 1-7 checking process
{
  var minutes = (+newTime.hour) * 60 + (+newTime.minute);  
  this.timerequiredinminutes = minutes;
  this.edittask.time_required=minutes;
  this.dividedtime=minutes / this.dividevalue;
  this.dividedtime100=minutes;
  this.dividedtime50 = Math.trunc((minutes * 50) / 100);
  this.edittask.t_50 = Math.trunc((minutes * 50) / 100);
}

  }
  timechangededit(newTime) {
    if(this.optionSelected == 1)
    {
    var minutes = (+newTime.hour) * 60 + (+newTime.minute);  
    this.timerequiredinminutes = minutes;
    this.dividedtime1=minutes / this.dividevalue;
  
  }
  else if(this.optionSelected == 2)
  {
    var minutes = (+newTime.hour) * 60 + (+newTime.minute);  
    this.timerequiredinminutes = minutes;
  this.assydividedtime1=minutes / 2;
  }
      }
  getworkstation(event)
  {
    var data = {"opcode": event};
    var method = "post";
    var url = "workstation";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {
            this.workstationnames = datas.message[0].list_of_operation;
          this.edittask.work_station=datas.message[0].list_of_operation;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
   
   //  this.drummodelchanged(this.drummodelid);
  }
  getoperationcode()
  {
    var data = {"basedon": this.optionSelected};
    var method = "post";
    var url = "operationcode";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {
            this.operationcodes = datas.message;
          
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    
  }
  getstationname(event)
  {
    this.opcode=event;
    this.dividevalue="";

    this.dividedtime1="";
    this.dividedtime17="";
    this.dividedtime33="";
    this.dividedtim50="";
    this.dividedtime67="";
    this.dividedtime83="";
    this.dividedtime100="";
    this.dividedtime14="";
    this.dividedtime29="";
    this.dividedtime43="";
    this.dividedtime57="";
    this.dividedtime71="";
    this.dividedtime86="";
    this.dividedtime13="";
    this.dividedtime25="";
    this.dividedtime38="";
    this.dividedtime50="";
    this.dividedtime63="";
    this.dividedtime75="";
    this.dividedtime88="";
    if(this.opcode == 50 || this.opcode == 51 || this.opcode == 52 || this.opcode == 53 || this.opcode == 54 || this.opcode == 55 || this.opcode == 56|| this.opcode == 57|| this.opcode == 58|| this.opcode == 59|| this.opcode == 60|| this.opcode == 61|| this.opcode == 62|| this.opcode == 63|| this.opcode == 64|| this.opcode == 65|| this.opcode == 66|| this.opcode == 67|| this.opcode == 68|| this.opcode == 69|| this.opcode == 70|| this.opcode == 71|| this.opcode == 72|| this.opcode == 73|| this.opcode == 74|| this.opcode == 75|| this.opcode == 76|| this.opcode == 77|| this.opcode == 78|| this.opcode == 79|| this.opcode == 80|| this.opcode == 81|| this.opcode == 82|| this.opcode == 83)
    {
      this.isavailable ='1' ;
    }  
    else if((this.opcode == 43 || this.opcode == 44 || this.opcode == 45 || this.opcode == 46 || this.opcode ==47 || this.opcode == 48 || this.opcode == 49)) 
    { 
      this.isavailable ='2' ;
    } 
    else if(this.opcode != 1 && this.opcode != 2 && this.opcode != 3 && this.opcode != 4 && this.opcode != 32 && this.opcode != 33 && this.opcode != 34 && this.opcode != 35 && this.opcode != 36 && this.opcode != 37 && this.opcode != 38 && this.opcode != 39 && this.opcode != 40 && this.opcode != 41 && this.opcode != 42)
    {
      this.isavailable = '3';
      this.dividevalue= '100';
    }
    else
    {
      this.isavailable = '4';
    }
    this.getworkstation(event);
  }
  reloadPage() {
    this.dividevalue="";
this.drummodelid = event;
this.dividedtime1="";
this.dividedtime17="";
this.dividedtime33="";
this.dividedtim50="";
this.dividedtime67="";
this.dividedtime83="";
this.dividedtime100="";
this.dividedtime14="";
this.dividedtime29="";
this.dividedtime43="";
this.dividedtime57="";
this.dividedtime71="";
this.dividedtime86="";
this.dividedtime13="";
this.dividedtime25="";
this.dividedtime38="";
this.dividedtime50="";
this.dividedtime63="";
this.dividedtime75="";
this.dividedtime88="";
this.workstationnames = "";
this.drummodel="";
     this.ngOnInit();
  }
 
  addtask(form: NgForm,add){

    if(this.optionSelected == "Select")
    {
      this.toastr.error('Select Station name');
    }
    else
    {
    var data = form.value;
    var cubicvalue="cubicvalue"
    data[cubicvalue]=this.dividevalue;
    var switchcase = "switchcase";
    var dividedvalue = "dividedvalue";
    var value = "insert";
    if(this.optionSelected == '1')
    {
    var value1 = this.dividedtime;
    }
    else
    if(this.optionSelected == '2')
    {
      var value1 = this.assydividedtime;
    }
    data[switchcase] = value;
    data[dividedvalue] = value1;
    var time= "time";
    data[time]=this.timerequiredinminutes;
    var method= "post";
    var url="taskassignmenttargets";
    this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => 
      {
        if(datas.code==201){
          this.toastr.success(datas.message, datas.type);                  
          this.type="";
          this.optionSelected="";
          this.dividevalue="";
          form.resetForm();
          this.reset();
          this.modalRef.close();
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }

      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
      
    
    );
  }
  };

  onOptionsSelected(event){
    this.optionSelected="";
    this.type="";
    this.dividevalue="";

this.dividedtime1="";
this.dividedtime17="";
this.dividedtime33="";
this.dividedtim50="";
this.dividedtime67="";
this.dividedtime83="";
this.dividedtime100="";
this.dividedtime14="";
this.dividedtime29="";
this.dividedtime43="";
this.dividedtime57="";
this.dividedtime71="";
this.dividedtime86="";
this.dividedtime13="";
this.dividedtime25="";
this.dividedtime38="";
this.dividedtime50="";
this.dividedtime63="";
this.dividedtime75="";
this.dividedtime88="";
this.workstationnames="";
this.optionSelected=event;
if(this.optionSelected=='1')
{
this.type=1;
}
else
{
this.type=2;
}
this.getoperationcode();
//alert(event);
   }
   drummodelchanged(event){
this.dividevalue="";
this.drummodelid = event;
this.dividedtime1="";
this.dividedtime17="";
this.dividedtime33="";
this.dividedtim50="";
this.dividedtime67="";
this.dividedtime83="";
this.dividedtime100="";
this.dividedtime14="";
this.dividedtime29="";
this.dividedtime43="";
this.dividedtime57="";
this.dividedtime71="";
this.dividedtime86="";
this.dividedtime13="";
this.dividedtime25="";
this.dividedtime38="";
this.dividedtime50="";
this.dividedtime63="";
this.dividedtime75="";
this.dividedtime88="";

var data = {"drumid": event};
var method = "post";
var url = "getdrummodelid";
this.AjaxService.ajaxpost(data, method, url)
  .subscribe(
    (datas) => {          
   
        this.drumcubic = datas[0].drum_model;
        //this.drumcubic = datas[0].drum_model.split(' ');
      //  this.drumcubicvalue=this.drumcubic[0];
    
        if(this.opcode == 59 || this.opcode == 60 || this.opcode == 61 || this.opcode == 62 || this.opcode == 63)
        {
          this.drum='1';   
              
         if(this.drumcubic.includes(8))
         {
           this.dividevalue = '6';
         }
         else if(this.drumcubic.includes(9))
         {
           this.dividevalue = '7';
         }
         else if(this.drumcubic.includes(10))
         {
           this.dividevalue = '8';
         }
         else
         {
          this.dividevalue = '1';
         }
        }
        else
        {
          this.dividevalue = '1';
        }
        console.log("divide",this.dividevalue);
       
    },
    (err) => {
      if (err.status == 403) {
        alert(err._body);
      }
    }
  );
}
  editfunction(item) {
    this.edittask=item;   
    this.onOptionsSelected(item.station_name);
    this.timerequiredmin=this.edittask.time_required;    
    console.log(this.edittask.time_required)
    if(this.timerequiredmin > 60)
    {
    this.hours=this.timerequiredmin / 60;
    console.log("hours"+ this.hours);
    if(this.hours.toString().split("."))
    {
      var splitedminutes = this.hours.toString().split(".");      
      if(splitedminutes[1])
      {
        console.log( Math.trunc((splitedminutes[1])));
        //         if( Math.trunc((splitedminutes[1]) > 60)))
        // {

        // }
        this.minutes= this.timerequiredmin % 60;
      }
    }
    else{
      this.minutes=0 * splitedminutes;
    }
  }
  else
{
  this.hours = '00';
  this.minutes = this.timerequiredmin;
}
 
   // this.hours=this.edittask.time_required.split(":");
  console.log(this.minutes);
    this.edittime={hour : this.hours,minute : this.minutes,second : '00'};
    this.dividedtime1 = item.splitted_time;
    this.assydividedtime1 = item.splitted_time;
    this.opcode= item.operation_code;
    this.drummodelid = item.drum_model;
    this.getstationname(this.opcode);
    this.drummodelchanged(this.drummodelid);
    console.log(this.edittime);
  };
  edittasks(editfrm,edit){
    var data = editfrm.value;
    var switchcase = "switchcase";
    var dividedvalue = "dividedvalue";
    var value = "edit";
    var value1 = this.dividedtime1;
    data[switchcase] = value;
    data[dividedvalue] = value1;
    var time= "time";
    data[time]=this.timerequiredinminutes;
    var cubicvalue="cubicvalue";
    data[cubicvalue]=this.dividevalue;
    var method = "post";
    var url = "taskassignmenttargets";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
          
            this.toastr.success(datas.message, datas.type);     
            this.reloadPage();       
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //this.toastr.success('Hello world!', 'Toastr fun!');
  };
  deletetaskassignment(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "taskassignmenttargets";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning', 
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
           
            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
              }
              else {
              
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
        
        this.reloadPage();
      }
    });
  };
  fileUpload(event) {
    const url = `http://localhost/schwingbackend/public/api/uploadfiles`;

    let fileList: FileList = event.target.files;
   this.file_name=fileList;
  }
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({ headers: headers });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/
      var data = formData;
      var method = "post";
      var url = "uploadfilestaskassignmenttargets";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for(i=0;i<data.length;i++){
              if(data[i].code==201){
                this.toastr.success(data[i].message, 'Success!');                
                this.modalRef.close();
              }else{
                this.toastr.error(data[i].message, 'Error!');
              }
            }    
            this.reloadPage();        
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  }
  reset()
  {
   this.reloadPage();
    this.opcode='';
    this.dividedtime='';
    this.dividedtime1='';
    this.workstationnames='';
    this.isavailable='';
    this.drumcubicvalue='';
    this.optionSelected='';
  }
  downloadFile() {

    return this.http
      .get('assets/uploads/taskassignmenttargets.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""  })
      .map(res => {
        return {
          filename: 'TaskAssignmentTargets.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
          console.log('start download:',res);
          var url = window.URL.createObjectURL(res.data);
          var a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = url;
          a.download = res.filename;
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove(); // remove the element
        }, error => {
          console.log('download error:', JSON.stringify(error));
        }, () => {
          console.log('Completed file download.')
        });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.optionSelected="";
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
}
