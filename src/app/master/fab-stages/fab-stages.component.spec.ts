import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabStagesComponent } from './fab-stages.component';

describe('FabStagesComponent', () => {
  let component: FabStagesComponent;
  let fixture: ComponentFixture<FabStagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabStagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabStagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
