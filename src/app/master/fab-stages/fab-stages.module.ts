import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { FabStagesComponent } from './fab-stages.component';

export const FabstagesRoutes: Routes = [
  {
    path: '',
    component: FabStagesComponent,
    data: {
      breadcrumb: 'Task Assignment Targets',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FabstagesRoutes),
    SharedModule
  ],
  declarations: [FabStagesComponent]
})
export class FabStagesModule { }
