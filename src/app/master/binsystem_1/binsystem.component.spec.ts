import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinsystemComponent } from './binsystem.component';

describe('BinsystemComponent', () => {
  let component: BinsystemComponent;
  let fixture: ComponentFixture<BinsystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinsystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinsystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
