import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MountingBomComponent } from './mounting-bom.component';

describe('MountingBomComponent', () => {
  let component: MountingBomComponent;
  let fixture: ComponentFixture<MountingBomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MountingBomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MountingBomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
