import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MountingBomComponent } from './mounting-bom.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const mounting_bomroutes: Routes = [
  {
    path: '',
    component: MountingBomComponent,
    data: {
      breadcrumb: 'Mounting BOM Master',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mounting_bomroutes),
    SharedModule
  ],
  declarations: [MountingBomComponent]
})
export class MountingBomModule { }
