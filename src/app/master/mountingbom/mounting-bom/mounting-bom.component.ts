import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mounting-bom',
  templateUrl: './mounting-bom.component.html',
  styleUrls: ['./mounting-bom.component.css']
})
export class MountingBomComponent implements OnInit {

  options1 :any[];
  optionscomp :any[];
  optionsMap:any[];
  dateval: string = null;
  file_name:FileList;
  options :any[];
  optionmodel :any[];
  compoptions:string="";
  optionSelected: any = "Select";
  optionSelected1: any = "Select";
  optionmodelSelected: any = "Select";
  editplanning : string ="";
  details : string ="";
  private modalRef: NgbModalRef;
  closeOther: boolean = false;
  closeResult: string;
  model: NgbDateStruct;
  welcomedate: NgbDateStruct;
  
  public mountinginternal:any={};
  public editmountinginternals:any={};
  public mountingheaders:any={};
  dates: string;
  revdates: string;;
  public editrevdate:any={};
  public editdates:any={};
  public headernos: any = {};


  constructor(public http: Http, private AjaxService: AjaxService, private router: Router,private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    this.model = this.calendar.getToday();  
    this.welcomedate = this.calendar.getToday();    
   }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mountinginternalpart";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mountinginternal = datas;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getheader();
  };  
  getheader(){
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mountingheader";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mountingheaders = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  addmountinbom(frm: NgForm, add) {
    var data = frm.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "mountinginternalpart";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.reset(frm);
            this.ngOnInit();
            this.headernos = "";
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );
  };
  reloadPage() {
    this.ngOnInit();
  }
  editfunction(item) {
    this.editmountinginternals = item;
    this.revdates = this.editmountinginternals.REVISED_date.split("-");
    var revdatesyear = +this.revdates[0];
    var revdatesmonth = +this.revdates[1];
    var revdatesday = +this.revdates[2];
    this.editrevdate = { 'year': revdatesyear, 'month': revdatesmonth, 'day': revdatesday };
    this.dates = this.editmountinginternals.DATE.split("-");
    var datesyear = +this.dates[0];
    var datesmonth = +this.dates[1];
    var datesday = +this.dates[2];
    this.editdates = { 'year': datesyear, 'month': datesmonth, 'day': datesday };
  };
  reset(form: NgForm) {
    form.resetForm();
    this.model = this.calendar.getToday();     
    this.headernos = "";
  };
  
  editmountinginternal(editfrm:NgForm){    
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "mountinginternalpart";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.reset(editfrm);
            this.ngOnInit();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );
  };
  
  deletemountingbom(id) {

    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "mountinginternalpart";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Deleted!',
                  response.message,
                  'success'
                )
              }
              else if(response.code==403) {
                swal({
                  title: 'Alert',
                  text: response.message,
                  type: 'error',
                  confirmButtonColor: '#3085d6',
            
                })
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.reloadPage();
      }
    });
  };

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  downloadFile() {

    return this.http
      .get('assets/uploads/Mountingbom.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'Mountingbom.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  };


  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
   this.file_name=fileList;
  };
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();

      let options = new RequestOptions({ headers: headers });

      var data = formData;
      var method = "post";
      var url = "uploadfilesmountinginternalpart";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for(i=0;i<data.length;i++){
              if(data[i].code==201){
                this.toastr.success(data[i].message, 'Success!');                               
                this.modalRef.close();
              }else{
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.reloadPage(); 
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };

}
