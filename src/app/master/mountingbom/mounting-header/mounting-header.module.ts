import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MountingHeaderComponent } from './mounting-header.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';


export const mounting_headerroutes: Routes = [
  {
    path: '',
    component: MountingHeaderComponent,
    data: {
      breadcrumb: 'Mounting Header',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mounting_headerroutes),
    SharedModule
  ],
  declarations: [MountingHeaderComponent]
})
export class MountingHeaderModule { }
