import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MountingHeaderComponent } from './mounting-header.component';

describe('MountingHeaderComponent', () => {
  let component: MountingHeaderComponent;
  let fixture: ComponentFixture<MountingHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MountingHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MountingHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
