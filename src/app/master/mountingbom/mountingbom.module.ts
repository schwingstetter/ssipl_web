import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MountingbomComponent } from './mountingbom.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { mountingbomRoutes } from './mountingbom.routing'

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mountingbomRoutes),
    SharedModule
  ],
  declarations: [MountingbomComponent]
})
export class MountingbomModule { }
