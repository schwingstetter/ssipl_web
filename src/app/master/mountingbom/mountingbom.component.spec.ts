import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MountingbomComponent } from './mountingbom.component';

describe('MountingbomComponent', () => {
  let component: MountingbomComponent;
  let fixture: ComponentFixture<MountingbomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MountingbomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MountingbomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
