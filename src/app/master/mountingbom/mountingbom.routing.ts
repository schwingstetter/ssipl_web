import { Routes } from '@angular/router';


export const mountingbomRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'mounting_header',
                loadChildren: './mounting-header/mounting-header.module#MountingHeaderModule'
            },{
                path: 'mounting_bom',
                loadChildren: './mounting-bom/mounting-bom.module#MountingBomModule'

            }  
        ]
    }
];
