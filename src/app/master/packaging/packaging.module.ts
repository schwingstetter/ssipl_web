import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagingComponent } from './packaging.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { packagingRoutes } from './packaging.routing'

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(packagingRoutes),
    SharedModule
  ],
  declarations: [PackagingComponent]
})
export class PackagingModule { }
