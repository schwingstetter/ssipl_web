import { Routes } from '@angular/router';


export const packagingRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'packaging_header',
                loadChildren: './packaging-header/packaging-header.module#PackagingHeaderModule'
            },{
                path: 'packaging_bom',
                loadChildren: './packaging-bom/packaging-bom.module#PackagingBomModule'

            }  
        ]
    }
];
