import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagingHeaderComponent } from './packaging-header.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const packaging_headerroutes: Routes = [
  {
    path: '',
    component: PackagingHeaderComponent,
    data: {
      breadcrumb: 'Packaging Header',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(packaging_headerroutes),
    SharedModule
  ],
  declarations: [PackagingHeaderComponent]
})
export class PackagingHeaderModule { }
