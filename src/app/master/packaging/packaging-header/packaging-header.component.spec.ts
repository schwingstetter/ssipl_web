import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackagingHeaderComponent } from './packaging-header.component';

describe('PackagingHeaderComponent', () => {
  let component: PackagingHeaderComponent;
  let fixture: ComponentFixture<PackagingHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackagingHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackagingHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
