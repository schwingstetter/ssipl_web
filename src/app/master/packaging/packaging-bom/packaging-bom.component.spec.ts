import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackagingBomComponent } from './packaging-bom.component';

describe('PackagingBomComponent', () => {
  let component: PackagingBomComponent;
  let fixture: ComponentFixture<PackagingBomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackagingBomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackagingBomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
