import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ForgotpwdComponent } from './forgotpwd.component';

export const forgotroutes: Routes = [
  {
    path: '',
    component: ForgotpwdComponent,
   
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(forgotroutes),
    SharedModule
  ],
  declarations: [ForgotpwdComponent]
})
export class ForgotpwdModule { }
