import { Routes } from '@angular/router';


export const storesmasterRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'binsystem',
                loadChildren: './binsystem/binsystem.module#BinsystemModule'
            },{
                path: 'binsizemake',
                loadChildren: './binsizemake/binsizemake.module#BinsizemakeModule'
            }   
        ]
    }
];
