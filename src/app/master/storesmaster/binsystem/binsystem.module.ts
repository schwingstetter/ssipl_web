import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BinsystemComponent } from './binsystem.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const binsystemRoutes: Routes = [
  {
    path: '',
    component: BinsystemComponent,
    data: {
      breadcrumb: '2 Bin System',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(binsystemRoutes),
    SharedModule
  ],
  declarations: [BinsystemComponent]
})
export class BinsystemModule { }
