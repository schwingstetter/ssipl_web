import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoresmasterComponent } from './storesmaster.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { storesmasterRoutes } from './storesmaster.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(storesmasterRoutes),
    SharedModule
  ],
  declarations: [StoresmasterComponent]
})
export class StoresmasterModule { }
