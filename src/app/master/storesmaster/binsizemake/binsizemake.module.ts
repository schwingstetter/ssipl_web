import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BinsizemakeComponent } from './binsizemake.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const binsizemakeRoutes: Routes = [
  {
    path: '',
    component: BinsizemakeComponent,
    data: {
      breadcrumb: 'Size Make',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(binsizemakeRoutes),
    SharedModule
  ],
  declarations: [BinsizemakeComponent]
})
export class BinsizemakeModule { }
