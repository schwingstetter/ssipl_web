import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinsizemakeComponent } from './binsizemake.component';

describe('BinsizemakeComponent', () => {
  let component: BinsizemakeComponent;
  let fixture: ComponentFixture<BinsizemakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinsizemakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinsizemakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
