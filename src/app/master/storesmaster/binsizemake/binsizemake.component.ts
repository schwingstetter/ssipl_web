import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-binsizemake',
  templateUrl: './binsizemake.component.html',
  styleUrls: ['./binsizemake.component.css']
})
export class BinsizemakeComponent implements OnInit {

  public binsystem: string = "";
  public editbinsystem: any = {};
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  public getlabelsize:string="";
  public storagelocaton: any = {};
  public sto_loc:any = {};
  public store_locat:any={};


  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {       

    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "binsizemake";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.binsystem = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

    var store_data: { switchcase: string } = { switchcase: "getstorelocation" };
    var store_method = "post";
    var store_url = "binsizemake";
    this.AjaxService.ajaxpost(store_data, store_method, store_url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.store_locat = datas;

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );

  }

  getlabesize(value){
    if(value=="1"){
      this.getlabelsize ="1 x 64 x 20";
      this.editbinsystem.label_size="1 x 64 x 20";
    }else if(value=="2"){
      this.getlabelsize ="1 X 93 X 32";
      this.editbinsystem.label_size="1 x 64 x 20";
    } else if (value == "3") {
      this.getlabelsize ="1 X 93 X 32";
      this.editbinsystem.label_size="1 x 64 x 20";
    } else if (value == "4") {
      this.getlabelsize ="1 X 153 X 52";
      this.editbinsystem.label_size="1 x 64 x 20";
    } else if (value == "5") {
      this.getlabelsize = "1 X 93 X 32";
      this.editbinsystem.label_size = "1 x 64 x 20";
    }
  }

  addbinsystem(form: NgForm, addnew) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "binsizemake";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.ngOnInit();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  editfunction(item) {
    this.editbinsystem = item;
    this.sto_loc=item.storage_location.split(',');
  };
  editbinsystems(editfrm, edit) {
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "binsizemake";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.ngOnInit();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  deletebinsystem(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "binsizemake";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                );
                this.ngOnInit();
                this.ngOnInit();
              }
              else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.ngOnInit();
      }
    });
  };


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
}
