import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoresmasterComponent } from './storesmaster.component';

describe('StoresmasterComponent', () => {
  let component: StoresmasterComponent;
  let fixture: ComponentFixture<StoresmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoresmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoresmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
