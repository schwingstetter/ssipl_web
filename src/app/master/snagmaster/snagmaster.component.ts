import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-snagmaster',
  templateUrl: './snagmaster.component.html',
  styleUrls: ['./snagmaster.component.css']
})
export class SnagmasterComponent implements OnInit {
  public snagmaster: string = "";
  public optionSelected: string = "";
  public editsnagmasters: any = "";
  public RequestOptions: any;
  public snagtype: any;
  public fitter: any;
  public welder: any;
  public na: any;
  public responsible: any;
  public editfitter: any;
  public editwelder: any;
  public editna: any;
  public resperson: any;
  public snagcodeinc: string = "";
  public closeOnConfirm: boolean;
  public workstationnames: any;
  public operationcodes: any;
  public version: any;
  public snagbasedcategory: string;
  public reportingstation: string;
  public total_hours: any=0;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  persons: string[] = new Array();
  words2 = [{ value: '' }];
  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.snagtype =null;
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "snagmasters";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
           
            for (var i = 0; i < datas.message.length; i++)
             {
              
              datas.message[i].responsible_person = JSON.parse(datas.message[i].responsible_person);
            }
            this.snagmaster = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

    this.getversion();
    //   this.getworkstation();
  };
  getstationname(event) {
    this.getworkstation(event);
  }
  getworkstation(event) {
    var data = { "opcode": event };
    var method = "post";
    var url = "workstation";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.workstationnames = datas.message[0].list_of_operation;
            this.editsnagmasters.workstation_name = datas.message[0].list_of_operation;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

  }
  editcheckValue(event, ischecked) {

    if (ischecked.target.checked) {
      if (event == 'W') {
        this.welder = '1';
        this.persons.push('W');
        
      }
      else if (event == 'F') {
        this.fitter = '1';
        this.persons.push('F');

      }
      else {
        this.na = '1';
        this.persons = new Array();
        this.persons.push('NA');
      }
    }
    else {
      if (event == 'W') {
        this.welder = '0';
        this.persons.splice(this.persons.indexOf('W'), 1);
      }
      else if (event == 'F') {
        this.fitter = '0';
        this.persons.splice(this.persons.indexOf('F'), 1);
      }
      else {
        this.na = '0';
      }
    }
   
  }
  checkValue(event, ischecked) {

    if (ischecked) {
      if (event == 'W') {
        this.welder = '1';
        this.persons.push('W');
      }
      else if (event == 'F') {
        this.fitter = '1';
        this.persons.push('F');

      }
      else {
        this.na = '1';
        this.persons = new Array();
        this.persons.push('NA');
      }
    }
    else {
      if (event == 'W') {
        this.welder = '0';
        this.persons.splice(this.persons.indexOf('W'), 1);
      }
      else if (event == 'F') {
        this.fitter = '0';
        this.persons.splice(this.persons.indexOf('F'), 1);
      }
      else {
        this.na = '0';
      }
    }
  }
  getsnagcategory(event) {
    //  alert(event.value);

    var datavalue = event.value;
    var data = { "opcode": this.optionSelected };
    var method = "post";
    var url = "snagcategory";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.snagbasedcategory = datas.message;

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getsnagcode(event);
  }

  getversion() {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "version";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.version = datas.message;

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );


  }
  totalhours(event,form)
  {
    if(form.value.noofwelders){
      console.log("welders",form.value.noofwelders);
    }else{
      form.value.noofwelders = 0;
    }
    if(form.value.nooffitters){
      console.log(form.value.nooffitters);
    }else{
      form.value.nooffitters = 0;
    }
    if(form.value.manhours){
      console.log(form.value.manhours);
    }else{
      form.value.manhours=0;
    }
    this.total_hours=0;
    /* console.log('fitters '+form.value.nooffitters);
    console.log('welders '+form.value.noofwelders);
    console.log('manhours '+form.value.manhours);
    console.log('manhours '+((form.value.nooffitters) + (form.value.noofwelders))); */
    let nooffitters:number=parseInt(form.value.nooffitters);
    let noofwelders:number=parseInt(form.value.noofwelders);

    this.total_hours = (((nooffitters) + (noofwelders)) * form.value.manhours);
    this.editsnagmasters.total_manhours = this.total_hours;
  }
  getsnagcode(event) {
    var data = { "snagtype": this.optionSelected };
    var method = "post";
    var url = "snagcode";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            //alert(datas.message.length);
            if (datas.message != null) {
              var splitstring = (datas.message.snag_code).toString();
              var splittedarray = splitstring.split(" ");
              var snagid = splittedarray[3];
              splittedarray.splice(splittedarray.indexOf(snagid));
              splittedarray.push(((+snagid) + 1).toString());
              this.snagcodeinc = splittedarray.join(' ');
            }
            else {
              if (this.optionSelected == '1') {
                this.snagcodeinc = "F - SNG 1"
              }
              if (this.optionSelected == '2') {
                this.snagcodeinc = "A - SNG 1"
              }
              if (this.optionSelected == '3') {
                this.snagcodeinc = "PDI - SNG 1"
              }
            }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  reloadPage() {
    this.ngOnInit();
  };
  addsnagmaster(form: NgForm, addnew) {
    var data = form.value;
    var switchcase = "switchcase";
    var persons = "persons";
    var value = "insert";
    data[switchcase] = value;
    data[persons] = this.persons;
    var method = "post";
    var url = "snagmasters";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.modalRef.close();
            form.resetForm();
            this.snagcodeinc = '';
            this.workstationnames = '';
            this.welder = '0';
            this.fitter = '0';
            this.total_hours = 0;
            /* this.editsnagmasters.total_manhours=0; */
            this.persons=[];
            this.reloadPage();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  reset(form:NgForm) {
    form.resetForm();
    this.snagcodeinc = '';
    this.workstationnames = '';
    this.ngOnInit();
  }
  resetaddform(form:NgForm){
    form.resetForm();
    this.snagcodeinc = '';
    this.workstationnames = '';
    this.ngOnInit();
  }
  optionsselected(event) {
    alert(event);

  }
  editfunction(item) {
    this.editsnagmasters = item;
    var headers = item.responsible_person;
    this.editfitter = '';
    this.editwelder = '';
    this.editna = '';
    this.persons = new Array();
    if (headers.includes('F')) {
      this.editfitter = '1';
      this.persons.push('F');
    }
    if (headers.includes('W')) {
      this.editwelder = '1';
      this.persons.push('W');
    }
    if (headers.includes('NA')) {
      this.editna = '1';
      this.persons.push('NA');
    }
    this.onOptionsSelected(item.snag_type);
  };
  editsnagmaster(editfrm, edit) {
    console.log(editfrm.value);
    console.log(editfrm.value.snagtype);
    var data = editfrm.value;
    var switchcase = "switchcase";
    var persons = "persons";
    var snagtype = "snagtype";
    var value = "edit";
    data[switchcase] = value;
    data[persons] = this.persons;
    data[snagtype] = this.editsnagmasters.snag_type;
    var method = "post";
    var url = "snagmasters";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.welder = '0';
            this.fitter = '0';
            this.total_hours = 0;
            this.editsnagmasters.total_manhours=0;
            this.persons=[];
            this.modalRef.close();
            this.reloadPage();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  downloadFile() {

    return this.http
      .get('assets/uploads/snagmasters.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'snagmasters.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  };
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;

    let fileList: FileList = event.target.files;
    this.file_name = fileList;
  }
  filesubmit(bulkupload) {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();

      let options = new RequestOptions({ headers: headers });
      var data = formData;
      var method = "post";
      var url = "snagmasters_uploadfiles";
      var i: number;
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for (i = 0; i < data.length; i++) {
              if (data[i].code == 201) {
                this.toastr.success(data[i].message, 'Success!');
              } else {
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.reloadPage();
            this.modalRef.close();
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };
  onOptionsSelected(event) {
    this.optionSelected = event;
    this.snagtype = event;
    this.getoperationcode();
    //alert(event);
  }
  getoperationcode() {
    var data = { "basedon": this.optionSelected };
    var method = "post";
    var url = "operationcode";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.operationcodes = datas.message;

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getsnagcategory(event);
  }

  deletedrumtype(id,snag_type) {
    var data: { switchcase: string, id: any ,snag_type:any} = { switchcase: "delete", id: id,snag_type:snag_type };
    var method = "post";
    var url = "snagmasters";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  response.message,
                  'success'
                );
                this.reloadPage();
              }
              else if(response.code == 403){
				   swal({
      title: 'Alert',
      text:response.message,
      type: 'error',
      
      confirmButtonColor: '#3085d6',
     
    })

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };

}
