import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnagmasterComponent } from './snagmaster.component';

describe('SnagmasterComponent', () => {
  let component: SnagmasterComponent;
  let fixture: ComponentFixture<SnagmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnagmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnagmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
