import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnagmasterComponent } from './snagmaster.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const snagmasterRoutes: Routes = [
  {
    path: '',
    component: SnagmasterComponent,
    data: {
      breadcrumb: 'Snag Master',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(snagmasterRoutes),
    SharedModule
  ],
  declarations: [SnagmasterComponent]
})
export class SnagmasterModule { }
