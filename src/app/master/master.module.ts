import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MasterComponent } from './master.component';
import { CommonModule } from '@angular/common';
import { masterRoutes } from './master.routing';
import { SharedModule } from '../shared/shared.module';
/* import { StoresComponent } from './stores/stores.component';
import { MaterialIssueconfirmationComponent } from './stores/material-issueconfirmation/material-issueconfirmation.component'; */


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(masterRoutes)
  ],
  declarations: [MasterComponent/* StoresComponent, MaterialIssueconfirmationComponent*/]
})
export class MasterModule { }
