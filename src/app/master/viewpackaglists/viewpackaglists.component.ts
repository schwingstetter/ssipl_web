import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LocationStrategy } from '@angular/common'; 

@Component({
  selector: 'app-viewpackaglists',
  providers: [AjaxService, AuthService],
  templateUrl: './viewpackaglists.component.html',
  styleUrls: ['./viewpackaglists.component.css']
})
export class ViewpackaglistsComponent implements OnInit {

  private modalRef: NgbModalRef;
  closeResult: string;
  public mountingheaderss:any={};
  public mountingaccessoriess:any={};
  public packagingdetails:any={};
  file_name: FileList;
  public params:any={};
  myParams:string;
  private sub: any;
  id: any={};
  public datafoc:any={};

  constructor(public http: Http, private AjaxService: AjaxService,private myRoute: Router, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal,private route: ActivatedRoute) {
    this.toastr.setRootViewContainerRef(vcr);
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
   });
   
  }

  ngOnInit() {
    
    var data: { switchcase: string,id:string } = { switchcase: "viewpackaginglist",id:this.id };
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {            
            this.packagingdetails = datas.message[0];
            this.getmountingheader(datas.message[0].mounting_header);
            this.getmountingaccessories(datas.message[0].accessories);
            this.getcustfoc(datas.message[0].cust_id);            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
     
  }
  getmountingheader(mountingheaders){
    var data: { switchcase: string,id:string } = { switchcase: "viewpackagingmounting",id:mountingheaders };
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {            
            this.mountingheaderss = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmountingaccessories(accessories){
    var data: { switchcase: string,id:string } = { switchcase: "viewpackagingaccessories",id:accessories };
    var method = "post";
    var url = "getdetails";
    this.mountingaccessoriess=[];
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {            
            this.mountingaccessoriess = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getcustfoc(custdata){
    console.log(custdata);
    var customer_id=custdata;
    var data: { switchcase: string,id:string } = { switchcase: "getfoc",id:customer_id };
    var method = "post";
    var url = "getfocdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.datafoc=datas.message;            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

}

