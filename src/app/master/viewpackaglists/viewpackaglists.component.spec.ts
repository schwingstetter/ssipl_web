import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpackaglistsComponent } from './viewpackaglists.component';

describe('ViewpackaglistsComponent', () => {
  let component: ViewpackaglistsComponent;
  let fixture: ComponentFixture<ViewpackaglistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpackaglistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpackaglistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
