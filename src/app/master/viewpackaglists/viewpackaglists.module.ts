import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewpackaglistsComponent } from './viewpackaglists.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';


export const viewpackagingRoutes: Routes = [
  {
    path: '',
    component: ViewpackaglistsComponent,
    data: {
      breadcrumb: 'View Packaging',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(viewpackagingRoutes),
    SharedModule
  ],
  declarations: [ViewpackaglistsComponent]
})
export class ViewpackaglistsModule { }
