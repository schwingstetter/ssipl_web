import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-packaginglist',
  templateUrl: './packaginglist.component.html',
  styleUrls: ['./packaginglist.component.css']
})
export class PackaginglistComponent implements OnInit {

  public mixerslno:any={};
  public mixerdetails:any={};
  private modalRef: NgbModalRef;
  datepicker: NgbDateStruct;
  public packagemapped:any={};
  public mountingheaders:any={};
  public packagebom:any={};
  /* public datafoc:any={}; */
  closeResult: string;
  datafoc :any[];

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day: dd };
    this.datepicker = this.calendar.getToday();
  }

  ngOnInit() {
    this.getmixerslno();
    this.mountingheaders={};
    this.packagebom={};
    this.datafoc=[];
  }
  getmixerslno(){
    var data: { switchcase: string } = { switchcase: "getenggslno" };
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mixerslno = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  changeengine(value){
    var data: { switchcase: string,id:string } = { switchcase: "getallpackagingdetails",id:value };
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mixerdetails = datas.message[0];
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmountingheader(form:NgForm){
    var custdata=form.value;
    var partnoids=custdata.partnoid
    var chassis_model=custdata.chassis_model_id
    var data: { switchcase: string,partnoid:string ,chassis_mod:string } = { switchcase: "get",partnoid:partnoids,chassis_mod:chassis_model };
    var method = "post";
    var url = "mountinginternalpart";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mountingheaders = datas;            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getpackagebom(custdata);
      this.getcustfoc(custdata);
  }
  getcustfoc(custdata){
    var customer_id=custdata.custid;
    var data: { switchcase: string,id:string } = { switchcase: "getfoc",id:customer_id };
    var method = "post";
    var url = "getfocdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.datafoc=datas.message;            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getpackagebom(custdata){
    var partnoids=custdata.partnoid
    var chassis_model=custdata.chassis_model_id
    var data: { switchcase: string,partnoid:string ,chassis_mod:string} = { switchcase: "get",partnoid:partnoids,chassis_mod:chassis_model };
    var method = "post";
    var url = "packaginginternalpart";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.packagebom = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getchassismapping(form:NgForm){
    var data=form.value;
    var switchcase = "switchcase";
    var value = "getpackagemapping";
    data[switchcase] = value;
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.packagemapped = datas.message;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  open(foc) {
    this.modalRef = this.modalService.open(foc, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  addfocedit()
  {
    event.preventDefault();
    this.datafoc.push(this.datafoc.length);
  }
  editfoc(form,edit)
  {
    var data =form.value;
    var switchcase = "switchcase";
    var value = "insert_foc";
    var nooffoc = "nooffoc";
    data[switchcase] = value;
    data[nooffoc] = this.datafoc.length;
    var method= "post";
    var url="customerspec";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.modalRef.close();
            this.toastr.success(datas.message, datas.type);    
            this.ngOnInit();      
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //this.toastr.success('Hello world!', 'Toastr fun!');
  };
  openfoc(datas){
    /* console.log(datas); */
    if(datas==""){

    }else{
      this.open(datas);
    }
  }
  savepackagelist(frm:NgForm){
    if(this.mountingheaders.message == "" && this.packagebom.message== ""){
      this.toastr.error("Header and Accessories are not loaded","error");
    }else{
      var data =frm.value;
      var switchcase = "switchcase";
      var value = "insertpackagelist";
      data[switchcase] = value;
      var method= "post";
      var url="getdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.toastr.success(datas.message, datas.type);
              frm.reset();
              this.mountingheaders={};
              this.packagebom={};
              this.datafoc=[];
              this.mixerdetails={};
            }
            else {
              this.toastr.error(datas.message, datas.type);
            }
          },
          (err) => {
            if (err.status == 403) {
              this.toastr.error(err._body);
              //alert(err._body);
            }
          }
        );
    }
  }

}
