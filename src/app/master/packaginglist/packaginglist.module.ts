import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackaginglistComponent } from './packaginglist.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const packaginglistRoutes: Routes = [
  {
    path: '',
    component: PackaginglistComponent,
    data: {
      breadcrumb: 'Packaging List',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,    
    RouterModule.forChild(packaginglistRoutes),
    SharedModule
  ],
  declarations: [PackaginglistComponent]
})
export class PackaginglistModule { }
