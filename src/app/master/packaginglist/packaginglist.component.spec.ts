import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackaginglistComponent } from './packaginglist.component';

describe('PackaginglistComponent', () => {
  let component: PackaginglistComponent;
  let fixture: ComponentFixture<PackaginglistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackaginglistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackaginglistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
