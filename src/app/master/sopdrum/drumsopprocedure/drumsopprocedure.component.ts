import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-drumsopprocedure',
  templateUrl: './drumsopprocedure.component.html',
  styleUrls: ['./drumsopprocedure.component.css']
})
export class DrumsopprocedureComponent implements OnInit {
  private modalRef: NgbModalRef;
  operation_no:any;
  options1 :any[];
  optionscomp :any[];
  optionsMap:any[];
  productname:any;
  selected_revno:any;
options :any[];
optionmodel :any[];
compoptions:string="";
optionSelected: any = "Select";
optionSelected1: any = "Select";
optionmodelSelected: any = "Select";
public switchcase:string = "";
  public id:number = 10;
  public employees:string="";
  public sopcomponent:any[];
  public component1:string="";
  public editsop:any = { };
  public processimage: string = "";
  public mixer_value: string = "";
public ischecked = true;
  //public selectedcomponents:any = { };
  selectedcomponents:string="";
  public stationname:string="";
  public stationname1:string="";
  public conetypes:string[] = new Array();
  public revino:any[] = new Array();
  public componentname:string="";
  public component_options:string="";
  public component_cm:string="";
  
  public component_optionsedit:string="";
  public comp_cm:string[] = new Array();
  public compvalue:string="";
  public comp:string="";
  arrayBuffer: any;
  closeResult: string;
  file: File;
  auditPhotoUploader: FileUploader;
  public RequestOptions:any;
  file_name:FileList;
  position: string = 'bottom-right';
  title: string;
  index: number;
  msg: string;
  showClose: boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: string = 'default';
  closeOther: boolean = false;
  tenentIDFileName:any;
  public rev_drummodel:any;
public rev_stationname:any;

  public form:any;
  checkboxValue: boolean = false;
  dtOptions: DataTables.Settings = {};
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }
  ngOnInit() {   
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "drumsopproceduregetvalues";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
      //    alert(datas.code);
          if(datas.code == 201){
            this.optionSelected="Select Workstation";
           this.options=datas.message;
           }else{
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      // setTimeout(() => {
      //   (<any>$('.datatable')).DataTable();
      // }, 2000);  
   this.getvalue();
  }
  getvalue(){
  
  
    var data: { switchcase: string, id:string } = { switchcase: "getmixervalue", id:'1' };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message)
            {
            this.mixer_value = datas.message;
            }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getworkstation();
      }
      getworkstation()
      {
        var data ={'opcode':'1'};
        var method = "post";
        var url = "workstationnames";
        this.AjaxService.ajaxpost(data, method, url)
          .subscribe(
            (datas) => {          
              if (datas.code == 201) {
                this.stationname = datas.message;
              
              } else {
    
              }
            },
            (err) => {
              if (err.status == 403) {
                alert(err._body);
              }
            }
          );
          this.getmodelnumber();
      }
  getmodelnumber()
  {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "drumsopproceduregetmodelnumber";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
   //       alert(datas.code);
          if(datas.code == 201){
            this.optionmodelSelected="Select Model Number";
           this.optionmodel=datas.message;
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      this.getproductname();
  }
  getproductname()
  {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "getsopproductname";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {
            this.productname = datas.message;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getconetype(event)
  {
    this.conetypes = new Array();
    var data ={"model":event};
    var method = "post";
    var url = "getconetype";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
   //       alert(datas.code);
          if(datas.code == 201){
           this.conetypes.push('Cone1_' + datas.message[0].cone1);
           this.conetypes.push('Cone2_' +datas.message[0].cone2);
           this.conetypes.push('Cone3_' +datas.message[0].cone3);
           if(datas.message[0].cone4 != 'NA')
           {
           this.conetypes.push('Cone4_' +datas.message[0].cone4);
           }
          
           }
           else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  onOptionsSelected(event)
  {
  //  this.optionSelected=event;
  this.rev_stationname=event;
    var data ={"opcode":event};
    var method = "post";
    var url = "drumworkstation";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
   //       alert(datas.code);
          if(datas.code == 201){
           this.stationname1=datas.message[0].list_of_operation;
           }
           else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      this.getrevno();
  }
  getcomponent()
  {
    var data={"stationcode":this.rev_stationname,"model":this.rev_drummodel};
    var method = "post";
    var url = "drumgetcomponent";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
           this.component_cm=datas.message;
         
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    //  this.getrevno();
  }
  getdrum(event)
  {
    this.rev_drummodel=event;
  }
  onOptionscomponentSelected(event)
  {
  this.rev_stationname=event;
    var data ={"component": event};
    var method = "post";
    var url = "workstationname"; var i :number;
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          this.componentname=datas[0].operation_code;
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
        this.getoperationno();
  }
  getoperationno()
  {
    var data ={"partno":this.rev_drummodel,"stationcode":this.rev_stationname};
    data["switchcase"] = "getopno";
    var method = "post";
    var url = "drumsopprocedure";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){
          if(datas.message)
          {
            if(datas.message.length == 0)
            {
              this.operation_no= "FAB/" + this.rev_stationname + "/" + '1';
              this.editsop.operation_no = "FAB/" + this.rev_stationname + "/" + '1';
            }
            else
            {
           this.operation_no= "FAB/" + this.rev_stationname + "/" + datas.message.length;
           this.editsop.operation_no = "FAB/" + this.rev_stationname + "/" + datas.message.length;
            }
          }
           }
           else{
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      this.getrevno();
  }
  getrevno()
  {
    this.revino=[];
    
    var data={"stationcode":this.rev_stationname,"model":this.rev_drummodel};
    var method = "post";
    var url = "getrev";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            var rev = datas.message.length;
          if(rev > 0)
          {
           for(var i=1;i<=rev;i++)
           {
             console.log(i);
             this.revino.push(i);
           }
          }
          else
          {
          
          }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
     this.getcomponent();
  }
  onOptionsrevSelected(event)
  {
   this.selected_revno=event;
  }
  onOptionsmodelSelected(event)
  {
    this.optionmodelSelected=event;
    this.rev_drummodel=event;
    this.revino=[];
    this.getrevno();
  }
  
  change(list,active)
  {
    console.log(list);
    /*if(active)
    {
      alert(list);
      this.selectedcomponents.push(list);
      alert(this.selectedcomponents);
    }*/
  }
  editemployees(editfrm,edit){
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    var components = "components";
    var valuecomp = this.comp_cm;
    data[switchcase] = value;
    data[components] = valuecomp;
    var method = "post";
    var url = "drumsopprocedure";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);    
            this.reloadPage();        
            edit.hide();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //this.toastr.success('Hello world!', 'Toastr fun!');
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  checkValue(option){
    //alert(option);
    //alert(options);
// if(this.comp.length ==0)
// {
//   this.comp=option;
// //  alert(this.comp);
// }
// else{
//   if(this.comp.includes(option))
//   {
// this.comp=this.comp.replace(option,"");
//   }
//   else{
//     this.comp=this.comp + ',' + option;
//   }
 
//}
this.comp_cm.push(option);
this.comp=this.comp_cm.join(",");
  }
  onOptionstationSelected(event)
  {
    
  this.optionSelected1=event;
    var data1={"stationname" : this.optionSelected1};
  var method1 = "post";
    var url1 ="drumsearchcomponent";
    this.AjaxService.ajaxpost(data1,method1,url1)
    .subscribe(
      (datas1) => {
        if(datas1.code=201)
        {
          var ids = [];
this.comp="";
          for ( let i = 0; i < datas1.message.length; i++) {
            this.component_options= datas1.message[i].components.split(",");
            console.log("component",this.component_options)
            console.log("component edit",this.component_optionsedit)
          }
        }
      }
    );
    var data={"opcode" : this.optionSelected1};
    var method = "post";
      var url ="drumgetcomponent";
      this.AjaxService.ajaxpost(data,method,url)
      .subscribe(
        (datas) => {
          if(datas.code=201)
          {
            var ids = [];
     
            for ( let i = 0; i < datas.message.length; i++) {
              this.component_optionsedit= datas.message[i].components.split(",");
          
            }
            console.log("component",this.component_options)
            console.log("component edit",this.component_optionsedit)
          }
        }
      );
      this.getmodelnumber();
  }
  onOptionstationSelectededit(event)
  {
    var data={"stationname" : this.optionSelected1};
  var method = "post";
    var url ="drumsearchcomponent";
    this.AjaxService.ajaxpost(data,method,url)
    .subscribe(
      (datas) => {
        if(datas.code=201)
        {
          var ids = [];
        this.component_optionsedit = event.components.split(",");
          for ( let i = 0; i < datas.message.length; i++) {
            this.component_options= datas.message[i].components.split(",");
          }
        }
      }
    );
  }
  checkifequal(options,options1)
  {
alert(options);
alert(options1);
  }
  search(form)
  {
  
  var data={"stationcode":this.rev_stationname,"model":this.rev_drummodel,"revno":this.selected_revno};
   var method = "post";
   var url="drumsopproceduresearch";
  
   this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if(datas.code==201)
        {
          this.employees=datas.message;
          this.conetypes= new Array();
          this.reloadPage();
          //add.hide();
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }    
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
    );
  }
  
  reloadPage() {
     this.ngOnInit();
  }
  addsopprocedure(form: NgForm,add){
   
  
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      let extension: string = form.value.visualaid.split('.').pop().toLowerCase();
      if ($.inArray(extension, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
        formData.append('file', file);
        formData.append('operationno', form.value.operationno);
        formData.append('desc', form.value.desc);
        formData.append('partno', form.value.drummodel);
        formData.append('revno', form.value.revno);
        formData.append('criticalpoints', form.value.criticalpoints);
        formData.append('tools', form.value.tools);
        formData.append('workstation', form.value.componentassembly);
        formData.append('component',this.comp);
        formData.append('switchcase', "insert");
    //var data =form.value;
    // var switchcase = "switchcase";
    // var value = "insert";
    // var components = "components";
    // var valuecomp = this.comp;
    //data[switchcase] = value;
    //data[components] = valuecomp;
    var method= "post";
    var url="drumsopprocedure";
    
    console.log(form.value);
    this.AjaxService.ajaxpost(formData, method, url)
    .subscribe(
      (datas) => {
        if(datas.code==201){
          this.toastr.success(datas.message, datas.type);
          form.resetForm(); 
          this.reloadPage();
          this.modalRef.close();    
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }    
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
    
    );
  } else {
    this.toastr.error('It should be an Image file', 'Error!');
  }}
};
  
  editfunction(item,station) {
   
    this.editsop=item;
    // var length = item.sop_name.length;
    // for(var i=0;i<length;i++)
    // {
    //   this.editsop.sop_name[i]=true;
    // }
    // console.log(item);
    this.optionSelected1=station;
    this.comp_cm=this.editsop.components;
    this.compvalue= this.stationname;
   this.onOptionstationSelected(item.workstation_name);
};
editsopprocedure(editfrm,edit){
    if(this.file_name){
      if (this.file_name.length > 0) {
        let file: File = this.file_name[0];
        let formData: FormData = new FormData();
        let extension: string = editfrm.value.fabprocesscharts.split('.').pop().toLowerCase();
        if ($.inArray(extension, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
          formData.append('file', file);
          formData.append('operationno', editfrm.value.operationno);
          formData.append('desc', editfrm.value.desc);
          formData.append('product', editfrm.value.product);
          formData.append('criticalpoints', editfrm.value.criticalpoints);
          formData.append('tools', editfrm.value.tools);
          formData.append('workstation', editfrm.value.workstation);
          formData.append('components',editfrm.value.component);
          formData.append('switchcase', "edit");
          formData.append('extension', extension);
    var method = "post";
    var url = "drumsopprocedure";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);    
            this.reloadPage();        
            edit.hide();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //this.toastr.success('Hello world!', 'Toastr fun!');
    } else {
      this.toastr.error('Process Flow Chart should be an Image file', 'Error!');
    }
  }
}
else{
  var data = editfrm.value;
  var switchcase = "switchcase";
  var value = "edit";
  data[switchcase] = value;
  var method = "post";
  var url = "drumsopprocedure";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if (datas.code == 201) {
          this.toastr.success(datas.message, datas.type);
          this.modalRef.close();
        }
        else {
          this.toastr.error(datas.message, datas.type);
        }
        this.ngOnInit();
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          this.ngOnInit();
        }
      }
    );
}
this.conetypes= new Array();
};  

  onKeydown(event){

    //var data = event.target.value;
    /* var data = '{"eventdata":'+event.target.value+',"switchcase":"get"}';    
    var method = "post";
    var url = "employee";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          console.log(datas);
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); */
  };

  bulkemployee(bulkfrm: NgForm){
    console.log(bulkfrm);
  };

  deleteproductvariant(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "drumsopprocedure";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning', 
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
           
            response => {
              if (response.code == 201) {

                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                
                this.reloadPage();

              }
              else {
               
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
       
        this.reloadPage();
      }
    });
  };
  
  showimage(image){
  //  alert(image);
    this.processimage = environment.file_url+'public/' + image;
  };
  open(addNew) {
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
   this.file_name=fileList;
 //  alert(this.file_name);
  }
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({ headers: headers });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/

      var data = formData;
      var method = "post";
      var url = "uploadfilesdrumsopprocedure";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for(i=0;i<data.length;i++){
              if(data[i].code==201){
                this.toastr.success(data[i].message, 'Success!');
                this.reloadPage();
                this.modalRef.close();


              }else{
                this.toastr.error(data[i].message, 'Error!');
              }
            }            
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  }
  downloadFile() {

    return this.http
      .get('assets/uploads/drumsopprocedure.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""  })
      .map(res => {
        return {
          filename: 'Drum SOP Procedure.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
          console.log('start download:',res);
          var url = window.URL.createObjectURL(res.data);
          var a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = url;
          a.download = res.filename;
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove(); // remove the element
        }, error => {
          console.log('download error:', JSON.stringify(error));
        }, () => {
          console.log('Completed file download.')
        });
  }
  showSuccess() {
    /* swal(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )  */
    //this.toastr.success('You are awesome!', 'Success!');
    this.toastr.success('You are awesome!', 'Success!');
  }
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  deletesopprocedure(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "drumsopprocedure";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning', 
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
           
            response => {
              if (response.code == 201) {
                this.reloadPage();
                this.conetypes= new Array();
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
              }
              else {
                
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
        
        this.reloadPage();
      }
    });
  };
}

