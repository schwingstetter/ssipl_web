import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumsopprocedureComponent } from './drumsopprocedure.component';

describe('DrumsopprocedureComponent', () => {
  let component: DrumsopprocedureComponent;
  let fixture: ComponentFixture<DrumsopprocedureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumsopprocedureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumsopprocedureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
