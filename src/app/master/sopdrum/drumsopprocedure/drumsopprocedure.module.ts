import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumsopprocedureComponent } from './drumsopprocedure.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
export const drumsopprocedureroutes: Routes = [
  {
    path: '',
    component: DrumsopprocedureComponent,
    data: {
      breadcrumb: 'SOP Procedure',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(drumsopprocedureroutes),
    SharedModule
  ],
  declarations: [DrumsopprocedureComponent]
})
export class DrumsopprocedureModule { }
