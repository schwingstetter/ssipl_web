import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SopdrumComponent } from './sopdrum.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { sopdrumRoutes } from './sopdrum.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(sopdrumRoutes),
    SharedModule
  ],
  declarations: [SopdrumComponent]
})
export class SopdrumModule { }
