import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-drumproductvariant',
  templateUrl: './drumproductvariant.component.html',
  styleUrls: ['./drumproductvariant.component.css']
})
export class DrumproductvariantComponent implements OnInit {
  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "";
  public sortOrder: string = "desc";
  optionmodelSelected :any;
  optionmodel : any;
  workstationnames :any;
  componentname :any;
  stationname :string;
  productname :string;
  public switchcase:string = "";
  public plantname: string = "D6";
  public id:number = 10;
  public employees:string="";
  public editemployee:any = { };
  arrayBuffer: any;
  file: File;
  auditPhotoUploader: FileUploader;
  public RequestOptions:any;
  file_name:FileList;
  position: string = 'bottom-right';
  title: string;
  msg: string;
  showClose: boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: string = 'default';
  closeOther: boolean = false;
  tenentIDFileName:any;
  public form:any;
public rev_drummodel:any;
public rev_stationname:any;
public revino:any;
  public show:boolean = false;
  public buttonName:any = 'Show';
mixer_value:any;
  dtOptions: DataTables.Settings = {};
  private modalRef: NgbModalRef;
  closeResult: string;

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);

   }

   ngOnInit(){
     var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "sopdrumproductvariant";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){
            this.employees = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );   
      this.getmodelnumber();
  }
  reloadPage() {
     this.ngOnInit();
  }
  addproductvariant(form: NgForm,add){
  console.log(form.value);
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method= "post";
    var url="sopdrumproductvariant";
    this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if(datas.code==201){
          this.toastr.success(datas.message, datas.type);
          form.resetForm();
          this.reloadPage();
          this.modalRef.close();
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }    
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
    );
  };
  editfunction(item) {
    this.editemployee=item;
    console.log(item);
    this.getmodelnumber();
  };
  editproductvariant(editfrm,edit){
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "sopdrumproductvariant";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);    
            this.reloadPage();        
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //this.toastr.success('Hello world!', 'Toastr fun!');
  };

  onKeydown(event){

    //var data = event.target.value;
    /* var data = '{"eventdata":'+event.target.value+',"switchcase":"get"}';    
    var method = "post";
    var url = "employee";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          console.log(datas);
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); */
  };

  deleteproductvariant(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "sopdrumproductvariant";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning', 
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
           
            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
              }
              else if (response.code == 202) {
                swal({
                  title: 'Alert',
                  text: "You won't be able to revert this,Because SOP available!",
                  type: 'error', 
                
                  confirmButtonColor: '#3085d6',
                 
                  
                })
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
       
        this.reloadPage();
      }
    });
  };

  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
   this.file_name=fileList;
  }
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();let options = new RequestOptions({ headers: headers });
      var data = formData;
      var method = "post";
      var url = "uploadfilessopdrumproductvariant";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for(i=0;i<data.length;i++){
              if(data[i].code==201){
                this.toastr.success(data[i].message, 'Success!');                               
                this.modalRef.close();
              

              }else{
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.reloadPage();           
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  }
  getmodelnumber()
  {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "getsopdrummodel";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
   //       alert(datas.code);
          if(datas.code == 201){
            this.optionmodelSelected="Select Model Number";
           this.optionmodel=datas.message;
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      this.getworkstation();
  }
  getworkstation()
  {
    var data ={'opcode':'1'};
    var method = "post";
    var url = "workstationnames";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {
            this.stationname = datas.message;
          
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getvalue();
  }
   getvalue(){
  
  
    var data: { switchcase: string, id:string } = { switchcase: "getmixervalue", id:'1' };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message)
            {
            this.mixer_value = datas.message;
            }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    
     
  };
  getrevno()
  {
    var data={"stationcode":this.rev_stationname,"model":this.rev_drummodel};
    var method = "post";
    var url = "getrev";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message[0])
          {
           this.revino= ((+datas.message[0].rev_no) + (1)) ;
          }
          else
          {
          this.revino=1;
          }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getproductname()
  {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "getsopproductname";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {
            this.productname = datas.message;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getdrum(event)
  {

    this.rev_drummodel=event;
  }
  onOptionscomponentSelected(event)
  {

  this.rev_stationname=event;
    var data ={"component": event};
    var method = "post";
    var url = "workstationname"; var i :number;
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          this.componentname=datas[0].operation_code;
        //   if (datas.code == 201) {
        //  this.componentname = datas.message;
        //  console.log(this.componentname);
        
        //   } else {

        //   }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
        this.getrevno();
  }
  downloadFile() {

    return this.http
      .get('assets/uploads/sop_drum_product_variant.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""  })
      .map(res => {
        return {
          filename: 'SOP Drum Product Variant.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
          console.log('start download:',res);
          var url = window.URL.createObjectURL(res.data);
          var a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = url;
          a.download = res.filename;
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove(); // remove the element
        }, error => {
          console.log('download error:', JSON.stringify(error));
        }, () => {
          console.log('Completed file download.')
        });
  }
  showSuccess() {
    /* swal(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )  */
    //this.toastr.success('You are awesome!', 'Success!');
    this.toastr.success('You are awesome!', 'Success!');
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
}

