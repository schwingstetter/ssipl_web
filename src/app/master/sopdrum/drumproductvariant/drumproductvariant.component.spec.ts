import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumproductvariantComponent } from './drumproductvariant.component';

describe('DrumproductvariantComponent', () => {
  let component: DrumproductvariantComponent;
  let fixture: ComponentFixture<DrumproductvariantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumproductvariantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumproductvariantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
