import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumproductvariantComponent } from './drumproductvariant.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
export const drumproductvariantroutes: Routes = [
  {
    path: '',
    component: DrumproductvariantComponent,
    data: {
      breadcrumb: 'Product Variant',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(drumproductvariantroutes),
    SharedModule
  ],
  declarations: [DrumproductvariantComponent]
})
export class DrumproductvariantModule { }
