import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SopdrumComponent } from './sopdrum.component';

describe('SopdrumComponent', () => {
  let component: SopdrumComponent;
  let fixture: ComponentFixture<SopdrumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SopdrumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SopdrumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
