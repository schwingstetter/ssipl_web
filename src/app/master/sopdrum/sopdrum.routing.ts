import { Routes } from '@angular/router';


export const sopdrumRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'drumproductvariant',
                loadChildren: './drumproductvariant/drumproductvariant.module#DrumproductvariantModule'
            },{
                path: 'drumsopprocedure',
                loadChildren: './drumsopprocedure/drumsopprocedure.module#DrumsopprocedureModule'
            }  ,{
                path: 'drumcmvariance',
                loadChildren: './drumcmvariance/drumcmvariance.module#DrumcmvarianceModule'
            }  
        ]
    }
];
