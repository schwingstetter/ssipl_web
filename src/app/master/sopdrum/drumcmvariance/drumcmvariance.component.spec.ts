import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumcmvarianceComponent } from './drumcmvariance.component';

describe('DrumcmvarianceComponent', () => {
  let component: DrumcmvarianceComponent;
  let fixture: ComponentFixture<DrumcmvarianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumcmvarianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumcmvarianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
