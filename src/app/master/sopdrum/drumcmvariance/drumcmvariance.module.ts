import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumcmvarianceComponent } from './drumcmvariance.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
export const drumcmvarianceroutes: Routes = [
  {
    path: '',
    component: DrumcmvarianceComponent,
    data: {
      breadcrumb: 'CM Variance',
      status: true
    }
  }
];

@NgModule({
  imports: [
       CommonModule,
    RouterModule.forChild(drumcmvarianceroutes),
    SharedModule
  ],
  declarations: [DrumcmvarianceComponent]
})
export class DrumcmvarianceModule { }
