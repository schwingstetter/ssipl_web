import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeperformanceComponent } from './employeeperformance.component';

describe('EmployeeperformanceComponent', () => {
  let component: EmployeeperformanceComponent;
  let fixture: ComponentFixture<EmployeeperformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeperformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeperformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
