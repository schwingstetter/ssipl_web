import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeperformanceComponent } from './employeeperformance.component';
import { SharedModule } from '../../shared/shared.module'; 
import { FileUploadModule } from 'ng2-file-upload';
import { DataTablesModule } from 'angular-datatables';

export const EmployeeperformanceRoutes: Routes = [
  {
    path: '',
    component: EmployeeperformanceComponent,
    data: {
      breadcrumb: 'Employee Performance Scale',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,    
    RouterModule.forChild(EmployeeperformanceRoutes),
    SharedModule
  ],
  declarations: [EmployeeperformanceComponent]
})
export class EmployeeperformanceModule { }
