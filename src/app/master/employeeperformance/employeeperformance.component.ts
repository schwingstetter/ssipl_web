import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
//import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare const $: any;
declare var Morris: any;
declare var Highcharts: any;
@Component({
  selector: 'app-employeeperformance',
  templateUrl: './employeeperformance.component.html',
  styleUrls: ['./employeeperformance.component.css']
})
export class EmployeeperformanceComponent implements OnInit {
  auditPhotoUploader: FileUploader;
  private modalRef: NgbModalRef;
  DataArray: any = [];
  closeResult: string;
  dtElement: DataTableDirective;
  datatableElement: DataTableDirective;
  public employees: string = "";
  public editemployees: string = "";
  public svalue: any = "";
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {

    this.toastr.setRootViewContainerRef(vcr);
    this.auditPhotoUploader = new FileUploader({
    });
  }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "employeescale";
    this.AjaxService.ajaxpost(data, method, url)
      .toPromise()
      .then(
        (datas) => {
          if (datas.code == 201) {
            this.employees = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "scalevalue";
      this.AjaxService.ajaxpost(data, method, url)
        .toPromise()
        .then(
          (datas) => {
            if (datas.code == 201) {
              if(datas.data.length >0)
              {
              this.svalue = datas.data[0].scale_value + 1;
              }
              else
              {
                this.svalue = 1;
              }
              // table.destroy();
              // setTimeout(() => {
              //   $('.datatable').DataTable();
              // }, 1000);
            } else {
  
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  //  this.getsnagcount('1');
  }
  addperformancescale(frm) {
    var table = $('.datatable').DataTable();
    var data = frm.value;
    var method = "post";
    var url = "employeeperformance";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            
            this.modalRef.close();
            this.ngOnInit();
            //add.hide();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addNew) {
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  onSearchChange(e) {
    /* if (e.which === 32 && !e.target.value.length)
      e.preventDefault(); */
  }
  editfunction(item) {
    this.editemployees = item;
  }
 
}
