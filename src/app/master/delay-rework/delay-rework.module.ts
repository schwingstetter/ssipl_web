import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DelayReworkComponent } from './delay-rework.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const delayreworkRoutes: Routes = [
  {
    path: '',
    component: DelayReworkComponent,
    data: {
      breadcrumb: 'Delay/Rework',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(delayreworkRoutes),
    SharedModule
  ],
  declarations: [DelayReworkComponent]
})
export class DelayReworkModule { }
