import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelayReworkComponent } from './delay-rework.component';

describe('DelayReworkComponent', () => {
  let component: DelayReworkComponent;
  let fixture: ComponentFixture<DelayReworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelayReworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelayReworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
