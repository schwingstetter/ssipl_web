import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductvariantComponent } from './productvariant.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const productvariantRoutes: Routes = [
  {
    path: '',
    component: ProductvariantComponent,
    data: {
      breadcrumb: 'Product Variant',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(productvariantRoutes),
    SharedModule
  ],
  declarations: [ProductvariantComponent]
})
export class ProductvariantModule { }
