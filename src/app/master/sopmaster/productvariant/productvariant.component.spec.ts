import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductvariantComponent } from './productvariant.component';

describe('ProductvariantComponent', () => {
  let component: ProductvariantComponent;
  let fixture: ComponentFixture<ProductvariantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductvariantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductvariantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
