import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmvarianceComponent } from './cmvariance.component';

describe('CmvarianceComponent', () => {
  let component: CmvarianceComponent;
  let fixture: ComponentFixture<CmvarianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmvarianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmvarianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
