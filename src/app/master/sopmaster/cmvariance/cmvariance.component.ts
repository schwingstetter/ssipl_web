import { Component, OnInit, ViewContainerRef,ElementRef, Renderer2, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-cmvariance',
  templateUrl:'./cmvariance.component.html',
  styleUrls: ['./cmvariance.component.css']
})

export class CmvarianceComponent {
  public comp:string="";
  public compvalue:string="";
  public component_cm:string="";
  public revino:any[] = new Array();
  public editsop:any = { };
  operation_no:any;
  public rev_drummodel:any;
  public componentname:string="";
  public rev_stationname:any;
  public closeResult:string;
  public options:string="";
  public options1:string="";
  public details:string="";
  public partno:string="";
  public workstationnames:string="";
  public cmvariance:any={};
  private modalRef: NgbModalRef;
  optionSelected1: any = "Select";
  file_name:FileList;
  public stationnam:string="";
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef,private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }
   ngOnInit(){
    var table=$('#example').DataTable();  
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mixerplanninggetpartno";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
      //    alert(datas.code);
          if(datas.code == 201){
           this.options=datas.message;
           
          table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
           }else{
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      this.getdata();
  }
	
  getdata()
  {
    //var table = $('.datatable').DataTable();

    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "cmvariance";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {
            this.details = datas.message;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getworkstation();
  }
  getworkstation()
  {
    var data ={'opcode':'2'};
    var method = "post";
    var url = "sopworkstation";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {
            this.workstationnames = datas.message;
          
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  deletecmvariance(id)
  {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "cmvariance";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                this.ngOnInit();
              }
              else  if (response.code == 403)  {
                swal(
                  {
                  title: 'Alert',
                  text: response.message,
                  type: 'error',
                 
                  confirmButtonColor: '#3085d6',
                  
                })
                // this.toastr.error(response.message, response.type);
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  }
  onOptionsSelected(event)
  {
this.partno = event;
var data = {"part_no":event};
var method = "post";
var url = "mixerpartno2";
var i: number; 

this.AjaxService.ajaxpost(data, method, url)
  .subscribe(
    (datas) => {          
      if (datas.code == 201) {
        for(i=0;i<datas.message.length;i++)
        {
          this.options1 = datas.message[0].mixer_model;
        }
     
      } else {

      }
    },
    (err) => {
      if (err.status == 403) {
        alert(err._body);
      }
    }
  );
  }
public valuecouont=1;
public datavalue:string;
words2 = [{value: ''}];
  length : any;
  add(event) {
    event.preventDefault();
    this.length = this.words2.length;
  //  alert(this.words2[this.length-1].value);
    if(this.words2[this.length-1].value)
    {
   this.words2.push({value: ''});
    }
    else
    {
      this.toastr.error('Enter component', '');
    } 
  }
  addcmvariance(form: NgForm){
    this.valuecouont=this.valuecouont+1;
    var data = form.value;
    var switchcase = "switchcase";
    var name = "modelno";
    var value = "insert";
    var value1 = this.options1;
    data[switchcase] = value;
    data[name] = value1;
    var method= "post";
    var url="cmvariance";    
    this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if(datas.code==201){
          this.toastr.success(datas.message, datas.type);
  this.words2 = [{value: ''}];
form.resetForm();
   this.reloadPage();
   
      //    this.addcomponent.length=1;
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }    
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
    );
  };
  remove(index)
  {
    if(this.words2.length == 1)
    {
      this.toastr.error('There Must be atleast 1 Component');

    }    else
    {
    this.words2.splice(index,1);
    }
  }
  reloadPage() {
    this.ngOnInit();
 }
	open(addNew) {
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
   // this.modalRef.result.then((result) => {
   //   this.closeResult = `Closed with: ${result}`;
  //  }, (reason) => {
     // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //});
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  downloadFile() {

    return this.http
      .get('assets/uploads/CMVariance.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'CMVariance.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  }
   fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
   this.file_name=fileList;
  };
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      let options = new RequestOptions({ headers: headers });
      var data = formData;
      var method = "post";
      var url = "uploadcmvariance";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            console.log(data.length);
            console.log(data);
            for(i=0;i<data.length;i++){
              if(data[i].code==201){
                this.toastr.success(data[i].message, 'Success!');
              }else{
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.reloadPage(); 
          },
          error => this.toastr.error(error)
        );
    } else {

    }
     this.reloadPage();
  };
  // Edit module function -04.06.2019
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  // editfunction(item)
  // {
    // this.cmvariance=item;
    // console.log(this.cmvariance);
    // this.getdata();
  // }

  onOptionscomponentSelected(event)
  {
    this.rev_stationname=event;
    var data ={"component": event};
    var method = "post";
    var url = "workstationname"; var i :number;
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          this.componentname=datas[0].operation_code;
         // this.operation_no="ASSY/" + event ;
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getoperationno();
    
  }
  getoperationno()
  {
    var data ={"partno":this.rev_drummodel,"stationcode":this.rev_stationname};
    data["switchcase"] = "getopno";
    var method = "post";
    var url = "sopprocedure";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){
          if(datas.message)
          {
            if(datas.message.length == 0)
            {
              this.operation_no= "ASSY/" + this.rev_stationname + "/" + '1';
              this.editsop.operation_no = "ASSY/" + this.rev_stationname + "/" + '1';
            }
            else
            {
           this.operation_no= "ASSY/" + this.rev_stationname + "/" + datas.message.length;
           this.editsop.operation_no = "ASSY/" + this.rev_stationname + "/" + datas.message.length;
            }
          }
           }
           else{
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
    
  }
 
  getcomponent()
  {
    var data={"stationcode":this.rev_stationname,"model":this.rev_drummodel};
    var method = "post";
    var url = "getcomponent";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
           this.component_cm=datas.message;         
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
 
  editfunction(item,station) {
    this.cmvariance=item;
    console.log(this.cmvariance);
    this.getdata();
    console.log(item);
    this.editsop=item;
    this.getworkstation();
  
    this.ngOnInit();
};
editcmvariance(editfrm,edit){
  var data = editfrm.value;
  var switchcase = "switchcase";
  var value = "edit";
  data[switchcase] = value;
  var method = "post";
  var url = "cmvariance";
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if (datas.code == 201) {
          this.toastr.success(datas.message, datas.type);    
          this.reloadPage();        
          this.modalRef.close();
          this.ngOnInit();
        }
        else {
          this.toastr.error(datas.message, datas.type);
        }
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
    );
    //this.toastr.success('Hello world!', 'Toastr fun!');
};
 }