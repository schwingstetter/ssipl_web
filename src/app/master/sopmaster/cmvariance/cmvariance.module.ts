import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CmvarianceComponent } from './cmvariance.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
export const cmvarianceRoutes: Routes = [
  {
    path: '',
    component: CmvarianceComponent,
    data: {
      breadcrumb: 'CM Variance',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(cmvarianceRoutes),
    SharedModule
  ],
  declarations: [CmvarianceComponent]
})
export class CmvarianceModule { }
