import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SopProcedureComponent } from './sop-procedure.component';

describe('SopProcedureComponent', () => {
  let component: SopProcedureComponent;
  let fixture: ComponentFixture<SopProcedureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SopProcedureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SopProcedureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
