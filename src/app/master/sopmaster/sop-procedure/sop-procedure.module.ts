import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SopProcedureComponent } from './sop-procedure.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
export const SopprocedureRoutes: Routes = [
  {
    path: '',
    component: SopProcedureComponent,
    data: {
      breadcrumb: 'SOP Procedure',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SopprocedureRoutes),
    SharedModule
  ],
  declarations: [SopProcedureComponent]
})
export class SopprocedureModule { }
