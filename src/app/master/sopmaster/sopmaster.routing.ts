import { Routes } from '@angular/router';


export const sopmasterRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'productvariant',
                loadChildren: './productvariant/productvariant.module#ProductvariantModule'
            },{
                path: 'sopprocedure',
                loadChildren: './sop-procedure/sop-procedure.module#SopprocedureModule'
            },{
                path: 'cmvariance',
                loadChildren: './cmvariance/cmvariance.module#CmvarianceModule'

            }    
        ]
    }
];
