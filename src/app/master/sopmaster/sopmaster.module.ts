import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SopmasterComponent } from './sopmaster.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { sopmasterRoutes } from './sopmaster.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(sopmasterRoutes),
    SharedModule
  ],
  declarations: [SopmasterComponent]
})
export class SopmasterModule { }
