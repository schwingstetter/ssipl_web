import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sopmaster',
  template: '<router-outlet></router-outlet>'
  })
export class SopmasterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
