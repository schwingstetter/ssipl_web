import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SopmasterComponent } from './sopmaster.component';

describe('SopmasterComponent', () => {
  let component: SopmasterComponent;
  let fixture: ComponentFixture<SopmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SopmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SopmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
