import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HydrauliccombinationComponent } from './hydrauliccombination.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HydrauliccombinationComponent]
})
export class HydrauliccombinationModule { }
