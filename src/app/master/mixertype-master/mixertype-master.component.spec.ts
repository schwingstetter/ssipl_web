import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixertypeMasterComponent } from './mixertype-master.component';

describe('MixertypeMasterComponent', () => {
  let component: MixertypeMasterComponent;
  let fixture: ComponentFixture<MixertypeMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixertypeMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixertypeMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
