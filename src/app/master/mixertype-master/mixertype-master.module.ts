import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MixertypeMasterComponent } from './mixertype-master.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const mixertypeRoutes: Routes = [
  {
    path: '',
    component: MixertypeMasterComponent,
    data: {
      breadcrumb: 'MixerType Master',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mixertypeRoutes),
    SharedModule
  ],
  declarations: [MixertypeMasterComponent]
})
export class MixertypeMasterModule { }
