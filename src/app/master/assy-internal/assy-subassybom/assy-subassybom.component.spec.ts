import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssySubassybomComponent } from './assy-subassybom.component';

describe('AssySubassybomComponent', () => {
  let component: AssySubassybomComponent;
  let fixture: ComponentFixture<AssySubassybomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssySubassybomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssySubassybomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
