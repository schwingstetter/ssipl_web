import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssySubassybomComponent } from './assy-subassybom.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const drumassyRoutes: Routes = [
  {
    path: '',
    component: AssySubassybomComponent,
    data: {
      breadcrumb: 'Assy Internal Parts BOM',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(drumassyRoutes),
    SharedModule
  ],
  declarations: [AssySubassybomComponent]
})
export class AssySubassybomModule { }
