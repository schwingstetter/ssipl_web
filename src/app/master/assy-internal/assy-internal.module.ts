import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssyInternalComponent } from './assy-internal.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { assyinternalRoutes } from './assy-internal.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(assyinternalRoutes),
    SharedModule
  ],
  declarations: [AssyInternalComponent]
})
export class AssyInternalModule { }
