import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assy-internal',
  template: '<router-outlet></router-outlet>'
})
export class AssyInternalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
