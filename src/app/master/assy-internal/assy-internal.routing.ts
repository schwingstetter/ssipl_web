import { Routes } from '@angular/router';

export const assyinternalRoutes: Routes = [
    {
        path: '',
        data: {
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'assyheader',
                loadChildren: './assy-header/assy-header.module#AssyHeaderModule'
            },{
                path: 'assysubassy',
                loadChildren: './assy-subassybom/assy-subassybom.module#AssySubassybomModule'
            }
        ]
    }
];
