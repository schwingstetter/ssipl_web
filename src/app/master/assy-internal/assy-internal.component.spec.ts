import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssyInternalComponent } from './assy-internal.component';

describe('AssyInternalComponent', () => {
  let component: AssyInternalComponent;
  let fixture: ComponentFixture<AssyInternalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssyInternalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssyInternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
