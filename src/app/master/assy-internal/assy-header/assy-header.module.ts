import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssyHeaderComponent } from './assy-header.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const assyheaderRoutes: Routes = [
  {
    path: '',
    component: AssyHeaderComponent,
    data: {
      breadcrumb: 'Assembly BOM Header',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(assyheaderRoutes),
    SharedModule
  ],
  declarations: [AssyHeaderComponent]
})
export class AssyHeaderModule { }
