import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssyHeaderComponent } from './assy-header.component';

describe('AssyHeaderComponent', () => {
  let component: AssyHeaderComponent;
  let fixture: ComponentFixture<AssyHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssyHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssyHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
