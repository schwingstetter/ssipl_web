import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TorqueComponent } from './torque.component';

describe('TorqueComponent', () => {
  let component: TorqueComponent;
  let fixture: ComponentFixture<TorqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TorqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TorqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
