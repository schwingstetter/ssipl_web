import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TorqueComponent } from './torque.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const torqueRoutes: Routes = [
  {
    path: '',
    component: TorqueComponent,
    data: {
      breadcrumb: 'Torque Setting',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(torqueRoutes),
    SharedModule
  ],
  declarations: [TorqueComponent]
})
export class TorqueModule { }
