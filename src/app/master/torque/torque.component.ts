import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-torque',
  templateUrl: './torque.component.html',
  styleUrls: ['./torque.component.css']
})
export class TorqueComponent implements OnInit {
  public torquedetails: string = "";
  public operationcodes: string = "";
  public workstationnames: string = "";
  public edittorquedisplay: string = "";
  public variance: string = "";
  public values: any =[];
  public comp: any =[];
  public selectedtype: string = "";
  public selecteddr: string = "";
  public drcodeinc: string = "";
  public edittorquesettings: any = "";
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  public component_options:any=[];
  menuaccess:string[] = new Array();
  file_name: FileList;
  file_names: FileList;
  private modalRef: NgbModalRef;
  public processimage: string = "";
  closeResult: string;

  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "torque";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);  
            this.torquedetails = datas.message;
            // for(var i=0;i<datas.message.length;i++)
            // {
            //   this.values=datas.message[i].sop_name[i].components;
            // }
            this.comp = this.values;   
        } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getoperationcode();
  };
  reloadPage() {
    this.menuaccess=[];
    this.ngOnInit();
  };
  getstationname(event) {
      this.component_options=[];
      var data1={"stationname" : event};
      var method1 = "post";
      var url1 ="cmvariancesearchcomponent";
      this.AjaxService.ajaxpost(data1,method1,url1)
      .subscribe(
        (datas1) => {
          if(datas1.code=201)
          {
            var ids = [];
            console.log("component",datas1.message);
            //this.comp="";
            for ( let i = 0; i < datas1.message.length; i++) {
              this.component_options.push(datas1.message[i]);              
            //  console.log("component edit",this.component_optionsedit)
            }
        
          }
        }
      );
    this.getworkstation(event);
  }
  getworkstation(event) {
    var data = { "opcode": event };
    var method = "post";
    var url = "workstation";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.workstationnames = datas.message[0].list_of_operation;
            this.edittorquesettings.station_name = datas.message[0].list_of_operation;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

  }
  addtorquesetting(form: NgForm, addnew) {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      let extension: string = form.value.photoreference.split('.').pop().toLowerCase();
      if ($.inArray(extension, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {       
        formData.append('file', file);
        formData.append('code', form.value.code);
        formData.append('workstation', form.value.workstation);
        formData.append('desc', form.value.desc);
        formData.append('torque', form.value.torque);
        formData.append('tooltype', form.value.tooltype);
        formData.append('toolsize', form.value.toolsize);
        formData.append('photoreference', form.value.photoreference);
        formData.append('boltsize', form.value.boltsize);
        formData.append('cmvariance',this.menuaccess.toString());
        formData.append('updated_by', localStorage.getItem('LoggedInUser'));
        formData.append('switchcase', "insert");
        var method = "post";
        var url = "torque";
        this.AjaxService.ajaxpost(formData, method, url)
          .subscribe(
            (datas) => {
              if (datas.code == 201) {
                this.toastr.success(datas.message, datas.type);
                form.resetForm();
                this.menuaccess=[];
                this.reloadPage();
                this.modalRef.close();
              }
              else {
                this.toastr.error(datas.message, datas.type);
              }
            },
            (err) => {
              if (err.status == 403) {
                this.toastr.error(err._body);
                this.ngOnInit();
              }
            }
          );
      }
    }
  };
  getoperationcode() {
    var data = { "opcode": 2 };
    var method = "post";
    var url = "sopworkstation";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.operationcodes = datas.message;

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

  }
  checkValue(option,options){
    console.log(option);
    if(this.menuaccess.length == 0){
      this.menuaccess.push(option);
    }else{
      if(options == false){
        this.menuaccess.splice(this.menuaccess.indexOf(option),1);
        console.log("new deleted numbers is : " + this.menuaccess );  
      }else{
        var length = this.menuaccess.push(option);
        console.log("new numbers is : " + this.menuaccess );
      }      
    }
  };
  ontypechanged(event) {
    this.selectedtype = event;
    if (this.selecteddr != '') {
      this.getcode();
    }
  }
  ondrchanged(event) {
    this.selecteddr = event;
    if (this.selectedtype != '') {
      this.getcode();
    }
  }
  reset(form) {
    this.reloadPage();
    this.selecteddr = '';
    this.selectedtype = '';
    this.drcodeinc = '';
    this.menuaccess=[];
    this.workstationnames="";
    this.component_options="";
  }
  getcode() {
    console.log("type", this.selectedtype);
    console.log("dr", this.selecteddr);
    var data = { "type": this.selectedtype, "category": this.selecteddr };
    var method = "post";
    var url = "drcode";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            //alert(datas.message.length);
            if (datas.message != null) {
              var splitstring = (datas.message.dr_code).toString();
              var splittedarray = splitstring.split(" ");
              var drcode = splittedarray[3];
              splittedarray.splice(splittedarray.indexOf(drcode), 1);
              splittedarray.push(((+drcode) + 1).toString());
              this.drcodeinc = splittedarray.join(' ');
            }
            else {


              if (this.selectedtype == '1') {
                if (this.selecteddr == '1') {
                  this.drcodeinc = "FAB / D 1"
                }
                else {
                  this.drcodeinc = "FAB / R 1"
                }
              }
              if (this.selectedtype == '2') {
                if (this.selecteddr == '1') {
                  this.drcodeinc = "ASS / D 1"
                }
                else {
                  this.drcodeinc = "ASS / R 1"
                }
              }
            }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  showimage(image){
    this.processimage = environment.file_url +'public/'+ image;
  };
  editfunction(item) {
    console.log(item);
    this.edittorquesettings = item;
    var length = item.sop_name.length;
   
    this.getstationname(item.station_code);
    this.checkValue(this.edittorquesettings.sop_cmvariance, this.edittorquesettings.sop_name)
  };
  edittorquesetting(editfrm, edit) {
    if (editfrm.form.valid) {
      if (this.file_names) {
        if (this.file_names.length > 0) {
          let files: File = this.file_names[0];
          let formData: FormData = new FormData();
          let extension: string = editfrm.value.photorefer.split('.').pop().toLowerCase();
          if ($.inArray(extension, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
            formData.append('files', files);
            formData.append('id', editfrm.value.id);
            formData.append('existing_image', editfrm.value.photoref);
            formData.append('extension', extension);
            formData.append('file', files);
            formData.append('code', editfrm.value.code);
            formData.append('workstation', editfrm.value.workstation);
            formData.append('desc', editfrm.value.desc);
            formData.append('torque', editfrm.value.torque);
            formData.append('tooltype', editfrm.value.tooltype);
            formData.append('toolsize', editfrm.value.toolsize);
            formData.append('boltsize', editfrm.value.boltsize);
            formData.append('cmvariance',this.menuaccess.toString());
            formData.append('photoreference', editfrm.value.photorefer);
            formData.append('updated_by', localStorage.getItem('LoggedInUser'));
            formData.append('switchcase', "edit");
             var method = "post";
             var url = "torque";
            this.AjaxService.ajaxpost(formData, method, url)
              .subscribe(
                (datas) => {
                  if (datas.code == 201) {
                    this.toastr.success(datas.message, datas.type);
                    this.ngOnInit();
                    this.modalRef.close();
                  }
                  else {
                    this.toastr.error(datas.message, datas.type);
                  }
                },
                (err) => {
                  if (err.status == 403) {
                    this.toastr.error(err._body);
                    this.ngOnInit();
                  }
                }
              );
          }
        }
      }
    }

  };
  downloadFile() {

    return this.http
      .get('assets/uploads/delayrework.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'delayrework.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  };
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;

    let fileList: FileList = event.target.files;
    this.file_name = fileList;
  }
  filesubmit(bulkupload) {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({ headers: headers });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/

      var data = formData;
      var method = "post";
      var url = "delayrework_uploadfiles";
      var i: number;
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for (i = 0; i < data.length; i++) {
              if (data[i].code == 201) {
                this.toastr.success(data[i].message, 'Success!');
              } else {
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.reloadPage();
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };
  deletetorquesetting(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "torque";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                );

                this.reloadPage();
              }
              else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.reloadPage();
      }
    });
  };

  fileUploads(events) {
    let fileList: FileList = events.target.files;
    this.file_names = fileList;
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };

}

