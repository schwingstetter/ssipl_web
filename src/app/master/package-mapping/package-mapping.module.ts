import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackageMappingComponent } from './package-mapping.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const packagechassisRoutes: Routes = [
  {
    path: '',
    component: PackageMappingComponent,
    data: {
      breadcrumb: 'Packaging Mapping',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,    
    RouterModule.forChild(packagechassisRoutes),
    SharedModule
  ],
  declarations: [PackageMappingComponent]
})
export class PackageMappingModule { }
