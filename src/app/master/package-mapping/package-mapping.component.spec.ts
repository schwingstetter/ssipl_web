import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageMappingComponent } from './package-mapping.component';

describe('PackageMappingComponent', () => {
  let component: PackageMappingComponent;
  let fixture: ComponentFixture<PackageMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
