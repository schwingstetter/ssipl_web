import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-live-dashboard',
  template: '<router-outlet></router-outlet>'
})
export class LiveDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
