import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { livedashboardRoutes } from './live-dashboard.routing';
import { SharedModule } from '../shared/shared.module';
import { LiveDashboardComponent } from './live-dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(livedashboardRoutes),
  ],
  declarations: [LiveDashboardComponent]
})
export class LiveDashboardModule { }
