import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoofworkordersComponent } from './noofworkorders.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const noofworkersRoutes: Routes = [
  {
    path: '',
    component: NoofworkordersComponent,
    data: {
      breadcrumb: 'No Of Workers',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(noofworkersRoutes),
    SharedModule
  ],
  declarations: [NoofworkordersComponent]
})
export class NoofworkordersModule { }
