import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoofworkordersComponent } from './noofworkorders.component';

describe('NoofworkordersComponent', () => {
  let component: NoofworkordersComponent;
  let fixture: ComponentFixture<NoofworkordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoofworkordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoofworkordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
