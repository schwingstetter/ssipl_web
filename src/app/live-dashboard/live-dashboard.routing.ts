import { Routes } from '@angular/router';


export const livedashboardRoutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [ 
            {
                path: 'noofworkers',
                loadChildren: './noofworkorders/noofworkorders.module#NoofworkordersModule'
            }/*, {
                path: 'drumtype',
                loadChildren: './drumtype/drumtype.module#DrumtypeModule'
            }, {
                path: 'fabstages',
                loadChildren: './fab-stages/fab-stages.module#FabStagesModule'
            }, {
                path: 'rawmaterial',
                loadChildren: './raw-material/raw-material.module#RawMaterialModule'
            }, {
                path: 'druminternal',
                loadChildren: './drum-internal/drum-internal.module#DrumInternalModule'
            }, {
                path: 'assystages',
                loadChildren: './assy-stages/assy-stages.module#AssyStagesModule'
            }, {
                path: 'assyinternal',
                loadChildren: './assy-internalparts/assy-internalparts.module#AssyInternalpartsModule'
            }, {
                path: 'delayrework',
                loadChildren: './delay-rework/delay-rework.module#DelayReworkModule'
            }, {
                path: 'mixertype',
                loadChildren: './mixertype-master/mixertype-master.module#MixertypeMasterModule'
            }, {
                path: 'snagmaster',
                loadChildren: './snagmaster/snagmaster.module#SnagmasterModule'
            }, {
                path: 'qamaster',
                loadChildren: './qamaster/qamaster.module#QamasterModule'
            }, {
                path: 'fabprocess',
                loadChildren: './fabprocess/fabprocess.module#FabprocessModule'
            }, {
                path: 'fingoods',
                loadChildren: './finishedgoods/finishedgoods.module#FinishedgoodsModule'
            }, {
                path: 'jobsheet',
                loadChildren: './jobsheet/jobsheet.module#JobsheetModule'
            }, {
                path: 'sopmaster',
                loadChildren: './sopmaster/sopmaster.module#SopmasterModule'
            }    */         
        ]
    }
];

//allinone 730s