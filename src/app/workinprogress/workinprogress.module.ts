import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkinprogressComponent } from './workinprogress.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [WorkinprogressComponent]
})
export class WorkinprogressModule { }
