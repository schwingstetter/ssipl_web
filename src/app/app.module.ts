import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { WithBgImageComponent } from './authentication/login/with-bg-image/with-bg-image.component';
import { ForgotComponent } from "./authentication/forgot/forgot.component";
import {AuthenticationModule} from './authentication/authentication.module';
import { SharedModule } from './shared/shared.module';
import { BreadcrumbsComponent } from './layouts/admin/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layouts/admin/title/title.component';
import { ScrollModule } from './scroll/scroll.module';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import { AjaxService } from './ajax.service';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import * as alasql from 'alasql';


@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    WithBgImageComponent,
    BreadcrumbsComponent,
    TitleComponent,
    ForgotComponent
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpModule,
    ToastModule.forRoot(),
    ScrollModule    
  ],
  exports: [ScrollModule],
  providers: [
    AjaxService, AuthService, AuthGuard,
    { provide: LocationStrategy, useClass: PathLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
