import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  constructor(private myRoute: Router) {}

  sendToken(token: string,empid : string,custid : string,empname : string) {

    localStorage.setItem("LoggedinRole", token);
    localStorage.setItem("LoggedInUser", empid);    
    localStorage.setItem("LoggedInUsername", empname);    
    localStorage.setItem("Custid", empid);    
        

  }

  getToken() {
    return localStorage.getItem("LoggedInUser")

  }

  isLoggednIn() {

    return this.getToken() !== null;

  }

  logout() {

    localStorage.removeItem("LoggedInUser");
    localStorage.removeItem("LoggedinRole");    
    localStorage.removeItem("LoggedInUser");    
    localStorage.removeItem("Custid");   
    localStorage.removeItem("engineno");   
    localStorage.removeItem("formvalues");   
    localStorage.removeItem("bomelements");  
     localStorage.removeItem("LoggedInUsername");   
    this.myRoute.navigate(["/"]);

  }

}
