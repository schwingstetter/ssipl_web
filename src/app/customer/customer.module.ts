import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CustomerComponent } from './customer.component';
import { customerRoutes } from './customer.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(customerRoutes),
    SharedModule
  ],
  declarations: [CustomerComponent]
})
export class CustomerModule { }