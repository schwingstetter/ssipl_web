import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawmaterialrequestComponent } from './rawmaterialrequest.component';

describe('RawmaterialrequestComponent', () => {
  let component: RawmaterialrequestComponent;
  let fixture: ComponentFixture<RawmaterialrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawmaterialrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawmaterialrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
