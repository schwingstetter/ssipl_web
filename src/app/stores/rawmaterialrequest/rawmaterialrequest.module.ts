import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RawmaterialrequestComponent } from './rawmaterialrequest.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const rawmaterialrequestRoutes: Routes = [
  {
    path: '',
    component: RawmaterialrequestComponent,
    data: {
      breadcrumb: 'Raw Material Request',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(rawmaterialrequestRoutes),
    SharedModule
  ],
  declarations: [RawmaterialrequestComponent]
})
export class RawmaterialrequestModule { }
