import { Routes } from '@angular/router';


export const storesRoutes: Routes = [
    {
        path: '',
        data: {
            /* breadcrumb: 'User Management', */
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'materialpicking',
                loadChildren: './material-picking/material-picking.module#MaterialPickingModule'
            },
            {
                path: 'materailshortage',
                loadChildren: './materailshortage/materailshortage.module#MaterailshortageModule'
            },{
                path: 'issuedmaterial',
                loadChildren: './issuedmaterials/issuedmaterials.module#IssuedmaterialsModule'
            }, {
                path: 'materialconfirmation',
                loadChildren: './material-issueconfirmation/material-issueconfirmation.module#MaterialIssueconfirmationModule'
            }, {
                path: 'chassisclearance',
                loadChildren: './chassisclearance/chassisclearance.module#ChassisclearanceModule'
            }, {
                path: 'binsystemupdate',
                loadChildren: './binsystemupdate/binsystemupdate.module#BinsystemupdateModule'
            }, {
                path: 'rawmaterialissue',
                loadChildren: './rawmaterialissue/rawmaterialissue.module#RawmaterialissueModule'
            }, {
                path: 'shellsegmentinward',
                loadChildren: './shellsegmentinward/shellsegmentinward.module#ShellsegmentinwardModule'
            }, {
                path: 'rawmaterialrequest',
                loadChildren: './rawmaterialrequest/rawmaterialrequest.module#RawmaterialrequestModule'
            }, {
                path: 'linerejectionstores',
                loadChildren: './linerejectionstores/linerejectionstores.module#LinerejectionstoresModule'
            },   
            {
                path: 'packaginglist',
                loadChildren: './../master/packaginglist/packaginglist.module#PackaginglistModule'
            }         
        ]
    }
];
