import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { AjaxService } from '../../ajax.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-binsystemupdate',
  templateUrl: './binsystemupdate.component.html',
  styleUrls: ['./binsystemupdate.component.css']
})
export class BinsystemupdateComponent implements OnInit {

  public binsystemupdate: string = "";
  public editbinsystemupdate: string = "";
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  public getpartno: any = {};
  public getbinsizemake: any = {};
  public storage_options: any = {};

  public mixer_value:any={};
  public drum_value:any={};
  menuaccess:string[] = new Array();
  public getmixervalues:any={};
  public mixer_values:any={};
  public mixer_va:any={};
  selectedAll: any;
  public weeklys:any={};
  public weeklyplan="";
  model: NgbDateStruct;
  model_todate: NgbDateStruct;
  model_fromdate: NgbDateStruct;
  public totalquantity:number=0;
  public issuedquantity:number=0;
  public totalquantitys:number =0;
  public overallquantitys:number =0;
  quantity:number;
  public availqty:any = new Array();

  constructor(public http: Http, public AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+10; //January is 0!
    var yyyy = today.getFullYear();
    //config.minDate = { year: yyyy, month: mm, day: dd };
    //this.model = this.calendar.getToday();
    this.model={ year: yyyy, month: mm, day: dd };
    this.model_fromdate={ year: yyyy, month: mm, day: dd };
    this.model_todate={ year: yyyy, month: mm, day: dd };
  }

  ngOnInit() {
    
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string} = { switchcase: "get"};
    var method = "post";
    var url = "mixerassemblymaster";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mixer_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    //this.loadsubassy()  
    this.getmixermodel();
  };
  getweeklyplan(mixer_value){
    this.weeklys={};
    this.drum_value={};
    
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1;
    var data: { switchcase: string,id:string,month_detail:number} = { switchcase: "getweeklyplan",id:mixer_value,month_detail:month};
    var method = "post";
    var url = "weeklyplan";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.weeklys = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmixermodel(){
    this.drum_value={};
    
    this.menuaccess=[];
    var data: { switchcase: string} = { switchcase: "getbomheader" };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {
            this.drum_value = datas;
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };

  getmixerassyvalue(id){
    var data = id;
    var switchcase = "switchcase";
    var value = "get";
    var materialissue="materialissue";
    var mat_value='2';
    data[materialissue]=mat_value;
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message[0]=="undefined" || datas.message[0]==""){
            }else if(datas.message[0]){
              var headers = datas.message[0].bom_elements.split(',');
              for(var i=0; i < headers.length; i++){            
                var e2 = <HTMLTableElement>document.getElementById(headers[i]);
                e2.setAttribute('checked', 'true');
                this.menuaccess.push(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  addsubassy(form: NgForm) {
    var data = form.value;
    var menu = "options";
    var overallvalue="overallvalue";    
    var menus = this.menuaccess;
    data[overallvalue]=this.overallquantitys;
    data[menu]=menus;
    var switchcase = "switchcase";
    var value = "insert_binsystem";
    data[switchcase] = value;
    var method = "post";
    var url = "binsystemupdate";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){            
            form.resetForm();
            this.reset(form);
            this.toastr.success(datas.message, datas.type);
          }else{  
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getassyvalue(mixermodel,weeklyplan){
    
    var data: { switchcase: string,mixermodel:string,weeklyplan:string } = { switchcase: "get_binsysupdate",mixermodel:mixermodel,weeklyplan:weeklyplan };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {            
            if(datas.message[0]=="undefined" || datas.message[0]==""){
            }else if(datas.message[0]){
              var headers = datas.message[0].bom_elements.split(',');
              for(var i=0; i < headers.length; i++){            
                var e2 = <HTMLTableElement>document.getElementById(headers[i]);
                e2.setAttribute('checked', 'true');
                this.menuaccess.push(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  reset(frm:NgForm){
    
    this.drum_value={};
    this.mixer_va=[];
    this.menuaccess=[];
    frm.resetForm();
    this.getmixermodel();
  };
  checkweeklyplan(fromdate,todate,bomheaders){
    //this.getselectedvalues(fromdate,todate,bomheaders);    
    var data: { switchcase: string,fromdate:any,todate:any,bomheader:string} = { switchcase: "getweeklyplan",fromdate:fromdate,todate:todate,bomheader:bomheaders };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mixer_va = datas.message[0];
            this.quantity=datas.message[1].quantity;
            this.quantity = +this.quantity;
            this.overallquantitys=this.mixer_va.length*this.quantity;
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  getselectedvalues(fromdate,todate,bomheaders){
    this.totalquantity=0;this.issuedquantity=0;
    this.availqty=[];
    var data: { switchcase: string,fromdate:any,todate:any,bomheader:string} = { switchcase: "getselectedvalue",fromdate:fromdate,todate:todate,bomheader:bomheaders };
    var method = "post";
    var url = "binsystemupdate";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.menuaccess=[];
            if(datas.message[0]=="undefined" || datas.message[0]==""){
            }else if(datas.message[0]){
              var headers = datas.message[0].mixer_model.split(',');
              for(var k=0; k < headers.length; k++){
                this.menuaccess.push(headers[k]);
              }
              this.totalquantity=datas.message[0].availablequantity;
              this.totalquantity=+this.totalquantity;
              this.issuedquantity=datas.message[0].remainingquantity;
              // console.log(this.issuedquantity);
              this.issuedquantity=+this.issuedquantity;

              var i:number;
              var j:number = this.totalquantity-this.issuedquantity;
              for(i = 0;i<j;i++) {
                this.availqty[i] = i + 1 ;
              }

              
              headers.forEach(function (value) {
                var e2 = <HTMLInputElement>document.getElementById(value);
                //e2.setAttribute('checked', 'true');
                console.log(e2.checked=true);
              });
            }
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  
  checkValue(option,options){
    this.availqty=[];
    if(this.menuaccess.length == 0){
      this.menuaccess.push(option);
      this.totalquantity=this.totalquantity + this.quantity;
    }else{
      if(options == false){
        this.menuaccess.splice(this.menuaccess.indexOf(option),1);
        //console.log("new deleted numbers is : " + this.menuaccess );
        this.totalquantity=this.totalquantity - this.quantity;
      }else{
        var length = this.menuaccess.push(option);
        //console.log("new numbers is : " + this.menuaccess );
        this.totalquantity=this.totalquantity + this.quantity;
      }
      //console.log(this.menuaccess);
    }
    var i:number;
    for(i = 0;i<this.totalquantity;i++) {
      this.availqty[i] = i + 1 ;
    }
  };
  /* selectAll() {
    var headers = this.drum_value.message;
    for(var i=0; i < headers.length; i++){
      var e2 = <HTMLTableElement>document.getElementById(headers[i].id);
      e2.setAttribute('checked', 'true');
    }
  } */

}
