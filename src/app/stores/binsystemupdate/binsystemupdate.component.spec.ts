import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinsystemupdateComponent } from './binsystemupdate.component';

describe('BinsystemupdateComponent', () => {
  let component: BinsystemupdateComponent;
  let fixture: ComponentFixture<BinsystemupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinsystemupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinsystemupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
