import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BinsystemupdateComponent } from './binsystemupdate.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const binsystemRoutes: Routes = [
  {
    path: '',
    component: BinsystemupdateComponent,
    data: {
      breadcrumb: '2 Bin System',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(binsystemRoutes),
    SharedModule
  ],
  declarations: [BinsystemupdateComponent]
})
export class BinsystemupdateModule { }
