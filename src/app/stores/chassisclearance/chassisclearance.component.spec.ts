import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChassisclearanceComponent } from './chassisclearance.component';

describe('ChassisclearanceComponent', () => {
  let component: ChassisclearanceComponent;
  let fixture: ComponentFixture<ChassisclearanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChassisclearanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChassisclearanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
