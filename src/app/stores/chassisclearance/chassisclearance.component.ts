import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-chassisclearance',
  templateUrl: './chassisclearance.component.html',
  styleUrls: ['./chassisclearance.component.css']
})
export class ChassisclearanceComponent implements OnInit {

  private modalRef: NgbModalRef;
  inward_model: NgbDateStruct;
  invoice_model:NgbDateStruct;
  private chassisclearance: string = "";
  private editinward: any = {};
  private editchassisclearance: any = {};  
  private editchassisclearanc: any = {};  
  private chassisinward: any = {}; 
  private maildata: any = ""; 
  private getchassis: any = {}; 
  closeResult: string;
  dates: string;
  date: string = "";
  public userrole:string="";
  private customer_details:any ={};
  private chassismodels:string="";
  public chassisnos:string="";
  public chassismake:any="";
  public customernames:any="";
  public editchassisclearance_invoicedate:any={};
  public editchassisclearance_inwarddate:any={};

  public dropdownSettings = {};   
  public dropdownList = [];
  public selectedItems = [];
  menuaccess:string[] = new Array();
  private mailid: any = ""; 


  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
  
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() +12; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day:1 };
    config.maxDate = { year: yyyy, month:mm+10, day:dd};
    this.inward_model = this.calendar.getToday();
   this.invoice_model = this.calendar.getToday();
  }

  ngOnInit() {

    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "chassisclearance";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.chassisclearance = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
     
   /*  var data: { switchcase: string } = { switchcase: "getchassimodel" };
    var method = "post";
    var url = "chassisclearance";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.chassisinward = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );  */
    this.getuserrole();
    this.getcustomer();

    this.dropdownSettings = {
      text: "Select Users",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "multidropdowncustom",
      primaryKey: "email",
      labelKey: "email",
      noDataLabel: "Select an User",
      enableSearchFilter: true,
      showCheckbox: true,
      disabled: false
    };
    this.getusers(); 
    this.selectedItems=[];
    this.menuaccess=[];
  };
  getchassismakes(value){

    this.chassisnos = "";
    this.chassismodels="";
    this.editchassisclearance.available_qty="";
    this.editchassisclearance.total_qty="";
    this.chassismake="";

    var customer_data: { switchcase: string, id:string } = { switchcase: "getchassimodel", id:value };
    var customer_method = "post";
    var customer_url = "chassisclearance";
    this.AjaxService.ajaxpost(customer_data, customer_method, customer_url)
      .subscribe(
        (datas) => {
  
          if(datas.code == 201){
            this.chassisinward = datas;
         
          }else{
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getcustomer(){
    var customer_data: { switchcase: string } = { switchcase: "get" };
    var customer_method = "post";
    var customer_url = "customerspec";
    this.AjaxService.ajaxpost(customer_data, customer_method, customer_url)
      .subscribe(
        (datas) => {
  
          if(datas.code == 201){
            this.customer_details = datas;
         
          }else{
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getuserrole()
  {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "usersrole";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
  
          if(datas.code == 201){
            this.userrole = datas.message;
         
          }else{
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  
    sendmail(frm){
     var data = {"mailid":this.menuaccess};
      var switchcase = "switchcase";
      var chassisno = "chassisno";
      var chassismake = "chassismake";
      var type = "type";
      var value = "insert";
      data[switchcase] = value;
      data[chassismake]=this.maildata.chassismake;
      data[chassisno]=this.maildata.chassisno;
      data[type]='1';
      var method= "post";
      var url="sendmail";
      this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code==201){
            this.toastr.success(datas.message, datas.type);
            frm.resetForm();
            this.modalRef.close();
           this.reloadPage();
            
          }
          else{
            this.toastr.error(datas.message, datas.type);
          }    
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    };
    reloadPage() {
      this.ngOnInit();
    }
  getchassismodel(chassismake){
    this.chassismodels="";
    this.editchassisclearance.available_qty="";
    this.editchassisclearance.total_qty="";

    var chassisdata: { switchcase: string, id: string } = { switchcase: "getchassismodel",id:chassismake };
    var chassismethod = "post";
    var chassisurl = "chassisclearance";
    this.AjaxService.ajaxpost(chassisdata, chassismethod, chassisurl)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.chassismodels=datas.message[0].chassismodel;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getchassisquantity(chassismake);
  }  
  getchassisquantity(chassismake){
    var chassisdata: { switchcase: string, id: string } = { switchcase: "getchassismake",id:chassismake };
    var chassismethod = "post";
    var chassisurl = "chassisclearance";
    this.AjaxService.ajaxpost(chassisdata, chassismethod, chassisurl)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.getchassis = datas.message;
            this.getchassis.available_qty=datas.message.total_qty - datas.message.available_qty;
            this.editchassisclearance.total_qty = datas.message.total_qty;
            this.editchassisclearance.available_qty = datas.message.total_qty - datas.message.available_qty;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }  
  addchassisclearance(form: NgForm, add) {
    if (this.getchassis.available_qty == 0){
      this.toastr.error('There is no Chassis Quantity is available!', '403');
    }else{
      this.maildata =form.value;
      var data = form.value;
      var switchcase = "switchcase";
      var value = "insert";
      data[switchcase] = value;
      var method = "post";
      var url = "chassisclearance";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.toastr.success(datas.message, datas.type);
              form.resetForm();
              this.chassisnos = "";
              this.chassismodels="";
              this.editchassisclearance.available_qty="";
              this.editchassisclearance.total_qty="";
              this.customernames="";  
              this.chassismake="";            
              this.reloadPage();
            }
            else {
              this.toastr.error(datas.message, datas.type);
            }
          },
          (err) => {
            if (err.status == 403) {
              this.toastr.error(err._body);
              //alert(err._body);
            }
          }
        );
        
    }   
   // this.sendmail(form.value); 
  };
  editfunction(item) {
    this.editchassisclearance = item;
    this.customernames=item.custid;
    this.editchassisclearance.chassis_make=item.chassis_make;
    this.editchassisclearance.chassis_model=item.chassismodel;
    this.getchassismakes(item.custid);

    this.editchassisclearance_inwarddate = this.editchassisclearance.inwarddate.split("-");
    var year = +this.editchassisclearance_inwarddate[0];
    var month = +this.editchassisclearance_inwarddate[1];
    var day = +this.editchassisclearance_inwarddate[2];
    this.editchassisclearance_inwarddate = { 'year': year, 'month': month, 'day': day };

    this.editchassisclearance_invoicedate = this.editchassisclearance.invoicedate.split("-");
    var invoiceyear = +this.editchassisclearance_invoicedate[0];
    var invoicemonth = +this.editchassisclearance_invoicedate[1];
    var invoiceday = +this.editchassisclearance_invoicedate[2];
    this.editchassisclearance_invoicedate = { 'year': invoiceyear, 'month': invoicemonth, 'day': invoiceday };
  };
  editchassisclearances(form: NgForm, edit) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "chassisclearance";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            //form.resetForm();
            this.ngOnInit();
            this.modalRef.close();

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );    
  };
  role(event)
  {
    var data={"roleid":event};
    var method = "post";
    var url = "getmailid";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
         
            this.mailid = datas;
          
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  notdeletechassisinward()
  {
    swal({
      title: 'Alert',
      text: "Delete Not Enable,Because Mixerplaning assign!!!",
      type: 'warning',
      
      confirmButtonColor: '#3085d6',
      
    })
  }
  deletechassisinward(id) {
    //var data: { switchcase: string, id: any } = { switchcase: "delete", id: item };
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "chassisclearance";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                this.ngOnInit();
              }
              else {
                swal(
                  'Deleted!',
                  'Your file is safe.',
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  open(addNew) {
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };

  getusers()
    {
      var data={"roleid":''};
      var method = "post";
      var url = "getmailidall";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              this.mailid = datas;
              this.dropdownList=datas;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        ); 
  }
  checkValue(option, options) {
    //alert(option);
    //alert(options);

    if (this.menuaccess.length == 0) {
      this.menuaccess.push(option);
    } else {
      if (options == false) {

        this.menuaccess.splice(this.menuaccess.indexOf(option), 1);
      } else {
        this.menuaccess.push(option);
      }
    }
  }
  onItemSelect(item:any){
    //console.log(item);
    //console.log(this.selectedItems);
  }
  OnItemDeSelect(item:any){
    //  console.log(item);
    //  console.log(this.selectedItems);
  }
  onSelectAll(items: any){
      // console.log(items);
  }
  onDeSelectAll(items: any){
    //  console.log(items);
  }
  addmailingstatus(frm:NgForm){
    if(this.selectedItems.length > 0 || this.menuaccess.length > 0){
      var data: { switchcase: string,id:any,email:any } = { switchcase: "sendingchassisclearanceplan",id:this.menuaccess,email:this.selectedItems };
      var method = "post";
      var url = "getdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.toastr.success(datas.message, datas.type);
            } else {
              this.toastr.error(datas.message, datas.type);
            }
            this.selectedItems=[];
            this.menuaccess=[];
            this.modalRef.close();
            this.ngOnInit();
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }else{
      this.toastr.error("Mail Id is not yet selected", "error");
    }
  }
  openmodal(addNew)
  {
    if(this.menuaccess.length > 0){
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    }else{
      this.toastr.error("No values are selected", "error");
    }
  }

}
