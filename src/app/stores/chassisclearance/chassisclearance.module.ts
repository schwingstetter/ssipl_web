import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChassisclearanceComponent } from './chassisclearance.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const chassisclearanceRoutes: Routes = [
  {
    path: '',
    component: ChassisclearanceComponent,
    data: {
      breadcrumb: 'Chassis Inward Acknowledgement',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(chassisclearanceRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [ChassisclearanceComponent]
})
export class ChassisclearanceModule { }
