import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinerejectionstoresComponent } from './linerejectionstores.component';

describe('LinerejectionstoresComponent', () => {
  let component: LinerejectionstoresComponent;
  let fixture: ComponentFixture<LinerejectionstoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinerejectionstoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinerejectionstoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
