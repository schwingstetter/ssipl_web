import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-linerejectionstores',
  templateUrl: './linerejectionstores.component.html',
  styleUrls: ['./linerejectionstores.component.css']
})
export class LinerejectionstoresComponent implements OnInit {


  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  date :string;
  public mixer_value:any={};
  public drum_value:any={};
  menuaccess:string[] = new Array();
  public getmixervalues:any={};
  public mixer_values:any={};
  stations :string;
  editlineitems :string;
  editpartno :string;
  editdesc :string;
  stationname :string;
  private getmixerparts: any = {};
  private mixerplanning: any = {};
  private drumworkorder:any={};
  private drumwoqty:string="";
  private workorder:string="";
  private basedon:string="";
  
  public availqty:any = new Array();
  public names:any={};
  containers = [];
  description = [];
  partnos = [];
  //selectedAll: any;
  public selectedAll: boolean = false;
  headers:any;
  add1:any=0;
  public selectall_value:any;
  public remainingqtys:any={};
  public mounting_wo:any={};
  public mountingqty:any={};
  public choose_matr:any={};
  private materialdetails:string="";
  private storestatus:string="";
  private qc_status:string="";
  private sap_status:string="";
  private qcreworkdetails:string="";
  private materialissuefor:string="";
  private editmaterialissuefor:string="";
  private part_model:string="";
  private wo_number:string="";
  private lineitems:string="";

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.partnos[0]="";
    
  }

  getvalue(mixervalue){

    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string, id:string } = { switchcase: "getmixervalue", id:mixervalue };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.materialissuefor=mixervalue;
            this.mixer_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
     // this.getmixerpartno();
      this.getworkstation(mixervalue);
  };
  getworkstation(materialissued_for)
  {
    var data: { switchcase: string, issuedfor:string } = { switchcase: "getworkstation", issuedfor:materialissued_for };
    var method = "post";
    var url = "linerejection";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            
            this.stations = datas.message;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  editfunction(item)
  {
    this.editlineitems=item;
    if(item.material_issue_for == '1')
    {
      this.editmaterialissuefor='Drum Model';
    }
    else  if(item.material_issue_for == '2')
    {
      this.editmaterialissuefor='Mixer Model';
    }
    else  if(item.material_issue_for == '3')
    {
      this.editmaterialissuefor='Mounting Model';
    }
    if(item.qc_status == '1')
    {
      this.qc_status='Approved';
    }
    else if (item.qc_status == '2')
    {
      this.qc_status='Rejected';
    }
    if(item.sap_status == '1')
    {
      this.sap_status='512 Update';
    }
    else if(item.sap_status == '2')
    {
      this.sap_status='612 Update';
    }
    if(item.qc_rework_details == '1')
    {
      this.qcreworkdetails='At SSI by Supplier';
    }
    else if(item.qc_rework_details == '2')
    {
      this.qcreworkdetails='At SSI by SSI';
    }
    else if(item.qc_rework_details == '3')
    {
      this.qcreworkdetails='At Supplier Premises';
    }
   this.editpartno = item.part_no;
   this.editdesc=item.description;
  }
  edit_line_items(item)
  {
    var data = item.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "linerejection";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){            
            item.resetForm();
            this.modalRef.dismiss();
            this.toastr.success(datas.message, datas.type);
     
          }else{  
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.modalRef.dismiss();
  }
  update_storestatus(item)
  {
  
    var data = item.value;
    var switchcase = "switchcase";
    var storeempid = "storeempid";
    var value = "updatestore_status";
    data[switchcase] = value;
    data[storeempid] = localStorage.getItem('LoggedInUser');
    var method = "post";
    var url = "linerejection";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){            
            item.resetForm();
            this.modalRef.dismiss();
            this.toastr.success(datas.message, datas.type);     
          }else{  
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.modalRef.dismiss();
  }
  partno(event)
  {
    this.editpartno=event;
    
  }
  getdesc(partno,id)
  {
    var data: { switchcase: string, id:string,issuedfor:string } = { switchcase: "getdescription", id:partno,issuedfor:this.materialissuefor };
    var method = "post";
    var url = "linerejection";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            console.log(datas.message[0]);
           this.partnos[id]=partno;
            this.description[id] = datas.message[0].description;
            
         //   this.containers[this.containers.length -1].description=this.description[this.containers.length -1];

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmaterials(workorder_qty,basedon){
    this.wo_number=workorder_qty;
    this.availqty=[];
    this.add1 =1;
  this.workorder=workorder_qty;
  this.basedon=basedon;
  }
  getdetails()
  {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string, id:string, basedon:string,stores:string } = { switchcase: "getlineitems", id:this.workorder, basedon:this.basedon , stores:'1'};
    var method = "post";
    var url = "linerejection";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
           
            this.materialdetails=datas.message;
           
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getpartnodetails(id)
  {
    var data: { switchcase: string, id:string} = { switchcase: "getlinerejectionitems", id:id };
    var method = "post";
    var url = "linerejection";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
          
           this.lineitems=datas.message;

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  length : any;
  add(event)
  {

    event.preventDefault();
    this.length = this.containers.length;
   
    if(this.length >0)
    {
      console.log(this.containers[this.length-1]);
    if(this.partnos[this.length-1] && this.description[this.length-1])
    {
      this.containers.push(this.containers.length);
    }
    else
    {
      this.toastr.error('Enter All Fields', '');
    } 
  }
  else
  {
    this.containers.push(this.containers.length);
  }

  }
  getmixerpartno(){
    var mixer_data: { switchcase: string} = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data,mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
           console.log(mixerpart);
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );    
  }

  getworkorderno(id){
    var operationdatas: { switchcase: string, id:string } = { switchcase: "getworkorderno", id:id };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          this.part_model=id;
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

 

  getmountingwo(){
    var data: { switchcase: string } = { switchcase: "getmounting_bom" };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
            this.mounting_wo=datas;
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  /* selectAll() {
    var headers = this.drum_value.message;
    for(var i=0; i < headers.length; i++){
      var e2 = <HTMLTableElement>document.getElementById(headers[i].id);
      var check=e2.getAttribute("checked");
      if(check==null){
        e2.setAttribute('checked', 'true');
        this.menuaccess.push(headers[i].id);
      }else{
        e2.removeAttribute('checked');
        this.menuaccess=[];
      }
    }
  } */

  selectAll($event) {
    this.selectall_value="";
    this.menuaccess=[];
    var headers = this.drum_value.message;
    this.selectall_value=$event.srcElement.checked;
    for (var i = 0; i < headers.length; i++) {
      headers[i].selected = $event.srcElement.checked;
      if(headers[i].selected==true){
        this.menuaccess.push(headers[i].id);      
      }      
    }
  }
  checkIfAllSelected() {
    var headers = this.drum_value.message;
    this.selectedAll = headers.every(function(item:any) {
      return item.selected == true;
    })
  }

  checkValue(option,options){
    if(this.menuaccess.length == 0){
      this.menuaccess.push(option);
    }else{
      if(options == false){
        this.menuaccess.splice(this.menuaccess.indexOf(option),1);
        console.log("new deleted numbers is : " + this.menuaccess );  
      }else{
        var length = this.menuaccess.push(option);
        console.log("new numbers is : " + this.menuaccess );
      }
      console.log(this.menuaccess);
    }
  };
  addlineitems(form: NgForm) {
    
    var data = form.value;
    var materialissuefor = "materialissuefor";
    var partmodel = "partmodel";
    var wonumber="wonumber";
    data[materialissuefor]=this.materialissuefor;
    data[partmodel]=this.part_model;
    data[wonumber]=this.wo_number;
    data["requested_by"]=localStorage.getItem("LoggedInUser");
    data["noofitems"]=this.containers.length;
    data["workstation"]=this.stationname;
    var switchcase = "switchcase";
    var value = "insert_web";
    data[switchcase] = value;
    var method = "post";
    var url = "linerejection";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){            
            form.resetForm();
            this.drum_value=[];
            this.menuaccess=[];
            this.containers=[];
            this.modalRef.close();
            this.toastr.success(datas.message, datas.type);
          }else{  
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getstationcode(event)
  {
this.stationname = event;
  }
  getassyvalue(form:NgForm){
    this.menuaccess=[];    
    var data = form.value;
    var switchcase = "switchcase";
    var value = "get";
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";    


    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message[0]=="undefined" || datas.message[0]==""){
            }else if(datas.message[0]){
              var headers = datas.message[0].bom_elements.split(',');
              for(var i=0; i < headers.length; i++){
                this.getchecked(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getchecked(value){
    this.menuaccess=[];
    var element = <HTMLInputElement> document.getElementById(value);
    element.checked = true;
    this.menuaccess.push(value);
  }
  getmixerassyvalue(id){
    var data = id;
    var switchcase = "switchcase";
    var value = "get";
    var materialissue="materialissue";
    var mat_value='2';
    data[materialissue]=mat_value;
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message[0]=="undefined" || datas.message[0]==""){
            }else if(datas.message[0]){
              var headers = datas.message[0].bom_elements.split(',');
              for(var i=0; i < headers.length; i++){            
                var e2 = <HTMLTableElement>document.getElementById(headers[i]);
                e2.setAttribute('checked', 'true');
                this.menuaccess.push(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  reset(frm:NgForm){
    this.drum_value={};
    this.menuaccess=[];
    frm.resetForm();
  };  
  getmixermodel(mixerwono){    
    this.getmixervalues={};
    var data: { switchcase: string,id:string } = { switchcase: "getmixerassembly",id:mixerwono };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.getmixervalues.part_no=datas.message[0].part_no;
            this.getmixervalues.mixer_modelno=datas.message[0].mixer_model;
            this.getmixervalues.mixer_master_id=datas.message[0].mixer_master_id;
            this.getmixervalues.mixer_production_id=datas.message[0].mixer_production_id;
            this.mixer_values=datas;
            this.mixerbom(this.getmixervalues.mixer_master_id);
            this.getmixerassyvalue(datas.message[0]);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  mixerbom(mixer_master_id){
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string, id:string } = { switchcase: "getmixerbom", id:mixer_master_id };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for(var i=0;i<datas.message.length;i++){
              datas.message[i].selected = false;
            }
            this.drum_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  mounting_getbom(){
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mountingarrangement";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.drum_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  drum_getwo(id){
    var data: { switchcase: string, id:string,basedon:string } = { switchcase: "get", id:id , basedon:this.materialissuefor};
    var method = "post";
    var url = "linerejection";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.part_model=id;
            this.drumworkorder = datas;           
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getdrumwoqty(workorder_qty,basedon){
    this.availqty=[];
    var data: { switchcase: string, id:string, basedon:string } = { switchcase: "getwoqty", id:workorder_qty, basedon:basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {            
            this.drumwoqty=datas.message.total_qty;
            var i:number;
            this.remainingqtys=datas.message;
            if(datas.message.remaining_qty==0){
              this.toastr.error("All Materials are given to this Work Order");
            }else{
              for(i = 0;i<datas.message.remaining_qty;i++) {
                this.availqty[i] = i + 1 ;
              }
            }
            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  
  getmountreleaseqty(value,basedon){
    var data: { switchcase: string, id:string, basedon:string } = { switchcase: "getmounting_bom", id:value, basedon:basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
            console.log(datas);
            //this.mountingqty=datas.message[0];
            this.drumwoqty=datas.message.total_qty;            
            var i:number;
            this.remainingqtys=datas.message;
            if(datas.message.remaining_qty==0){
              this.toastr.error("All Materials are given to this Work Order");
            }else{
              for(i = 0;i<datas.message.remaining_qty;i++) {
                this.availqty[i] = i + 1 ;
              }
            }
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  delete(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "linerejection";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                this.ngOnInit();
              }
              else {
                swal(
                  'Deleted!',
                  'Your file is safe.',
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  }; 
  deleteline(id)
  {
   
      if(this.containers.length == 1)
      {
        this.toastr.error('There Must be atleast 1 Item');
  
      }    else
      {
      this.containers.splice(id,1);
      }
    
  }
  deletelineitem(id) {
    var data: { switchcase: string, id: any } = { switchcase: "deletelineitem", id: id };
    var method = "post";
    var url = "linerejection";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                this.ngOnInit();
              }
              else {
                swal(
                  'Deleted!',
                  'Your file is safe.',
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  }; 
  getdrumstatus(value,basedon){
    var data: { switchcase: string, id:string, basedon:string } = { switchcase: "getdrumstatus", id:value, basedon:basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
            console.log(datas);
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
}
