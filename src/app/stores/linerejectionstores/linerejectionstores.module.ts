import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinerejectionstoresComponent } from './linerejectionstores.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const linerejectionRoutes: Routes = [
  {
    path: '',
    component: LinerejectionstoresComponent,
    data: {
      breadcrumb: 'Line Rejection',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(linerejectionRoutes),
    SharedModule
  ],
  declarations: [LinerejectionstoresComponent]
})
export class LinerejectionstoresModule { }
