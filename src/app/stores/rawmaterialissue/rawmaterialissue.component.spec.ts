import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawmaterialissueComponent } from './rawmaterialissue.component';

describe('RawmaterialissueComponent', () => {
  let component: RawmaterialissueComponent;
  let fixture: ComponentFixture<RawmaterialissueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawmaterialissueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawmaterialissueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
