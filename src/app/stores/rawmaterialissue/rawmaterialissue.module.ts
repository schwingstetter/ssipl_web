import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RawmaterialissueComponent } from './rawmaterialissue.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const rawmaterialpickingRoutes: Routes = [
  {
    path: '',
    component: RawmaterialissueComponent,
    data: {
      breadcrumb: 'Raw Material Issue',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(rawmaterialpickingRoutes),
    SharedModule
  ],
  declarations: [RawmaterialissueComponent]
})
export class RawmaterialissueModule { }
