import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-rawmaterialissue',
  providers: [AjaxService, AuthService],
  templateUrl: './rawmaterialissue.component.html',
  styleUrls: ['./rawmaterialissue.component.css']
})
export class RawmaterialissueComponent implements OnInit {

  public drummuom: string = "";
  public editdrumuoms: string = "";
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "";
  public sortOrder: string = "desc";
  model: NgbDateStruct;

  public switchcase: string = "";
  public id: number = 10;
  public employees: string = "";
  public editdrumheader: any = {};
  modelno: any;
  arrayBuffer: any;
  file: File;
  auditPhotoUploader: FileUploader;
  position: string = 'bottom-right';
  title: string;
  msg: string;
  showClose: boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: string = 'default';
  closeOther: boolean = false;
  tenentIDFileName: any;
  workstationnames: any;
  public show: boolean = false;
  public buttonName: any = 'Show';
  dtOptions: DataTables.Settings = {};
  public rawmaterial:any={};
  public rawmaterialdescription:string="";
  public editmodel:any={};

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day: dd };
    this.model = this.calendar.getToday();
  }

  ngOnInit() {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "rawmaterialissue";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.employees = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getrawmaterial();    
  }

  getrawmaterial(){
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "rawmateraildetail";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.rawmaterial = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  getrawsmaterialdetail(value){
    var data: { switchcase: string,id:string } = { switchcase: "getrawmaterial",id:value };
    var method = "post";
    var url = "rawmaterialissue";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            console.log(datas); 
            this.rawmaterialdescription = datas.message[0].description;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  adddruminternal(form: NgForm, add) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "rawmaterialissue";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();            
            this.reset(form);
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );
  };
  reloadPage() {
    this.ngOnInit();
  }
  editfunction(item) {
    this.editdrumheader = item;
    this.editmodel = this.editdrumheader.datess.split("-");
    var year = +this.editmodel[0];
    var month = +this.editmodel[1];
    var day = +this.editmodel[2];
    this.editmodel = { 'year': year, 'month': month, 'day': day };
    this.getrawsmaterialdetail(item.partno);
  };
  reset(form: NgForm){
    form.resetForm();
  };
  editdruminternal(editfrm, edit) {
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "rawmaterialissue";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.modalRef.close();
            this.reloadPage();
            this.toastr.success(datas.message, datas.type);
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    //this.toastr.success('Hello world!', 'Toastr fun!');
  };
  deletedruminternal(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "rawmaterialissue";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
              }
              else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.reloadPage();
      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addnew) {
    this.modalRef = this.modalService.open(addnew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };

}
