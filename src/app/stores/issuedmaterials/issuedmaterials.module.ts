import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IssuedmaterialsComponent } from './issuedmaterials.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const issuedmaterialRoutes: Routes = [
  {
    path: '',
    component: IssuedmaterialsComponent,
    data: {
      breadcrumb: 'Issued Material',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(issuedmaterialRoutes),
    SharedModule
  ],
  declarations: [IssuedmaterialsComponent]
})
export class IssuedmaterialsModule { }
