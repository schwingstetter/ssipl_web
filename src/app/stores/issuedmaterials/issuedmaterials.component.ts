import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-issuedmaterials',
  providers: [AjaxService, AuthService],
  templateUrl: './issuedmaterials.component.html',
  styleUrls: ['./issuedmaterials.component.css']
})
export class IssuedmaterialsComponent implements OnInit {
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  public mixer_value:any={};
  public drum_value:any={};
  public form_value:any={};
  menuaccess:number[] = new Array();
  public getmixervalues:any={};
  public mixer_values:any={};
  private getmixerparts: any = {};
  private mixerplanning: any = {};
  private drumworkorder:any={};
  private drumwoqty:string="";
  public availqty:any = new Array();
  public names:any={};
  //selectedAll: any;
  public selectedAll: boolean = false;
  headers:any;
  public selectall_value:any;
  public remainingqtys:any={};
  public mounting_wo:any={};
  public mountingqty:any={};
  public choose_matr:any={};
  public getvaluesss:any={};
  public editmaterialpicking:any={};
  public option:any={};
  public header_no:number;
  constructor(public http: Http, private AjaxService: AjaxService,private router: Router, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
      }
  getvalue(mixervalue,frm:NgForm){
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string, id:string } = { switchcase: "getmixervalue", id:mixervalue };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            
            this.mixer_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getmixerpartno();
      this.getmountingwo();
  };

  getmixerpartno(){
    var mixer_data: { switchcase: string} = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data,mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );    
  }

  getworkorderno(id){
    var operationdatas: { switchcase: string, id:string } = { switchcase: "get", id:id };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  drum_getbom(drummodel){
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string, id:string } = { switchcase: "getdrumbom", id:drummodel };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for(var i=0;i<datas.message.length;i++){              
              datas.message[i].selected = false;              
            }
            this.drum_value = datas;
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };

  getmountingwo(){
    var data: { switchcase: string } = { switchcase: "getmounting_bom" };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
            this.mounting_wo=datas;
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  selectAll($event) {
    this.selectall_value="";
    this.menuaccess=[];
    var headers = this.drum_value.message;
    this.selectall_value=$event.srcElement.checked;
    for (var i = 0; i < headers.length; i++) {
      headers[i].selected = $event.srcElement.checked;
      if(headers[i].selected==true){
        this.menuaccess.push(headers[i].id);      
      }      
    }
  }
  checkIfAllSelected() {
    var headers = this.drum_value.message;
    this.selectedAll = headers.every(function(item:any) {
      return item.selected == true;
    })
  }

  checkValue(option,options,selected){
   
    if(this.menuaccess.length == 0){
      this.menuaccess.push(option);
    }else{
      if(options == false){
        this.menuaccess.splice(this.menuaccess.indexOf(option),1);
     //   console.log("new deleted numbers is : " + this.menuaccess );  
      }else{
        var length = this.menuaccess.push(option);
     //   console.log("new numbers is : " + this.menuaccess );
      }
   //   console.log(this.menuaccess);
    }
  };
  addsubassy(form: NgForm) {
    var data = form.value;
    var menu = "options";
    var menus = this.menuaccess;
    var selectall_values="selectall";
    data[selectall_values]=this.selectall_value;
    data[menu]=menus;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){            
            form.resetForm();
            this.drum_value=[];
            this.menuaccess=[];
            //this.reset(form);
            this.toastr.success(datas.message, datas.type);
          }else{  
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getassyvalue(form:NgForm){
    this.menuaccess=[];    
    var data = form.value;
    var switchcase = "switchcase";
    var value = "get";
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";    
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message[0]=="undefined" || datas.message[0]==""){
            }else if(datas.message[0]){
              var headers = datas.message[0].bom_elements.split(',');
      //        console.log(headers);

              /* for(var i=0; i < headers.length; i++){
                this.getchecked(headers[i]);
              } */
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getchecked(value){
    this.menuaccess=[];
   console.log(<HTMLInputElement> document.getElementById('10'));
    //var element = <HTMLInputElement> document.getElementById("10");
    //var check=element.setAttribute("checked","true");
    //element.checked = true;
    /* element.checked = true;
    this.menuaccess.push(value); */
  }
  getmixerassyvalue(id){
    var data = id;
    var switchcase = "switchcase";
    var value = "get";
    var materialissue="materialissue";
    var mat_value='2';
    data[materialissue]=mat_value;
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message[0]=="undefined" || datas.message[0]==""){
            }else if(datas.message[0]){
              var headers = datas.message[0].bom_elements.split(',');
              for(var i=0; i < headers.length; i++){            
                var e2 = <HTMLTableElement>document.getElementById(headers[i]);
                e2.setAttribute('checked', 'true');
                this.menuaccess.push(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  reset(frm:NgForm){
    this.drum_value={};
    this.menuaccess=[];
    frm.resetForm();
  };  
  getmixermodel(mixerwono){    
    this.getmixervalues={};
    var data: { switchcase: string,id:string } = { switchcase: "getmixerassembly",id:mixerwono };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.getmixervalues.part_no=datas.message[0].part_no;
            this.getmixervalues.mixer_modelno=datas.message[0].mixer_model;
            this.getmixervalues.mixer_master_id=datas.message[0].mixer_master_id;
            this.getmixervalues.mixer_production_id=datas.message[0].mixer_production_id;
            this.mixer_values=datas;
            this.mixerbom(this.getmixervalues.mixer_master_id);
            this.getmixerassyvalue(datas.message[0]);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  mixerbom(mixer_master_id){
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string, id:string } = { switchcase: "getmixerbom", id:mixer_master_id };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for(var i=0;i<datas.message.length;i++){
              datas.message[i].selected = false;
            }
            this.drum_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  mounting_getbom(){
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mountingarrangement";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.drum_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  drum_getwo(id){
    var data: { switchcase: string, id:string } = { switchcase: "get", id:id };
    var method = "post";
    var url = "drumwogeneration";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.drumworkorder = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getdrumwoqty(workorder_qty,basedon){
    this.availqty=[];
    var data: { switchcase: string, id:string, basedon:string } = { switchcase: "getwoqty", id:workorder_qty, basedon:basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {            
            this.drumwoqty=datas.message.total_qty;
            var i:number;
            this.remainingqtys=datas.message;
            if(datas.message.remaining_qty==0){
              this.toastr.error("All Materials are given to this Work Order");
            }else{
              for(i = 0;i<datas.message.remaining_qty;i++) {
                this.availqty[i] = i + 1 ;
              }
            }
            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  
  getmountreleaseqty(value,basedon){
    var data: { switchcase: string, id:string, basedon:string } = { switchcase: "getmounting_bom", id:value, basedon:basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
       //     console.log(datas);
            //this.mountingqty=datas.message[0];
            this.drumwoqty=datas.message.total_qty;            
            var i:number;
            this.remainingqtys=datas.message;
            if(datas.message.remaining_qty==0){
              this.toastr.error("All Materials are given to this Work Order");
            }else{
              for(i = 0;i<datas.message.remaining_qty;i++) {
                this.availqty[i] = i + 1 ;
              }
            }
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  getdrumstatus(value,basedon){
    var data: { switchcase: string, id:string, basedon:string } = { switchcase: "getdrumstatus", id:value, basedon:basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
          //  console.log(datas);
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getvalues(form:NgForm){
   this.form_value= form.value;
    this.getvaluesss={};
    var table=$('#example').DataTable();
    var data = form.value;
    var switchcase = "switchcase";
    var value = "getvaluess";
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){
            table.destroy(); 
            this.getvaluesss=datas;
            setTimeout(() => {
              $('.table').DataTable();
            }, 1000);
          }else{  
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  } 
  notdeletemat()
  {
    swal({
      title: 'Alert',
      text: "don't delete this item!",
      type: 'warning',
      confirmButtonColor: '#3085d6',
         })

  }
  deletemat(id) {
    //var data: { switchcase: string, id: any } = { switchcase: "delete", id: item };
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "materailpicking";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                this.ngOnInit();
              }
              else {
                swal(
                  'Deleted!',
                  'Your file is safe.',
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  };
  editfunction_partial(item)
  {
    localStorage.removeItem('formvalues');  
    localStorage.setItem("formvalues[]", this.form_value);
   // console.log(item);
    localStorage.setItem('formvalues', JSON.stringify(this.form_value));
    this.router.navigateByUrl('/stores/materialpicking');
  }
  editfunction_drum(item){
    console.log("items",item);
    localStorage.removeItem('totalbomelements');
    localStorage.setItem('totalbomelements',JSON.stringify(this.drum_value));
     var drum_values=this.drum_value.message;
    // for(var j=0;j<drum_values.length;j++){
    //   var checks=drum_values[j].id;
    //   var cheks=<HTMLInputElement>document.getElementById(checks);
    //   cheks.checked=false;
    //   cheks.setAttribute('selected','false');
    //   this.menuaccess=[];
    // }
    this.editmaterialpicking=item;
    // var headers = item.bom_elements.split(',');
    // for(var i=0;i<headers.length;i++){  
    //   var checkedd=<HTMLInputElement>document.getElementById(headers[i]);
    //   checkedd.checked=true;
    //   checkedd.setAttribute('selected','true');
    //   let header_no: number =parseInt(headers[i]);
    //   this.menuaccess.push(header_no);
    // //  console.log(this.menuaccess);
      localStorage.removeItem('bomelements');
      localStorage.setItem('bomelements', JSON.stringify(this.menuaccess));
      localStorage.removeItem('formvalues');  
      localStorage.setItem("formvalues[]", this.form_value);
      localStorage.setItem("items", item);
    //  console.log(item);
      localStorage.setItem('formvalues', JSON.stringify(this.form_value));  
      this.router.navigateByUrl('/stores/materailshortage');
   // }
  }
  drum_getassy(bom_elements,drum_value){
    var headers = bom_elements.split(',');
    console.log('2  '+headers);
    //console.log(headers);
    this.menuaccess=[];

    var drum_value = this.drum_value.message;
    //var checkedd=new Array();
    var checkedd=<HTMLInputElement>document.getElementById('3');
    console.log(checkedd);
    
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  openedit(edit,item) {
   this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });  
  };
  editmaterialpickings(frm:NgForm){
    var data = frm.value;
    var menu = "options";
    var menus = this.menuaccess;
    data[menu]=menus;
    data['materialissue']=menus;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){
            console.log(datas);
              /*           
            form.resetForm();
            this.drum_value=[];
            this.menuaccess=[];
            //this.reset(form);
            this.toastr.success(datas.message, datas.type); */
          }else{  
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

}
