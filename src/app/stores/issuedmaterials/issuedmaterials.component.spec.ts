import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuedmaterialsComponent } from './issuedmaterials.component';

describe('IssuedmaterialsComponent', () => {
  let component: IssuedmaterialsComponent;
  let fixture: ComponentFixture<IssuedmaterialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssuedmaterialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuedmaterialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
