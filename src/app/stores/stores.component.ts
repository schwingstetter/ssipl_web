import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stores',
  template: '<router-outlet></router-outlet>'
})
export class StoresComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
