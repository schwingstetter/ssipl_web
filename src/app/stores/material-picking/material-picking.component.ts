import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-material-picking',
  providers: [AjaxService, AuthService],
  templateUrl: './material-picking.component.html',
  styleUrls: ['./material-picking.component.css']
})
export class MaterialPickingComponent implements OnInit {

  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  materialissues:string;
  drummodels:string;
  mixerpartnos:string;
  workorder:string;
  
  public mixer_value:any={};
  public drum_value:any={};
  public form_value:any=[];
  public bom_elements:any=[];
  menuaccess:string[] = new Array();
  public getmixervalues:any={};
  public mixer_values:any={};
  private getmixerparts: any = {};
  private mixerplanning: any = {};
  private drumworkorder:any={};
  private drumwoqty:string="";
  public availqty:any = new Array();
  public names:any={};
  //selectedAll: any;
  public selectedAll: boolean = false;
  headers:any;
  public selectall_value:any;
  public remainingqtys:any={};
  public mounting_wo:any={};
  public mountingqty:any={};
  public choose_matr:any={};
  id: any;
  private sub: any;
formvalues:NgForm;
  constructor(public http: Http,private route: ActivatedRoute, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
 localStorage.getItem('formvalues');
 this.form_value = JSON.parse(localStorage.getItem('formvalues'));
 this.bom_elements = JSON.parse(localStorage.getItem('bomelements'));
 console.log("localstorage",(this.form_value));
 console.log("bom",(this.bom_elements));
      // In a real app: dispatch action to load the details here.
 this.materialissues=this.form_value.materialissue;
console.log(this.materialissues);
 this.workorder=this.form_value.workorder;

if(this.materialissues == '1')
{
  this.drummodels=this.form_value.drummodel;
this.drum_getwo(this.drummodels);
this.getdrumwoqty(this.workorder,this.materialissues);
this.drum_getbom(this.drummodels);
}
else if(this.materialissues == '2')
{
  this.mixerpartnos=this.form_value.drummodel;
this.getworkorderno(this.mixerpartnos);
this.mixerbom(this.mixerpartnos);
}
else if(this.materialissues == '3')
{

this.getmountingwo();
this.mounting_getbom();
}
  }

  getvalue(mixervalue){

    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string, id:string } = { switchcase: "getmixervalue", id:mixervalue };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            
            this.mixer_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getmixerpartno();
      this.getmountingwo();
  };

  getmixerpartno(){
    var mixer_data: { switchcase: string} = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data,mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );    
  }

  getworkorderno(id){
    var operationdatas: { switchcase: string, id:string } = { switchcase: "getworkorderno", id:id };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  drum_getbom(drummodel){
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string, id:string } = { switchcase: "getdrumbom", id:drummodel };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) { 
            for(var i=0;i<datas.message.length;i++){              
              datas.message[i].selected = false;
            }
            this.drum_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };

  getmountingwo(){
    var data: { switchcase: string } = { switchcase: "getmounting_bom" };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
            this.mounting_wo=datas;
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  /* selectAll() {
    var headers = this.drum_value.message;
    for(var i=0; i < headers.length; i++){
      var e2 = <HTMLTableElement>document.getElementById(headers[i].id);
      var check=e2.getAttribute("checked");
      if(check==null){
        e2.setAttribute('checked', 'true');
        this.menuaccess.push(headers[i].id);
      }else{
        e2.removeAttribute('checked');
        this.menuaccess=[];
      }
    }
  } */

  selectAll($event) {
   // alert();
    this.selectall_value="";
    this.menuaccess=[];
    var headers = this.drum_value.message;
    this.selectall_value=$event.srcElement.checked;
    for (var i = 0; i < headers.length; i++) {
       headers[i].selected = $event.srcElement.checked;
      if(headers[i].selected==true){
        this.menuaccess.push(headers[i].id);      
      }      
    }
  }
  checkIfAllSelected() {
    var headers = this.drum_value.message;
    this.selectedAll = headers.every(function(item:any) {
      return item.selected == true;
    })
  }

  checkValue(option,options){
    if(this.menuaccess.length == 0){
      this.menuaccess.push(option);
    }else{
      if(options == false){
        this.menuaccess.splice(this.menuaccess.indexOf(option),1);
        console.log("new deleted numbers is : " + this.menuaccess );  
      }else{
        var length = this.menuaccess.push(option);
        console.log("new numbers is : " + this.menuaccess );
      }
      console.log(this.menuaccess);
    }
  };
  addsubassy(form: NgForm) {
    var data = form.value;
    var menu = "options";
    var menus = this.menuaccess;
    var selectall_values="selectall";
    data[selectall_values]=this.selectall_value;
    data[menu]=menus;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){            
            form.resetForm();
            this.drum_value=[];
            this.menuaccess=[];
            //this.reset(form);
            this.toastr.success(datas.message, datas.type);
          }else{  
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getassyvalue(form:NgForm){
    this.menuaccess=[];    
    var data = form.value;
    var switchcase = "switchcase";
    var value = "get";
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";    
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message[0]=="undefined" || datas.message[0]==""){
            }else if(datas.message[0]){
              var headers = datas.message[0].bom_elements.split(',');
              for(var i=0; i < headers.length; i++){
                this.getchecked(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  getchecked(value){
    this.menuaccess=[];
    var element = <HTMLInputElement> document.getElementById(value);
    element.checked = true;
    this.menuaccess.push(value);
  }
  getmixerassyvalue(id){
    var data = id;
    var switchcase = "switchcase";
    var value = "get";
    var materialissue="materialissue";
    var mat_value='2';
    data[materialissue]=mat_value;
    data[switchcase] = value;
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            if(datas.message[0]=="undefined" || datas.message[0]==""){
            }else if(datas.message[0]){
              var headers = datas.message[0].bom_elements.split(',');
              for(var i=0; i < headers.length; i++){            
                var e2 = <HTMLTableElement>document.getElementById(headers[i]);
                e2.setAttribute('checked', 'true');
                this.menuaccess.push(headers[i]);
              }
            }
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  reset(frm:NgForm){
    this.drum_value={};
    this.menuaccess=[];
    frm.resetForm();
  };  
  getmixermodel(mixerwono){    
    this.getmixervalues={};
    var data: { switchcase: string,id:string } = { switchcase: "getmixerassembly",id:mixerwono };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.getmixervalues.part_no=datas.message[0].part_no;
            this.getmixervalues.mixer_modelno=datas.message[0].mixer_model;
            this.getmixervalues.mixer_master_id=datas.message[0].mixer_master_id;
            this.getmixervalues.mixer_production_id=datas.message[0].mixer_production_id;
            this.mixer_values=datas;
            this.mixerbom(this.getmixervalues.mixer_master_id);
            this.getmixerassyvalue(datas.message[0]);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  };
  mixerbom(mixer_master_id){
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string, id:string } = { switchcase: "getmixerbom", id:mixer_master_id };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            for(var i=0;i<datas.message.length;i++){
              datas.message[i].selected = false;
            }
            this.drum_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      // this.getdrumwoqty(workordernos.value,materialissue.value)
  };
  mounting_getbom(){
    this.drum_value={};
    this.menuaccess=[];
    var data: { switchcase: string } = { switchcase: "getmountingboms" };
    var method = "post";
    var url = "mountinginternalpart";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {            
            this.drum_value = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  drum_getwo(id){
    var data: { switchcase: string, id:string } = { switchcase: "get", id:id };
    var method = "post";
    var url = "drumwogeneration";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.drumworkorder = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getdrumwoqty(workorder_qty,basedon){
    this.availqty=[];
    var data: { switchcase: string, id:string, basedon:string } = { switchcase: "getwoqty", id:workorder_qty, basedon:basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {            
            this.drumwoqty=datas.message.total_qty;
            var i:number;
            this.remainingqtys=datas.message;
            if(datas.message.remaining_qty==0){
              this.toastr.error("All Materials are given to this Work Order");
            }else{
              for(i = 0;i<datas.message.remaining_qty;i++) {
                this.availqty[i] = i + 1 ;
              }
            }
            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  
  getmountreleaseqty(value,basedon){
    var data: { switchcase: string, id:string, basedon:string } = { switchcase: "getmounting_bom", id:value, basedon:basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
            console.log(datas);
            //this.mountingqty=datas.message[0];
            this.drumwoqty=datas.message.total_qty;            
            var i:number;
            this.remainingqtys=datas.message;
            if(datas.message.remaining_qty==0){
              this.toastr.error("All Materials are given to this Work Order");
            }else{
              for(i = 0;i<datas.message.remaining_qty;i++) {
                this.availqty[i] = i + 1 ;
              }
            }
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  getdrumstatus(value,basedon){
    var data: { switchcase: string, id:string, basedon:string } = { switchcase: "getdrumstatus", id:value, basedon:basedon };
    var method = "post";
    var url = "materailpicking";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){ 
            console.log(datas);
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  

}
