import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialPickingComponent } from './material-picking.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const materialpickingRoutes: Routes = [
  {
    path: '',
    component: MaterialPickingComponent,
    data: {
      breadcrumb: 'Material Picking',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(materialpickingRoutes),
    SharedModule
  ],
  declarations: [MaterialPickingComponent]
})
export class MaterialPickingModule { }
