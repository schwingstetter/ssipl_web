import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialPickingComponent } from './material-picking.component';

describe('MaterialPickingComponent', () => {
  let component: MaterialPickingComponent;
  let fixture: ComponentFixture<MaterialPickingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialPickingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialPickingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
