import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialIssueconfirmationComponent } from './material-issueconfirmation.component';

describe('MaterialIssueconfirmationComponent', () => {
  let component: MaterialIssueconfirmationComponent;
  let fixture: ComponentFixture<MaterialIssueconfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialIssueconfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialIssueconfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
