import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialIssueconfirmationComponent } from './material-issueconfirmation.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const MaterialIssueconfirmationRoutes: Routes = [
  {
    path: '',
    component: MaterialIssueconfirmationComponent,
    data: {
      breadcrumb: 'Material Issue Confirmation',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MaterialIssueconfirmationRoutes),
    SharedModule
  ],
  declarations: [MaterialIssueconfirmationComponent]
})
export class MaterialIssueconfirmationModule { }
