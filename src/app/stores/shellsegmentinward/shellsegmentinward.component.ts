import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-shellsegmentinward',
  providers: [AjaxService, AuthService],
  templateUrl: './shellsegmentinward.component.html',
  styleUrls: ['./shellsegmentinward.component.css']
})
export class ShellsegmentinwardComponent implements OnInit {
  public createsegmentinward:any={};
  public editseginward: string = "";
  public drummuom: String = '';
  public editdrumuoms: String = '';
  public RequestOptions: any;
  public closeOnConfirm: boolean;
  file_name: FileList;
  private modalRef: NgbModalRef;
  closeResult: string;
  public drummodel: String = '';
  public data: any;
  public rowsOnPage: Number = 10;
  public filterQuery: String = '';
  public sortBy: String = '';
  public sortOrder: String = 'desc';
  model: NgbDateStruct;

  public switchcase: String = '';
  public id: Number = 10;
  public employees: String = '';
  public editdrumheader: any = {};
  modelno: any;
  arrayBuffer: any;
  file: File;
  auditPhotoUploader: FileUploader;
  position: String = 'bottom-right';
  title: String;
  msg: String;
  showClose: Boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: string = 'default';
  closeOther: boolean = false;
  tenentIDFileName: any;
  workstationnames: any;
  shellcategory:string[] = new Array();
  public currentuserid:Number=0;
  public options1: any;
  public show: boolean = false;
  public buttonName: any = 'Show';
  dtOptions: DataTables.Settings = {};
  public rawmaterial:any={};
  public rawmaterialdescription:string="";
  public editmodel:any={};
  public createsegmentinward_drummodel="";
  public createsegmentinward_shellcategory="";
  public createsegmentinward_segmentcount="";
  public formclicked:boolean=false;
  public editseginward_approvalstatustext:String="";
  
 constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.createsegmentinward_shellcategory=null;
    this.createsegmentinward_drummodel=null;
    console.log(localStorage.getItem('LoggedInUser'));
    this.currentuserid=parseInt(localStorage.getItem('LoggedInUser'));
   this.reloaddtable();
        var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "conemappingmodel";
      //alert(url);
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
        //    alert(datas.code);
            if(datas.code == 201){
             this.options1=datas.message;
             setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
             }else{
    
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    
  }
onOptionsSelected(event)
  {
    //console.log(event);
    this.shellcategory=Array();
    this.drummodel=event;
    var data = {"drummodel":event};
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
  //  alert(event);
    var method = "post";
    var url = "getconedetails";
    if(event!=null){
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
        console.log(datas.message[0])
         if(datas.message[0])
        {
      for(var i=1;i<=datas.message[0].noofshell;i++)
      {
        this.shellcategory.push('Shell'+i);
      }
        }
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  }
  }
  reset(frm)
  {
   // frm.resetForm();
   this.createsegmentinward_drummodel="";
   this.createsegmentinward_shellcategory="";
   this.createsegmentinward_segmentcount="";

  }
  addshellsegmentinward(form: NgForm, add)
  {
      var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "shellsegmentinward";
   // alert();
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
          //  alert('hello');
             this.createsegmentinward_drummodel="";
            this.toastr.success(datas.message, datas.type);
           // form.resetForm();
           this.formclicked=false;
         this.reloadPage();
            this.reset(form);
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );
  }
  editfunction(item) {
    
   var data = {"drummodel":item.drumid};
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "getconedetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
             console.log(datas)
        console.log(datas.message[0])
        if(datas.message[0])
        {
              for(var i=1;i<=datas.message[0].noofshell;i++)
                {
                  this.shellcategory.push('Shell'+i);
                }
        }
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            this.ngOnInit();
          }
        }
      );
  };
  reloaddtable()
  {
       var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "shellsegmentinward";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.employees = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
   openedit( edit, item) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    //this.onOptionsSelected(item.drumid);
    // this.onOptionsSelected(item.drumid);
    console.log(item);
    console.log(item.shell_category)
    this.editseginward = item;
    this.editseginward_approvalstatustext="Waiting For Approval";
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  

  editshellsegmentationinward(editfrm, edit) {
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "shellsegmentinward";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.modalRef.close();
            this.reloadPage();
            this.toastr.success(datas.message, datas.type);
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    //this.toastr.success('Hello world!', 'Toastr fun!');
  };

  deleteshellsegmentinward(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "shellsegmentinward";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Deleted!',
                  'Your data has been deleted.',
                  'success'
                )
              }
              else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.reloadPage();
      }
    });
  };
   reloadPage() {
    this.ngOnInit();
  }
}
