import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellsegmentinwardComponent } from './shellsegmentinward.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const shellsegmentinwardRoutes: Routes = [
  {
    path: '',
    component: ShellsegmentinwardComponent,
    data: {
      breadcrumb: 'Shell Segment Inward',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(shellsegmentinwardRoutes),
    SharedModule
  ],
  declarations: [ShellsegmentinwardComponent]
})
export class ShellsegmentinwardModule { }
