import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterailshortageComponent } from './materailshortage.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const MaterialshortageRoutes: Routes = [
  {
    path: '',
    component: MaterailshortageComponent,
    data: {
      breadcrumb: 'Material Picking',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MaterialshortageRoutes),
    SharedModule
  ],
  declarations: [MaterailshortageComponent]
})
export class MaterailshortageModule { }
