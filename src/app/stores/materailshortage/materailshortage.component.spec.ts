import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterailshortageComponent } from './materailshortage.component';

describe('MaterailshortageComponent', () => {
  let component: MaterailshortageComponent;
  let fixture: ComponentFixture<MaterailshortageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterailshortageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterailshortageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
