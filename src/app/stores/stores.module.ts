import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StoresComponent } from './stores.component';
import { CommonModule } from '@angular/common';
import { storesRoutes } from './stores.routing';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(storesRoutes),
  ],
  declarations: [StoresComponent]
})
export class StoresModule { }
