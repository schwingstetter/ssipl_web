import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MountreleaseComponent } from './mountrelease.component';

describe('MountreleaseComponent', () => {
  let component: MountreleaseComponent;
  let fixture: ComponentFixture<MountreleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MountreleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MountreleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
