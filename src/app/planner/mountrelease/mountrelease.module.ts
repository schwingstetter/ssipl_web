import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MountreleaseComponent } from './mountrelease.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const mountreleaseRoutes: Routes = [
  {
    path: '',
    component: MountreleaseComponent,
    data: {
      breadcrumb: 'Mount Release',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mountreleaseRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [MountreleaseComponent]
})
export class MountreleaseModule { }
