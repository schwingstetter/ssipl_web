import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mountrelease',
  templateUrl: './mountrelease.component.html',
  styleUrls: ['./mountrelease.component.css']
})
export class MountreleaseComponent implements OnInit {
  options1 :any[];
  optionscomp :any[];
  optionsMap:any[];
  dateval: string = null;

options :any[];
optionmodel :any[];
compoptions:string="";
optionSelected: any = "Select";
optionSelected1: any = "Select";
optionmodelSelected: any = "Select";
editplanning : string ="";
workorderno : string ="";
details : string ="";
private modalRef: NgbModalRef;
closeOther: boolean = false;
closeResult: string;
private getmixerparts: any = {};
private mixerplanning: any = {};
private editworelease:any={};
private getcustomerspec:any={};
public mountingheaders:any={};

public dropdownSettings = {};   
public dropdownList = [];
public selectedItems = [];
menuaccess:string[] = new Array();
private mailid: any = ""; 

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef , private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }
  ngOnInit() {
    this.getmountrelease();

      this.customerspec();

      //this.getworkorderno();
      //this.getmixerpartno();
      this.getmountheader();
      this.dropdownSettings = {
        text: "Select Users",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "multidropdowncustom",
        primaryKey: "email",
        labelKey: "email",
        noDataLabel: "Select an User",
        enableSearchFilter: true,
        showCheckbox: true,
        disabled: false
      };
      this.getusers();  
      this.selectedItems=[];
      this.menuaccess=[];
  }
  getmountrelease(){
    var table=$('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mountrelease";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){    
            table.destroy();
            this.options=datas.message;
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);        
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  customerspec(){
    var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "customerspec";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.getcustomerspec = datas;
            } else {

            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
  }
  getmountheader(){
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mountingheader";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mountingheaders = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  /* getmixerpartno(value){
    var mixer_data: { switchcase: string,id:string} = { switchcase: "getmountingpartno",id:value };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data,mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );    
  } */

  getworkorderno(id){
    var operationdatas: { switchcase: string, id:string } = { switchcase: "get", id:id };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  /* getworkorderno()
  {
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "mixerwo";
    this.AjaxService.ajaxpost(data, method, url)
      .toPromise()
      .then(
        (datas) => {
          if (datas.code == 201) {
            this.workorderno = datas.message;
           
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  } */
  reset(form)
  {
form.resetForm();
  }
  releasewo(form)
  {
    var data =form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method= "post";
    var url="mountrelease";
    this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if(datas.code==201){
          this.toastr.success(datas.message, datas.type);
          form.resetForm(); 
          this.reloadPage();
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }    
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
        }
      }
    
    );
  }
  reloadPage() {
    this.getmountrelease();
  }
  editfunction(item) {
    this.editworelease = item;
    /* this.getmixerpartno(item.custid); */
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  editworkorderrelease(form: NgForm, edit){
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "mountrelease";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  deleteworelease(id) {
    //var data: { switchcase: string, id: any } = { switchcase: "delete", id: item };
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "mountrelease";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  response.message,
                  'success'
                )
                this.ngOnInit();
              }
              else {
                swal(
                  'Not Deleted!',
                  response.message,
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  };

  open(addNew) {
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  getusers()
    {
      var data={"roleid":''};
      var method = "post";
      var url = "getmailidall";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              this.mailid = datas;
              this.dropdownList=datas;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        ); 
  }
  checkValue(option, options) {
    //alert(option);
    //alert(options);

    if (this.menuaccess.length == 0) {
      this.menuaccess.push(option);
    } else {
      if (options == false) {

        this.menuaccess.splice(this.menuaccess.indexOf(option), 1);
      } else {
        this.menuaccess.push(option);
      }
    }
  }
  onItemSelect(item:any){
    //console.log(item);
    //console.log(this.selectedItems);
  }
  OnItemDeSelect(item:any){
    //  console.log(item);
    //  console.log(this.selectedItems);
  }
  onSelectAll(items: any){
      // console.log(items);
  }
  onDeSelectAll(items: any){
    //  console.log(items);
  }
  addmailingstatus(frm:NgForm){
    if(this.selectedItems.length > 0 || this.menuaccess.length > 0){
      var data: { switchcase: string,id:any,email:any } = { switchcase: "sendingmountreleaseplan",id:this.menuaccess,email:this.selectedItems };
      var method = "post";
      var url = "getdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.toastr.success(datas.message, datas.type);
            } else {
              this.toastr.error(datas.message, datas.type);
            }
            this.selectedItems=[];
            this.menuaccess=[];
            this.modalRef.close();
            this.ngOnInit();
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }else{
      this.toastr.error("Mail Id is not yet selected", "error");
    }
  }
  openmodal(addNew)
  {
    if(this.menuaccess.length > 0){
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    }else{
      this.toastr.error("No values are selected", "error");
    }
  }
}

