import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeeklyplanComponent } from './weeklyplan.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const Weeklyplanroutes: Routes = [
  {
    path: '',
    component: WeeklyplanComponent,
    data: {
      breadcrumb: 'Drum Weekly Plan Schedule',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(Weeklyplanroutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [WeeklyplanComponent]
})
export class WeeklyplanModule { }
