import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyplanComponent } from './weeklyplan.component';

describe('WeeklyplanComponent', () => {
  let component: WeeklyplanComponent;
  let fixture: ComponentFixture<WeeklyplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
