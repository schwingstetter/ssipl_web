import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-weeklyplan',
  templateUrl: './weeklyplan.component.html',
  styleUrls: ['./weeklyplan.component.css']
})
export class WeeklyplanComponent implements OnInit {
  options1: any[];
  optionscomp: any[];
  optionsMap: any[];
  dateval: string = null;
  file_name: FileList;
  todate_min: any = {};
  // this.minDate = {year: 2017, month: 1, day: 1};
  options: any[];
  partno: any;
  optionmodel: any[];
  compoptions: string = "";
  optionSelected: any = "Select";
  optionSelected1: any = "Select";
  optionmodelSelected: any = "Select";
  public editplanning: any = {};
  details: string = "";
  private mailid: any = "";
  private maildata: any = "";
  private modalRef: NgbModalRef;
  menuaccess: string[] = new Array();
  closeOther: boolean = false;
  closeResult: string;
  public userrole: string = "";
  model: NgbDateStruct;
  model_todate: NgbDateStruct;
  model_fromdate: NgbDateStruct;
  models: NgbDateStruct;
  fromdate: string;
  todate: string;
  private editfromdate: any = {};
  private edittodate: any = {};
  viewquantity: {
    date: string,
    quantity: number
  } [] = [];
  invalidnumber: boolean = false;
  totalerror: boolean = false;
  viewquantityquality: number = 0;
  no: number = 0;
  total: number = 0;
  viewinitcount: number = 0;
  viewquantityid: any = {};

  public dropdownSettings = {};   
  public dropdownList = [];
  public selectedItems = [];

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
   // config.minDate = { year: yyyy, month: mm, day: dd };
  //  this.model = this.calendar.getToday();
  this.model={ year: yyyy, month: mm, day: dd };
  this.model_fromdate={ year: yyyy, month: mm, day: dd };
  this.model_todate={ year: yyyy, month: mm, day: dd };  
   }
  ngOnInit() {

    this.getdrumweeklyplan();
      this.dropdownSettings = {
        text: "Select Users",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "multidropdowncustom",
        primaryKey: "email",
        labelKey: "email",
        noDataLabel: "Select an User",
        enableSearchFilter: true,
        showCheckbox: true,
        disabled: false
      };
      this.getusers();
    this.getmodelno();
    this.selectedItems=[];
    this.menuaccess=[];
  }
  getdrumweeklyplan(){
    var table = $('.datatable').DataTable();
    var data: { switchcase: string} = {switchcase: "get"};
    var method = "post";
    var url = "drumweeklygeneration";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            this.options1 = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getusers()
  {
    var data={"roleid":''};
    var method = "post";
    var url = "getmailidall";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
            this.mailid = datas;
            this.dropdownList=datas;
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }

  getmodelno() {
    var data: {
      switchcase: string
    } = {
      switchcase: "get"
    };
    var method = "post";
    var url = "drumweeklygenerationmodel";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          //    alert(datas.code);
          if (datas.code == 201) {
            this.options = datas.message;
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    this.getuserrole();
  }
  reset(form) {
    form.resetForm();
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    this.model_fromdate={ year: yyyy, month: mm, day: dd };
    this.model_todate={ year: yyyy, month: mm, day: dd };
  }
  adddrumweeklywo(form: NgForm) {
    this.maildata = form.value;
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "drumweeklygeneration";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            this.reset(form);
            this.reloadPage();
            // add.hide();
          } else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }

      );
  }
  reloadPage() {
    this.getdrumweeklyplan();
  }
  deletedrumwo(id) {
    var data: {
      switchcase: string,
      id: any
    } = {
      switchcase: "delete",
      id: id
    };
    var method = "post";
    var url = "drumweeklygeneration";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {

                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                this.reloadPage();

              } else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.reloadPage();
      }
    });
  };
  onOptionsSelected(event) {
    var data = {
      'modelno': event
    };
    var method = "post";
    var url = "drumpartno";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.partno = datas.message[0].drumpart_no;
            this.editplanning.part_no = datas.message[0].drumpart_no;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  editfunction(item) {
    this.editplanning = item;
    this.fromdate = item.fromdate.split("-");
    var year = +this.fromdate[0];
    var month = +this.fromdate[1];
    var day = +this.fromdate[2];
    this.editfromdate = {
      'year': year,
      'month': month,
      'day': day
    };

    this.todate = item.todate.split("-");
    var todateyear = +this.todate[0];
    var todatemonth = +this.todate[1];
    var todateday = +this.todate[2];
    this.edittodate = {
      'year': todateyear,
      'month': todatemonth,
      'day': todateday
    };
  };

  viewquantityfunction(item) {
    console.log(item)
    this.viewinitcount = 0;
    this.invalidnumber = false;
    //let total=0;
    this.total = 0;
    this.totalerror = false;
    this.viewquantity = [];
    this.invalidnumber = false;

    this.viewquantityid = item.id;
    this.viewquantityquality = item.quantity;
    if (item.dailyplan != null && item.dailyplan != "") {
      console.log("if")
      console.log(JSON.parse(item.dailyplan))
      let dailyplanarray: any = [];
      dailyplanarray = JSON.parse(item.dailyplan);
      for (let i = 0; i < dailyplanarray.length; i++) {
        let quantityno: number = Number(dailyplanarray[i]['quantity']);
        this.viewquantity.push({
          date: dailyplanarray[i]['date'],
          quantity: quantityno
        })
      }
      console.log(this.viewquantity)
    } else {
      console.log("else")
      let currentdate = new Date(item.fromdate);
      let enddate = new Date(item.todate);
      do {
        let quantitymonth = currentdate.getMonth() + 1;
        let quantitydate = currentdate.getDate();
        let quantityyear = currentdate.getFullYear();
        currentdate.setDate(currentdate.getDate() + 1);
        this.viewquantity.push({
          date: quantitydate + "/" + quantitymonth + "/" + quantityyear,
          quantity: 0
        })
      }
      while (currentdate <= enddate)
      console.log(this.viewquantity);
    }
  }

  editdailyplan() {
    this.invalidnumber = false;
    this.total = 0;
    this.totalerror = false;
    // console.log(this.viewquantity.length)
    for (let i = 0; i < this.viewquantity.length; i++) {
      this.no = Number(this.viewquantity[i]['quantity']);
      //  console.log(this.viewquantity[i]['quantity'])
      if (isNaN(this.viewquantity[i]['quantity'])) {
        console.log("found");
        this.invalidnumber = true;
      }
      let no1: number = Number(this.total);
      let no2: number = Number(this.no);
      this.total = no1 + no2;
      no1 = Number(this.total);
      console.log(no1)
    }
    console.log(this.invalidnumber)
    if (this.invalidnumber != true) {
      if (this.total != this.viewquantityquality) {
        this.totalerror = true;
      }
      if (this.totalerror != true) {
        var data = {
          "id": this.viewquantityid,
          "data": JSON.stringify(this.viewquantity)
        };
        var switchcase = "switchcase";
        var value = "edit";

        var method = "post";
        var url = "drumdailyupdate";
        this.AjaxService.ajaxpost(data, method, url)
          .subscribe(
            (datas) => {
              if (datas.code == 201) {
                this.modalRef.close();
                this.toastr.success(datas.message, datas.type);
                this.reloadPage();
              } else {
                this.toastr.error(datas.message, datas.type);
              }
            },
            (err) => {
              if (err.status == 403) {
                this.toastr.error(err._body);
                //alert(err._body);
              }
            }
          );
      }
    }
    console.log("quantity=" + this.total)
    console.log("totalquantity=" + this.viewquantityquality)
    console.log("total=" + this.totalerror)
    console.log("total=" + this.invalidnumber)


  }
  editdrumwo(form, edit) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "drumweeklygeneration";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.modalRef.close();
            this.toastr.success(datas.message, datas.type);
            this.reset(form);
            this.reloadPage();
          } else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
    //this.toastr.success('Hello world!', 'Toastr fun!');
  };

  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
    this.file_name = fileList;
  };
  filesubmit(bulkupload) {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({
        headers: headers
      });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/

      var data = formData;
      var method = "post";
      var url = "uploadfilesdrumweeklygeneration";
      var i: number;
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for (i = 0; i < data.length; i++) {
              if (data[i].code == 201) {
                this.toastr.success(data[i].message, 'Success!');
              } else {
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.reloadPage();
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };
  checkValue(option, options) {
    //alert(option);
    //alert(options);

    if (this.menuaccess.length == 0) {
      this.menuaccess.push(option);
    } else {
      if (options == false) {

        this.menuaccess.splice(this.menuaccess.indexOf(option), 1);
      } else {
        this.menuaccess.push(option);
      }
    }
  }
  sendmail(frm) {

    var data = {
      "mailid": this.menuaccess
    };
    var switchcase = "switchcase";
    var modelno = "modelno";
    var partno = "partno";
    var quantity = "quantity";
    var perdayplan = "perdayplan";
    var type = "type";
    var value = "insert";
    data[switchcase] = value;
    data[modelno] = this.maildata.modelno;
    data[partno] = this.maildata.partno;
    data[quantity] = this.maildata.quantity;
    data[perdayplan] = this.maildata.plan;
    data[type] = '3';
    var method = "post";
    var url = "sendmail";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            frm.resetForm();
            this.modalRef.close();
            this.reloadPage();

          } else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  getuserrole() {
    var data: {
      switchcase: string
    } = {
      switchcase: "get"
    };
    var method = "post";
    var url = "usersrole";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {

          if (datas.code == 201) {
            this.userrole = datas.message;

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  role(event) {
    var data = {
      "roleid": event
    };
    var method = "post";
    var url = "getmailid";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {

          this.mailid = datas;

        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  downloadFile() {

    return this.http
      .get('assets/uploads/DrumWeeklyPlan.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'DrumWeeklyPlan.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  open(addNew) {
    this.modalRef = this.modalService.open(addNew, {
      backdrop: 'static',
      keyboard: false,
      size: 'lg'
    });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, {
      backdrop: 'static',
      keyboard: false,
      size: 'lg'
    });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, {
      backdrop: 'static',
      keyboard: false,
      size: 'lg'
    });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openview(view) {
    this.modalRef = this.modalService.open(view, {
      backdrop: 'static',
      keyboard: false,
      size: 'lg'
    });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  openmodal(addNew)
  {
    if(this.menuaccess.length > 0){
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    }else{
      this.toastr.error("No values are selected", "error");
    }
  }
  onItemSelect(item:any){
    //console.log(item);
    //console.log(this.selectedItems);
  }
  OnItemDeSelect(item:any){
    //  console.log(item);
    //  console.log(this.selectedItems);
  }
  onSelectAll(items: any){
      // console.log(items);
  }
  onDeSelectAll(items: any){
    //  console.log(items);
  }
  addmailingstatus(frm:NgForm){    
    if(this.selectedItems.length > 0 || this.menuaccess.length > 0){
      var data: { switchcase: string,id:any,email:any } = { switchcase: "sendingweeklyplan",id:this.menuaccess,email:this.selectedItems };
      var method = "post";
      var url = "getdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.toastr.success(datas.message, datas.type);
            } else {
              this.toastr.error(datas.message, datas.type);
            }
            this.selectedItems=[];
            this.menuaccess=[];
            this.modalRef.close();
            this.reloadPage();
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }else{
      this.toastr.error("Mail Id is not yet selected", "error");
    }
  }

}


