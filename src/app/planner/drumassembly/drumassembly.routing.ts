import { Routes } from '@angular/router';


export const drumassemblyroutes: Routes = [
    {
        path: '',
        data: {            
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'workorder',
                loadChildren: './workorder/workorder.module#WorkorderModule'
            },{
                path: 'weeklyplan',
                loadChildren: './weeklyplan/weeklyplan.module#WeeklyplanModule'

            }  
        ]
    }
];
