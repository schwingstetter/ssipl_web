import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrumassemblyComponent } from './drumassembly.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { WeeklyplanComponent } from './weeklyplan/weeklyplan.component';
import { drumassemblyroutes } from './drumassembly.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(drumassemblyroutes),
    SharedModule
  ],
  declarations: [DrumassemblyComponent]
})
export class DrumassemblyModule { }
