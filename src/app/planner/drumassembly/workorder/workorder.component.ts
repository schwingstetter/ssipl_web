import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../../ajax.service';
import { AuthService } from '../../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-workorder',
  templateUrl: './workorder.component.html',
  styleUrls: ['./workorder.component.css']
})
export class WorkorderComponent implements OnInit {
  options1 :any[];
  optionscomp :any[];
  optionsMap:any[];
  dateval: string = null;
  partno: string = '';
file_name:FileList;
options :any[];
optionmodel :any[];
compoptions:string="";
optionSelected: any = "Select";
optionSelected1: any = "Select";
optionmodelSelected: any = "Select";
editplanning : string ="";
details : string ="";
private modalRef: NgbModalRef;
closeOther: boolean = false;
closeResult: string;
dtOptions: DataTables.Settings = {};

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef , private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }
  ngOnInit() {
    this.getdrumworkorder();
   this.getmodelno();
  }
  getdrumworkorder(){
    var table = $('.datatable').DataTable();

    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "drumwogeneration";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
      //    alert(datas.code);
          if(datas.code == 201){
           this.options1=datas.message;
           table.destroy();

           setTimeout(() => {
            $('.datatable').DataTable();
          }, 1000);
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  adddrumwo(form:NgForm)
{
  var data =form.value;
  var switchcase = "switchcase";
  var value = "insert";
  data[switchcase] = value;
  var method= "post";
  var url="drumwogeneration";
  this.AjaxService.ajaxpost(data, method, url)
  .subscribe(
    (datas) => {
      if(datas.code==201){
        this.toastr.success(datas.message, datas.type);
        form.resetForm(); 
        this.reloadPage();
       // add.hide();
      }
      else{
        this.toastr.error(datas.message, datas.type);
      }    
    },
    (err) => {
      if (err.status == 403) {
        this.toastr.error(err._body);
        //alert(err._body);
      }
    }
  
  );
}
reset(form)
{
  form.resetForm();
}
reloadPage() {
   this.getdrumworkorder();
}
deletedrumwo(id) {
  var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
  var method = "post";
  var url = "drumwogeneration";
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning', 
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!',
  }).then((isconfirm) => {
    if (isconfirm.value == true) {
      this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
        .subscribe(
         
          response => {
            if (response.code == 201) {

              swal(
                'Deleted!',
                'Your Data has been deleted.',
                'success'
              )
              this.reloadPage();

            }
            else if(response.code == 405) {
              swal(
                'Not Deleted!',
                'Material Issue is Issued from the Store, So the Workorder is unable to Delete!',
                'error'
              )
            }else{
              swal(
                'Not Deleted!',
                'Your Data is safe.',
                'error'
              )
            }
          },
          error => console.log('error : ' + error)
        );
    } else {
     
      this.reloadPage();
    }
  });
};
editfunction(item) {
  this.editplanning = item;
  this.onOptionsSelected(item.drum_model_no);
  this.getmodelno();
};
editdrumwo(form,edit)
{
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "drumwogeneration";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.modalRef.close();
            this.toastr.success(datas.message, datas.type);    
            this.reloadPage();        
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //this.toastr.success('Hello world!', 'Toastr fun!');
  };


  onOptionsSelected(event)
  {
    var data={'modelno' : event};
    var method = "post";
    var url = "drumpartno";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {          
          if (datas.code == 201) {
            this.partno = datas.message[0].drumpart_no;          
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

openedit(edit) {
this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
this.modalRef.result.then((result) => {
  this.closeResult = `Closed with: ${result}`;
}, (reason) => {
  this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
});
};
private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return `with: ${reason}`;
  }
}
fileUpload(event) {
  //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
  let fileList: FileList = event.target.files;
 this.file_name=fileList;
};
filesubmit(bulkupload)
{
  if (this.file_name.length > 0) {
    let file: File = this.file_name[0];
    let formData: FormData = new FormData();
    formData.append('photo', file);
    let currentUser = localStorage.getItem('LoggedInUser');
    let headers = new Headers();
    /* headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept','application/json');
    headers.append('Authorization', 'Bearer ' + currentUser);   */
    /*  headers.append('withCredentials', 'true');
     headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

    /* headers.append('Authorization', 'Nantha ' + currentUser);  */

    let options = new RequestOptions({ headers: headers });
    /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
      .map(res => res.json())
      .catch(error => Observable.throw(error))
      .subscribe(
        data => this.toastr.success(data.message, data.type),
        error => this.toastr.error(error)
      )*/

    var data = formData;
    var method = "post";
    var url = "uploadfilesdrumwogeneration";
    var i: number; 
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        data => {
          for(i=0;i<data.length;i++){
            if(data[i].code==201){
              this.toastr.success(data[i].message, 'Success!');  
            }else{
              this.toastr.error(data[i].message, 'Error!');
            }
          }
          this.modalRef.close();
          this.reloadPage(); 
        },
        error => this.toastr.error(error)
      );
  } else {

  }
};


downloadFile() {

  return this.http
    .get('assets/uploads/drumworkorder.xlsx', {
      responseType: ResponseContentType.Blob,
      search: ""
    })
    .map(res => {
      return {
        filename: 'DrumWorkordergeneration.xlsx',
        data: res.blob()
      };
    })
    .subscribe(res => {
      console.log('start download:', res);
      var url = window.URL.createObjectURL(res.data);
      var a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = res.filename;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove(); // remove the element
    }, error => {
      console.log('download error:', JSON.stringify(error));
    }, () => {
      console.log('Completed file download.')
    });
}
open(bulkupload) {
  this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
  this.modalRef.result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
};
getmodelno()
{
  var data: { switchcase: string } = { switchcase: "get" };
  var method = "post";
  var url = "drumweeklygenerationmodel";
  //alert(url);
  this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if(datas.code == 201){
         this.options=datas.message;
        
         }else{

        }
      },
      (err) => {
        if (err.status == 403) {
          alert(err._body);
        }
      }
    ); 
}
}


