import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkorderComponent } from './workorder.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

export const WorkorderRoutes: Routes = [
  {
    path: '',
    component: WorkorderComponent,
    data: {
      breadcrumb: 'Work Order Generation',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(WorkorderRoutes),
    SharedModule
  ],
  declarations: [WorkorderComponent]
})
export class WorkorderModule { }
