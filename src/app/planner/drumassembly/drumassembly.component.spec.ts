import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrumassemblyComponent } from './drumassembly.component';

describe('DrumassemblyComponent', () => {
  let component: DrumassemblyComponent;
  let fixture: ComponentFixture<DrumassemblyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrumassemblyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrumassemblyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
