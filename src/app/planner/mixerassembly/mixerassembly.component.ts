import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mixerassembly',
  templateUrl: './mixerassembly.component.html',
  styleUrls: ['./mixerassembly.component.css']
})
export class MixerassemblyComponent implements OnInit {
  options1 :any[];
  optionscomp :any[];
  optionsMap:any[];
  dateval: string = null;
  public partnos: any = {};

options :any[];
optionmodel :any[];
compoptions:string="";
optionSelected: any = "Select";
optionSelected1: any = "Select";
optionmodelSelected: any = "Select";
editplanning : string ="";
details : string ="";
private modalRef: NgbModalRef;
closeOther: boolean = false;
closeResult: string;
private mailid: any = ""; 
private maildata: any = ""; 
menuaccess:string[] = new Array();
public userrole:string="";
file_name:FileList;

public dropdownSettings = {};   
public dropdownList = [];
public selectedItems = [];
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef , private modalService: NgbModal) {
    this.toastr.setRootViewContainerRef(vcr);
   }
  ngOnInit() {
    
    
      this.dropdownSettings = {
        text: "Select Users",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "multidropdowncustom",
        primaryKey: "email",
        labelKey: "email",
        noDataLabel: "Select an User",
        enableSearchFilter: true,
        showCheckbox: true,
        disabled: false
      };
      this.getusers();  
      this.getdetails();
      this.selectedItems=[];
      this.menuaccess=[];
      this.getmixerdetails();
  }
  getdetails()
  {
    var table = $('.datatable').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "addmixerplanning";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.details = datas.message;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
      this.getuserrole();
  }
  getmixerdetails(){
    var data: { switchcase: string } = { switchcase: "getmixermodel" };
    var method = "post";
    var url = "addmixerplanning";
    //alert(url);
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
      //    alert(datas.code);
          if(datas.code == 201){
           this.options=datas.message;
          
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  dateSelected(val) {
    this.dateval = val;
  }
  onOptionsSelected(event){
  
      var data={'modelno' : event};
      var method = "post";
      var url = "mixerpartno";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {          
            if (datas.code == 201) {              
              this.partnos = datas;
            } else {
    
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }    //alert(event);
       
       reset(form)
       {
        form.resetForm();
       }
       reloadPage() {
        // Solution 1:   
        // this.router.navigate('localhost:4200/new');
      
        // Solution 2:
         this.getdetails();         
      }
      deletemixerplanning(id) {
        var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
        var method = "post";
        var url = "addmixerplanning";
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning', 
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!',
        }).then((isconfirm) => {
          if (isconfirm.value == true) {
            this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
              .subscribe(
               
                response => {
                  if (response.code == 201) {
    
                    swal(
                      'Deleted!',
                      response.message,
                      'success'
                    )
                    this.reloadPage();
    
                  }
                  else if(response.code==405){
                    swal(
                      'Not Deleted!',
                      response.message,
                      'error'
                    )
                  }
                },
                error => console.log('error : ' + error)
              );
          } else {
           
            this.reloadPage();
          }
        });
      };
      editfunction(item) {
      //  alert("");
        this.editplanning = item;
        this.onOptionsSelected(item.mixer_modelno);
      };
      editmixerplanning(form,edit)
      {
          var data = form.value;
          var switchcase = "switchcase";
          var value = "editmixerassemblyplan";
          data[switchcase] = value;
          var method = "post";
          var url = "addmixerplanning";
          this.AjaxService.ajaxpost(data, method, url)
            .subscribe(
              (datas) => {
                if (datas.code == 201) {
                  this.modalRef.close();
                  this.toastr.success(datas.message, datas.type);    
                  this.reloadPage();        
                }
                else {
                  this.toastr.error(datas.message, datas.type);
                }
              },
              (err) => {
                if (err.status == 403) {
                  this.toastr.error(err._body);
                  //alert(err._body);
                }
              }
            );
            //this.toastr.success('Hello world!', 'Toastr fun!');
        };
        checkValue(option,options){
          //alert(option);
          //alert(options);
      
         if(this.menuaccess.length == 0)
         {
        this.menuaccess.push(option);
         } 
         else
         {
             if(options == false)
          {
      
      this.menuaccess.splice(this.menuaccess.indexOf(option),1);
          }
       else
       {
        this.menuaccess.push(option);
       }  
         }
          }
        sendmail(frm){
          var data = {"mailid":this.menuaccess};
           var switchcase = "switchcase";
           var workorderno = "workorderno";
           var modelno = "modelno";
           var partno = "partno";
           var planschedule = "planschedule";
           var type = "type";
           var value = "insert";
           data[switchcase] = value;
           data[workorderno]=this.maildata.workorderno;
           data[modelno]=this.maildata.modelno;
           data[partno]=this.maildata.partno;
           data[planschedule]=this.maildata.date;
           data[type]='4';
           var method= "post";
           var url="sendmail";
           this.AjaxService.ajaxpost(data, method, url)
           .subscribe(
             (datas) => {
               if(datas.code==201){
                 this.toastr.success(datas.message, datas.type);
                 frm.resetForm();
                 this.modalRef.close();
                this.reloadPage();
                 
               }
               else{
                 this.toastr.error(datas.message, datas.type);
               }    
             },
             (err) => {
               if (err.status == 403) {
                 this.toastr.error(err._body);
                 //alert(err._body);
               }
             }
           );
         };
         getuserrole()
         {
           var data: { switchcase: string } = { switchcase: "get" };
           var method = "post";
           var url = "usersrole";
           this.AjaxService.ajaxpost(data, method, url)
             .subscribe(
               (datas) => {
         
                 if(datas.code == 201){
                   this.userrole = datas.message;
                
                 }else{
         
                 }
               },
               (err) => {
                 if (err.status == 403) {
                   alert(err._body);
                 }
               }
             );
         }
         role(event)
         {
           var data={"roleid":event};
           var method = "post";
           var url = "getmailid";
           this.AjaxService.ajaxpost(data, method, url)
             .subscribe(
               (datas) => {
                
                   this.mailid = datas;
                 
               },
               (err) => {
                 if (err.status == 403) {
                   alert(err._body);
                 }
               }
             ); 
         }
       createmixerplanning(form)
       {
        this.maildata=form.value;
        var data =form.value;
        var switchcase = "switchcase";
        var value = "insert";
        data[switchcase] = value;
        var method= "post";
        var url="addmixerplanning";
        this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if(datas.code==201){
              this.toastr.success(datas.message, datas.type);
              form.resetForm(); 
              this.reloadPage();
             // add.hide();
            }
            else{
              this.toastr.error(datas.message, datas.type);
            }    
          },
          (err) => {
            if (err.status == 403) {
              this.toastr.error(err._body);
              //alert(err._body);
            }
          }
        
        );
      
    }
    openedit(edit) {
      this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    };
    open(addNew) {
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
      //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
    };
    openbulkupload(bulkupload) {
      this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
      this.modalRef.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    };
    fileUpload(event) {
      //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
      let fileList: FileList = event.target.files;
     this.file_name=fileList;
    };
    filesubmit(bulkupload)
    {
      if (this.file_name.length > 0) {
        let file: File = this.file_name[0];
        let formData: FormData = new FormData();
        formData.append('photo', file);
        let currentUser = localStorage.getItem('LoggedInUser');
        let headers = new Headers();
        /* headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Accept','application/json');
        headers.append('Authorization', 'Bearer ' + currentUser);   */
        /*  headers.append('withCredentials', 'true');
         headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */
  
        /* headers.append('Authorization', 'Nantha ' + currentUser);  */
  
        let options = new RequestOptions({ headers: headers });
        /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
          .map(res => res.json())
          .catch(error => Observable.throw(error))
          .subscribe(
            data => this.toastr.success(data.message, data.type),
            error => this.toastr.error(error)
          )*/
  
        var data = formData;
        var method = "post";
        var url = "mixerassemblyuploadfiles";
        var i: number; 
        this.AjaxService.ajaxpost(data, method, url)
          .subscribe(
            data => {
              for(i=0;i<data.length;i++){
                if(data[i].code==201){
                  this.toastr.success(data[i].message, 'Success!');                               
                  this.modalRef.close();
                }else{
                  this.toastr.error(data[i].message, 'Error!');
                }
              }
              this.modalRef.close();
              this.ngOnInit();
            },
            error => this.toastr.error(error)
          );
      } else {
  
      }
    };
    downloadFile() {
  
      return this.http
        .get('assets/uploads/mixerassemblyplanning.xlsx', {
          responseType: ResponseContentType.Blob,
          search: ""
        })
        .map(res => {
          return {
            filename: 'MixerAssemblyPlanning.xlsx',
            data: res.blob()
          };
        })
        .subscribe(res => {
          console.log('start download:', res);
          var url = window.URL.createObjectURL(res.data);
          var a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = url;
          a.download = res.filename;
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove(); // remove the element
        }, error => {
          console.log('download error:', JSON.stringify(error));
        }, () => {
          console.log('Completed file download.')
        });
    }
    getusers()
    {
      var data={"roleid":''};
      var method = "post";
      var url = "getmailidall";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              this.mailid = datas;
              this.dropdownList=datas;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        ); 
    }
    onItemSelect(item:any){
      //console.log(item);
      //console.log(this.selectedItems);
    }
    OnItemDeSelect(item:any){
      //  console.log(item);
      //  console.log(this.selectedItems);
    }
    onSelectAll(items: any){
        // console.log(items);
    }
    onDeSelectAll(items: any){
      //  console.log(items);
    }
    addmailingstatus(frm:NgForm){
      if(this.selectedItems.length > 0 || this.menuaccess.length > 0){
        var data: { switchcase: string,id:any,email:any } = { switchcase: "sendingmixerassemblyplan",id:this.menuaccess,email:this.selectedItems };
        var method = "post";
        var url = "getdetails";
        this.AjaxService.ajaxpost(data, method, url)
          .subscribe(
            (datas) => {
              if (datas.code == 201) {
                this.toastr.success(datas.message, datas.type);
              } else {
                this.toastr.error(datas.message, datas.type);
              }
              this.selectedItems=[];
              this.menuaccess=[];
              this.modalRef.close();
              this.ngOnInit();
            },
            (err) => {
              if (err.status == 403) {
                alert(err._body);
              }
            }
          );
      }else{
        this.toastr.error("Mail Id is not yet selected", "error");
      }
    }
  openmodal(addNew)
  {
    if(this.menuaccess.length > 0){
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    }else{
      this.toastr.error("No values are selected", "error");
    }
  }
}