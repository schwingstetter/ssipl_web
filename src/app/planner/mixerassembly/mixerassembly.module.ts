import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MixerassemblyComponent } from './mixerassembly.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const mixerassemblyRoutes: Routes = [
  {
    path: '',
    component: MixerassemblyComponent,
    data: {
      breadcrumb: 'Mixer Assembly Plan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mixerassemblyRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [MixerassemblyComponent]
})
export class MixerassemblyModule { }
