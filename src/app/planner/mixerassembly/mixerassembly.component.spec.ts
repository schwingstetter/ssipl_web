import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixerassemblyComponent } from './mixerassembly.component';

describe('MixerassemblyComponent', () => {
  let component: MixerassemblyComponent;
  let fixture: ComponentFixture<MixerassemblyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixerassemblyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixerassemblyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
