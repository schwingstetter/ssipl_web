import {RouterModule,Routes } from '@angular/router';


export const plannerRoutes: Routes = [
    {
        path: '',
        data: {
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'drumassembly',
                loadChildren: './drumassembly/drumassembly.module#DrumassemblyModule'
            }, 
            {
                path: 'mixerassembly',
                loadChildren: './mixerassembly/mixerassembly.module#MixerassemblyModule'
            }, {
                path: 'mountrelease',
                loadChildren: './mountrelease/mountrelease.module#MountreleaseModule'
            }, {
                path: 'monthlyplan',
                loadChildren: './monthlyplan/monthlyplan.module#MonthlyplanModule'
            }, {
                path: 'mixerweeklyplan',
                loadChildren: './mixerweeklyplan/mixerweeklyplan.module#MixerweeklyplanModule'
            },
            {
                path: 'viewpackaginglist',
                loadChildren: './../master/viewpackaginglist/viewpackaginglist.module#ViewpackaginglistModule'
            },
            {
            path: 'viewpackage',
            loadChildren: './../master/viewpackaglists/viewpackaglists.module#ViewpackaglistsModule'
            }
        ]
    }
];

//allinone 730s