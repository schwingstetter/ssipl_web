import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonthlyplanComponent } from './monthlyplan.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const monthlyplanRoutes: Routes = [
  {
    path: '',
    component: MonthlyplanComponent,
    data: {
      breadcrumb: 'Monthly Plan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(monthlyplanRoutes),
    SharedModule,
  ],
  declarations: [MonthlyplanComponent]
})
export class MonthlyplanModule { }
