import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyplanComponent } from './monthlyplan.component';

describe('MonthlyplanComponent', () => {
  let component: MonthlyplanComponent;
  let fixture: ComponentFixture<MonthlyplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
