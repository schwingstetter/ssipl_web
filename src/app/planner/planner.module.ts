import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PlannerComponent } from './planner.component';
import { plannerRoutes } from './planner.routing';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(plannerRoutes),
  ],
  declarations: [PlannerComponent]
})
export class PlannerModule { }
