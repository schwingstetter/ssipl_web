import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixerweeklyplanComponent } from './mixerweeklyplan.component';

describe('MixerweeklyplanComponent', () => {
  let component: MixerweeklyplanComponent;
  let fixture: ComponentFixture<MixerweeklyplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixerweeklyplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixerweeklyplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
