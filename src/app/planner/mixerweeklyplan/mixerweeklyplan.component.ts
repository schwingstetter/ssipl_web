import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-mixerweeklyplan',
  templateUrl: './mixerweeklyplan.component.html',
  styleUrls: ['./mixerweeklyplan.component.css']
})
export class MixerweeklyplanComponent implements OnInit {
  model: NgbDateStruct;
  public rawmaterial:any={};
  public rawmaterialdescription:string="";
  fromdate:NgbDateStruct;
  todate:NgbDateStruct;
  public startyear:any;
  public endyear:any = new Array();
  public employees: any = {};
  closeResult: string;
  private modalRef: NgbModalRef;
  public createweeklyplan:any={};
  public weeklyplans:any={};
  public editweeklyplanadded:any={};
  public editfromdate:any={};
  public edittodate:any={};

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var lastday=this.lastday(yyyy,mm);
    var lastdays=new Date(yyyy, mm +1, 0).getDate();
    config.minDate = { year: yyyy, month: mm, day: dd };
    config.maxDate = { year: yyyy, month: mm, day: lastdays };
    /* this.fromdate=this.calendar.getToday();
    this.todate=this.calendar.getToday(); */
  }

  ngOnInit() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    /* var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();

    var newdate = year + "/" + month + "/" + day; */

    this.getonthlyplan(month);
  }

  lastday(y,m){
    //console.log(new Date(y, m +1, 0).getDate());
  }

  getonthlyplan(month){ 
    var table = $('.datatable').DataTable();
    var data: { switchcase: string,id:string } = { switchcase: "get",id:month };
    var method = "post";
    var url = "monthlyplan";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.employees = datas;
            console.log(datas);
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);

          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  createviewplan(id){
    this.createweeklyplan=id;
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  opencreate(crateweekly) {
    this.modalRef = this.modalService.open(crateweekly, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  viewweeklyplan(viewweekly){
    this.modalRef = this.modalService.open(viewweekly, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  editweeklyplan(editweekly){
    this.modalRef = this.modalService.open(editweekly, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  createweeklyplans(form:NgForm){
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "weeklyplan";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();      
            this.reloadPage();                  
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
          this.modalRef.close();
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );
  }
  viewfunction(item){
    var data: { switchcase: string,id:string } = { switchcase: "get",id:item.id };
    var method = "post";
    var url = "weeklyplan";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.weeklyplans = datas;            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  editfunction(editfunction){
    this.editweeklyplanadded = editfunction;
    this.editfromdate = this.editweeklyplanadded.fromdate.split("-");
    var fromyear = +this.editfromdate[0];
    var frommonth = +this.editfromdate[1];
    var fromday = +this.editfromdate[2];
    this.editfromdate = { 'year': fromyear, 'month': frommonth, 'day': fromday };

    this.edittodate = this.editweeklyplanadded.todate.split("-");
    var toyear = +this.edittodate[0];
    var tomonth = +this.edittodate[1];
    var today = +this.edittodate[2];
    this.edittodate = { 'year': toyear, 'month': tomonth, 'day': today };

  }
  editweeklyplans(editfrm:NgForm){
    var data = editfrm.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "weeklyplan";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);               
            editfrm.resetForm();  
            this.modalRef.close();
            this.reloadPage();                  
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }          
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
          }
        }
      );
  }
  deleteweeklyplan(id,part_no){
    this.modalRef.close();
    var data: { switchcase: string, id: any,part_no:any } = { switchcase: "delete", id: id,part_no:part_no };
    var method = "post";
    var url = "weeklyplan";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning', 
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            
            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Deleted!',
                  'Your Data has been deleted.',
                  'success'
                )
              }
              else if (response.code == 403) {
                swal(
                  ' Not Deleted!',
                  'Your Data has been not deleted.',
                  'error'
                )
                
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
        
        this.ngOnInit();
      }
    });
  }
  reloadPage(){
    this.ngOnInit();
  }

}
