import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MixerweeklyplanComponent } from './mixerweeklyplan.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const monthlyweeklyplanRoutes: Routes = [
  {
    path: '',
    component: MixerweeklyplanComponent,
    data: {
      breadcrumb: 'Mixer Weekly Plan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(monthlyweeklyplanRoutes),
    SharedModule,
  ],
  declarations: [MixerweeklyplanComponent]
})
export class MixerweeklyplanModule { }
