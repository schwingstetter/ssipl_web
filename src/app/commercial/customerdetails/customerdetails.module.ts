import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerdetailsComponent } from './customerdetails.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const customerdetailsRoutes: Routes = [
  {
    path: '',
    component: CustomerdetailsComponent,
    data: {
      breadcrumb: 'Customer Details',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(customerdetailsRoutes),
    SharedModule
  ],
  declarations: [CustomerdetailsComponent]
})
export class CustomerdetailsModule { }
