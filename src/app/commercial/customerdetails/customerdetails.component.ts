import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod ,ResponseContentType} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router'
import { json } from 'd3';
import { decode } from 'punycode';
import { environment } from './../../../environments/environment';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-customerdetails',
  templateUrl: './customerdetails.component.html',
  styleUrls: ['./customerdetails.component.css']
})
export class CustomerdetailsComponent implements OnInit {
options1 :any[];
optionscomp :any[];
optionsMap:any[];
dateval: string = null;
length : any;
file_name:FileList;
options :any[];
datafoc :any[];
optionmodel :any[];
compoptions:string="";
optionSelected: any = "Select";
optionSelected1: any = "Select";
optionmodelSelected: any = "Select";
editplanning : string ="";
isselected : string ="";
selecteditem : string ="";
public editfocdetails:any = { };
cust :any;
details : string ="";
selectedcust : string ="";
containers = [];
focitems = [];
focvalues = [];
private modalRef: NgbModalRef;
closeOther: boolean = false;
closeResult: string;
constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef , private modalService: NgbModal) {
  this.toastr.setRootViewContainerRef(vcr);
 }

  ngOnInit() {
    var table=$('#example').DataTable();
    var data: { switchcase: string } = { switchcase: "get" };
    var method = "post";
    var url = "custid";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
  
          if(datas.code == 201){
            this.cust = datas.message;
  
          }else{
  
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  add()
  {
    event.preventDefault();
    this.containers.push(this.containers.length);
    this.length = this.containers.length;
   
  }
  addfocedit()
  {
    event.preventDefault();
    this.datafoc.push(this.datafoc.length);
    
  }
  deletefoc(id) {
    var data: { switchcase: string, id: any } = { switchcase: "deletefoc", id: id };
    var method = "post";
    var url = "customerspec";
    // swal({
    //   title: 'Are you sure?',
    //   text: "You won't be able to revert this!",
    //   type: 'warning', 
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Yes, delete it!',
    // }).then((isconfirm) => {
    //   if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
           
            response => {
              if (response.code == 201) {
  
                swal(
                  'Deleted!',
                  'Your Data has been deleted.',
                  'success'
                )
                this.reloadPage();
  
              }
              else {
               
              }
            },
            error => console.log('error : ' + error)
          );
      // } else {
       
      //   this.reloadPage();
      // }
  //  });
  };
  addfoc(form:NgForm)
  {
    var data =form.value;
    var switchcase = "switchcase";
    var value = "insert_foc";
    var nooffoc = "nooffoc";
    var custid = "custid";
    data[switchcase] = value;
    data[nooffoc] = this.containers.length;
    data[custid] = this.selectedcust;
    var method= "post";
    var url="customerspec";
    this.AjaxService.ajaxpost(data, method, url)
    .subscribe(
      (datas) => {
        if(datas.code==201){
          this.toastr.success(datas.message, datas.type);
        this.getfoc(this.selectedcust);
          this.modalRef.close();
          this.containers=[];
         // this.reloadPage();
        }
        else{
          this.toastr.error(datas.message, datas.type);
        }    
      },
      (err) => {
        if (err.status == 403) {
          this.toastr.error(err._body);
          //alert(err._body);
        }
      }
    
    );
  }
  editfunction(item) {
 
    this.editfocdetails=item;
   
  };
  getfoc(item)
{

  this.selecteditem=item;
  var table=$('#example').DataTable();
  var data: { switchcase: string, id:string } = { switchcase: "getfoc" , id: item };
    var method = "post";
    var url = "getfocdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if(datas.code == 201){
            
           this.focitems=datas.message;
           table.destroy(); 
           setTimeout(() => {
             $('#example').DataTable();
           }, 1000);
           }else{

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); 
}
editfoc(form,edit)
{
 
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "customerspec";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.modalRef.close();
            this.toastr.success(datas.message, datas.type);    
            this.ngOnInit();      
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //this.toastr.success('Hello world!', 'Toastr fun!');
  };
  reloadPage() {
   this.isselected = '';
   this.selectedcust='';

     this.ngOnInit();
  }
  oncustselected(event)
  {
   // this.globalvariable.custid=event;
  this.isselected= '1';
  this.selectedcust = event;
  this.getfoc(event);
  }
  deletecustomerspec(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "customerspec";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning', 
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
           
            response => {
              if (response.code == 201) {
                this.reloadPage();
                // swal(
                //   'Deleted!',
                //   'Your file has been deleted.',
                //   'success'
                // )
                this.toastr.success('Success','Deleted Successfully',);    
                // this.getfoc(this.selecteditem);
             
  
              }
              else {
               
              }
            },
            error => console.log('error : ' + error)
          );
      } else {
       
        this.reloadPage();
      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open(addNew) {
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
}



// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/commercial/customerdetails/customerdetails.component.ts