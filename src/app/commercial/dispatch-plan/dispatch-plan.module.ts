import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DispatchPlanComponent } from './dispatch-plan.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const dispatchplanRoutes: Routes = [
  {
    path: '',
    component: DispatchPlanComponent,
    data: {
      breadcrumb: 'Dispatch Plan',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(dispatchplanRoutes),
    SharedModule
  ],
  declarations: [DispatchPlanComponent]
})
export class DispatchPlanModule { }
