import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dispatch-plan',
  templateUrl: './dispatch-plan.component.html',
  styleUrls: ['./dispatch-plan.component.css']
})
export class DispatchPlanComponent implements OnInit {

  private modalRef: NgbModalRef;
  workorderdate: NgbDateStruct;
  private chassisinward: string = "";
  private editinward: any = {};
  private editchassisdates: any = {};
  closeResult: string;
  dates: string;
  date: string = "";
  model: NgbDateStruct;
  assembly_dates: string = "";
  fg_dates: string = "";
  workorder_dates: string = "";
  private dispatchplan: any = {};
  private chassisclearance: any = {};  
  private getmixerassembly: any = {};
  private customer: any = {};
  private mixerplanning: any = {};
  private mixerwip: any = {};
  private editdispatchplan: any = {};
  private editplandate: any = {};
  private editchassisclearance: any = {};
  private getmixerparts:any={};
  public getdescriptions:string="";


  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day: dd };
    this.model = this.calendar.getToday();
  }

  ngOnInit() {

    this.getdispatchplan();
    this.getcustomer();
    this.getmixerpartno();    
  }
  getdispatchplan(){
  
    var table = $('.datatable').DataTable();
    var operationdatas: { switchcase: string } = { switchcase: "get" };
    var meth = "post";
    var url = "dispatchplan";
    
    this.AjaxService.ajaxpost(operationdatas, meth, url)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.dispatchplan = mixerplannings;            
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmixerpartno(){
    var mixer_data: { switchcase: string} = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data,mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );    
  }
  getpartno(value) {   
    this.getmixerassembly={};
    var data: { switchcase: string,id:string} = { switchcase: "getdispatchdetails",id:value};
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {
          if (datass.code == 201) {
            this.getmixerassembly = datass.message[0];
            //this.getmixerassembly.   

            //  this.getmixerassembly.mixerstatus="null";
             if(datass.message[0]['work_station']<19 && datass.message[0]['work_station']>=5 && datass.message[0]['mixer_preparation_status'] == 0 && datass.message[0]['sap_posting_800']==0)
            {
               this.getmixerassembly.mixerstatus = "7";// In Progress
            }
           else if(datass.message[0]['work_station']==19)
            {
              this.getmixerassembly.mixerstatus = "1";//under testing
            }
            else if(datass.message[0]['tested'] !=null && datass.message[0]['reworked']==null )//under rework
            {
            this.getmixerassembly.mixerstatus = "2";
            }
            else if(datass.message[0]['mixer_preparation_status'] == 1 && datass.message[0]['work_station']>19)//Ready For painting
            {
            this.getmixerassembly.mixerstatus = "3";
            }
           else if( datass.message[0]['mixer_preparation_status'] >= 2 && datass.message[0]['work_station']==25  || datass.message[0]['work_station']==26 )//Painted
            {
            this.getmixerassembly.mixerstatus = "4";
            }
             else if(datass.message[0]['sap_posting_800']==1 && datass.message[0]['is_dispatched']==0) //FG
            {
            this.getmixerassembly.mixerstatus = "5";
            }
            else if(datass.message[0]['sap_posting_800']==1 && datass.message[0]['is_dispatched']==1) //dispacthed
            {
            this.editdispatchplan.status = "8";
            }
             else //Yet to start
            {
               this.getmixerassembly.mixerstatus = "6";
            }

          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }  
  getcustomer(){
    var custo: { switchcase: string } = { switchcase: "get" };
    var methodd = "post";
    var urlsss = "customerspec";
    this.AjaxService.ajaxpost(custo, methodd, urlsss)
      .subscribe(
        (customers) => {
          if (customers.code == 201) {
            this.customer = customers;            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );  
  }
  

  getmixerdetails(value){
    var data: { switchcase: string,id:string} = { switchcase: "getmixerassemblypart",id:value};
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {
          if (datass.code == 201) {
            this.getdescriptions=datass.message[0].mixer_description;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  getworkorderno(id){
    this.mixerplanning={};
    this.getmixerassembly={};
    var operationdatas: { switchcase: string, id:string } = { switchcase: "getnotdispatched", id:id };
    var meth = "post";
    var ur = "getdetails";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {
            this.toastr.success(mixerplannings.message, mixerplannings.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  adddispatchplan(form: NgForm, add) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var status="status";
    data[status] = this.getmixerassembly.mixerstatus;

    var method = "post";
    var url = "dispatchplan";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.getdispatchplan();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  editfunction(item) {
    this.editdispatchplan = item;
    this.dates = this.editdispatchplan.plandate.split("-");
    var year = +this.dates[0];
    var month = +this.dates[1];
    var day = +this.dates[2];

     this.editdispatchplan.status="null";
       /*      if(item.work_station<19 && item.work_station>=5)
            {
               this.editdispatchplan.status = "7";
            }
            else if(item.work_station<5)
            {
               this.editdispatchplan.status = "6";
            }
           else if(item.work_station==19)
            {
              this.editdispatchplan.status = "1";//under testing
            }
            else if(item.shfh_completionstatus==1)//under rework
            {
            this.editdispatchplan.status = "2";
            }
            else if(item.mixer_preparation_status == 1)//Ready For painting
            {
            this.editdispatchplan.status = "3";
            }
           else if( item.mixer_preparation_status == 2)//Painted
            {
            this.editdispatchplan.status = "4";
            }
             else if(item.sap_posting_800==1)
            {
            this.editdispatchplan.status = "5";
            }
*/
 if(item.work_station<19 && item.work_station>=5 && item.mixer_preparation_status == 0 && item.sap_posting_800==0)
            {
               this.editdispatchplan.status = "7";// In Progress
            }
           
           else if(item.work_station==19)
            {
              this.editdispatchplan.status = "1";//under testing
            }
            else if(item.tested != null && item.reworked == null)//under rework
            {
            this.editdispatchplan.status = "2";
            }
            else if(item.mixer_preparation_status == 1  && item.work_station >19)//Ready For painting
            {
            this.editdispatchplan.status = "3";
            }
           else if( item.mixer_preparation_status >= 2 && item.work_station==25 || item.work_station==26 )//Painted
            {
            this.editdispatchplan.status = "4";
            }
             else if(item.sap_posting_800==1 && item.is_dispatched==0) //FG
            {
            this.editdispatchplan.status = "5";
            }
            else if(item.sap_posting_800==1 && item.is_dispatched==1) //dispacthed
            {
            this.editdispatchplan.status = "8";
            }
             else //Yet to start
            {
              this.editdispatchplan.status= "6";
            }
    this.editplandate = { 'year': year, 'month': month, 'day': day };
    this.getworkorderno(item.mixpartid);
    this.getpartno(item.mixerwoid);
  };
  editdispatchplans(form: NgForm) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    var status = "status";
    data[switchcase] = value;
     data[status] = this.editdispatchplan.status;
    var method = "post";
    var url = "dispatchplan";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.getdispatchplan();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  deletedispatchplan(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "dispatchplan";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {                

                swal(
                  'Deleted!',
                  response.message,
                  'success'
                )
                this.ngOnInit()
              }
              else {
                swal(
                  'Alert!',
                  response.message,
                  'error'
                )
                this.ngOnInit()
              }
            },
            //error => console.log('error : ' + error)
          );
      } else {

      this.reloadPage();
      }
    });
  };
  private getDismissReason(reason: any): string {
    
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  reloadPage(){
   this.ngOnInit();
  }
}



// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/commercial/dispatch-plan/dispatch-plan.component.ts