import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-mountingscope',
  templateUrl: './mountingscope.component.html',
  styleUrls: ['./mountingscope.component.css']
})
export class MountingscopeComponent implements OnInit {

  private modalRef: NgbModalRef;
  model: NgbDateStruct;
  date_models: NgbDateStruct;
  private getcustomerspec: string = "";
  private customercolor: any = {};
  private editchassisclearance: any = {};
  private editmountingscope: any = {};
  private getmountingscope: any = {};
  private chassisclearance: any = {};
  closeResult: string;
  dates: string;
  ddates: string = "";
  private editfbvdates:any={};
  private editdates: any = {};
  private getmixerparts: any = {};
  private mixerplanning: any = {};
  private mixerslnoss:any={};
  private getcolors:any={};
  private packaging_list:any={};
  private getchassisno:any={};
  public mixerwips:any={};

  public dropdownSettings = {};   
  public dropdownList = [];
  public selectedItems = [];
  menuaccess:string[] = new Array();
  private mailid: any = ""; 
  
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day: dd };
    this.model = this.calendar.getToday();
    this.date_models = this.calendar.getToday();
  }

  ngOnInit() {    
    this.getmountingscopes();


    var data: { switchcase: string } = { switchcase: "getcustomer" };
    var method = "post";
    var url = "customerspec";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.getcustomerspec = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

    /* var chassis_data: { switchcase: string } = { switchcase: "get" };
    var chassis_method = "post";
    var chassis_url = "chassisclearance";
    this.AjaxService.ajaxpost(chassis_data, chassis_method, chassis_url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.chassisclearance = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); */
    /* this.getmixerpartno();   */
    //this.getmixerslno();
    this.getpackaging_partno();
    this.getmixerpartno();

    this.dropdownSettings = {
      text: "Select Users",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "multidropdowncustom",
      primaryKey: "email",
      labelKey: "email",
      noDataLabel: "Select an User",
      enableSearchFilter: true,
      showCheckbox: true,
      disabled: false
    };
    this.getusers(); 
    this.selectedItems=[];
    this.menuaccess=[];
  }
  getmountingscopes(){
    var table=$('.datatable').DataTable();
    var mounting_data: { switchcase: string } = { switchcase: "get" };
    var mounting_method = "post";
    var mounting_url = "mountingscope";
    this.AjaxService.ajaxpost(mounting_data, mounting_method, mounting_url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            table.destroy();
            this.getmountingscope = datas;
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmixerslno(value){
    var mixerslnodatas: { switchcase: string,id:number} = { switchcase: "getmixerslno",id:value};
    var mixerslnometh = "post";
    var mixerslnour = "getdetails";
    this.AjaxService.ajaxpost(mixerslnodatas, mixerslnometh, mixerslnour)
      .subscribe(
        (mixerslnos) => {
          if (mixerslnos.code == 201) {
            this.mixerslnoss = mixerslnos;
          } else {
            this.mixerslnoss = [];
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  getmixerpartno(){
    var mixer_data: { switchcase: string} = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data,mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );    
  }

  getpackaging_partno(){
    var data: { switchcase: string} = { switchcase: "get"};
    var method = "post";
    var url = "mountingheader";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.packaging_list = datas;         
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  
  getcustomer(value){
    var data: { switchcase: string, id: string; } = { switchcase: "getmixerwips", id:value };
    var method = "post";
    var url = "mixerwip";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mixerwips = datas.message[0];
            /* this.customercolor = datas.message[0];
            this.editmountingscope.color = datas.message[0].color; */            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
    
    /* this.getmixervalue(value); */
  } 
  getmixervalue(value){
    var data: { switchcase: string, id: string; } = { switchcase: "get", id:value };
    var method = "post";
    var url = "mixerwip";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.mixerslnoss = datas;
            /* this.customercolor = datas.message[0];
            this.editmountingscope.color = datas.message[0].color; */            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      /* this.getchassisclearance(value); */
  } 

  getchassisclearance(value){
    var chassis_data: { switchcase: string, id:string } = { switchcase: "getchassisclearance", id:value };
    var chassis_method = "post";
    var chassis_url = "mountingscope";
    this.AjaxService.ajaxpost(chassis_data, chassis_method, chassis_url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.chassisclearance = datas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  addmoutingscope(form: NgForm, add) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "mountingscope";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  editfunction(item) {
    this.editmountingscope = item;
    this.dates = this.editmountingscope.fbvdate.split("-");
    var year = +this.dates[0];
    var month = +this.dates[1];
    var day = +this.dates[2];
    this.editfbvdates = { 'year': year, 'month': month, 'day': day };
    this.ddates = this.editmountingscope.date.split("-");
    var dyear = +this.ddates[0];
    var dmonth = +this.ddates[1];
    var dday = +this.ddates[2];
    this.editdates = { 'year': dyear, 'month': dmonth, 'day': dday };
    this.getcustomer(item.customerid);
    this.getchassisclearance(item.customerid);
    this.getmixerslno(item.part_no);
    this.getcolor(item.asmainmatid);
  };
  editmoutingscope(form: NgForm, edit) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "mountingscope";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };  
  deletemoutingscope(id) {
    //var data: { switchcase: string, id: any } = { switchcase: "delete", id: item };
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "mountingscope";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your Data has been deleted.',
                  'success'
                )
                this.reloadPage();
              }
              else {
                swal(
                  'Not Deleted!',
                  'Your file is safe.',
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  };
  notdeletemoutingscope() 
  {
    swal({
      title: 'Alert',
      text: "Mixer completed FBV process. So we are not able to delete!!!",
      type: 'warning',
      //showCancelButton: true,
      confirmButtonColor: '#3085d6',
      //cancelButtonColor: '#d33',
      //confirmButtonText: 'Yes, delete it!',
    })
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  open(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  getcolor(value){
    var chassis_data: { switchcase: string, id:string } = { switchcase: "getcustomercolor", id:value };
    var chassis_method = "post";
    var chassis_url = "mixerwip"; //23.04.19  mounting data fetch problem
    this.AjaxService.ajaxpost(chassis_data, chassis_method, chassis_url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.getcolors = datas.message[0];
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  getusers()
    {
      var data={"roleid":''};
      var method = "post";
      var url = "getmailidall";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              this.mailid = datas;
              this.dropdownList=datas;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        ); 
  }
  checkValue(option, options) {
    //alert(option);
    //alert(options);

    if (this.menuaccess.length == 0) {
      this.menuaccess.push(option);
    } else {
      if (options == false) {

        this.menuaccess.splice(this.menuaccess.indexOf(option), 1);
      } else {
        this.menuaccess.push(option);
      }
    }
  }
  onItemSelect(item:any){
    //console.log(item);
    //console.log(this.selectedItems);
  }
  OnItemDeSelect(item:any){
    //  console.log(item);
    //  console.log(this.selectedItems);
  }
  onSelectAll(items: any){
      // console.log(items);
  }
  onDeSelectAll(items: any){
    //  console.log(items);
  }
  addmailingstatus(frm:NgForm){
    if(this.selectedItems.length > 0 || this.menuaccess.length > 0){
      var data: { switchcase: string,id:any,email:any } = { switchcase: "sendingmountingscopeplan",id:this.menuaccess,email:this.selectedItems };
      var method = "post";
      var url = "getdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.toastr.success(datas.message, datas.type);
            } else {
              this.toastr.error(datas.message, datas.type);
            }
            this.selectedItems=[];
            this.menuaccess=[];
            this.modalRef.close();
            this.reloadPage();
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }else{
      this.toastr.error("Mail Id is not yet selected", "error");
    }
  }
  openmodal(addNew)
  {
    if(this.menuaccess.length > 0){
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    }else{
      this.toastr.error("No values are selected", "error");
    }
  }
  reloadPage(){
    this.mixerslnoss=[];
    this.chassisclearance=[];
    this.getmountingscopes();
  }
  reset(frm:NgForm){
    frm.resetForm();
    this.reloadPage();
  }
}



// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/commercial/mountingscope/mountingscope.component.ts