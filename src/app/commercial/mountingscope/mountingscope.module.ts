import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MountingscopeComponent } from './mountingscope.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const mountingscopeRoutes: Routes = [
  {
    path: '',
    component: MountingscopeComponent,
    data: {
      breadcrumb: 'Mounting Scope',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mountingscopeRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [MountingscopeComponent]
})
export class MountingscopeModule { }
