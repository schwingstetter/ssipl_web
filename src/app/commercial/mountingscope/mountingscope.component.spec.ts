import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MountingscopeComponent } from './mountingscope.component';

describe('MountingscopeComponent', () => {
  let component: MountingscopeComponent;
  let fixture: ComponentFixture<MountingscopeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MountingscopeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MountingscopeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
