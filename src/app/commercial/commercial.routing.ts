import { Routes } from '@angular/router';


export const commercialRoutes: Routes = [
    {
        path: '',
        data: {
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'workinprogress',
                loadChildren: './workinprogress/workinprogress.module#WorkinprogressModule'
            }, {
                path: 'mountingscope',
                loadChildren: './mountingscope/mountingscope.module#MountingscopeModule'
            }, {
                path: 'customerspec',
                loadChildren: './customer-spec/customer-spec.module#CustomerSpecModule'
            },
            {
                path: 'foc',
                loadChildren: './customerdetails/customerdetails.module#CustomerdetailsModule'
            }, {
                path: 'dispatchplan',
                loadChildren: './dispatch-plan/dispatch-plan.module#DispatchPlanModule'
            },
            {
                path: 'chassisinward',
                loadChildren: './chassisinwardclearance/chassisinwardclearance.module#ChassisinwardclearanceModule'
            },
            {
                path: 'salesorderno',
                loadChildren: './salesorder/salesorder.module#SalesorderModule'
            }      ,
            {
                path: 'billingstatus',
                loadChildren: './billingstatus/billingstatus.module#BillingstatusModule'
            }
        ]
    }
];

//allinone 730s