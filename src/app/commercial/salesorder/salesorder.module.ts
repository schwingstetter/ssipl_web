import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesorderComponent } from './salesorder.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
export const salesorderroutes: Routes = [
  {
    path: '',
    component: SalesorderComponent,
    data: {
      breadcrumb: 'Sales Order No. Circulation',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(salesorderroutes),
    SharedModule
  ],
  declarations: [SalesorderComponent]
})
export class SalesorderModule { }
