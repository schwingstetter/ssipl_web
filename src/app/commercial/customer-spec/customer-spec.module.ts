import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerSpecComponent } from './customer-spec.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const customerspecRoutes: Routes = [
  {
    path: '',
    component: CustomerSpecComponent,
    data: {
      breadcrumb: 'Customer Specification',
      status: true
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(customerspecRoutes),
    SharedModule
  ],
  declarations: [CustomerSpecComponent]
})
export class CustomerSpecModule { }
