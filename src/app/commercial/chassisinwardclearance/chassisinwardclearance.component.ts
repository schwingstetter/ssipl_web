import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-chassisinwardclearance',
  templateUrl: './chassisinwardclearance.component.html',
  styleUrls: ['./chassisinwardclearance.component.css'],
  providers: [NgbDatepickerConfig]
})
export class ChassisinwardclearanceComponent implements OnInit {

  private modalRef: NgbModalRef;
  model: NgbDateStruct;
  private chassisinward:string=""; 
  private editinward: any = {};
  private editchassisdates:any={};
  closeResult: string;
  dates: string;
  date:string="";
  
  private chassisclearance: string = "";
  private editchassisclearance: any = {};
  private editchassisclearanc: any = {};  
  private maildata: any = ""; 
  private getchassis: any = {}; 
  private customer_details:any ={};
  public packagechassis:any={};
  public package_make:any={};
 
  public userrole:string="";

  public dropdownSettings = {};   
  public dropdownList = [];
  public selectedItems = [];
  menuaccess:string[] = new Array();
  private mailid: any = ""; 

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day: dd };
    this.model = this.calendar.getToday();
  }

  ngOnInit() {
    this.getchassisinwards();
      this.getuserrole();
      this.getcustomer();
      this.dropdownSettings = {
        text: "Select Users",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        classes: "multidropdowncustom",
        primaryKey: "email",
        labelKey: "email",
        noDataLabel: "Select an User",
        enableSearchFilter: true,
        showCheckbox: true,
        disabled: false
      };
      this.getusers();  
      this.selectedItems=[];
      this.menuaccess=[];
    };
    getchassisinwards(){
      var table = $('.datatable').DataTable();
      var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "chassisinward";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.chassisinward = datas.message;
              table.destroy();
              setTimeout(() => {
                $('.datatable').DataTable();
              }, 1000);
            } else {

            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
    getcustomer(){
      var customer_data: { switchcase: string } = { switchcase: "getcustomer" };
      var customer_method = "post";
      var customer_url = "customerspec";
      this.AjaxService.ajaxpost(customer_data, customer_method, customer_url)
        .subscribe(
          (datas) => {
    
            if(datas.code == 201){
              this.customer_details = datas;
           
            }else{
    
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
    getuserrole()
    {
      var data: { switchcase: string } = { switchcase: "get" };
      var method = "post";
      var url = "usersrole";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
    
            if(datas.code == 201){
              this.userrole = datas.message;
           
            }else{
    
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
    getchassismake(value){
      var data: { switchcase: string,id:string } = { switchcase: "getchassismake",id:value };
      var method = "post";
      var url = "chassisinward";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
    
            if(datas.code == 201){
              this.packagechassis = datas.message[0];           
            }else{
    
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }
    /* getpackagemodel(package_make){
      var data: { switchcase: string,id:string } = { switchcase: "getchassismake",id:package_make };
      var method = "post";
      var url = "chassisinward";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
    
            if(datas.code == 201){
              this.package_make = datas.message[0];
           
            }else{
    
            }
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }  */   
      sendmail(frm){
       var data = {"mailid":this.menuaccess};
        var switchcase = "switchcase";
        var chassismodel = "chassismodel";
        var chassismake = "chassismake";
        var qty = "qty";
        var date = "date";
        var type = "type";
        var value = "insert";
        data[switchcase] = value;
        data[chassismake]=this.maildata.chassismake;
        data[chassismodel]=this.maildata.chassismodel;
        data[qty]=this.maildata.qty;
        data[date]=this.maildata.calendar;
        data[type]='2';
        var method= "post";
        var url="sendmail";
        this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if(datas.code==201){
              this.toastr.success(datas.message, datas.type);
              frm.resetForm();
              this.modalRef.close();
              //this.ngOnInit();
              this.reloadPage();
            }
            else{
              this.toastr.error(datas.message, datas.type);
            }    
          },
          (err) => {
            if (err.status == 403) {
              this.toastr.error(err._body);
              //alert(err._body);
            }
          }
        );
      };
      role(event)
      {
        var data={"roleid":event};
        var method = "post";
        var url = "getmailid";
        this.AjaxService.ajaxpost(data, method, url)
          .subscribe(
            (datas) => {
             
                this.mailid = datas;
              
            },
            (err) => {
              if (err.status == 403) {
                alert(err._body);
              }
            }
          ); 
      }
  reloadPage() {
    this.getchassisinwards();
  }
  addchassisinward(form: NgForm) {    
    this.maildata =form.value;
    var data = form.value;
    var switchcase = "switchcase";
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "chassisinward";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  editfunction(item) {
    this.editinward = item;
    this.dates = this.editinward.chassisdate.split("-");
    var year = +this.dates[0];
    var month = +this.dates[1];
    var day = +this.dates[2];
    this.editchassisdates = { 'year': year, 'month': month, 'day': day };
    this.getchassismake(item.custid);
  };
  editchassisinward(form: NgForm) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "chassisinward";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  deletechassisinward(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "chassisinward";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {
                this.reloadPage();

                swal(
                  'Deleted!',
                  'Your Data has been deleted.',
                  'success'
                )
              }
              else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.reloadPage();
      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  open(addNew) {
    this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg'});
  };
  getusers()
    {
      var data={"roleid":''};
      var method = "post";
      var url = "getmailidall";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              this.mailid = datas;
              this.dropdownList=datas;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        ); 
  }
  checkValue(option, options) {
    //alert(option);
    //alert(options);

    if (this.menuaccess.length == 0) {
      this.menuaccess.push(option);
    } else {
      if (options == false) {

        this.menuaccess.splice(this.menuaccess.indexOf(option), 1);
      } else {
        this.menuaccess.push(option);
      }
    }
  }
  onItemSelect(item:any){
    //console.log(item);
    //console.log(this.selectedItems);
  }
  OnItemDeSelect(item:any){
    //  console.log(item);
    //  console.log(this.selectedItems);
  }
  onSelectAll(items: any){
      // console.log(items);
  }
  onDeSelectAll(items: any){
    //  console.log(items);
  }
  addmailingstatus(frm:NgForm){
    if(this.selectedItems.length > 0 || this.menuaccess.length > 0){
      var data: { switchcase: string,id:any,email:any } = { switchcase: "sendingchassisinwardplan",id:this.menuaccess,email:this.selectedItems };
      var method = "post";
      var url = "getdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.toastr.success(datas.message, datas.type);
            } else {
              this.toastr.error(datas.message, datas.type);
            }
            this.selectedItems=[];
            this.menuaccess=[];
            this.modalRef.close();
            this.reloadPage();
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }else{
      this.toastr.error("Mail Id is not yet selected", "error");
    }
  }
  openmodal(addNew)
  {
    if(this.menuaccess.length > 0){
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    }else{
      this.toastr.error("No values are selected", "error");
    }
  }



}



// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/commercial/chassisinwardclearance/chassisinwardclearance.component.ts