import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChassisinwardclearanceComponent } from './chassisinwardclearance.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const chassisinwardroutes: Routes = [
  {
    path: '',
    component: ChassisinwardclearanceComponent,
    data: {
      breadcrumb: 'Chassis Inward Clearance',
      status: true
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(chassisinwardroutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [ChassisinwardclearanceComponent]
})
export class ChassisinwardclearanceModule { }
