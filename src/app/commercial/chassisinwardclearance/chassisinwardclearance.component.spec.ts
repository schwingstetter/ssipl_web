import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChassisinwardclearanceComponent } from './chassisinwardclearance.component';

describe('ChassisinwardclearanceComponent', () => {
  let component: ChassisinwardclearanceComponent;
  let fixture: ComponentFixture<ChassisinwardclearanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChassisinwardclearanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChassisinwardclearanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
