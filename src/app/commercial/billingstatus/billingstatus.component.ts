import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-billingstatus',
  templateUrl: './billingstatus.component.html',
  styleUrls: ['./billingstatus.component.css']
})
export class BillingstatusComponent implements OnInit {
  private modalRef: NgbModalRef;
  workorderdate: NgbDateStruct;
  private chassisinward: string = "";
  private editinward: any = {};
  private editchassisdates: any = {};
  closeResult: string;
  dispatcheddate: string;
  model: NgbDateStruct;
  date: string = ""; 
  billdate:string="";  
  assembly_dates: string = "";
  fg_dates: string = "";
  workorder_dates: string = "";
  private partno: any = {};
  private dispatchplan: any = {};
  private getmixerassembly: any = {};
  private customer: any = {};
  private mixerplanning: any = {};
  private mixerwip: any = {};
  private editdispatchplan: any = {};
  private editplandate: any = {};
  private chassisclearance: any = {};
  private billingstatus:any = {};
  private editdispatcheddate:any={};
  private editbilldate: any = {};
  private getmixerparts: any = {};
  public getmixerslno:any={};
  public mixerdescription:string="";
  public isReadOnly=false;
    constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day: dd };
    this.model = this.calendar.getToday();
  }

  ngOnInit() {

    this.getbillingdetails();

    /* var operationdatas: { switchcase: string } = { switchcase: "get" };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
          
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );  */
    
    var chassis_data: { switchcase: string } = { switchcase: "get" };
    var chassis_method = "post";
    var chassis_url = "chassisclearance";
    this.AjaxService.ajaxpost(chassis_data, chassis_method, chassis_url)
      .subscribe(
        (chassisdatas) => {
          if (chassisdatas.code == 201) {
            this.chassisclearance = chassisdatas;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      this.getmixerpartno();

  }
  getbillingdetails(){
    var table = $('.datatable').DataTable();
    var billingdatas: { switchcase: string } = { switchcase: "get" };
    var billingmethod = "post";
    var billingurl = "billingstatus";
    this.AjaxService.ajaxpost(billingdatas, billingmethod, billingurl)
      .subscribe(
        (billing) => {
          if (billing.code == 201) {
            this.billingstatus = billing;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  getmixerpartno(){
    var mixer_data: { switchcase: string} = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data,mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );    
  }
  getmixerdetails(value){
    var mixer_data: { switchcase: string,id:string} = { switchcase: "get",id:value };
    var mixer_method = "post";
    var mixer_url = "mixerassemblymaster";
    this.AjaxService.ajaxpost(mixer_data,mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.mixerdescription = mixerpart.message[0].mixer_description;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      ); 
  }

  getslno(id){
    var operationdatas: { switchcase: string, id:string } = { switchcase: "getserialno", id:id };
    var meth = "post";
    var ur = "getdetails";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          this.getmixerslno=""
          if (mixerplannings.code == 201) {
            this.getmixerslno = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  getmixerslnos(value){
    var operationdatas: { switchcase: string, id:string } = { switchcase: "getbillingdetails", id:value };
    var meth = "post";
    var ur = "getdetails";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.getmixerassembly = mixerplannings.message[0];
         
            console.log()
            if(this.getmixerassembly.mountingcategory=='4'|| this.getmixerassembly.mountingcategory==4)
            {
              this.isReadOnly=false;
            }
            else{
              this.isReadOnly=true;
            }
            //
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  /* getpartno(value) {
    var data: { switchcase: string, id: string } = { switchcase: "getpartno", id: value };
    var method = "post";
    var url = "dispatchplan";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {
          if (datass.code == 201) {
            this.getmixerassembly = datass.message[0];

            this.editdispatchplan.part_no = datass.message[0].PARTNO;
            this.editdispatchplan.description = datass.message[0].MIXER_DESC;
            this.editdispatchplan.customername = datass.message[0].customername;
            this.editdispatchplan.color_scheme = datass.message[0].COLORSCHEME;
            this.editdispatchplan.cds_no = datass.message[0].CDSNO;

          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };  */ 
  addbillingstatus(form: NgForm,add) {
    var data = form.value;
    var switchcase = "switchcase";
    var mountingcategory = "mountingcategory";
    var value = "insert";
    data[switchcase] = value;
    data[mountingcategory] = this.getmixerassembly.mountingcategory;
    var method = "post";
    var url = "billingstatus";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
           
            this.getbillingdetails();

            
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  editfunction(item) {
    this.editdispatchplan = item;

    this.dispatcheddate = this.editdispatchplan.dispatcheddate.split("-");
    var year = +this.dispatcheddate[0];
    var month = +this.dispatcheddate[1];
    var day = +this.dispatcheddate[2];
    this.editdispatcheddate = { 'year': year, 'month': month, 'day': day };

    this.billdate = this.editdispatchplan.billdate.split("-");
    var billdateyear = +this.billdate[0];
    var billdatemonth = +this.billdate[1];
    var billdateday = +this.billdate[2];
    this.editbilldate = { 'year': billdateyear, 'month': billdatemonth, 'day': billdateday };
    this.getmixerdetails(item.mixerid);
    this.getslno(item.mixerid);
    this.getmixerslnos(item.asmainmatid);

  };
  editbillingstatus(form: NgForm) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "billingstatus";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.getbillingdetails();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  deletebillingstatus(id) {
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "billingstatus";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {
                this.getbillingdetails();

                swal(
                  'Deleted!',
                  'Your Data has been deleted.',
                  'success'
                )
              }
              else {

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.getbillingdetails();
      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  reloadPage(){
    this.getbillingdetails();
  }

}



// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/commercial/billingstatus/billingstatus.component.ts