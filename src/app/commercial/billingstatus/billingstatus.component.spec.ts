import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingstatusComponent } from './billingstatus.component';

describe('BillingstatusComponent', () => {
  let component: BillingstatusComponent;
  let fixture: ComponentFixture<BillingstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
