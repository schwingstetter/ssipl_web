import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-commercial',
  template: '<router-outlet></router-outlet>'
})
export class CommercialComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
