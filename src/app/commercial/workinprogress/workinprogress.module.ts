import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkinprogressComponent } from './workinprogress.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const workinprogressRoutes: Routes = [
  {
    path: '',
    component: WorkinprogressComponent,
    data: {
      breadcrumb: 'Mixer Planning',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(workinprogressRoutes),
    SharedModule
  ],
  declarations: [WorkinprogressComponent]
})
export class WorkinprogressModule { }
