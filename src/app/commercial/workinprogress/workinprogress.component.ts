import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-workinprogress',
  templateUrl: './workinprogress.component.html',
  styleUrls: ['./workinprogress.component.css']
})
export class WorkinprogressComponent implements OnInit {

  private modalRef: NgbModalRef;
  workorderdate: NgbDateStruct;
  private chassisinward: string = "";
  private editinward: any = {};
  private editchassisdates: any = {};
  closeResult: string;
  dates: string;
  date: string = "";
  assembly_dates:string="";
  fg_dates: string = "";
  workorder_dates: string = "";
  private mixerassembly:any={};
  private getmixerassembly:any={};
  private customer: any = {};
  private mixerplanning: any = {};
  private partno: any = {};
  private mixerwip:any = {};
  private editwipprocess:any = {};
  private assembly_date:any = {};
  private fg_date: any = {};
  private workorder_date: any = {};
  private workorderno:any = {};
  private getmixerparts: any = {};
  private mixerslnoss:any={};
  file_name:FileList;
  public mixerdetails:any={};

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) { 
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day: dd };
  }

  ngOnInit() {

    //Get values of the Mixer WIP 
    this.getwipprocess();
    
    //Get values of the Workorders from Production Planning

    /* var operationdatas: { switchcase: string } = { switchcase: "get" };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
      (mixerplannings) => {
        if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); */  
   
    var custo: { switchcase: string } = { switchcase: "get" };
    var methodd = "post";
    var urlsss = "customerspec";
    this.AjaxService.ajaxpost(custo, methodd, urlsss)
      .subscribe(
        (customers) => {
          if (customers.code == 201) {
            this.customer = customers;            
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );  
      this.getmixerpartno();
      this.getmixerslno();
      
  };
  getwipprocess(){
    var table = $('.datatable').DataTable();
    var data_wip: { switchcase: string } = { switchcase: "get" };
    var method_wip = "post";
    var url_wip = "mixerwip";
    this.AjaxService.ajaxpost(data_wip, method_wip, url_wip)
      .subscribe(
        (datas_wip) => {
          if (datas_wip.code == 201) {
            this.mixerwip = datas_wip;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  getmixerslno(){
    var mixerslnodatas: { switchcase: string} = { switchcase: "getmixerslno"};
    var mixerslnometh = "post";
    var mixerslnour = "mountingscope";
    this.AjaxService.ajaxpost(mixerslnodatas, mixerslnometh, mixerslnour)
      .subscribe(
        (mixerslnos) => {
          if (mixerslnos.code == 201) {
            this.mixerslnoss = mixerslnos;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }

  getmixerpartno(){
    var mixer_data: { switchcase: string} = { switchcase: "getmixerassemblypart" };
    var mixer_method = "post";
    var mixer_url = "getdetails";
    this.AjaxService.ajaxpost(mixer_data,mixer_method, mixer_url)
      .subscribe(
        (mixerpart) => {
          if (mixerpart.code == 201) {
            this.getmixerparts = mixerpart;
          }
          else {
            this.toastr.error(mixerpart.message, mixerpart.type);
            
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );    
  }

  getworkorderno(id){
    this.mixerplanning={};
    var operationdatas: { switchcase: string, id:string } = { switchcase: "get", id:id };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;   
            this.getmixerdetails(id);
                   
          } else {
            this.toastr.error("No Workorder this Part Number!!!");
            
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
      // this.getmixerdetails(id);
  }
  getmixerdetails(id){
    var operationdatas: { switchcase: string, id:string } = { switchcase: "getmixerdetails", id:id };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerdetails = mixerplannings.message[0];
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }


  reloadPage() {
    this.getwipprocess();
  };
  getpartno(value){
    this.getmixerassembly={};
    var data: { switchcase: string, id: string } = { switchcase: "getwithmixerassemblydetails", id: value };
    var method = "post";
    var url = "addmixerplanning";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {         
          if (datass.code == 201) {            
            this.getmixerassembly = datass.message[0];
            this.editwipprocess.mixer_description = datass.message[0].mixer_description;
            this.editwipprocess.channelassembly_widthheight = datass.message[0].channelassembly_widthheight;
            this.editwipprocess.waterpump_hydcombution = datass.message[0].waterpump_hydcombution;   
            this.editwipprocess.plan_schedule = datass.message[0].plan_schedule;
          //  this.editwipprocess.salesregion = datass.message[0].salesregion;
            this.getmixerassembly.selmixstatu="null";
            this.editwipprocess.mixerstatus="null";
            if(datass.message[0]['work_station']<19 && datass.message[0]['work_station']>=5 && datass.massage[0]['mixer_preparation_status']==0 &&  datass.massage[0]['item.sap_posting_800']==0)
            {
               this.getmixerassembly.selmixstatu = "7";
               this.editwipprocess.mixerstatus="7";
            }
            else if(datass.message[0]['work_station']<5)
            {
               this.getmixerassembly.selmixstatu = "6";
               this.editwipprocess.mixerstatus="6";
            }
           else if(datass.message[0]['work_station']==19)
            {
              this.getmixerassembly.selmixstatu = "1";//under testing
              this.editwipprocess.mixerstatus="1";
            }
            else if(datass.message[0]['tested']!=null && datass.message[0]['reworked'] == null)//under rework
            {
            this.getmixerassembly.selmixstatu = "2";
            this.editwipprocess.mixerstatus="2";
            }
            else if(datass.message[0]['mixer_preparation_status'] == 1 && datass.message[0]['work_station']>19 )//Ready For painting
            {
            this.getmixerassembly.selmixstatu = "3";
            this.editwipprocess.mixerstatus="3";
            }
           else if( datass.message[0]['mixer_preparation_status'] == 2 && datass.message[0]['work_station']==25 || datass.message[0]['work_station']==26 || datass.message[0]['work_station']==27|| datass.message[0]['work_station']==28)//Painted
            {
            this.getmixerassembly.selmixstatu = "4";
            this.editwipprocess.mixerstatus="4";
            }
             else if(datass.message[0]['sap_posting_800']==1 && datass.message[0]['is_dispatched']==0)
            {
            this.getmixerassembly.selmixstatu = "5";
            this.editwipprocess.mixerstatus="5";
            }
            else if(datass.message[0]['sap_posting_800']==1 && datass.message[0]['is_dispatched']==1)
            {
            this.getmixerassembly.selmixstatu = "8";
            this.editwipprocess.mixerstatus="8";
            }
            else
            {
              this.getmixerassembly.selmixstatu = "6";
              this.editwipprocess.mixerstatus="6";
            }


          
          }
          else {
            this.toastr.error("No datas are mapped with this workorder no");
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
      //this.getallpartno(value);
  }
  getallpartno(value)
  {
    var data: { switchcase: string, id: string } = { switchcase: "getallpartno", id: value };
    var method = "post";
    var url = "addmixerplanning";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {         
          if (datass.code == 201) {            
            this.partno = datass;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  addmixerwip(form: NgForm, add) {
    var data = form.value;
    var switchcase = "switchcase";
    var mixerstatus="mixerstatus";
     data[mixerstatus] = this.getmixerassembly.selmixstatu;
    
    var value = "insert";
    data[switchcase] = value;
    var method = "post";
    var url = "mixerwip";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.ngOnInit();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  editfunction(item){
    this.editwipprocess = item;
    console.log(item);
    this.getworkorderno(item.partnoid);
    this.getpartno(item.workorderno);
  };
  editmixerwip(form: NgForm, edit) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "mixerwip";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.reloadPage();
            this.modalRef.close();

          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };  
  notdelete()
  {
    swal({
      title: 'Alert ',
      text: "This workorder already completed mixer finishing. So we are not able to delete!!!",
      type:'warning',
      //showCancelButton: true,
      //cancelButtonColor: '#d33',
     
    })
  }
  deletewip(id) {
    //var data: { switchcase: string, id: any } = { switchcase: "delete", id: item };
    var data: { switchcase: string, id: any } = { switchcase: "delete", id: id };
    var method = "post";
    var url = "mixerwip";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(
            response => {
              if (response.code == 201) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                this.ngOnInit();
              }
              else {
                swal(
                  'Deleted!',
                  'Your file is safe.',
                  'error'
                )
              }
            },
            error => console.log('error : ' + error)
          );
      } else {

      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  resetform(addfrm:NgForm){
    addfrm.resetForm();
  }
  openbulkupload(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  downloadFile() {

    return this.http
      .get('assets/uploads/MixerPlanning.xlsx', {
        responseType: ResponseContentType.Blob,
        search: ""
      })
      .map(res => {
        return {
          filename: 'MixerPlanning.xlsx',
          data: res.blob()
        };
      })
      .subscribe(res => {
        console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  }
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
   this.file_name=fileList;
  };
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();
      /* headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Accept','application/json');
      headers.append('Authorization', 'Bearer ' + currentUser);   */
      /*  headers.append('withCredentials', 'true');
       headers.append('Content-Type', 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2));  */

      /* headers.append('Authorization', 'Nantha ' + currentUser);  */

      let options = new RequestOptions({ headers: headers });
      /*this.http.post('http://localhost/schwingbackend/public/api/uploadfiles', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          data => this.toastr.success(data.message, data.type),
          error => this.toastr.error(error)
        )*/

      var data = formData;
      var method = "post";
      var url = "mixerplanninguploadfiles";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for(i=0;i<data.length;i++){
              if(data[i].code==201){
                this.toastr.success(data[i].message, 'Success!');                               
                this.modalRef.close();
              }else{
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.ngOnInit();
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };
}


// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/commercial/workinprogress/workinprogress.component.ts