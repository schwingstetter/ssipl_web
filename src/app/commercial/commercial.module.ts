import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommercialComponent } from './commercial.component';
import { CommonModule } from '@angular/common';
import { commercialRoutes } from './commercial.routing';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(commercialRoutes),
  ],
  declarations: [CommercialComponent]
})
export class CommercialModule { }
