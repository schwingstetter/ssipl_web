import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DispatchComponent } from './dispatch.component';
import { CommonModule } from '@angular/common';
import { dispatchRoutes } from './dispatch.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(dispatchRoutes),
  ],
  declarations: [DispatchComponent]
})
export class DispatchModule { }
