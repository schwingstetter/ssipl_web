import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dispatch',
  template: '<router-outlet></router-outlet>'
})
export class DispatchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
