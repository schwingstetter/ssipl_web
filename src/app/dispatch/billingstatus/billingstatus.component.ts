import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-billingstatus',
  templateUrl: './billingstatus.component.html',
  styleUrls: ['./billingstatus.component.css']
})
export class BillingstatusComponent implements OnInit {

  private modalRef: NgbModalRef;
  workorderdate: NgbDateStruct;
  private chassisinward: string = "";
  private editinward: any = {};
  private editchassisdates: any = {};
  closeResult: string;
  dates: string;
  date: string = "";
  private mixerplanning: any = {};
  private getmixerassembly: any = {};
  

  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day: dd };
  }

  ngOnInit() {

    var operationdatas: { switchcase: string } = { switchcase: "get" };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

  };
  getpartno(value) {
    var data: { switchcase: string, id: string } = { switchcase: "getpartno", id: value };
    var method = "post";
    var url = "dispatchplan";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {
          if (datass.code == 201) {
            this.getmixerassembly = datass.message[0];

            /* this.editdispatchplan.PARTNO = datass.message[0].PARTNO;
            this.editdispatchplan.MIXER_DESC = datass.message[0].MIXER_DESC;
            this.editdispatchplan.customername = datass.message[0].customername;
            this.editdispatchplan.COLORSCHEME = datass.message[0].COLORSCHEME;
            this.editdispatchplan.CDSNO = datass.message[0].CDSNO;
            this.editdispatchplan.SONO = datass.message[0].SONO;
            this.editdispatchplan.premarks = datass.message[0].premarks;
            this.editdispatchplan.CREMARKS = datass.message[0].CREMARKS; */

          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  } 

}
