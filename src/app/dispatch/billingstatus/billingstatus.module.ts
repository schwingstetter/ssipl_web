import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillingstatusComponent } from './billingstatus.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const billingstatusRoutes: Routes = [
  {
    path: '',
    component: BillingstatusComponent,
    data: {
      breadcrumb: 'Billing Status',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(billingstatusRoutes),
    SharedModule
    
  ],
  declarations: [BillingstatusComponent]
})
export class BillingstatusModule { }
