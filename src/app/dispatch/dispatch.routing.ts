import { Routes } from '@angular/router';


export const dispatchRoutes: Routes = [
    {
        path: '',
        data: {
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            // {
            //     path: 'billingstatus',
            //     loadChildren: './billingstatus/billingstatus.module#BillingstatusModule'
            // }, 
            {
                path: 'mixerloading',
                loadChildren: './mixerloading/mixerloading.module#MixerloadingModule'
            }
                   
        ]
    }
];

//allinone 730s