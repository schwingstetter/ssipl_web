import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MixerloadingComponent } from './mixerloading.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const mixerloadingRoutes: Routes = [
  {
    path: '',
    component: MixerloadingComponent,
    data: {
      breadcrumb: 'Mixer loading & Invoice Confirmation',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mixerloadingRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [MixerloadingComponent]
})
export class MixerloadingModule { }
