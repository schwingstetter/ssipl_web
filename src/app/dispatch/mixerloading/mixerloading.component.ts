import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Http, Headers, HttpModule } from "@angular/http";
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import swal from 'sweetalert2';
import { AjaxService } from '../../ajax.service';
import { AuthService } from '../../auth.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { from } from 'rxjs/observable/from';

@Component({
  selector: 'app-mixerloading',
  templateUrl: './mixerloading.component.html',
  styleUrls: ['./mixerloading.component.css']
})
export class MixerloadingComponent implements OnInit {

  private modalRef: NgbModalRef;
  workorderdate: NgbDateStruct;
  private chassisinward: string = "";
  private editinward: any = {};
  private editchassisdates: any = {};
  closeResult: string;
  model: NgbDateStruct;
  dates: string;
  date: string = "";
  assembly_dates: string = "";
  fg_dates: string = "";
  workorder_dates: string = "";
  private dispatchplan: any = {};
  private chassisclearance: any = {};
  private getmixerassembly: any = {};
  private mixerloading: any = {};
  private mixerplanning: any = {};
  private mixerwip: any = {};
  private editdispatchplan: any = {};
  private mixerloadingdate: any = {};
  private editchassisclearance: any = {};
  private getengineno:any={};
  public getfocdetails:any={};
  public isReadOnly=false;
  file_name:FileList;
  public Task=[];
  public users:any={};
  public dropdownSettings = {};   
  public dropdownList = [];
  public selectedItems = [];
  menuaccess:string[] = new Array();
  private mailid: any = ""; 
  constructor(public http: Http, private AjaxService: AjaxService, private toastyService: ToastyService, public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, config: NgbDatepickerConfig, private calendar: NgbCalendar) {
    this.toastr.setRootViewContainerRef(vcr);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    config.minDate = { year: yyyy, month: mm, day: dd };
    this.model = this.calendar.getToday();
  }

  ngOnInit() {

    var table = $('.datatable').DataTable();
    var mixerdatas: { switchcase: string } = { switchcase: "get" };
    var mixermethod = "post";
    var mixerurl = "mixerloading";
    this.AjaxService.ajaxpost(mixerdatas, mixermethod, mixerurl)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerloading = mixerplannings;
            table.destroy();
            setTimeout(() => {
              $('.datatable').DataTable();
            }, 1000);
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );

    /* var operationdatas: { switchcase: string } = { switchcase: "get" };
    var meth = "post";
    var ur = "addmixerplanning";
    this.AjaxService.ajaxpost(operationdatas, meth, ur)
      .subscribe(
        (mixerplannings) => {
          if (mixerplannings.code == 201) {
            this.mixerplanning = mixerplannings;
          } else {

          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      ); */
    this.getengineslno(); 
    this.getwebusers();
    this.dropdownSettings = {
      text: "Select Users",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "multidropdowncustom",
      primaryKey: "email",
      labelKey: "email",
      noDataLabel: "Select an User",
      enableSearchFilter: true,
      showCheckbox: true,
      disabled: false
    };
    this.getusers(); 
    this.selectedItems=[];
    this.menuaccess=[];
  }
  getwebusers(){
    var data: { switchcase: string} = { switchcase: "getwebusers"};
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {             
            this.users=datass;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  getengineslno(){
    var data: { switchcase: string} = { switchcase: "getengineslno"};
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            this.getengineno=datass;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }

  getalldetails(value){    
    var data: { switchcase: string,id:string} = { switchcase: "getalldetails",id:value};
    var method = "post";
    var url = "getdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {            
            this.getmixerassembly=datass.message[0];
            if(this.getmixerassembly.mountingcategory=='4'|| this.getmixerassembly.mountingcategory==4)
            {
        this.isReadOnly=false;
            }
            else{
        this.isReadOnly=true;
            }
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  

  /* getpartno(value) {
    var data: { switchcase: string, id: string } = { switchcase: "getpartno", id: value };
    var method = "post";
    var url = "mixerloading";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            this.getmixerassembly = datass.message[0];
            this.editdispatchplan.PARTNO = datass.message[0].PARTNO;
            this.editdispatchplan.COLORSCHEME = datass.message[0].customername;
            this.editdispatchplan.CUSTOMERNAME = datass.message[0].COLORSCHEME;
            this.editdispatchplan.CHASSISMODEL = datass.message[0].CHASSISMODEL;
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };  */ 
  addbillingstatus(form: NgForm, add) {
    var data = form.value;
    var switchcase = "switchcase";
   var mountingcategory = "mountingcategory";
    var value = "insert";
    data[switchcase] = value;
    data[mountingcategory] = this.getmixerassembly.mountingcategory;
    var method = "post";
    var url = "mixerloading";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.ngOnInit();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  editfunction(item) {
    this.editdispatchplan = item;
    this.dates = this.editdispatchplan.mixerloadingdate.split("-");
    var year = +this.dates[0];
    var month = +this.dates[1];
    var day = +this.dates[2];
    this.mixerloadingdate = { 'year': year, 'month': month, 'day': day };
    this.getmixerassembly.part_no=this.editdispatchplan.part_no;
    this.getmixerassembly.mixer_s_no=this.editdispatchplan.mixer_s_no;
    this.getmixerassembly.waterpump_cum3=this.editdispatchplan.waterpump_cum3;
    this.getmixerassembly.color=this.editdispatchplan.color;
    this.getmixerassembly.customer_name=this.editdispatchplan.customer_name;
    this.getmixerassembly.chassismodel=this.editdispatchplan.chassismodel;
    this.getmixerassembly.sono=this.editdispatchplan.sono;
    this.getmixerassembly.chassisaccessories=this.editdispatchplan.chassisaccessories;
  };
  editdispatchplans(form: NgForm) {
    var data = form.value;
    var switchcase = "switchcase";
    var value = "edit";
    data[switchcase] = value;
    var method = "post";
    var url = "mixerloading";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => {
          if (datas.code == 201) {
            this.toastr.success(datas.message, datas.type);
            form.resetForm();
            this.ngOnInit();
            this.modalRef.close();
          }
          else {
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  };
  // notdeletedispatchplan()
  // {
  //   swal({ 
  //     title: 'Alert',
  //     text: "The mixer billing already completed. So we are not able to delete!!!",
  //     type: 'warning',
  //     //showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     //cancelButtonColor: '#d33',
  //     //confirmButtonText: 'Yes, delete it!',
  //   })
  // }
  deletedispatchplan(id,eng_no)
   {
    var data: { switchcase: string, id: any ,eng_no:any} = { switchcase: "delete", id: id,eng_no:eng_no };
    var method = "post";
    var url = "mixerloading";
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((isconfirm) => {
      if (isconfirm.value == true) {
        this.AjaxService.ajaxpost(data, method, url) // can't access to this. (this.filter or this.asyncService)
          .subscribe(

            response => {
              if (response.code == 201) {
                
                this.ngOnInit();
               
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
              }
              else {
                this.ngOnInit();
               
                swal(
                  'Not Deleted!',
                  response.message,
                  'error'
                )

              }
            },
            error => console.log('error : ' + error)
          );
      } else {

        this.ngOnInit();
      }
    });
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  };
  openedit(edit) {
    this.modalRef = this.modalService.open(edit, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  openfoc(openfocs) {
    this.modalRef = this.modalService.open(openfocs, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  open(bulkupload) {
    this.modalRef = this.modalService.open(bulkupload, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  };
  getfoc(value){
    var data: { switchcase: string,id:string} = { switchcase: "getfoc",id:value};
    var method = "post";
    var url = "getfocdetails";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {            
            this.getfocdetails=datass;   
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  downloadFile(){
    var data: { switchcase: string} = { switchcase: "getnotdispatched"};
    var method = "post";
    var url = "mixerloading";
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datass) => {          
          if (datass.code == 201) {
            console.log(datass.message);
           for(var i=0;i<datass.message.length;i++){
              datass.message[i].veh_type = "";
              datass.message[i].Truck_No = "";
              datass.message[i].Destination = "";
              datass.message[i].TPT = "";
              datass.message[i].LR_NO = "";
              datass.message[i].PO_NO = "";
              datass.message[i].Date = "";
            }           
            this.saveAsXlsx(name,datass.message); 
          }
          else {
            this.toastr.error(datass.message, datass.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            this.toastr.error(err._body);
            //alert(err._body);
          }
        }
      );
  }
  saveAsXlsx(name,parsed){
    alasql('SELECT * INTO XLSX("Mixerloading.XLSX",{headers:true}) FROM ?',[parsed]);
  } 
  fileUpload(event) {
    //const url = `http://localhost/schwingbackend/public/api/uploadfiles`;
    let fileList: FileList = event.target.files;
   this.file_name=fileList;
  };
  filesubmit(bulkupload)
  {
    if (this.file_name.length > 0) {
      let file: File = this.file_name[0];
      let formData: FormData = new FormData();
      formData.append('photo', file);
      let currentUser = localStorage.getItem('LoggedInUser');
      let headers = new Headers();

      let options = new RequestOptions({ headers: headers });

      var data = formData;
      var method = "post";
      var url = "uploadmixerloading";
      var i: number; 
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          data => {
            for(i=0;i<data.length;i++){
              if(data[i].code==201){
                this.toastr.success(data[i].message, 'Success!');                               
                this.modalRef.close();
              }else{
                this.toastr.error(data[i].message, 'Error!');
              }
            }
            this.modalRef.close();
            this.ngOnInit();
          },
          error => this.toastr.error(error)
        );
    } else {

    }
  };
  
  sendmails(){
    var method = "post";
    var url = "getdetails";
    var data: { switchcase: string,options:string[]} = { switchcase: "getsendmail",options:this.menuaccess };
    this.AjaxService.ajaxpost(data, method, url)
      .subscribe(
        (datas) => { 
          if(datas.code == 201){
          }else{  
            this.toastr.error(datas.message, datas.type);
          }
        },
        (err) => {
          if (err.status == 403) {
            alert(err._body);
          }
        }
      );
  }
  openbulkupload(sendmail) {
    this.modalService.open(sendmail);
  }
  getusers()
    {
      var data={"roleid":''};
      var method = "post";
      var url = "getmailidall";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
              this.mailid = datas;
              this.dropdownList=datas;
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        ); 
  }
  checkValue(option, options) {
    //alert(option);
    //alert(options);

    if (this.menuaccess.length == 0) {
      this.menuaccess.push(option);
    } else {
      if (options == false) {

        this.menuaccess.splice(this.menuaccess.indexOf(option), 1);
      } else {
        this.menuaccess.push(option);
      }
    }
  }
  onItemSelect(item:any){
    //console.log(item);
    //console.log(this.selectedItems);
  }
  OnItemDeSelect(item:any){
    //  console.log(item);
    //  console.log(this.selectedItems);
  }
  onSelectAll(items: any){
      // console.log(items);
  }
  onDeSelectAll(items: any){
    //  console.log(items);
  }
  addmailingstatus(frm:NgForm){
    if(this.selectedItems.length > 0 || this.menuaccess.length > 0){
      var data: { switchcase: string,id:any,email:any } = { switchcase: "sendingmixerloadingplan",id:this.menuaccess,email:this.selectedItems };
      var method = "post";
      var url = "getdetails";
      this.AjaxService.ajaxpost(data, method, url)
        .subscribe(
          (datas) => {
            if (datas.code == 201) {
              this.toastr.success(datas.message, datas.type);
            } else {
              this.toastr.error(datas.message, datas.type);
            }
            this.selectedItems=[];
            this.menuaccess=[];
            this.modalRef.close();
            this.ngOnInit();
          },
          (err) => {
            if (err.status == 403) {
              alert(err._body);
            }
          }
        );
    }else{
      this.toastr.error("Mail Id is not yet selected", "error");
    }
  }
  openmodal(addNew)
  {
    if(this.menuaccess.length > 0){
      this.modalRef = this.modalService.open(addNew, { backdrop: 'static', keyboard: false, size: 'lg' });
    }else{
      this.toastr.error("No values are selected", "error");
    }
  }
}



// WEBPACK FOOTER //
// /home/kaspon/lampp/apache2/htdocs/schwing_new/ssipl_web/src/app/dispatch/mixerloading/mixerloading.component.ts