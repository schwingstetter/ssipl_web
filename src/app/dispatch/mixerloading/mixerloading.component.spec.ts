import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixerloadingComponent } from './mixerloading.component';

describe('MixerloadingComponent', () => {
  let component: MixerloadingComponent;
  let fixture: ComponentFixture<MixerloadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixerloadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixerloadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
