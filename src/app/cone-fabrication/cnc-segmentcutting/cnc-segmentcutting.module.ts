import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CncSegmentcuttingComponent } from './cnc-segmentcutting.component';
import { SharedModule } from '../../shared/shared.module';

export const CncSegmentcuttingRoutes: Routes = [
  {
    path: '',
    component: CncSegmentcuttingComponent,
    data: {
      breadcrumb: 'CNC Segment Cutting',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CncSegmentcuttingRoutes),
    SharedModule
  ],
  declarations: [CncSegmentcuttingComponent]
})
export class CncSegmentcuttingModule { }
