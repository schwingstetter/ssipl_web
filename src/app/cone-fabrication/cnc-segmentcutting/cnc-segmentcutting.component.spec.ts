import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CncSegmentcuttingComponent } from './cnc-segmentcutting.component';

describe('CncSegmentcuttingComponent', () => {
  let component: CncSegmentcuttingComponent;
  let fixture: ComponentFixture<CncSegmentcuttingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CncSegmentcuttingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CncSegmentcuttingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
