import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cone-fabrication',
  template: '<router-outlet></router-outlet>'
})
export class ConeFabricationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
