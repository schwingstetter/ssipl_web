import { Routes } from '@angular/router';


export const conefabricationRoutes: Routes = [
    {
        path: '',
        data: {
            breadcrumb: 'Cone Fabrication',
            icon: 'icofont-home bg-c-blue',
            status: false
        },
        children: [
            {
                path: 'cnccutting',
                loadChildren: './cnc-segmentcutting/cnc-segmentcutting.module#CncSegmentcuttingModule'
            }
        ]
    }
];
