import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ConeFabricationComponent } from './cone-fabrication.component';
import { CommonModule } from '@angular/common';
import { conefabricationRoutes } from './cone-fabrication.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(conefabricationRoutes),
  ],
  declarations: [ConeFabricationComponent]
})
export class ConeFabricationModule { }
